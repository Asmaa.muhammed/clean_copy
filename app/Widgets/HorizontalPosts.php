<?php

namespace App\Widgets;

use App\Models\Post;
use Arrilot\Widgets\AbstractWidget;

class HorizontalPosts extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'page'=>'page'
    ];


    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //

        $relatedCategories = $this->config['page']->categories()->pluck('taxonomies.id');
        $posts = Post::whereHas('categories', function ($q) use ($relatedCategories) {
                $q->whereIn('taxonomies.id', $relatedCategories);
            })
            ->frontPublish()
            ->paginate(12);

        return view('widgets.horizontal_posts', [
            'posts' => $posts,
            'page'=>$this->config['page']
        ]);
    }
}
