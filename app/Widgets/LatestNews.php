<?php

namespace App\Widgets;

use App\Models\Category;
use App\Models\Post;
use Arrilot\Widgets\AbstractWidget;

class LatestNews extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $relatedCategory = Category::where('id', 1)->first();
        $posts = Post::whereHas('categories', function ($q) use ($relatedCategory) {
            $q->where('taxonomies.id', $relatedCategory->id);
        })
            ->frontPublish()
            ->showFrontHomePage()
            ->get();

        return view('widgets.latest_news', [
            'posts' => $posts,
            'category'=>$relatedCategory
        ]);
    }
}
