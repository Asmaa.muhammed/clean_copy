<?php

namespace App\Widgets;

use App\Models\Category;
use App\Models\Event;
use Arrilot\Widgets\AbstractWidget;

class OurUpcomingEvents extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $relatedCategory = Category::where('id', 4)->first();
        $events = Event::whereHas('categories', function ($q) use ($relatedCategory) {
            $q->where('taxonomies.id', $relatedCategory->id);
        })
            ->frontPublish()
            ->showFrontHomePage()
            ->get();

        return view('widgets.our_upcoming_events', [
            'events' => $events,
            'category'=>$relatedCategory
        ]);

    }
}
