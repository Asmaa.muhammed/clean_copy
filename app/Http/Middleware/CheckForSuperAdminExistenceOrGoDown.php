<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Support\Facades\Artisan;


class CheckForSuperAdminExistenceOrGoDown
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!User::whereEmail(config('app.super_admin_mail'))->exists()){
            Artisan::call('down');
            abort(500);
        }
        return $next($request);
    }
}
