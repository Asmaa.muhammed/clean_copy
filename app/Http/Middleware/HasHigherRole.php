<?php

namespace App\Http\Middleware;

use Closure;

class HasHigherRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->roles->first()->id > $request->route('user')->roles->first()->id)
        {
            abort(403);
        }
        return $next($request);
    }
}
