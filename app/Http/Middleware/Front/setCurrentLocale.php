<?php

namespace App\Http\Middleware\Front;

use Closure;

class setCurrentLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $current = \LaravelLocalization::getCurrentLocale();
        app()->setLocale($current);
        return $next($request);
    }
}
