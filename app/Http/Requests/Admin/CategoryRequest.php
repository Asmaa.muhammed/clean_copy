<?php

namespace App\Http\Requests\Admin;

use App\Foundation\Traits\TranslationRequest;
use App\Models\Category;
use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    use TranslationRequest;

    public $requiredTranslationFields= [
        'name'=>'required',
    ];

    public function getRequiredFields(){
        if(isset($this->route()->parameter('category')->id)) {
            return [
                'name'=>"required" 
            ];
        }
        return $this->requiredTranslationFields;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

/**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $parents = implode(',',array_merge(['0'],Category::all()->pluck('id')->toArray()));
        return array_merge([
            'status'=>'required|boolean',
            'parent'=>'required|in:'.$parents,
            'sorting'=>'required|numeric',
        ], $this->getTranslationRules());
    }
}
