<?php

namespace App\Http\Requests\Admin;

use App\Foundation\Traits\RequestWithTranslatable;
use Illuminate\Foundation\Http\FormRequest;

class MenuLinkRequest extends FormRequest
{
    use RequestWithTranslatable;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge($this->resolveTranslatableRules(),
            [
                'status' => 'required|boolean',
                'sorting' => 'required|numeric',
                'parent' => 'required',
                'target' => 'required|boolean',
                'icon' => 'required|string',
                'link' => 'required_if:target,1',
                'target_type' => 'required_if:target,0',
                'target_id' => 'required_if:target,0',
            ]
        );
    }

    public function translatableRules()
    {
        return [
            'name' => 'required|string|min:3',
        ];
    }

    public function getLinkData()
    {
        return $this->input('target') ? $this->only(['link']) : $this->only(['target_type','target_id']);
    }


}
