<?php

namespace App\Http\Requests\Admin;

use App\Foundation\Traits\TranslationRequest;
use App\Models\Enum;
use Illuminate\Foundation\Http\FormRequest;

class EnumRequest extends FormRequest
{
    use TranslationRequest;

    public $requiredTranslationFields= [
        'name'=>'required|unique:enum_translations,name',
    ];

    public function getRequiredFields(){
        if(isset($this->route()->parameter('enum')->id)) {
            return [
                'name'=>"required|unique:enum_translations,name,{$this->route()->parameter('enum')->id},enum__id",
            ];
        }
        return $this->requiredTranslationFields;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $parents = implode(',',array_merge(['0'],Enum::whereParent_id(0)->pluck('id')->toArray()));
        return array_merge([
            'status'=>'boolean',
            'parent_id'=>'required|in:'.$parents,
            'value'=>'required_unless:parent_id,0',
        ], $this->getTranslationRules());
    }
}
