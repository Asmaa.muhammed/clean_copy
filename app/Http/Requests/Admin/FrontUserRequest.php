<?php

namespace App\Http\Requests\Admin;

use App\Foundation\Traits\NeedFilteredUpdateData;
use Illuminate\Foundation\Http\FormRequest;
use App\Foundation\Traits\ModuleRequest;
use Illuminate\Support\Str;

class FrontUserRequest extends FormRequest
{
    use ModuleRequest, NeedFilteredUpdateData;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $routeName = explode('.',$this->route()->getName())[0];
        return array_merge([
            'first_name' => 'required|string|min:3',
            'last_name' => 'required|string|min:3',
            'email' => [
                'required',
                'email',
                $this->uniqueWithIgnore('customers'
                    , $this->route()->hasParameter(Str::singular($routeName))
                        ? $this->route(Str::singular($routeName)) : null),
            ],
            'phone' => [
                'required',
                'size:11',
                $this->uniqueWithIgnore('customers'
                    , $this->route()->hasParameter(Str::singular($routeName))
                        ? $this->route(Str::singular($routeName)) : null)
            ],
            'password' => $this->routeIs($routeName.'.store')
                ? 'required|min:8|confirmed'
                : 'sometimes|nullable|min:8|confirmed',
            'status' => 'required|boolean',
            'profile_picture' => 'sometimes|nullable',
        ]);
    }


}
