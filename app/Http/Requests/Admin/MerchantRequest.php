<?php

namespace App\Http\Requests\Admin;

use App\Foundation\Traits\RequestWithTranslatable;
use Illuminate\Foundation\Http\FormRequest;

class MerchantRequest extends FrontUserRequest
{
    use RequestWithTranslatable;

    public function infoRules()
    {
        return array_merge([
            'info.front_mail' => 'required|email',
            'info.front_phone' => 'required',
            'info.thumbnail' => 'required|url',
            'info.category_id' => 'required|exists:taxonomies,id',
            'info.region_id' => 'required|exists:regions,id',
            'info.location' => 'required|url',
        ], $this->resolveTranslatableRules());
    }

    public function translatableRules()
    {
        return [
            'info.front_name' => 'required|string|min:3'
        ];
    }
}
