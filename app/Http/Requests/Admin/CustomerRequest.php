<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FrontUserRequest
{
    public function infoRules()
    {
        return [
            'info.region_id' => 'required|exists:regions,id',
            'info.address' => 'required|string',
        ];
    }
}
