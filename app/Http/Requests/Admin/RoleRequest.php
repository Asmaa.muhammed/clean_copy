<?php

namespace App\Http\Requests\Admin;

use App\Foundation\Traits\RequestWithTranslatable;
use Illuminate\Foundation\Http\FormRequest;

class RoleRequest extends FormRequest
{
    use RequestWithTranslatable;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function translatableRules()
    {
        return [
            'display_name' => 'required|min:3',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge($this->resolveTranslatableRules(),[
            'name' => 'required|min:3',
            'permissions' => 'required',
            'status' => 'required|boolean',
        ]);
    }
}
