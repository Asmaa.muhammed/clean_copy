<?php

namespace App\Http\Requests\Admin;

use App\Models\Enum;
use App\Models\Plugin;
use Illuminate\Foundation\Http\FormRequest;

class LayoutModelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $pluginGroups = Plugin::all()->groupBy('position');
        $pluginsValidation = $pluginGroups->mapWithKeys(function($group, $key){
            return ["pluginGroups.".$key.".*"=> "in:".$group->pluck('id')->implode(',')];
        })->toArray();
        return array_merge([
            "name" => 'required|string|min:3',
            "layout_type"=> "required|in:".Enum::whereValue('layout_types')->first()->children->pluck('id')->implode(','),
            "type" => 'required|in:'.Enum::whereValue('nodeTypes')->first()->children->pluck('value')->implode(','),
            "theme_id" => "required|exists:themes,id",
        ], $pluginsValidation);
    }

    public function getPlugins()
    {
        return collect($this->pluginGroups)->flatten()->toArray();
    }
}
