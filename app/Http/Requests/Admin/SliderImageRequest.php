<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SliderImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'image' => 'required',
            'sorting' => 'required|numeric',
            'target' => 'required|boolean',
            'link' => 'required_if:target,1',
            'target_type' => 'required_if:target,0',
            'target_id' => 'required_if:target,0',
        ];
    }
    public function getLinkData()
    {
        return $this->input('target') ? $this->only(['link']) : $this->only(['target_type','target_id']);
    }
    protected function prepareForValidation(){
        $directory_path = explode('/' . basename($this->input('image')), $this->input('image'))[0];
        $this->merge(['name'=>basename($this->input('image'))]);
        $this->merge(['image_url'=>$this->input('image')]);
        $this->merge(['directory_name'=>basename($directory_path)]);

    }
}
