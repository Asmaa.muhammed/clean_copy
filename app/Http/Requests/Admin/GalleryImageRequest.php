<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class GalleryImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'image'=>'required',
            'sorting' => 'required|numeric'
        ];
    }
    protected function prepareForValidation(){
        $directory_path = explode('/' . basename($this->input('image')), $this->input('image'))[0];
        $this->merge(['name'=>basename($this->input('image'))]);
        $this->merge(['image_url'=>$this->input('image')]);
        $this->merge(['directory_name'=>basename($directory_path)]);

    }
}
