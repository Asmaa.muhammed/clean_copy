<?php

namespace App\Http\Requests\Admin;

use App\Foundation\Traits\RequestWithTranslatable;
use App\Models\Gallery;
use App\Models\LayoutModel;
use App\Models\Category;
use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    use RequestWithTranslatable;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function translatableRules()
    {
        return [
            'title' => 'required|min:5|max:250',

        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //
        $categories = implode(',',Category::all()->pluck('id')->toArray());
    $galleries = implode(',',Gallery::all()->pluck('id')->toArray());
        $models =  implode(',',LayoutModel::all()->pluck('id')->toArray());
        return array_merge($this->resolveTranslatableRules(),[
            'status' => 'required|boolean',
            'layout_model_id'=>'required|in:'.$models,
            //'categories' =>'in:'.$categories,
            //'gallery'=>'in:'.$galleries,
            'publishOptions.start_publishing'=>'required|date',
            'publishOptions.end_publishing'=>'required|date|after:publishOptions.start_publishing'
            ]);
    }
    public function getImageData(){
        $directory_path = explode('/' . basename($this->input('image_url')), $this->input('image_url'))[0];
        return  ['name'=>basename($this->input('image_url')),'image_url'=>$this->input('image_url'),'directory_name'=>basename($directory_path) ];
    }
}
