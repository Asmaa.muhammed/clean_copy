<?php


namespace App\Http\Requests\Admin;

use App\Foundation\Traits\RequestWithTranslatable;
use Illuminate\Foundation\Http\FormRequest;

class SessionRequest extends FormRequest
{
    use RequestWithTranslatable;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function translatableRules()
    {
        return [
            'title'=>'required',
            'subject'=>'required',
            'description'=>'required',
            'place'=>'required',
            'location'=>'required',

        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge($this->resolveTranslatableRules(),[
                'node_id'=>'required',
                'date'=>'required',
                'day'=>'required',
                'time_from'=>'required',
                'time_to'=>'required',
        ]);

    }
    
}
