<?php

namespace App\Http\Requests\Admin;

use App\Foundation\Traits\RequestWithTranslatable;
use Illuminate\Foundation\Http\FormRequest;

class PersonRequest extends FormRequest
{
    use RequestWithTranslatable;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function translatableRules()
    {
        return [
            'title'=>'required',
            'name'=>'required',
            'position'=>'required',
            'about'=>'required',
            'description'=>'required',

        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge($this->resolveTranslatableRules(),[
            'status' => 'required|boolean',
            'portfolio_link' => 'required',
        ]);

    }
    
    public function getImageData(){
        $directory_path = explode('/' . basename($this->input('image_url')), $this->input('image_url'))[0];
        return  ['name'=>basename($this->input('image_url')),'image_url'=>$this->input('image_url'),'directory_name'=>basename($directory_path) ];
    }
}
