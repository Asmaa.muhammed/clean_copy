<?php

namespace App\Http\Requests\Admin;

use App\Foundation\Traits\RequestWithTranslatable;
use Illuminate\Foundation\Http\FormRequest;

class ModuleRequest extends FormRequest
{
    use RequestWithTranslatable;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function translatableRules()
    {
        return [
            'name' => 'required|min:3',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge($this->resolveTranslatableRules(),[
            'sorting' => 'required|numeric',
            'route' => 'required|string',
            'status' => 'required|boolean',
            'icon' => 'required|string',
        ]);
    }
}
