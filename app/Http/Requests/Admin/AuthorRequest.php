<?php

namespace App\Http\Requests\Admin;

use App\Foundation\Traits\TranslationRequest;
use Illuminate\Foundation\Http\FormRequest;

class AuthorRequest extends FormRequest
{
    use TranslationRequest;

    public $requiredTranslationFields= [
        'name'=>'required',
    ];

    public function getRequiredFields(){
        return $this->requiredTranslationFields;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge([
            'status'=>'boolean',
        ], $this->getTranslationRules());
    }
}
