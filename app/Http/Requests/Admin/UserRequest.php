<?php

namespace App\Http\Requests\Admin;

use App\Foundation\Traits\ModuleRequest;
use App\Foundation\Traits\NeedFilteredUpdateData;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    use ModuleRequest, NeedFilteredUpdateData;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|min:3|max:20',
            'last_name' => 'required|min:3|max:20',
            'email' => [
                'required',
                'email',
                $this->uniqueWithIgnore('users', $this->route('user')),
            ],
            'phone' => [
                'required',
                'size:11',
                $this->uniqueWithIgnore('users', $this->route('user'))
            ],
            'password' => $this->routeIs('users.store')
                ? 'required|min:8|confirmed'
                : 'sometimes|nullable|min:8|confirmed',
            'status' => 'required|boolean',
            'roles' => 'required'
        ];
    }
}
