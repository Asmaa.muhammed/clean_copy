<?php

namespace App\Http\Controllers;

use Carbon\Language;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Session;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $perPage = 10;

    public function getImageAttributes($image_url)
    {
        $directory_path = explode('/' . basename($image_url), $image_url)[0];
        if (!empty($directory_path)) {
            return [
                'name' => basename($image_url),
                'image_url' => $image_url,
                'directory_name' => basename($directory_path)
            ];

        } else
            return [];
    }

}
