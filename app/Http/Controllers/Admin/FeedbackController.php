<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseModuleController;
use App\Models\Feedback;
use Illuminate\Http\Request;

class FeedbackController extends BaseModuleController
{
    protected $module = 'feedbacks';

    protected function getDataTableColumns(): array
    {
        return [
            'id' => [
                'title' => __('common.id'),
                'searchable' => false,
                'orderable' => false,
            ],
            "name" => [
                'title' => __('feedbacks.name'),
                'searchable' => true,
                'orderable' => true,
            ],
            "email" => [
                'title' => __('feedbacks.email'),
                'searchable' => true,
                'orderable' => true,
            ],
            "phone" => [
                'title' => __('feedbacks.phone'),
                'searchable' => true,
                'orderable' => true,
            ],
            "message" => [
                'title' => __('feedbacks.message'),
                'searchable' => true,
                'orderable' => false,
            ],
            "feedback" => [
                'title' => __('feedbacks.feedback'),
                'searchable' => true,
                'orderable' => false,
            ]
        ];
    }

    /**
     * Display a listing of the resource.
     * @Permission("list-feedbacks")
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->makeDatatable(Feedback::get(), $this->getModule("route"));
        }
        $html = $this->tableHtmlBuilder($this->builder, $this->getDataTableColumns(), false);
        return view('admin.'.$this->getModule('route').'.index', compact('html'))
            ->with('cmsModule', $this->getModule());
    }
}
