<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseModuleController;
use App\Http\Requests\Admin\MenuGroupRequest;
use App\Models\MenuGroup;
use Exception;
use Illuminate\Http\Request;
use League\CommonMark\HtmlElement;

/**
 * Class MenuGroupController
 * @package App\Http\Controllers\Admin
 * @Auth
 */
class MenuGroupController extends BaseModuleController
{

    protected $module = 'menuGroups';

    protected function getDataTableColumns(): array
    {
        return [
            'id' => [
                'title' => __('common.id'),
                'searchable' => false,
                'orderable' => false,
            ],
            'name' => [
                'title' => __('menuGroups.name'),
                'searchable' => true,
                'orderable' => true,
            ],
            'short_code' => [
                'title' => __('menuGroups.short_code'),
                'searchable' => true,
                'orderable' => true,
            ],
            'status' => [
                'title' => __('common.status'),
                'searchable' => true,
                'orderable' => true,
            ],
            'links' => [
                'title' => __('menuGroups.links'),
                'searchable' => false,
                'orderable' => false,
            ],
        ];
    }

    /**
     * Display a listing of the resource.
     * @Permission("list-menuGroups")
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->makeDatatable(MenuGroup::get(), $this->getModule('route'), function ($dataTable){
                return $dataTable->editColumn('links', function ($record){
                    return (string)(new HtmlElement('div', ['class'=>'d-flex justify-content-around']))
                        ->setContents(
                        (new HtmlElement('a', ['class'=>'btn btn-sm btn-icon btn-success'
                            ,'href'=>route('menuLinks.index', $record->id)]))
                            ->setContents(new HtmlElement('i', ['class'=>'fas fa-link']))
                        . (new HtmlElement('a', ['class'=>'btn btn-sm btn-icon btn-success'
                            ,'href'=>route('menuLinks.create', $record->id)]))
                            ->setContents(new HtmlElement('i', ['class'=>'fas fa-plus']))
                    );
                });
            }, ['links']);
        }
        $html = $this->tableHtmlBuilder($this->builder, $this->getDataTableColumns());
        return view('admin.menuGroups.index', compact('html'))->with('cmsModule', $this->getModule());
    }

    /**
     * Show the form for creating a new resource.
     * @Permission("create-menuGroups")
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.menuGroups.create')->with('cmsModule', $this->getModule());
    }

    /**
     * Store a newly created resource in storage.
     * @Permission("create-menuGroups")
     * @param MenuGroupRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(MenuGroupRequest $request)
    {
        try {
            $newMenuGroup = MenuGroup::create($request->all());
            session()->flash('success', __('common.successCreate',
                ['module'=> __($this->getModule('route') . ".singularModuleName")]));
            return redirect()->route('menuLinks.create', $newMenuGroup->id);
        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();
    }

    /**
     * Display the specified resource.
     * @Permission("read-menuGroups")
     * @param  \App\Models\MenuGroup $menuGroup
     * @return \Illuminate\Http\Response
     */
    public function show(MenuGroup $menuGroup)
    {
        return view('admin.menuGroups.show', compact('menuGroup'))->with('cmsModule', $this->getModule());
    }

    /**
     * Show the form for editing the specified resource.
     * @Permission("update-menuGroups")
     * @param  \App\Models\MenuGroup $menuGroup
     * @return \Illuminate\Http\Response
     */
    public function edit(MenuGroup $menuGroup)
    {
        return view('admin.menuGroups.edit', compact('menuGroup'))->with('cmsModule', $this->getModule());
    }

    /**
     * Update the specified resource in storage.
     * @Permission("update-menuGroups")
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\MenuGroup $menuGroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MenuGroup $menuGroup)
    {
        try {
            $menuGroup->update($request->all());
            session()->flash('success', __('common.successUpdate',
                ['module'=> __($this->getModule('route') . ".singularModuleName")]));
            return redirect()->route('menuGroups.index');
        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     * @Permission("delete-menuGroups")
     * @param  \App\Models\MenuGroup $menuGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(MenuGroup $menuGroup)
    {
        try {
            $menuGroup->delete();
            return response()->json(['success' => __('common.successDelete',
                ['module'=> __($this->getModule('route').".singularModuleName")])], 200);
        } catch (Exception $exception) {
            return response()->json(['error' => __('common.error')], 200);
        }
    }
}
