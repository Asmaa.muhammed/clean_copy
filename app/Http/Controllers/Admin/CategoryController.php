<?php

namespace App\Http\Controllers\Admin;

use App\Foundation\Traits\TableIndex;
use App\Http\Controllers\Controller;
use App\Http\Middleware\Admin\SetCurrentLocaleLang;
use App\Http\Requests\Admin\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Parent_;
use Yajra\DataTables\Html\Builder;
/**
 * Class CategoryController
 * @package App\Http\Controllers
 * @Auth
 */
class CategoryController extends Controller
{
    use TableIndex;
    /**
     * Datatables Html Builder
     * @var Builder
     */
    protected $htmlBuilder;
    protected $module;
    public function __construct(Builder $htmlBuilder)
    {
        $this->htmlBuilder = $htmlBuilder;
        $this->module = "categories";
        $this->middleware(SetCurrentLocaleLang::class);

    }
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @Permission("list-categories")
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if ($request->ajax()) {
            return $this->makeDatatable(Category::withTranslation()->get(),$this->module);
        }
        $columns = ['id'=>['title'=>'ID','searchable'=>false,'orderable'=>false],'name'=>['title'=>'Name','searchable'=>true,'orderable'=>true],'status'=>['title'=>'Status','searchable'=>true,'orderable'=>true]];
        $html = $this->tableHtmlBuilder($this->htmlBuilder,$columns);
        return view('admin.categories.index', compact('html'));

    }
    /**
     * Display tree view of all resource
     * @Permission("list-categories")
     *@return \Illuminate\Http\Response
     */
    public function indexTree(){
        $categories = Category::whereParent(0)->get();
        return view('admin.categories.tree',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *@Permission("create-categories")
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CategoryRequest  $request
     * @Permission("create-categories")
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $request['type'] = 1;
        $category = Category::create($request->all());
        if ($category){
            $directory_path =  explode('/'.basename($request['image_url']),$request['image_url'])[0];
                if ($request['image_url']){
                    $category->image()->create([
                        'name'=>basename($request['image_url']),
                        'image_url'=>$request['image_url'],
                        'directory_name'=>basename($directory_path)
                    ]);
                }

            session()->flash('success', 'Category Created Successfully');
        }
        else{
            session()->flash('errors', ['something went wrong']);
        }
        return redirect()->route('categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  Category $category
     * @Permission("read-categories")
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
        $image = ($category->image) ? $category->image->image_url : null;
        return view('admin.categories.show',compact('category','image'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Category $category
     * @Permission("update-categories")
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
        $image = ($category->image) ? $category->image->image_url : null;
        return view('admin.categories.edit',compact('category','image'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CategoryRequest $request
     * @param  Category $category
     * @Permission("update-categories")
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, Category $category)
    {
        if($category->update($request->all())){
           $directory_path =  explode('/'.basename($request['image_url']),$request['image_url'])[0];
           if ($request['image_url']){
               if ($category->image()){
                   $category->image()->update([
                       'name'=>basename($request['image_url']),
                       'image_url'=>$request['image_url'],
                       'directory_name'=>basename($directory_path)

                   ]);
               }else{
                   $category->image()->update([
                       'name'=>basename($request['image_url']),
                       'image_url'=>$request['image_url'],
                       'directory_name'=>basename($directory_path)

                   ]);
               }

            }
            session()->flash('success', 'Category Updated Successfully');
        }
        else{
            session()->flash('errors', ['Something Went Wrong']);
        }
        return redirect()->route('categories.index');//



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category
     * @return \Illuminate\Http\Response
     * @Permission("delete-categories")
     * @throws \Exception
     */
    public function destroy(Category $category)
    {
        //
        if($category->delete()){
            return response()->json(['success'=>'Category deleted successfully.'], 200);
        }
        else{
            return response()->json(['error'=>'Something went wrong.'], 200);
        }
    }
}
