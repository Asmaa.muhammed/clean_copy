<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseModuleController;
use App\Http\Requests\Admin\PermissionRequest;
use App\Models\Permission;
use Exception;

/**
 * Class PermissionController
 * @package App\Http\Controllers\Admin
 * @Auth
 */
class PermissionController extends BaseModuleController
{

    protected $module = 'permissions';

    protected function getDataTableColumns(): array
    {
        return [
            'id' => [
                'title' => __('common.id'),
                'searchable' => false,
                'orderable' => false,
            ],
            'display_name' => [
                'title' => __('permissions.display_name'),
                'searchable' => true,
                'orderable' => true,
            ],
            'status' => [
                'title' => __('common.status'),
                'searchable' => true,
                'orderable' => true,
            ]
        ];
    }

    /**
     * Display a listing of the resource.
     * @Permission("list-acl")
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->makeDatatable(Permission::withTranslation()->get(), $this->getModule("route"));
        }
        $html = $this->tableHtmlBuilder($this->builder, $this->getDataTableColumns());
        return view('admin.acl.permissions.index', compact('html'))->with('cmsModule', $this->getModule());
    }

    /**
     * Show the form for creating a new resource.
     * @Permission("create-acl")
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.acl.permissions.create')->with('cmsModule', $this->getModule());
    }

    /**
     * Store a newly created resource in storage.
     * @Permission("create-acl")
     * @param PermissionRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(PermissionRequest $request)
    {
        try {
            $newPermission = Permission::create($request->all());
            session()->flash('success', __('common.successCreate',
                ['module'=> __($this->getModule('route').".singularModuleName")]));
            return redirect()->route('permissions.index');
        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();
    }

    /**
     * Display the specified resource.
     * @Permission("read-acl")
     * @param Permission $permission
     * @return \Illuminate\Http\Response
     */
    public function show(Permission $permission)
    {
        return view('admin.acl.permissions.show', compact('permission'))->with('cmsModule', $this->getModule());
    }

    /**
     * Show the form for editing the specified resource.
     * @Permission("update-acl")
     * @param Permission $permission
     * @return \Illuminate\Http\Response
     */
    public function edit(Permission $permission)
    {
        return view('admin.acl.permissions.edit', compact('permission'))->with('cmsModule', $this->getModule());
    }

    /**
     * Update the specified resource in storage.
     * @Permission("update-acl")
     * @param PermissionRequest $request
     * @param Permission $permission
     * @return \Illuminate\Http\Response
     */
    public function update(PermissionRequest $request, Permission $permission)
    {
        try {
            $permission->update($request->all());
            session()->flash('success', __('common.successUpdate',
                ['module'=> __($this->getModule('route').".singularModuleName")]));
            return redirect()->route('permissions.index');
        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     * @Permission("delete-acl")
     * @param Permission $permission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permission $permission)
    {
        try {
            $permission->delete();
            return response()->json(['success' => __('common.successDelete',
                ['module'=> __($this->getModule('route').".singularModuleName")])], 200);
        } catch (Exception $exception) {
            return response()->json(['error' => __('common.error')], 200);
        }
    }
}
