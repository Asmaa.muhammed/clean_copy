<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseModuleController;
use App\Models\Post;
use App\Http\Requests\Admin\PostRequest;

class PostController extends BaseModuleController
{
    protected $module = 'posts';

    protected function getDataTableColumns(): array
    {
        return [
            'id' => [
                'title' => '#',
                'searchable' => false,
                'orderable' => false,
            ],
            'title' => [
                'title' => __('posts.title'),
                'searchable' => true,
                'orderable' => true,
            ],
            'status' => [
                'title' => __('common.status'),
                'searchable' => true,
                'orderable' => true,
            ]
        ];
    }

    /**
     * Display a listing of the resource.
     * @Permission("list-posts")
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (request()->ajax()) {
            return $this->makeDatatable(Post::withTranslation()->get(), $this->getModule('route'));
        }
        $html = $this->tableHtmlBuilder($this->builder, $this->getDataTableColumns());
        return view('admin.posts.index', compact('html'))->with('cmsModule', $this->getModule());
    }

    /**
     * Show the form for creating a new resource.
     *@Permission("create-posts")
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.posts.create')->with('cmsModule', $this->getModule());
    }

    /**
     * Store a newly created resource in storage.
     *@Permission("create-posts")
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {

        $request['type'] = 2;
        try {
            $newPost =  Post::create($request->all());
            $newPost->publishOptions()->create($request['publishOptions']);
            if ($newPost){
                if(!empty($request->input('image_url'))){
                    $newPost->image()->create($request->getimageData());
                }
                if($request->input('gallery')){

                    $newPost->gallery()->create(['gallery_id'=>$request['gallery']]);
                }
                if($request['categories']){
                    $newPost->categories()->attach($request['categories']);
                }
                if($request['tags']){
                    $newPost->tags()->attach($request['tags']);
                }
                if($request->input('minor')){
                    $newPost->minor()->create(['minor_id'=>$request['minor']]);
                }

                if ($request['author']){
                    $newPost->author()->create(['taxonomy_id'=>$request['author']]);
                }
                session()->flash('success', __('common.successCreate',
                    ['module'=> __($this->getModule('route') . ".singularModuleName")]));
                return redirect()->route('posts.index');
            }else{
                session()->flash('error', __('common.error'));
            }

        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();
    }

    /**
     * Display the specified resource.
     *@Permission("read-posts")
     * @param Post $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //

        $image = ($post->image()->exists()) ? $post->image->image_url : '' ;
        return view('admin.posts.show',compact('post','image'))->with('cmsModule', $this->getModule());
    }

    /**
     * Show the form for editing the specified resource.
     *@Permission("update-posts")
     * @param Post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //

        $image = ($post->image()->exists()) ? $post->image->image_url : '' ;
        return view('admin.posts.edit',compact('post','image'))->with('cmsModule', $this->getModule());
    }

    /**
     * Update the specified resource in storage.
     *@Permission("update-posts")
     * @param PostRequest $request
     * @param Post $post
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, Post $post)
    {

        try {
            $post->update($request->all());
            $post->publishOptions()->update($request['publishOptions']);
            $post->image()->delete();
            if($request->input('image_url')){
                $post->image()->create($request->getimageData());
            }
            if($request->input('gallery')){
                ($post->gallery()->exists() ) ? $post->gallery()->update(['gallery_id'=>$request['gallery']]) : $post->gallery()->create(['gallery_id'=>$request['gallery']]);
            }
            if($request->input('categories')) {
                $post->categories()->sync($request['categories']);
            }
            if($request->input('minor')){
                ($post->minor()->exists() ) ? $post->minor()->update(['minor_id'=>$request['minor']]) : $post->minor()->create(['minor_id'=>$request['minor']]);
            }


            if($request->input('tags')){
                $post->tags()->sync($request['tags']);
            }
            session()->flash('success', __('common.successUpdate',
                ['module' => __($this->getModule('route') . ".singularModuleName")]));
            return redirect()->route('posts.index');
        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Post $post
     * @return \Illuminate\Http\Response
     * @Permission("delete-posts")
     * @throws \Exception
     */
    public function destroy(Post $post)
    {
        //
        try {
            $post->delete();
            return response()->json(['success' => __('common.successDelete',
                ['module' => __($this->getModule('route') . ".singularModuleName")])], 200);
        } catch (Exception $exception) {
            return response()->json(['error' => __('common.error')], 200);
        }
    }
}
