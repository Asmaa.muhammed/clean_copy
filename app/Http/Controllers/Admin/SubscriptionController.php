<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseModuleController;
use App\Models\Plan;
use App\Models\Subscription;
use App\Http\Requests\Admin\SubscriptionRequest as Request;
use Exception;

/**
 * Class SubscriptionController
 * @package App\Http\Controllers\Admin
 * @Auth
 */
class SubscriptionController extends BaseModuleController
{

    protected $module = 'subscriptions';

    protected function getDataTableColumns(): array
    {
        return [
            'id' => [
                'title' => __('common.id'),
                'searchable' => false,
                'orderable' => false,
            ],
            'plan' => [
                'title' => __('subscriptions.plan'),
                'searchable' => true,
                'orderable' => true,
            ],
            'starts_at' => [
                'title' => __('subscriptions.starts_at'),
                'searchable' => true,
                'orderable' => true,
            ],
            'ends_at' => [
                'title' => __('subscriptions.ends_at'),
                'searchable' => true,
                'orderable' => true,
            ],
            'user' => [
                'title' => __('subscriptions.user'),
                'searchable' => true,
                'orderable' => true,
            ],
            'status' => [
                'title' => __('common.status'),
                'searchable' => true,
                'orderable' => true,
            ]
        ];
    }

    /**
     * Display a listing of the resource.
     * @Permission("list-subscriptions")
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->makeDatatable(Subscription::with(['plan', 'user'])->get()
                , $this->getModule("route")
                , function ($builder){
                    return $builder->editColumn('plan', function ($record){
                        return $record->plan->name;
                    })
                    ->editColumn('user', function ($record){
                        return $record->user->full_name;
                    });
                });
        }
        $html = $this->tableHtmlBuilder($this->builder, $this->getDataTableColumns());
        return view('admin.'.$this->getModule('route').'.index',
            compact('html'))->with('cmsModule', $this->getModule());
    }

    /**
     * Display the specified resource.
     * @Permission("read-subscriptions")
     * @param  \App\Models\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function show(Subscription $subscription)
    {
        return view('admin.'.$this->getModule('route').'.show',
            compact('subscription'))->with('cmsModule', $this->getModule());
    }

    /**
     * Show the form for editing the specified resource.
     * @Permission("update-subscriptions")
     * @param  \App\Models\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function edit(Subscription $subscription)
    {
        return view('admin.'.$this->getModule('route').'.edit',
            compact('subscription'))->with('cmsModule', $this->getModule());
    }

    /**
     * Update the specified resource in storage.
     * @Permission("update-subscriptions")
     * @param Request $request
     * @param  \App\Models\Subscription $subscription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subscription $subscription)
    {
        try{
            $subscription->update($request->getUpdatableData());
            session()->flash('success', __('common.successUpdate',
                ['module'=> __($this->getModule('route').".singularModuleName")]));
            return redirect()->route($this->getModule('route').'.index');
        }
        catch(Exception $exception){
            dd($exception->getMessage());
            session()->flash('error', __('common.error'));
        }
        return back();
    }
}
