<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseModuleController;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;
use App\Http\Requests\Admin\FrontUserRequest;

/**
 * Class CustomerController
 * @package App\Http\Controllers\Admin
 * @Auth
 * @Permission("list-customers", only="index")
 * @Permission("read-customers", only="show")
 * @Permission("update-customers", only="edit,update")
 * @Permission("delete-customers", only="destroy")
 */
class CustomerController extends BaseModuleController
{
    protected $module = 'customers';
    public function __construct(Builder $builder)
    {
        parent::__construct($builder);

    }

    protected function getDataTableColumns(): array
    {
        return [
            'id' => [
                'title' => __('common.id'),
                'searchable' => false,
                'orderable' => false,
            ],
            'full_name' => [
                'title' => __('front_users.fullName'),
                'searchable' => true,
                'orderable' => true,
            ],
            'email' => [
                'title' => __('front_users.email'),
                'searchable' => true,
                'orderable' => true,
            ],
            'phone' => [
                'title' => __('front_users.phone'),
                'searchable' => true,
                'orderable' => true,
            ],
            'status' => [
                'title' => __('common.status'),
                'searchable' => true,
                'orderable' => true,
            ],
            'verification' => [
                'title' => "verification",
                'searchable' => true,
                'orderable' => true,
            ],
        ];
    }

    /**
     * Display a listing of the resources
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->makeDatatable(Customer::orderBy("created_at","DESC")->get()
                , $this->getModule("route")
                , function ($builder) {
                     $builder->editColumn('full_name', function ($record) {
                        return $record->full_name;
                    });
                    $builder->editColumn('verification', function ($record) {
                        return  (!empty($record->email_verified_at)) ?  "Active": "Not Active";
                    });
                    return $builder;
                });
        }
        $html = $this->tableHtmlBuilder($this->builder, $this->getDataTableColumns());
        return view('admin.' . $this->getModule('route') . '.index'
            , compact('html'))->with('cmsModule', $this->getModule());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.' . $this->getModule('route') . '.create')->with('cmsModule', $this->getModule());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param FrontUserRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(FrontUserRequest $request)
    {
        try {
            $newUser = Customer::create($request->all());
            session()->flash('success', __('common.successCreate',
                ['module' => __($this->getModule('route') . ".singularModuleName")]));
            return redirect()->route($this->getModule('route') . ".index");
        } catch (Exception $exception) {
            dd($exception);
            session()->flash('error', __('common.error'));
        }
        return back();
    }


    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::find($id);
        return view('admin.' . $this->getModule('route') . '.show', compact('customer'))
            ->with('cmsModule', $this->getModule());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);
        return view('admin.' . $this->getModule('route') . '.edit', compact('customer'))
            ->with('cmsModule', $this->getModule());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param FrontUserRequest $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(FrontUserRequest $request, $id)
    {
        $customer = Customer::find($id);
        try {
            $customer->update($request->getData());
            session()->flash('success', __('common.successUpdate',
                ['module' => "Customer"]));
            return redirect()->route($this->getModule('route') . ".index");
        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::find($id);
        try {
            $customer->delete();
            return response()->json(['success' => __('common.successDelete',
                ['module' => "Customer"])], 200);
        } catch (Exception $exception) {
            return response()->json(['error' => __('common.error')], 200);
        }
    }


}
