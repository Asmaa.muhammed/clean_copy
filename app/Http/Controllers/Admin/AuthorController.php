<?php

namespace App\Http\Controllers\Admin;

use App\Foundation\Traits\TableIndex;
use App\Http\Controllers\Controller;
use App\Http\Middleware\Admin\SetCurrentLocaleLang;
use App\Http\Requests\Admin\AuthorRequest;
use App\Models\Author;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;

/**
 * Class AuthorController
 * @package App\Http\Controllers
 * @Auth
 */

class AuthorController extends Controller
{
    use TableIndex;
    /**
     * Datatables Html Builder
     * @var Builder
     */
    protected $htmlBuilder;
    protected $module;
    public function __construct(Builder $htmlBuilder)
    {
//        Parent::__construct();
        $this->htmlBuilder = $htmlBuilder;
        $this->module = "authors";
        $this->middleware(SetCurrentLocaleLang::class);

    }
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if ($request->ajax()) {
            return $this->makeDatatable(Author::withTranslation()->get(),$this->module);
        }
        $columns = ['id'=>['title'=>'ID','searchable'=>false,'orderable'=>false],'name'=>['title'=>'Name','searchable'=>true,'orderable'=>true],'status'=>['title'=>'Status','searchable'=>true,'orderable'=>true]];

        $html = $this->tableHtmlBuilder($this->htmlBuilder,$columns);
        return view('admin.authors.index', compact('html'));

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.authors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  AuthorRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AuthorRequest $request)
    {
        //
        $request['type'] =3;
        $author = Author::create($request->all());
        if ($author){
            session()->flash('success', 'Author Created Successfully');
        }
        else{
            session()->flash('errors', ['something went wrong']);
        }
        return redirect()->route('authors.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  Author $author
     * @return \Illuminate\Http\Response
     */
    public function show(Author $author)
    {
        //
        return view('admin.authors.show',compact('author'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Author $author
     * @Permission("list-authors")
     * @return \Illuminate\Http\Response
     */
    public function edit(Author $author)
    {
        //
        return view('admin.authors.edit',compact('author'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  AuthorRequest $request
     * @param  Author $author
     * @return \Illuminate\Http\Response
     */
    public function update(AuthorRequest $request,Author $author)
    {
        if($author->update($request->all())){
            session()->flash('success', 'Author Updated Successfully');
        }
        else{
            session()->flash('errors', ['Something Went Wrong']);
        }
        return redirect()->route('authors.index');//



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Author $author
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Author $author)
    {
        //
        if($author->delete()){
            return response()->json(['success'=>'Author deleted successfully.'], 200);
        }
        else{
            return response()->json(['error'=>'Something went wrong.'], 200);
        }
    }
}
