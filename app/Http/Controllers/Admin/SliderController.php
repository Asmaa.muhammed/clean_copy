<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseModuleController;
use App\Http\Requests\Admin\SliderRequest;
use App\Models\Slider;

/**
 * Class SliderController
 * @package App\Http\Controllers\Admin
 * @Auth
 */
class SliderController extends BaseModuleController
{
    protected $module = 'sliders';

    protected function getDataTableColumns(): array
    {
        return [
            'title' => [
                'title' => __('sliders.title'),
                'searchable' => true,
                'orderable' => true,
            ],
            'short_code' => [
                'title' => __('sliders.short_code'),
                'searchable' => true,
                'orderable' => true,
            ],
            'status' => [
                'title' => __('common.status'),
                'searchable' => true,
                'orderable' => true,
            ],
            'images' => [
                'title' => __('sliders.images'),
                'searchable' => false,
                'orderable' => false,
            ],

        ];
    }

    /**
     * Display a listing of the resource.
     * @Permission("list-sliders")
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (request()->ajax()) {
            return $this->makeDatatable(Slider::all(), $this->getModule('route'),
                function ($dataTable) {
                    return $dataTable->addColumn('images', function ($record)  {
                        return view('admin.sliders.image_actions', [ 'slider' => $record]);

                    });

                }, ['images']);
        }

        $html = $this->tableHtmlBuilder($this->builder, $this->getDataTableColumns());
        return view('admin.sliders.index', compact('html'))->with('cmsModule', $this->getModule());

    }

    /**
     * Show the form for creating a new resource.
     * @Permission("create-sliders")
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.sliders.create')->with('cmsModule', $this->getModule());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SliderRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(SliderRequest $request)
    {
        //
        try {
            $slider = Slider::create($request->all());
            if ($request['main'] == '1'){
                Slider::query()->where('id','!=',$slider->id)->update(['main'=>0]);
            }
            session()->flash('success', __('common.successCreate',
                ['module'=> __($this->getModule('route') . ".singularModuleName")]));
            return redirect()->route('sliderImages.create', $slider->id);
        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();

    }

    /**
     * Display the specified resource.
     * @Permission("read-sliders")
     * @param Slider $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        //
        return view('admin.sliders.show', compact('slider'))->with('cmsModule', $this->getModule());

    }

    /**
     * Show the form for editing the specified resource.
     * @Permission("update-sliders")
     * @param Slider $slider
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider)
    {
        //
        return view('admin.sliders.edit', compact('slider'))->with('cmsModule', $this->getModule());

    }

    /**
     * Update the specified resource in storage.
     * @Permission("update-sliders")
     * @param SliderRequest $request
     * @param Slider $slider
     * @return \Illuminate\Http\Response
     */
    public function update(SliderRequest $request, Slider $slider)
    {
        //
        try {
            $slider->update($request->all());
            if ($request['main'] == '1'){
                Slider::query()->where('id','!=',$slider->id)->update(['main'=>0]);
            }
            session()->flash('success', __('common.successUpdate',
                ['module' => __($this->getModule('route') . ".singularModuleName")]));
            return redirect()->route('sliders.index');
        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Slider $slider
     * @return \Illuminate\Http\Response
     * @Permission("delete-sliders")
     * @throws \Exception
     */
    public function destroy(Slider $slider)
    {
        //
        try {
            $slider->delete();
            return response()->json(['success' => __('common.successDelete',
                ['module' => __($this->getModule('route') . ".singularModuleName")])], 200);
        } catch (Exception $exception) {
            return response()->json(['error' => __('common.error')], 200);
        }
    }
}
