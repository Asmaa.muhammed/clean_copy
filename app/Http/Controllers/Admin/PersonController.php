<?php

namespace App\Http\Controllers\Admin;

use App\Foundation\Traits\TableIndex;
use App\Http\Controllers\Controller;
use App\Http\Middleware\Admin\SetCurrentLocaleLang;
use App\Http\Requests\Admin\PersonRequest;
use App\Models\Person;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseModuleController;
/**
 * Class PersonController
 * @package App\Http\Controllers
 * @Auth
 */
class PersonController extends BaseModuleController
{
    protected $module = 'persons';

    protected function getDataTableColumns(): array {
        return [
            'id'          => [
                'title'=>'Record ID',
                'searchable'=>false,'orderable'=>false
            ],
            'title'        => [
                'title'=>'title',
                'searchable'=>true,
                'orderable'=>true
            ],
            'name'        => [
                'title'=>'name',
                'searchable'=>true,
                'orderable'=>true
            ],
            'status'        => [
                'title'=>'status',
                'searchable'=>true,
                'orderable'=>true
            ],
            
        ];
    }
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @Permission("list-persons")
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        if (request()->ajax()) {
            return $this->makeDatatable(Person::withTranslation()->get(), $this->getModule('route'));
        }
        $html = $this->tableHtmlBuilder($this->builder, $this->getDataTableColumns());
        return view('admin.persons.index', compact('html'))->with('cmsModule', $this->getModule());
    }

    /**
     * Show the form for creating a new resource.
     *@Permission("create-persons")
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.persons.create')->with('cmsModule', $this->getModule());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PersonRequest  $request
     * @Permission("create-persons")
     * @return \Illuminate\Http\Response
     */
    public function store(PersonRequest $request)
    {
        try {
            $person =  Person::create($request->all());
            if ($person){
                if(!empty($request->input('image_url'))){
                    $person->image()->create($request->getimageData());
                }
            
                session()->flash('success', __('common.successCreate',
                    ['module'=> __($this->getModule('route') . ".singularModuleName")]));
                return redirect()->route('persons.index');
            }else{
                session()->flash('error', __('common.error'));
            }

        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();
     }

    /**
     * Display the specified resource.
     *
     * @param  Person $person
     * @Permission("read-persons")
     * @return \Illuminate\Http\Response
     */
    public function show(Person $person)
    { 
        $image = ($person->image()->exists()) ? $person->image->image_url : '' ;
        return view('admin.persons.show',compact('person','image'))->with('cmsModule', $this->getModule());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Person $person
     * @Permission("update-persons")
     * @return \Illuminate\Http\Response
     */
    public function edit(Person $person)
    {
        $image = ($person->image()->exists()) ? $person->image->image_url : '' ;
        return view('admin.persons.edit',compact('person','image'))->with('cmsModule', $this->getModule());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PersonRequest $request
     * @param  Person $person
     * @Permission("update-persons")
     * @return \Illuminate\Http\Response
     */
    public function update(PersonRequest $request, Person $person)
    {
        try {
            $person->update($request->all());
            $person->image()->delete();
            if($request->input('image_url')){
                $person->image()->create($request->getimageData());
            }
            session()->flash('success', __('common.successUpdate',
                ['module' => __($this->getModule('route') . ".singularModuleName")]));
            return redirect()->route('persons.index');
        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Person $person
     * @return \Illuminate\Http\Response
     * @Permission("delete-persons")
     * @throws \Exception
     */
    public function destroy(Person $person)
    {
        //
        try {
            $person->delete();
            return response()->json(['success' => __('common.successDelete',
                ['module' => __($this->getModule('route') . ".singularModuleName")])], 200);
        } catch (Exception $exception) {
            return response()->json(['error' => __('common.error')], 200);
        }
    }
}
