<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseModuleController;
use App\Models\Enum;
use App\Models\Plugin;
use Illuminate\Database\Eloquent\Relations\Relation;
use Webwizo\Shortcodes\Facades\Shortcode;

/**
 * Class PluginController
 * @package App\Http\Controllers\Admin
 * @Auth
 */
class PluginController extends BaseModuleController
{
    protected $module = 'plugins';

    protected function getDataTableColumns(): array
    {
        return [
            'id' => [
                'title' => __('common.id'),
                'searchable' => false,
                'orderable' => false,
            ],
            'name' => [
                'title' => __('plugins.name'),
                'searchable' => true,
                'orderable' => true,
            ],
            'short_code' => [
                'title' => __('plugins.short_code'),
                'searchable' => true,
                'orderable' => true,
            ],
            'type' => [
                'title' => __('plugins.type'),
                'searchable' => true,
                'orderable' => true,
            ],
            'position' => [
                'title' => __('plugins.position'),
                'searchable' => true,
                'orderable' => true,
            ],
            'status' => [
                'title' => __('common.status'),
                'searchable' => true,
                'orderable' => true,
            ],
        ];
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @Permission("list-plugins")
     */
    public function index()
    {
        Shortcode::disable();
        if (request()->ajax()) {
            return $this->makeDatatable(Plugin::get(),
                $this->getModule("route"), function ($builder) {
                    return $builder
                        ->editColumn('type', function ($record) {
                            return class_basename(Relation::getMorphedModel($record->type));
                        })
                        ->editColumn('position', function ($record) {
                            return Enum::whereValue('layout_positions')
                                ->first()
                                ->children
                                ->pluck('name', 'value')[$record->position];
                        });
                }, [], [], 'admin.plugins.generator_modal_action');
        }
        $html = $this->tableHtmlBuilder($this->builder, $this->getDataTableColumns());
        return view('admin.plugins.index', compact('html'))->with('cmsModule', $this->getModule());
    }


    /**
     * @Permission("create-plugins")
     */
    public function create()
    {
        return abort(404);
    }
}
