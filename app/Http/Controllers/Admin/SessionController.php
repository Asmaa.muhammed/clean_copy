<?php

namespace App\Http\Controllers\Admin;

use App\Foundation\Traits\TableIndex;
use App\Http\Controllers\Controller;
use App\Http\Middleware\Admin\SetCurrentLocaleLang;
use App\Http\Requests\Admin\SessionRequest;
use App\Models\Event;
use App\Models\Person;
use App\Models\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseModuleController;
/**
 * Class SessionController
 * @package App\Http\Controllers
 * @Auth
 */
class SessionController extends BaseModuleController
{
    protected $module = 'sessions';

    protected function getDataTableColumns(): array {
        return [
            'id'          => [
                'title'=>'Record ID',
                'searchable'=>false,'orderable'=>false
            ],
            'subject'        => [
                'title'=> 'subject',
                'searchable'=>true,
                'orderable'=>true
            ],
            'day'        => [
                'title'=> 'day',
                'searchable'=>true,
                'orderable'=>true
            ],
            'status'        => [
                'title'=> 'status',
                'searchable'=>true,
                'orderable'=>true
            ],
            
        ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)

    {
        if (request()->ajax()) {
            return $this->makeDatatable(Session::withTranslation()->get(), $this->getModule('route'));
        }
        $html = $this->tableHtmlBuilder($this->builder, $this->getDataTableColumns());
        return view('admin.sessions.index', compact('html'))->with('cmsModule', $this->getModule());
    }

    /**
     * Show the form for creating a new resource.
     *@Permission("create-sessions")
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $persons = Person::withTranslation()->get()->pluck('name', 'id')->toArray();
        $events = Event::withTranslation()->get()->pluck('title', 'id')->toArray();

        return view('admin.sessions.create', compact('persons', 'events'))->with('cmsModule', $this->getModule());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  SessionRequest  $request
     * @Permission("create-sessions")
     * @return \Illuminate\Http\Response
     */
    public function store(SessionRequest $request)
    {
        try {
            $request['node_id'] = 2;
            
            $session = Session::create($request->all());
            if ($session){
                if($request['persons']){
                    $session->persons()->attach($request['persons']);
                }
            
                session()->flash('success', __('common.successCreate',
                    ['module'=> __($this->getModule('route') . ".singularModuleName")]));
                return redirect()->route('sessions.index');
            }else{
                session()->flash('error', __('common.error'));
            }

        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  Session $session
     * @Permission("read-sessions")
     * @return \Illuminate\Http\Response
     */
    public function show(Session $session)
    {
        return view('admin.sessions.show',compact('session'))->with('cmsModule', $this->getModule());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Session $session
     * @Permission("update-sessions")
     * @return \Illuminate\Http\Response
     */
    public function edit(Session $session)
    {
        return view('admin.sessions.edit',compact('session'))->with('cmsModule', $this->getModule());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  SessionRequest $request
     * @param  Session $session
     * @Permission("update-sessions")
     * @return \Illuminate\Http\Response
     */
    public function update(SessionRequest $request, Session $session)
    {
        if($session->update($request->all())){
            session()->flash('success', 'Session Updated Successfully');
        }
        else{
            session()->flash('errors', ['Something Went Wrong']);
        }
        return redirect()->route('sessions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Session $session
     * @return \Illuminate\Http\Response
     * @Permission("delete-sessions")
     * @throws \Exception
     */
    public function destroy(Session $session)
    {
        //
        if($session->delete()){
            return response()->json(['success'=>'Session deleted successfully.'], 200);
        }
        else{
            return response()->json(['error'=>'Something went wrong.'], 200);
        }
    }
}
