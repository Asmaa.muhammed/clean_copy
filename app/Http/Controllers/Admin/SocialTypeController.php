<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseModuleController;
use App\Models\SocialType;
use Illuminate\Http\Request;

class SocialTypeController extends BaseModuleController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SocialType  $socialType
     * @return \Illuminate\Http\Response
     */
    public function show(SocialType $socialType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SocialType  $socialType
     * @return \Illuminate\Http\Response
     */
    public function edit(SocialType $socialType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SocialType  $socialType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SocialType $socialType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SocialType  $socialType
     * @return \Illuminate\Http\Response
     */
    public function destroy(SocialType $socialType)
    {
        //
    }

    protected function getDataTableColumns(): array
    {
        // TODO: Implement getDataTableColumns() method.
    }
}
