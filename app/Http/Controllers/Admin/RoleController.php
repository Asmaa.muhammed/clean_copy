<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseModuleController;
use App\Http\Requests\Admin\RoleRequest;
use App\Models\Role;
use Exception;

/**
 * @Auth
 */
class RoleController extends BaseModuleController
{

    protected $module = 'roles';

    protected function getDataTableColumns(): array
    {
        return [
            'id' => [
                'title' => __('common.id'),
                'searchable' => false,
                'orderable' => false,
            ],
            'display_name' => [
                'title' => __('roles.display_name'),
                'searchable' => true,
                'orderable' => true,
            ],
            'permissions' => [
                'title' => __('roles.permissions'),
                'searchable' => true,
                'orderable' => true,
            ],
            'status' => [
                'title' => __('common.status'),
                'searchable' => true,
                'orderable' => true,
            ]
        ];
    }

    /**
     * Display a listing of the resource.
     * @Permission("list-acl")
     * @Permission("list-roles")
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authUserMinimumRole = auth()->user()->roles->first()->id;
        if (request()->ajax()) {
            return $this->makeDatatable(Role::where('id', in_array($authUserMinimumRole
                , config('app.sorted_privileged_admins_ids')) ? '>=' : '>'
                , $authUserMinimumRole)
                ->withTranslation()->get(), $this->getModule("route"),
                function ($builder) {
                    return $builder->editColumn('permissions', function ($record) {
                        return $record->permissions->pluck('display_name')->implode(', ');
                    });
                });
        }
        $html = $this->tableHtmlBuilder($this->builder, $this->getDataTableColumns());
        return view('admin.acl.roles.index', compact('html'))->with('cmsModule', $this->getModule());
    }

    /**
     * Show the form for creating a new resource.
     * @Permission("create-acl")
     * @Permission("create-roles")
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.acl.roles.create')->with('cmsModule', $this->getModule());
    }

    /**
     * Store a newly created resource in storage.
     * @Permission("create-acl")
     * @Permission("create-roles")
     * @param RoleRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        try {
            $role = Role::create($request->all());
            $role->permissions()->sync($request->permissions);
            session()->flash('success', __('common.successCreate',
                ['module' => __($this->getModule('route') . ".singularModuleName")]));
            return redirect()->route('roles.index');
        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();
    }

    /**
     * Display the specified resource.
     * @Permission("read-acl")
     * @Permission("read-roles")
     * @param Role $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        return view('admin.acl.roles.show', compact('role'))->with('cmsModule', $this->getModule());
    }

    /**
     * Show the form for editing the specified resource.
     * @Permission("update-acl")
     * @Permission("update-roles")
     * @param Role $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        return view('admin.acl.roles.edit', compact('role'))->with('cmsModule', $this->getModule());
    }

    /**
     * Update the specified resource in storage.
     * @Permission("update-acl")
     * @Permission("update-roles")
     * @param RoleRequest $request
     * @param Role $role
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, Role $role)
    {
        try {
            $role->update($request->all());
            $role->permissions()->sync($request->permissions);
            session()->flash('success', __('common.successUpdate',
                ['module' => __($this->getModule('route') . ".singularModuleName")]));
            return redirect()->route('roles.index');
        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     * @Permission("delete-acl")
     * @Permission("delete-roles")
     * @param Role $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        try {
            $role->delete();
            return response()->json(['success' => __('common.successDelete',
                ['module' => __($this->getModule('route') . ".singularModuleName")])], 200);
        } catch (Exception $exception) {
            return response()->json(['error' => __('common.error')], 200);
        }
    }
}
