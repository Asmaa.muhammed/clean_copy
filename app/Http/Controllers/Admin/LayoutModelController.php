<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseModuleController;
use App\Http\Requests\Admin\LayoutModelRequest;
use App\Models\Enum;
use App\Models\LayoutModel;
use Exception;
use Illuminate\Http\Request;

/**
 * Class LayoutModelController
 * @package App\Http\Controllers\Admin
 * @Auth
 */
class LayoutModelController extends BaseModuleController
{
    protected $module = 'layoutModels';

    protected function getDataTableColumns(): array
    {
        return [
            'id' => [
                'title' => __('common.id'),
                'searchable' => false,
                'orderable' => false,
            ],
            'name' => [
                'title' => __('layoutModels.name'),
                'searchable' => true,
                'orderable' => true,
            ],
            'layout_type' => [
                'title' => __('layoutModels.layout_type'),
                'searchable' => true,
                'orderable' => true,
            ],
            'type' => [
                'title' => __('layoutModels.type'),
                'searchable' => true,
                'orderable' => true,
            ],
            'theme_id' => [
                'title' => __('layoutModels.theme'),
                'searchable' => true,
                'orderable' => true,
            ],
            'status' => [
                'title' => __('common.status'),
                'searchable' => true,
                'orderable' => true,
            ],
        ];
    }

    /**
     * Display a listing of the resource.
     * @Permission("list-layoutModels")
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = Enum::whereValue('nodeTypes')->first()->children->pluck('name', 'value');
        if (request()->ajax()) {
            return $this->makeDatatable(LayoutModel::get()
                , $this->getModule("route")
                , function ($builder) use($types) {
                    return $builder->editColumn('layout_type', function ($record) {
                        return $record->layoutType->name;
                    })
                    ->editColumn('theme_id', function ($record){
                        return $record->theme->name;
                    })
                    ->editColumn('type', function ($record) use ($types){
                        return ucwords($types[$record->type]);
                    });
                });
        }
        $html = $this->tableHtmlBuilder($this->builder, $this->getDataTableColumns());
        return view('admin.layoutModels.index', compact('html'))->with('cmsModule', $this->getModule());
    }

    /**
     * Show the form for creating a new resource.
     * @Permission("create-layoutModels")
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.layoutModels.create')->with('cmsModule', $this->getModule());
    }

    /**
     * Store a newly created resource in storage.
     * @Permission("create-layoutModels")
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(LayoutModelRequest $request)
    {
        try {
            $newLayoutModel = LayoutModel::create($request->all());
            $newLayoutModel->plugins()->sync($request->getPlugins());
            session()->flash('success', __('common.successCreate',
                ['module'=> __($this->getModule('route') . ".singularModuleName")]));
            return redirect()->route('layoutModels.index');
        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();
    }

    /**
     * Display the specified resource.
     * @Permission("read-layoutModels")
     * @param  \App\Models\LayoutModel $layoutModel
     * @return \Illuminate\Http\Response
     */
    public function show(LayoutModel $layoutModel)
    {
        return view('admin.layoutModels.create', compact('layoutModel'))->with('cmsModule', $this->getModule());
    }

    /**
     * Show the form for editing the specified resource.
     * @Permission("update-layoutModels")
     * @param  \App\Models\LayoutModel $layoutModel
     * @return \Illuminate\Http\Response
     */
    public function edit(LayoutModel $layoutModel)
    {
        return view('admin.layoutModels.edit', compact('layoutModel'))->with('cmsModule', $this->getModule());
    }

    /**
     * Update the specified resource in storage.
     * @Permission("update-layoutModels")
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\LayoutModel $layoutModel
     * @return \Illuminate\Http\Response
     */
    public function update(LayoutModelRequest $request, LayoutModel $layoutModel)
    {
        try {
            $layoutModel->update($request->all());
            $layoutModel->plugins()->sync($request->getPlugins());
            session()->flash('success', __('common.successUpdate',
                ['module'=> __($this->getModule('route') . ".singularModuleName")]));
            return redirect()->route('layoutModels.index');
        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     * @Permission("delete-layoutModels")
     * @param  \App\Models\LayoutModel $layoutModel
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(LayoutModel $layoutModel)
    {
        try {
            $layoutModel->delete();
            return response()->json(['success' => __('common.successDelete',
                ['module'=> __($this->getModule('route').".singularModuleName")])], 200);
        } catch (Exception $exception) {
            return response()->json(['error' => __('common.error')], 200);
        }
    }
}
