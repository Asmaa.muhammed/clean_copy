<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseModuleController;
use App\Http\Requests\Admin\GalleryImageRequest;
use App\Models\Gallery;
use App\Models\Image;

/**
 * Class GalleryImageController
 * @package App\Http\Controllers\Admin
 * @Auth
 */
class GalleryImageController extends BaseModuleController
{ protected $module = 'galleryImages';

    protected function getDataTableColumns(): array
    {
        return [
            'image' => [
                'title' => __('galleryImages.image_preview'),
                'searchable' => false,
                'orderable' => false,
            ],
            'name' => [
                'title' => __('galleryImages.image_name'),
                'searchable' => true,
                'orderable' => true,
            ],
            'sorting' => [
                'title' => __('galleryImages.sorting'),
                'searchable' => true,
                'orderable' => true,
            ],
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Display a listing of the resource.
     * @param  Gallery $gallery
     * @Permission("list-galleryImages")
     * @return \Illuminate\Http\Response
     */
    public function index(Gallery $gallery)
    {
        //
        if (request()->ajax()) {
            return $this->makeDatatable($gallery->images, $this->getModule('route'),
                function ($dataTable) {
                    return $dataTable->editColumn('image', function ($record) {
                        return view('admin.layout.includes.image_preview', ['record' => $record]);

                    });

                }, ['icon'], ['gallery'=>$gallery]);

        }

        $html = $this->tableHtmlBuilder($this->builder, $this->getDataTableColumns());
        return view('admin.gallery_images.index', compact('html','gallery'))->with('cmsModule', $this->getModule());

    }

    /**
     * Show the form for creating a new resource.
     * @param  Gallery $gallery
     * @Permission("create-galleryImages")
     * @return \Illuminate\Http\Response
     */
    public function create(Gallery $gallery)
    {
        //
        return view('admin.gallery_images.create',compact('gallery'))->with('cmsModule', $this->getModule());
    }

    /**
     * Store a newly created resource in storage.
     * @param  Gallery $gallery
     * @Permission("create-galleryImages")
     * @param  GalleryImageRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(GalleryImageRequest $request,Gallery $gallery)
    {
        //
        try {
            $gallery->images()->create($request->all());
            session()->flash('success', __('common.successCreate',
                ['module'=> __($this->getModule('route') . ".singularModuleName")]));
            return redirect()->route('galleryImages.index', $gallery->id);
        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();
    }

    /**
     * Display the specified resource.
     * @Permission("read-galleryImages")
     * @param  Gallery $gallery
     * @param Image $galleryImage
     * @return \Illuminate\Http\Response
     */
    public function show(Gallery $gallery ,Image $galleryImage)
    {
        //
        return view('admin.gallery_images.show', compact('gallery','galleryImage'))->with('cmsModule', $this->getModule());

    }

    /**
     * Show the form for editing the specified resource.
     * @Permission("update-galleryImages")
     * @param  Gallery $gallery
     * @param Image $galleryImage
     * @return \Illuminate\Http\Response
     */
    public function edit(Gallery $gallery ,Image $galleryImage)
    {
        //
        return view('admin.gallery_images.edit', compact('gallery','galleryImage'))->with('cmsModule', $this->getModule());

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  GalleryImageRequest  $request
     * @Permission("update-galleryImages")
     * @param  Gallery $gallery
     * @param Image $galleryImage
     * @return \Illuminate\Http\Response
     */
    public function update(GalleryImageRequest $request, Gallery $gallery, Image $galleryImage)
    {
        //
        try {
            $galleryImage->update($request->all());
            session()->flash('success', __('common.successUpdate',
                ['module'=> __($this->getModule('route') . ".singularModuleName")]));
            return redirect()->route('galleryImages.index', $gallery->id);
        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Gallery $gallery
     * @param  Image $galleryImage;
     * @return \Illuminate\Http\Response
     * @Permission("delete-galleryImages")
     * @throws \Exception
     */
    public function destroy(Gallery $gallery , Image $galleryImage)
    {
        //
        try {
            $galleryImage->delete();
            return response()->json(['success' => __('common.successDelete',
                ['module' => __($this->getModule('route') . ".singularModuleName")])], 200);
        } catch (Exception $exception) {
            return response()->json(['error' => __('common.error')], 200);
        }

    }
}
