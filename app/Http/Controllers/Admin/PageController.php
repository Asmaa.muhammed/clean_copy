<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseModuleController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PageRequest;
use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends BaseModuleController
{
    protected $module = 'pages';

    protected function getDataTableColumns(): array
    {
        return [
            'id' => [
                'title' => '#',
                'searchable' => false,
                'orderable' => false,
            ],
            'title' => [
                'title' => __('pages.title'),
                'searchable' => true,
                'orderable' => true,
            ],
            'status' => [
                'title' => __('common.status'),
                'searchable' => true,
                'orderable' => true,
            ]
        ];
    }

    /**
     * Display a listing of the resource.
     * @Permission("list-pages")
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (request()->ajax()) {
            return $this->makeDatatable(Page::withTranslation()->get(), $this->getModule('route'));
        }
        $html = $this->tableHtmlBuilder($this->builder, $this->getDataTableColumns());
        return view('admin.pages.index', compact('html'))->with('cmsModule', $this->getModule());
    }

    /**
     * Show the form for creating a new resource.
     *@Permission("create-pages")
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('admin.pages.create')->with('cmsModule', $this->getModule());
    }

    /**
     * Store a newly created resource in storage.
     *@Permission("create-pages")
     * @param PageRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(PageRequest $request)
    {

        $request['type'] = 1;
       // dd($request->all());
        try {
           $newPage =  Page::create($request->all());
           $newPage->publishOptions()->create($request['publishOptions']);
           if ($newPage){
               if($request->input('image_url')){
                   $newPage->image()->create($request->getImageData());
               }
               if($request->input('gallery')){

                  $newPage->gallery()->create(['gallery_id'=>$request['gallery']]);
               }
               if($request->input('minor')){
                $newPage->minor()->create(['minor_id'=>$request['minor']]);
                }
               if($request['categories']){
                   $newPage->categories()->attach($request['categories']);
               }
               if($request['tags']){
                   $newPage->tags()->attach($request['tags']);
               }
               session()->flash('success', __('common.successCreate',
                   ['module'=> __($this->getModule('route') . ".singularModuleName")]));
               return redirect()->route('pages.index');
           }else{
               session()->flash('error', __('common.error'));
           }

        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();
    }

    /**
     * Display the specified resource.
     *@Permission("read-pages")
     * @param Page $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //

        $image = ($page->image()->exists()) ? $page->image->image_url : '' ;
        return view('admin.pages.show',compact('page','image'))->with('cmsModule', $this->getModule());
    }

    /**
     * Show the form for editing the specified resource.
     *@Permission("update-pages")
     * @param Page $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        //

        $image = ($page->image()->exists()) ? $page->image->image_url : '' ;
        return view('admin.pages.edit',compact('page','image'))->with('cmsModule', $this->getModule());
    }

    /**
     * Update the specified resource in storage.
     *@Permission("update-pages")
     * @param PageRequest $request
     * @param Page $page
     * @return \Illuminate\Http\Response
     */
    public function update(PageRequest $request, Page $page)
    {
        try {
            $page->update($request->all());
            $page->publishOptions()->update($request['publishOptions']);
            $page->image()->delete();
            if($request->input('image_url')){
                $page->image()->create($request->getImageData());
            }
            if($request->input('gallery')){
                ($page->gallery()->exists() ) ? $page->gallery()->update(['gallery_id'=>$request['gallery']]) : $page->gallery()->create(['gallery_id'=>$request['gallery']]);
            }
            if($request->input('categories')) {
                $page->categories()->sync($request['categories']);
            }
            if($request->input('tags')){
                $page->tags()->sync($request['tags']);
            }
            if($request->input('minor')){
                ($page->minor()->exists() ) ? $page->minor()->update(['minor_id'=>$request['minor']]) : $page->minor()->create(['minor_id'=>$request['minor']]);
            }

            session()->flash('success', __('common.successUpdate',
                ['module' => __($this->getModule('route') . ".singularModuleName")]));
            return redirect()->route('pages.index');
        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Page $page
     * @return \Illuminate\Http\Response
     * @Permission("delete-pages")
     * @throws \Exception
     */
    public function destroy(Page $page)
    {
        //
        try {
            $page->delete();
            return response()->json(['success' => __('common.successDelete',
                ['module' => __($this->getModule('route') . ".singularModuleName")])], 200);
        } catch (Exception $exception) {
            return response()->json(['error' => __('common.error')], 200);
        }
    }
}
