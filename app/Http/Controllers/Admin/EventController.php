<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseModuleController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\EventRequest;
use App\Models\Event;


class EventController extends BaseModuleController
{
    protected $module = 'events';

    protected function getDataTableColumns(): array
    {
        return [
            'id' => [
                'title' => '#',
                'searchable' => false,
                'orderable' => false,
            ],
            'title' => [
                'title' => __('events.title'),
                'searchable' => true,
                'orderable' => true,
            ],
            'status' => [
                'title' => __('common.status'),
                'searchable' => true,
                'orderable' => true,
            ]
        ];
    }

    /**
     * Display a listing of the resource.
     * @Permission("list-events")
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (request()->ajax()) {
            return $this->makeDatatable(Event::withTranslation()->get(), $this->getModule('route'));
        }
        $html = $this->tableHtmlBuilder($this->builder, $this->getDataTableColumns());
        return view('admin.events.index', compact('html'))->with('cmsModule', $this->getModule());
    }

    /**
     * Show the form for creating a new resource.
     *@Permission("create-events")
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('admin.events.create')->with('cmsModule', $this->getModule());
    }

    /**
     * Store a newly created resource in storage.
     *@Permission("create-events")
     * @param EventRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventRequest $request)
    {

        $request['type'] = 3;
        try {
            $newEvent =  Event::create($request->all());
            $newEvent->publishOptions()->create($request['publishOptions']);
            $newEvent->eventDetails()->create($request['eventDetails']);
            if ($newEvent){
                if($request->input('image_url')){
                    $newEvent->image()->create($request->getimageData());
                }

                if($request->input('gallery')){

                    $newEvent->gallery()->create(['gallery_id'=>$request['gallery']]);
                }
                if($request['categories']){
                    $newEvent->categories()->attach($request['categories']);
                }
                if($request->input('minor')){
                    $newEvent->minor()->create(['minor_id'=>$request['minor']]);
                }

                if($request['tags']){
                    $newEvent->tags()->attach($request['tags']);
                }
                session()->flash('success', __('common.successCreate',
                    ['module'=> __($this->getModule('route') . ".singularModuleName")]));
                return redirect()->route('events.index');
            }else{
                session()->flash('error', __('common.error'));
            }

        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();
    }

    /**
     * Display the specified resource.
     *@Permission("read-events")
     * @param Event $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //

        $image = ($event->image()->exists()) ? $event->image->image_url : '' ;
        return view('admin.events.show',compact('event','image'))->with('cmsModule', $this->getModule());
    }

    /**
     * Show the form for editing the specified resource.
     *@Permission("update-events")
     * @param Event $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        //

        $image = ($event->image()->exists()) ? $event->image->image_url : '' ;

        return view('admin.events.edit',compact('event','image'))->with('cmsModule', $this->getModule());
    }

    /**
     * Update the specified resource in storage.
     *@Permission("update-events")
     * @param EventRequest $request
     * @param Event $event
     * @return \Illuminate\Http\Response
     */
    public function update(EventRequest $request, Event $event)
    {
        //dd($request->all());
        try {

            $event->update($request->all());
            $event->publishOptions()->update($request['publishOptions']);
            $event->eventDetails()->update($request['eventDetails']);
            $event->image()->delete();
            if(!empty($request['image_url'])){
                $event->image()->create($request->getimageData());
            }
            if($request->input('categories')) {
                $event->categories()->sync($request['categories']);
            }
            if($request->input('gallery')){
                ($event->gallery()->exists() ) ? $event->gallery()->update(['gallery_id'=>$request['gallery']]) : $event->gallery()->create(['gallery_id'=>$request['gallery']]);
            }
            if($request->input('tags')){
                $event->tags()->sync($request['tags']);
            }
            if($request->input('minor')){
                ($event->minor()->exists() ) ? $event->minor()->update(['minor_id'=>$request['minor']]) : $event->minor()->create(['minor_id'=>$request['minor']]);
            }
            session()->flash('success', __('common.successUpdate',
                ['module' => __($this->getModule('route') . ".singularModuleName")]));
            return redirect()->route('events.index');
        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Event $event
     * @return \Illuminate\Http\Response
     * @Permission("delete-events")
     * @throws \Exception
     */
    public function destroy(Event $event)
    {
        //
        try {
            $event->delete();
            return response()->json(['success' => __('common.successDelete',
                ['module' => __($this->getModule('route') . ".singularModuleName")])], 200);
        } catch (Exception $exception) {
            return response()->json(['error' => __('common.error')], 200);
        }
    }
}
