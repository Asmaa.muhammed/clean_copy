<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseModuleController;
use App\Http\Requests\Admin\ThemeRequest;
use App\Models\Theme;
use Exception;
use Illuminate\Support\Str;
use League\CommonMark\HtmlElement;

/**
 * Class ThemeController
 * @package App\Http\Controllers\Admin
 * @Auth
 */
class ThemeController extends BaseModuleController
{
    protected $module = 'themes';

    protected function getDataTableColumns(): array
    {
        return [
            'id' => [
                'title' => __('common.id'),
                'searchable' => false,
                'orderable' => false,
            ],
            'name' => [
                'title' => __('themes.name'),
                'searchable' => true,
                'orderable' => true,
            ],
            'path' => [
                'title' => __('themes.path'),
                'searchable' => true,
                'orderable' => true,
            ],
            'thumbnail' => [
                'title' => __('themes.thumbnail'),
                'searchable' => false,
                'orderable' => false,
            ],
            'status' => [
                'title' => __('common.status'),
                'searchable' => true,
                'orderable' => true,
            ],
        ];
    }
    /**
     * Display a listing of the resource.
     * @Permission("list-themes")
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->makeDatatable(Theme::get(), $this->getModule("route"), function ($builder){
                return $builder->editColumn('thumbnail', function ($record){
                    return (string) (new HtmlElement('img', ['src'=>$record->thumbnail,'width'=>"100px"]));
                })
                ->editColumn('path', function ($record){
                    return (string) (new HtmlElement('a', ['href'=>asset($record->path)]))
                                        ->setContents(Str::limit($record->path, 30));
                });
            }, ['thumbnail', 'path']);
        }
        $html = $this->tableHtmlBuilder($this->builder, $this->getDataTableColumns());
        return view('admin.themes.index', compact('html'))->with('cmsModule', $this->getModule());
    }

    /**
     * Show the form for creating a new resource.
     * @Permission("create-themes") @Permission("list-themes")
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.themes.create')->with('cmsModule', $this->getModule());
    }

    /**
     * Store a newly created resource in storage.
     * @Permission("create-themes") @Permission("list-themes")
     * @param ThemeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ThemeRequest $request)
    {
        try {
            $newTheme = Theme::create($request->all());
            session()->flash('success', __('common.successCreate',
                ['module'=> __($this->getModule('route') . ".singularModuleName")]));
            return redirect()->route('themes.index');
        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();
    }

    /**
     * Display the specified resource.
     * @Permission("read-themes") @Permission("list-themes")
     * @param  \App\Models\Theme  $theme
     * @return \Illuminate\Http\Response
     */
    public function show(Theme $theme)
    {
        return view('admin.themes.show', compact('theme'))->with('cmsModule', $this->getModule());
    }

    /**
     * Show the form for editing the specified resource.
     * @Permission("update-themes")
     * @param  \App\Models\Theme  $theme
     * @return \Illuminate\Http\Response
     */
    public function edit(Theme $theme)
    {
        return view('admin.themes.edit', compact('theme'))->with('cmsModule', $this->getModule());
    }

    /**
     * Update the specified resource in storage.
     * @Permission("update-themes")
     * @param ThemeRequest $request
     * @param  \App\Models\Theme $theme
     * @return \Illuminate\Http\Response
     */
    public function update(ThemeRequest $request, Theme $theme)
    {
        try {
            $theme->update($request->all());
            session()->flash('success', __('common.successUpdate',
                ['module'=> __($this->getModule('route') . ".singularModuleName")]));
            return redirect()->route('themes.index');
        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     * @Permission("delete-themes")
     * @param  \App\Models\Theme  $theme
     * @return \Illuminate\Http\Response
     */
    public function destroy(Theme $theme)
    {
        try {
            $theme->delete();
            return response()->json(['success' => __('common.successDelete',
                ['module'=> __($this->getModule('route').".singularModuleName")])], 200);
        } catch (Exception $exception) {
            return response()->json(['error' => __('common.error')], 200);
        }
    }
}
