<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseModuleController;
use App\Http\Requests\Admin\MenuLinkRequest;
use App\Models\Enum;
use App\Models\MenuGroup;
use App\Models\MenuLink;
use Exception;
use League\CommonMark\HtmlElement;

/**
 * Class MenuLinkController
 * @package App\Http\Controllers\Admin
 * @Auth
 */
class MenuLinkController extends BaseModuleController
{
    protected $module = 'menuLinks';

    protected function getDataTableColumns(): array
    {
        return [
            'id' => [
                'title' => __('common.id'),
                'searchable' => false,
                'orderable' => false,
            ],
            'name' => [
                'title' => __('menuLinks.name'),
                'searchable' => true,
                'orderable' => true,
            ],
            'target' => [
                'title' => __('menuLinks.target'),
                'searchable' => true,
                'orderable' => true,
            ],
            'icon' => [
                'title' => __('menuLinks.icon'),
                'searchable' => true,
                'orderable' => false,
            ],
            'status' => [
                'title' => __('common.status'),
                'searchable' => true,
                'orderable' => true,
            ],
        ];
    }

    /**
     * Display a listing of the resource.
     * @Permission("list-menuLinks")
     * @param MenuGroup $menuGroup
     * @return \Illuminate\Http\Response
     */
    public function index(MenuGroup $menuGroup)
    {
        if (request()->ajax()) {
            return $this->makeDatatable($menuGroup->links()->withTranslation()->get(), $this->getModule('route'),
                function ($dataTable) {
                    return $dataTable->editColumn('icon', function ($record) {
                        return (string)(new HtmlElement('i', ['class' => $record->icon]));
                    })
                    ->editColumn('target', function ($record){
                        return Enum::whereValue('menu_link_target')
                            ->first()
                            ->children
                            ->pluck('name', 'value')[$record->target];
                    });
                }, ['icon'], ['menuGroup'=>$menuGroup]);
        }
        $html = $this->tableHtmlBuilder($this->builder, $this->getDataTableColumns());
        return view('admin.menuLinks.index', compact('html', 'menuGroup'))->with(['cmsModule' => $this->getModule()]);
    }

    /**
     * @param MenuGroup $menuGroup
     * @Permission("list-menuLinks")
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tree(MenuGroup $menuGroup)
    {
        $level = 1;
        return view('admin.menuLinks.tree', compact('menuGroup', 'level'))->with(['cmsModule' => $this->getModule()]);
    }

    /**
     * Show the form for creating a new resource.
     * @Permission("create-menuLinks")
     * @param MenuGroup $menuGroup
     * @return \Illuminate\Http\Response
     */
    public function create(MenuGroup $menuGroup)
    {
        return view('admin.menuLinks.create', compact('menuGroup'))->with(['cmsModule' => $this->getModule()]);
    }

    /**
     * Store a newly created resource in storage.
     * @Permission("create-menuLinks")
     * @param MenuLinkRequest $request
     * @param MenuGroup $menuGroup
     * @return \Illuminate\Http\Response
     */
    public function store(MenuLinkRequest $request, MenuGroup $menuGroup)
    {

        //dd($request->getLinkData());
        try {
            $newMenuLink = $menuGroup->links()->create($request->all());
            $newMenuLink->link()->create($request->getLinkData());
            session()->flash('success', __('common.successCreate',
                ['module'=> __($this->getModule('route') . ".singularModuleName")]));
            return redirect()->route('menuLinks.index', $menuGroup->id);
        } catch (Exception $exception) {
            dd($exception);
            session()->flash('error', __('common.error'));
        }
        return back();
    }

    /**
     * Display the specified resource.
     * @Permission("read-menuLinks")
     * @param MenuGroup $menuGroup
     * @param  \App\Models\MenuLink $menuLink
     * @return \Illuminate\Http\Response
     */
    public function show(MenuGroup $menuGroup,MenuLink $menuLink)
    {
        return view('admin.menuLinks.show', compact('menuGroup','menuLink'))->with(['cmsModule' => $this->getModule()]);
    }

    /**
     * Show the form for editing the specified resource.
     * @Permission("update-menuLinks")
     * @param MenuGroup $menuGroup
     * @param MenuLink $menuLink
     * @return \Illuminate\Http\Response
     */
    public function edit(MenuGroup $menuGroup,MenuLink $menuLink)
    {
        return view('admin.menuLinks.edit', compact('menuGroup','menuLink'))->with(['cmsModule' => $this->getModule()]);
    }

    /**
     * Update the specified resource in storage.
     * @Permission("update-menuLinks")
     * @param MenuLinkRequest $request
     * @param MenuGroup $menuGroup
     * @param  \App\Models\MenuLink $menuLink
     * @return \Illuminate\Http\Response
     */
    public function update(MenuLinkRequest $request,MenuGroup $menuGroup, MenuLink $menuLink)
    {

        try {
            $menuLink->update($request->all());
            $menuLink->link->update($request->getLinkData());
            session()->flash('success', __('common.successUpdate',
                ['module'=> __($this->getModule('route') . ".singularModuleName")]));
            return redirect()->route('menuLinks.index', $menuGroup->id);
        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     * @Permission("delete-menuLinks")
     * @param MenuGroup $menuGroup
     * @param  \App\Models\MenuLink $menuLink
     * @return \Illuminate\Http\Response
     */
    public function destroy(MenuGroup $menuGroup, MenuLink $menuLink)
    {
        try {
            $menuLink->delete();
            return response()->json(['success' => __('common.successDelete',
                ['module'=> __($this->getModule('route') . ".singularModuleName")])], 200);
        } catch (Exception $exception) {
            return response()->json(['error' => __('common.error')], 200);
        }
    }
}
