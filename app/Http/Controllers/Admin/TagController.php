<?php

namespace App\Http\Controllers\Admin;

use App\Foundation\Traits\TableIndex;
use App\Http\Controllers\Controller;
use App\Http\Middleware\Admin\SetCurrentLocaleLang;
use App\Http\Requests\Admin\TagRequest;
use App\Models\Tag;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;
/**
 * Class TagController
 * @package App\Http\Controllers
 * @Auth
 */

class TagController extends Controller
{
    use TableIndex;
    /**
     * Datatables Html Builder
     * @var Builder
     */
    protected $htmlBuilder;
    protected $module;
    public function __construct(Builder $htmlBuilder)
    {
//        Parent::__construct();
        $this->htmlBuilder = $htmlBuilder;
        $this->module = "tags";
        $this->middleware(SetCurrentLocaleLang::class);

    }
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if ($request->ajax()) {
            return $this->makeDatatable(Tag::withTranslation()->get(),$this->module);
        }
        $columns = ['id'=>['title'=>'ID','searchable'=>false,'orderable'=>false],'name'=>['title'=>'Name','searchable'=>true,'orderable'=>true],'status'=>['title'=>'Status','searchable'=>true,'orderable'=>true]];

        $html = $this->tableHtmlBuilder($this->htmlBuilder,$columns);
        return view('admin.tags.index', compact('html'));

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  TagRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TagRequest $request)
    {
        //
        $request['type'] =2;
        $tag = Tag::create($request->all());
        if ($tag){
            session()->flash('success', 'Tag Created Successfully');
        }
        else{
            session()->flash('errors', ['something went wrong']);
        }
        return redirect()->route('tags.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  Tag $tag
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)
    {
        //
        return view('admin.tags.show',compact('tag'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Tag $tag
     * @Permission("list-tags")
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        //
        return view('admin.tags.edit',compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  TagRequest $request
     * @param  Tag $tag
     * @return \Illuminate\Http\Response
     */
    public function update(TagRequest $request,Tag $tag)
    {
        if($tag->update($request->all())){
            session()->flash('success', 'tag Updated Successfully');
        }
        else{
            session()->flash('errors', ['Something Went Wrong']);
        }
        return redirect()->route('tags.index');//



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Tag $tag
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Tag $tag)
    {
        //
        if($tag->delete()){
            return response()->json(['success'=>'Tag deleted successfully.'], 200);
        }
        else{
            return response()->json(['error'=>'Something went wrong.'], 200);
        }
    }
}
