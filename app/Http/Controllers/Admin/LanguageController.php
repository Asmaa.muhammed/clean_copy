<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Middleware\Admin\SetCurrentLocaleLang;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;
use JoeDixon\Translation\Drivers\Translation;
use JoeDixon\Translation\Http\Requests\LanguageRequest;
use JoeDixon\Translation\Language;
use phpDocumentor\Reflection\Types\Parent_;

/**
 * Class LanguageController
 * @package App\Http\Controllers
 * @Auth
 */
class LanguageController extends Controller
{


    private $translation;

    public function __construct(Translation $translation)
    {
//        parent::__construct();
        $this->translation = $translation;
        $this->middleware(SetCurrentLocaleLang::class);
    }

    /**
     * Display a listing of the resource.
     * @Permission("list-languages")
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $langueges = \Illuminate\Support\Facades\File::directories(resource_path("lang"));

        $languages = Language::all();

        return view('translation::languages.index', compact('languages'));
    }

    /**
     * Show the form for creating a new resource.
     * @Permission("create-languages")
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('translation::languages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param LanguageRequest $request
     * @Permission("create-languages")
     * @return \Illuminate\Http\Response
     */

    public function store(LanguageRequest $request)
    {
        $request["language"] = $request["locale"];
        $lang = Language::create($request->except("locale"));
        if ($lang) {
            $this->translation->addLanguage($request['locale'], $request['name']);
        }
        return redirect()
            ->route('languages.index')
            ->with('success', __('translation::translation.language_added'));
    }

    /**
     * Display the specified resource.
     *
     * @param Language $language
     * @return \Illuminate\Http\Response
     */
    public function show(Language $language)
    {
        //
        return view('translation::languages.show',compact('language'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Language $language
     * @return \Illuminate\Http\Response
     */
    public function edit( Language $language)
    {
        //
        return view('translation::languages.edit',compact('language'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Language $language
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Language $language )
    {
        //
        if($language->update($request->all())){
            session()->flash('success', 'Language Updated Successfully');
        }
        else{
            session()->flash('errors', ['Something Went Wrong']);
        }
        return redirect()->route('languages.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Language $language
     * @return \Illuminate\Http\Response
     * @Permission("delete-enums")
     * @throws \Exception
     */
    public function destroy(Language $language)
    {
        //
        $languagePath = resource_path("lang".DIRECTORY_SEPARATOR . $language->language);
        $languagePathJsonFile =  resource_path("lang".DIRECTORY_SEPARATOR . $language->language.".json");
        if ($language->delete()) {
            $delete = File::deleteDirectory($languagePath);
            $deleteJsonFile =  File::delete($languagePathJsonFile);
            if ($delete && $deleteJsonFile) {
                return response()->json(['success' => 'Enum deleted successfully.'], 200);
            } else {
                return response()->json(['error' => 'Something went wrong.'], 200);
            }

        } else {
            return response()->json(['error' => 'Something went wrong.'], 200);
        }
    }
}
