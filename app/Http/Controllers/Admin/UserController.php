<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseModuleController;
use App\Http\Requests\Admin\ChangePasswordRequest;
use App\Http\Requests\Admin\UserRequest;
use App\Models\User;
use Exception;

/**
 * @Auth
 */
class UserController extends BaseModuleController
{
    protected $module = 'users';

    protected function getDataTableColumns(): array
    {
        return [
            'id' => [
                'title' => __('common.id'),
                'searchable' => false,
                'orderable' => false,
            ],
            'full_name' => [
                'title' => __('permissions.display_name'),
                'searchable' => true,
                'orderable' => true,
            ],
            'email' => [
                'title' => __('users.email'),
                'searchable' => true,
                'orderable' => true,
            ],
            'phone' => [
                'title' => __('users.phone'),
                'searchable' => true,
                'orderable' => true,
            ],
            'status' => [
                'title' => __('common.status'),
                'searchable' => true,
                'orderable' => true,
            ],
        ];
    }

    /**
     * Display a listing of the resource.
     * @Permission("list-users")
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->makeDatatable(User::where('id', '>=', auth()->user()->id)
                ->get(), $this->getModule("route"), function ($builder) {
                return $builder->editColumn('full_name', function ($record) {
                    return $record->full_name;
                });
            }, [], [], 'admin.users.custom_action_buttons');
        }
        $html = $this->tableHtmlBuilder($this->builder, $this->getDataTableColumns());
        return view('admin.users.index', compact('html'))->with('cmsModule', $this->getModule());
    }

    /**
     * Show the form for creating a new resource.
     * @Permission("create-users")
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.users.create")->with('cmsModule', $this->getModule());
    }

    /**
     * Store a newly created resource in storage.
     * @Permission("create-users")
     * @param UserRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        try {
            $newUser = User::create($request->all());
            $newUser->syncRoles($request->roles);
            session()->flash('success',
                __('common.successCreate',
                    ['module'=> __($this->getModule('route') . ".singularModuleName")]));
            return redirect()->route('users.index');
        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();
    }

    /**
     * Display the specified resource.
     * @HasHigherRole
     * @Permission("read-users")
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view("admin.users.show", compact('user'))->with('cmsModule', $this->getModule());
    }

    /**
     * Show the form for editing the specified resource.
     * @HasHigherRole
     * @Permission("update-users")
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view("admin.users.edit", compact('user'))->with('cmsModule', $this->getModule());
    }

    /**
     * Update the specified resource in storage.
     * @HasHigherRole
     * @Permission("update-users")
     * @param UserRequest $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserRequest $request, User $user)
    {
        try {
            $user->update($request->getData());
            $user->syncRoles($request->roles);
            session()->flash('success',
                __('common.successUpdate',
                    ['module'=> __($this->getModule('route') . ".singularModuleName")]));
            return redirect()->route('users.index');
        } catch (Exception $exception) {
            dd($exception);
            session()->flash('error', __('common.error'));
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     * @HasHigherRole
     * @Permission("delete-users")
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        try {
            $user->delete();
            return response()->json(['success' => __('common.successDelete',
                ['module'=> __($this->getModule('route') . ".singularModuleName")])], 200);
        } catch (Exception $exception) {
            return response()->json(['error' => __('common.error')], 200);
        }
    }


    /**
     * @param User $user
     * @HasHigherRole
     * @Role("superadministrator|administrator")
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function changePassword(User $user)
    {
        return view('admin.users.change_password', compact('user'))->with('cmsModule', $this->getModule());
    }


    /**
     * @param ChangePasswordRequest $request
     * @HasHigherRole
     * @Role("superadministrator|administrator")
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function submitChangePassword(ChangePasswordRequest $request, User $user)
    {
        try{
            $user->update(['password' => $request->input('password')]);
            session()->flash('success', __('common.successUpdate',
                ['module'=> __($this->getModule('route') . ".singularModuleName")]));
            return redirect()->route('users.index');
        }
        catch (Exception $exception){
            dd($exception);
            session()->flash('error', __('common.error'));
        }
        return back();
    }
}
