<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseModuleController;
use App\Models\NewsletterSubscriber;

class NewsletterSubscriberController extends BaseModuleController
{
    protected $module = 'newsletterSubscribers';

    protected function getDataTableColumns(): array
    {
        return [
            'id' => [
                'title' => __('common.id'),
                'searchable' => false,
                'orderable' => false,
            ],
            'name' => [
                'title' => __('newsletterSubscribers.name'),
                'searchable' => true,
                'orderable' => true,
            ],
            'email' => [
                'title' => __('newsletterSubscribers.email'),
                'searchable' => true,
                'orderable' => true,
            ],
        ];
    }

    /**
     * Display a listing of the resource.
     * @Permission("list-newsletterSubscribers")
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->makeDatatable(NewsletterSubscriber::get(), $this->getModule("route")
                , function ($builder){
                    return $builder->editColumn('name', function ($record){
                        return $record->name ?? __('newsletterSubscribers.unknown');
                    });
                });
        }
        $html = $this->tableHtmlBuilder($this->builder, $this->getDataTableColumns(), false);
        return view('admin.'.$this->getModule('route').'.index', compact('html'))
            ->with('cmsModule', $this->getModule());
    }
}
