<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseModuleController;

use App\Http\Requests\Admin\GalleryRequest;
use App\Models\Enum;
use App\Models\Gallery;
use Illuminate\Http\Request;
use League\CommonMark\HtmlElement;

/**
 * Class GalleryController
 * @package App\Http\Controllers\Admin
 * @Auth
 */
class GalleryController extends BaseModuleController
{
    protected $module = 'galleries';

    protected function getDataTableColumns(): array
    {
        return [
            'title' => [
                'title' => __('galleries.title'),
                'searchable' => true,
                'orderable' => true,
            ],
            'short_code' => [
                'title' => __('galleries.short_code'),
                'searchable' => true,
                'orderable' => true,
            ],
            'status' => [
                'title' => __('common.status'),
                'searchable' => true,
                'orderable' => true,
            ],
            'images' => [
                'title' => __('galleries.images'),
                'searchable' => false,
                'orderable' => false,
            ],

        ];
    }

    /**
     * Display a listing of the resource.
     * @Permission("list-galleries")
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (request()->ajax()) {
            return $this->makeDatatable(Gallery::all(), $this->getModule('route'),
                function ($dataTable) {
                    return $dataTable->addColumn('images', function ($record)  {
                        return view('admin.galleries.image_actions', [ 'gallery' => $record]);

                    });

                }, ['images']);
        }

        $html = $this->tableHtmlBuilder($this->builder, $this->getDataTableColumns());
        return view('admin.galleries.index', compact('html'))->with('cmsModule', $this->getModule());

    }

    /**
     * Show the form for creating a new resource.
     * @Permission("create-galleries")
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.galleries.create')->with('cmsModule', $this->getModule());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param GalleryRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(GalleryRequest $request)
    {
        //
        try {
            $gallery = Gallery::create($request->all());
            session()->flash('success', __('common.successCreate',
                ['module'=> __($this->getModule('route') . ".singularModuleName")]));
            return redirect()->route('galleryImages.create', $gallery->id);
        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();

    }

    /**
     * Display the specified resource.
     * @Permission("read-galleries")
     * @param Gallery $gallery
     * @return \Illuminate\Http\Response
     */
    public function show(Gallery $gallery)
    {
        //
        return view('admin.galleries.show', compact('gallery'))->with('cmsModule', $this->getModule());

    }

    /**
     * Show the form for editing the specified resource.
     * @Permission("update-galleries")
     * @param Gallery $gallery
     * @return \Illuminate\Http\Response
     */
    public function edit(Gallery $gallery)
    {
        //
        return view('admin.galleries.edit', compact('gallery'))->with('cmsModule', $this->getModule());

    }

    /**
     * Update the specified resource in storage.
     * @Permission("update-galleries")
     * @param GalleryRequest $request
     * @param Gallery $gallery
     * @return \Illuminate\Http\Response
     */
    public function update(GalleryRequest $request, Gallery $gallery)
    {
        //
        try {
            $gallery->update($request->all());
            session()->flash('success', __('common.successUpdate',
                ['module' => __($this->getModule('route') . ".singularModuleName")]));
            return redirect()->route('galleries.index');
        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Gallery $gallery
     * @return \Illuminate\Http\Response
     * @Permission("delete-galleries")
     * @throws \Exception
     */
    public function destroy(Gallery $gallery)
    {
        //
        try {
            $gallery->delete();
            return response()->json(['success' => __('common.successDelete',
                ['module' => __($this->getModule('route') . ".singularModuleName")])], 200);
        } catch (Exception $exception) {
            return response()->json(['error' => __('common.error')], 200);
        }
    }
}
