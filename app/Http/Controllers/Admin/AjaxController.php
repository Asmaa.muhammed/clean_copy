<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;

/**
 * Class AjaxController
 * @package App\Http\Controllers\Admin
 */
class AjaxController extends Controller
{
    public function getTargetTypeData(Request $request)
    {
        $request->validate([
            'model' => 'required'
        ]);
        $model = Relation::getMorphedModel($request->input('model'));
        return response()->json([
            'data' => $model::all()->map(function ($model) {
                if (!empty($model->name)){
                    return ["name" => $model->name, "id" => $model->id];
                }else{
                    return ["name" => $model->title, "id" => $model->id];
                }


            })
        ]);
    }
}
