<?php

namespace App\Http\Controllers\Admin;

use App\Foundation\Traits\TableIndex;
use App\Http\Controllers\Controller;
use App\Http\Middleware\Admin\SetCurrentLocaleLang;
use App\Http\Requests\Admin\EnumRequest;
use App\Models\Enum;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;


/**
 * Class EnumController
 * @package App\Http\Controllers
 * @Auth
 */

class EnumController extends Controller
{

    use TableIndex;
    /**
     * Datatables Html Builder
     * @var Builder
     */
    protected $htmlBuilder;
    protected $module;


    public function __construct(Builder $htmlBuilder)
    {
//        parent::__construct();
        $this->htmlBuilder = $htmlBuilder;
        $this->module = "enums";
        $this->middleware(SetCurrentLocaleLang::class);
    }


    /**
     * Display a listing of the resource.
     * @param Request $request
     * @Permission("list-enums")
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //

        if ($request->ajax()) {
            return $this->makeDatatable(Enum::withTranslation()->get(),$this->module);
        }
        $columns = ['id'=>['title'=>'ID','searchable'=>false,'orderable'=>false],'name'=>['title'=>'Name','searchable'=>true,'orderable'=>true],'status'=>['title'=>'Status','searchable'=>true,'orderable'=>true]];
        $html = $this->tableHtmlBuilder($this->htmlBuilder,$columns);
        return view('admin.enums.index', compact('html'));
    }
    /**
     * Display tree view of all resource
     *  @Permission("list-enums")
     *@return \Illuminate\Http\Response
     */
    public function indexTree(){
        $enums = Enum::whereParent_id(0)->get();
        return view('admin.enums.tree',compact('enums'));
    }


    /**
     * Show the form for creating a new resource.
     * @Permission("create-enums")
     *@return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       return view('admin.enums.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @Permission("create-enums")
     * @return \Illuminate\Http\Response
     */
    public function store(EnumRequest $request)
    {
        //
        $enum = Enum::create($request->all());
        if ($enum){
            session()->flash('success', 'Enumeration Created Successfully');
        }
        else{
            session()->flash('errors', ['something went wrong']);
        }
        return redirect()->route('enums.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  Enum $enum
     *  @Permission("read-enums")
     *  @return \Illuminate\Http\RedirectResponse
     */
    public function show(Enum $enum)
    {
        //
        return view('admin.enums.show',compact('enum'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Enum  $enum
     *  @Permission("update-enums")
     * @return \Illuminate\Http\Response
     */
    public function edit(Enum $enum)
    {

        return view('admin.enums.edit',compact('enum'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  EnumRequest  $request
     * @param  Enum $enum
     * @Permission("update-enums")
     * @return \Illuminate\Http\Response
     */
    public function update(EnumRequest $request, Enum $enum)
    {
        //
        if($enum->update($request->all())){
            session()->flash('success', 'Enum Updated Successfully');
        }
        else{
            session()->flash('errors', ['Something Went Wrong']);
        }
        return redirect()->route('enums.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Enum $enum
     * @return \Illuminate\Http\Response
     * @Permission("delete-enums")
     * @throws \Exception
     */
    public function destroy(Enum $enum)
    {
        //
        if($enum->delete()){
            return response()->json(['success'=>'Enum deleted successfully.'], 200);
        }
        else{
            return response()->json(['error'=>'Something went wrong.'], 200);
        }
    }
}
