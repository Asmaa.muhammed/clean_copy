<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseModuleController;
use App\Models\Minor;

class MinorController extends BaseModuleController
{
    protected $module = 'minors';

    protected function getDataTableColumns(): array
    {
        return [
            'title' => [
                'title' => __('galleries.title'),
                'searchable' => true,
                'orderable' => true,
            ],
            'status' => [
                'title' => __('common.status'),
                'searchable' => true,
                'orderable' => true,
            ],


        ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $record)
    {
        //
   
        if (request()->ajax()) {
            return $this->makeDatatable(Minor::all(), $this->getModule('route'));
        }
        $html = $this->tableHtmlBuilder($this->builder, $this->getDataTableColumns());
        return view('admin.minors.index', compact('html'))->with('cmsModule', $this->getModule());

    }

    /**
     * Show the form for creating a new resource.
     * @Permission("create-galleries")
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.minors.create')->with('cmsModule', $this->getModule());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param GalleryRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Minor::create($request->all());
        session()->flash('success', __('common.successCreate',
            ['module'=> __($this->getModule('route') . ".singularModuleName")]));
        return redirect()->route('minors.index');

    }

    /**
     * Display the specified resource.
     * @Permission("read-galleries")
     * @param Gallery $gallery
     * @return \Illuminate\Http\Response
     */
    public function show(Minor $minor)
    {
        //
        return view('admin.minors.show', compact('minor'))->with('cmsModule', $this->getModule());

    }

    /**
     * Show the form for editing the specified resource.
     * @Permission("update-galleries")
     * @param Gallery $gallery
     * @return \Illuminate\Http\Response
     */
    public function edit(Minor $minor)
    {
        //
        return view('admin.minors.edit', compact('minor'))->with('cmsModule', $this->getModule());

    }

    /**
     * Update the specified resource in storage.
     * @Permission("update-galleries")
     * @param GalleryRequest $request
     * @param Gallery $gallery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Minor $minor)
    {
        $minor->update($request->all());
        session()->flash('success', __('common.successUpdate',
            ['module' => __($this->getModule('route') . ".singularModuleName")]));
        return redirect()->route('minors.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Gallery $gallery
     * @return \Illuminate\Http\Response
     * @Permission("delete-galleries")
     * @throws \Exception
     */
    public function destroy(Minor $minor)
    {
        $minor->delete();
            return response()->json(['success' => __('common.successDelete',
        ['module' => __($this->getModule('route') . ".singularModuleName")])], 200);
        return redirect()->route('minors.index');
    }
}
