<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseModuleController;
use App\Http\Requests\Admin\ModuleRequest;
use App\Models\Module;
use Exception;
use League\CommonMark\HtmlElement;
/**
 * Class ModuleController
 * @package App\Http\Controllers
 * @Auth
 */
class ModuleController extends BaseModuleController
{

    protected $module = 'modules';

    protected function getDataTableColumns(): array
    {
        return [
            'id' => [
                'title' => __('common.id'),
                'searchable' => false,
                'orderable' => false,
            ],
            'name' => [
                'title' => __('modules.name'),
                'searchable' => true,
                'orderable' => true,
            ],
            'icon' => [
                'title' => __('modules.icon'),
                'searchable' => false,
                'orderable' => false,
            ],
            'status' => [
                'title' => __('common.status'),
                'searchable' => true,
                'orderable' => true,
            ]
        ];
    }


    /**
     * Display a listing of the resource.
     * @Permission("list-modules")
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->makeDatatable(Module::withTranslation()->get(), $this->getModule("route"),
                function ($dataTable) {
                    return $dataTable->editColumn('icon', function ($record) {
                        return (string)(new HtmlElement('icon', ['class' => $record->icon]));
                    });
                }, ['icon']);
        }
        $html = $this->tableHtmlBuilder($this->builder, $this->getDataTableColumns());
        return view('admin.modules.index', compact('html'))->with('cmsModule', $this->getModule());
    }

    /**
     * Show the form for creating a new resource.
     * @Permission("create-modules")
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modules.create')->with('cmsModule', $this->getModule());
    }

    /**
     * Store a newly created resource in storage.
     * @Permission("create-modules")
     * @param ModuleRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ModuleRequest $request)
    {
        try {
            $newModule = Module::create($request->all());
            session()->flash('success', __('common.successCreate',
                ['module'=> __($this->getModule('route').".singularModuleName")]));
            return redirect()->route('modules.index');
        } catch (Exception $exception) {
            dd($exception);
            session()->flash('error', __('common.error'));
        }
        return back();
    }

    /**
     * Display the specified resource.
     * @Permission("read-modules")
     * @param  \App\Models\Module $module
     * @return \Illuminate\Http\Response
     */
    public function show(Module $module)
    {
        return view('admin.modules.show', compact('module'))->with('cmsModule', $this->getModule());
    }

    /**
     * Show the form for editing the specified resource.
     * @Permission("update-modules")
     * @param  \App\Models\Module $module
     * @return \Illuminate\Http\Response
     */
    public function edit(Module $module)
    {
        return view('admin.modules.edit', compact('module'))->with('cmsModule', $this->getModule());
    }

    /**
     * Update the specified resource in storage.
     * @Permission("update-modules")
     * @param ModuleRequest $request
     * @param  \App\Models\Module $module
     * @return \Illuminate\Http\Response
     */
    public function update(ModuleRequest $request, Module $module)
    {

        try {
            $module->update($request->all());
            session()->flash('success', __('common.successUpdate',
                ['module'=> __($this->getModule('route').".singularModuleName")]));
            return redirect()->route('modules.index');
        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     * @Permission("delete-modules")
     * @param  \App\Models\Module $module
     * @return \Illuminate\Http\Response
     */
    public function destroy(Module $module)
    {
        try {
            $module->delete();
            return response()->json(['success' => __('common.successDelete',
                ['module'=> __($this->getModule('route').".singularModuleName")])], 200);
        } catch (Exception $exception) {
            return response()->json(['error' => __('common.error')], 200);
        }
    }
}
