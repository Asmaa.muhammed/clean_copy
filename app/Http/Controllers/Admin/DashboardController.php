<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseModuleController;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Gallery;
use App\Models\MenuGroup;
use App\Models\Page;
use App\Models\Post;
use App\Models\Slider;
use App\Models\User;
use Illuminate\Support\Facades\Session;
use JoeDixon\Translation\Language;
use Spatie\Activitylog\Models\Activity;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Admin
 * @Auth
 */
class DashboardController extends BaseModuleController
{
    protected  $module = "Dashboard";
    protected function getDataTableColumns(): array
    {

        return [


            'causer' => [
                'title' => __('common.causer'),
                'searchable' => true,
                'orderable' => true,
            ],
            'subject' => [
                'title' => __('common.subject'),
                'searchable' => true,
                'orderable' => true,
            ],
            'description' => [
                'title' => __('common.description'),
                'searchable' => true,
                'orderable' => true,
            ],
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (request()->ajax()) {
            return $this->makeDatatable(Activity::orderBy('id','DESC')->get()->take(20), $this->getModule('route'),
                function ($dataTable) {
                    return $dataTable->editColumn('subject', function ($record) {
                      if($record->subject_type == "users"){
                          return $record->subject_type ." - ".  $record->subject->first_name." ". $record->subject->last_name;
                      }else if(in_array( $record->subject_type,['posts','pages','events','sliders','galleries'])) {
                          return $record->subject_type . "  -  " . $record->subject->title;
                      }else if($record->subject_type == "roles"){
                          return $record->subject_type ." - ".  $record->subject->dispay_name;
                      }else if (isset($record->name)){
                          return $record->subject_type ." - ". $record->subject->name;
                      }else{
                          return '';
                      }

                    })
                    ->editColumn('causer', function ($record) {

                            return ($record->causer) ?$record->causer->first_name." ".$record->causer->last_name : 'System';
                        });

                }, [],[],'',false);
        }
        $html = $this->tableHtmlBuilder($this->builder, $this->getDataTableColumns(),false);
        $users = User::all()->count();
        $menus = MenuGroup::all()->count();
        $galleries = Gallery::all()->count();
        $sliders = Slider::all()->count();
        $pages = Page::all()->count();
        $posts = Post::all()->count();
        $categories = Category::all()->count();
        return  view("admin.dashboard.index",compact('html','users','menus','galleries','sliders','categories','pages','posts'));
    }
    public function changeLanguage(Language $language){
        Session::put('lang',$language->language);
        return redirect()->back();
    }




}
