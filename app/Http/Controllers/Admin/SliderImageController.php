<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseModuleController;
use App\Models\Image;
use App\Models\Slider;
use App\Http\Requests\Admin\SliderImageRequest;

/**
 * Class SliderImageController
 * @package App\Http\Controllers\Admin
 * @Auth
 */
class SliderImageController extends BaseModuleController
{
     protected $module = 'sliderImages';

    protected function getDataTableColumns(): array
    {
        return [
            'image' => [
                'title' => __('sliderImages.image_preview'),
                'searchable' => false,
                'orderable' => false,
            ],
            'name' => [
                'title' => __('sliderImages.image_name'),
                'searchable' => true,
                'orderable' => true,
            ],
            'sorting' => [
                'title' => __('sliderImages.sorting'),
                'searchable' => true,
                'orderable' => true,
            ],

        ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Display a listing of the resource.
     * @param  Slider $slider
     * @Permission("list-sliderImages")
     * @return \Illuminate\Http\Response
     */
    public function index(Slider $slider)
    {
        //
        if (request()->ajax()) {
            return $this->makeDatatable($slider->images, $this->getModule('route'),
                function ($dataTable) {
                    return $dataTable->editColumn('image', function ($record) {
                        return view('admin.layout.includes.image_preview', ['record' => $record]);

                    });

                }, ['icon'], ['slider'=>$slider]);

        }

        $html = $this->tableHtmlBuilder($this->builder, $this->getDataTableColumns());
        return view('admin.slider_images.index', compact('html','slider'))->with('cmsModule', $this->getModule());

    }

    /**
     * Show the form for creating a new resource.
     * @param  Slider $slider
     * @Permission("create-sliderImages")
     * @return \Illuminate\Http\Response
     */
    public function create(Slider $slider)
    {
        //
        return view('admin.slider_images.create',compact('slider'))->with('cmsModule', $this->getModule());
    }

    /**
     * Store a newly created resource in storage.
     * @param  Slider $slider
     * @Permission("create-sliderImages")
     * @param  SliderImageRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(SliderImageRequest $request,Slider $slider)
    {

        //
        try {
         $sliderImage =  $slider->images()->create($request->all());
            $sliderImage->link()->create($request->getLinkData());

         session()->flash('success', __('common.successCreate',
                ['module'=> __($this->getModule('route') . ".singularModuleName")]));
            return redirect()->route('sliderImages.index', $slider->id);
        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();
    }

    /**
     * Display the specified resource.
     * @Permission("read-sliderImages")
     * @param  Slider $slider
     * @param Image $sliderImage
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider ,Image $sliderImage)
    {
        //
        return view('admin.slider_images.show', compact('slider','sliderImage'))->with('cmsModule', $this->getModule());

    }

    /**
     * Show the form for editing the specified resource.
     * @Permission("update-sliderImages")
     * @param  Slider $slider
     * @param Image $sliderImage
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider ,Image $sliderImage)
    {
        return view('admin.slider_images.edit', compact('slider','sliderImage'))->with('cmsModule', $this->getModule());

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  SliderImageRequest  $request
     * @Permission("update-sliderImages")
     * @param  Slider $slider
     * @param Image $sliderImage
     * @return \Illuminate\Http\Response
     */
    public function update(SliderImageRequest $request, Slider $slider, Image $sliderImage)
    {
        //
    
        try {
            $sliderImage->update($request->all());
            $sliderImage->link()->update($request->getLinkData());
            session()->flash('success', __('common.successUpdate',
                ['module'=> __($this->getModule('route') . ".singularModuleName")]));
            return redirect()->route('sliderImages.index', $slider->id);
        } catch (Exception $exception) {
            session()->flash('error', __('common.error'));
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Slider $slider
     * @param  Image $sliderImage;
     * @return \Illuminate\Http\Response
     * @Permission("delete-sliderImages")
     * @throws \Exception
     */
    public function destroy(Image $sliderImage,Slider $slider)
    {
        //
        try {
            $sliderImage->delete();
            return response()->json(['success' => __('common.successDelete',
                ['module' => __($this->getModule('route') . ".singularModuleName")])], 200);
        } catch (Exception $exception) {
            return response()->json(['error' => __('common.error')], 200);
        }
    }
}
