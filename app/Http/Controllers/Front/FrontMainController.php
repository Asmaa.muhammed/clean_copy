<?php

namespace App\Http\Controllers\Front;


use App\Http\Controllers\Controller;
use App\Http\Middleware\Front\setCurrentLocale;
use App\Models\NewsletterSubscriber;
use App\Models\Node;
use App\Models\Page;
use App\Models\Post;
use App\Models\Category;
use App\Models\Event;
use App\Models\Person;
use App\Models\Theme;
use App\Models\Slider;
use App\Models\Gallery;
use Illuminate\Http\Request;

class FrontMainController extends Controller
{
    protected $mainThemePath;

    public function __construct()
    {
        $this->middleware(setCurrentLocale::class);
        $mainTheme = Theme::whereStatus(1)->get()->first();
        $this->mainThemePath = $mainTheme->path;

    }
    public function index()
    {
    
        $homePage = Page::find(1);
        $layout = $homePage->layoutModel;

        
        return view($this->mainThemePath . '.index', compact('layout', 'homePage'));
    }

    public function newsletterSubmit(Request $request)
    {

        if (empty($request['email'])) {
            session()->flash('success',__("front.email_validation"));
        } else if (NewsletterSubscriber::where('email', '=', $request['email'])->exists()) {
            session()->flash('success',__("front.email_exist"));
        } else {
            if (NewsletterSubscriber::create($request->all())) {
                session()->flash('success',__("front.subscription_successfully"));
            } else {
                session()->flash('success',__("Error"));
            }
        }
        return redirect()->route('index');
    }

    public function PageDetails(Page $page)
    {
        return view($this->mainThemePath . '.page', compact('page'));
    }
    public function PostDetails(Post $post)
    {
        return view($this->mainThemePath . '.post', compact('post'));
    }
    public function EventDetails(Event $event)
    {
        return view($this->mainThemePath . '.event', compact('event'));
    }
    public function NewsDetails(Post $post)
    {
        return view($this->mainThemePath . '.news', compact('post'));
    }
   
    public function newsAndEvents()
    {
        $relatedCategory = Category::where('id', 1)->first();

        $news = Post::whereHas('categories', function ($q) use ($relatedCategory) {
                        $q->where('taxonomies.id', $relatedCategory->id);
                    })
                        ->frontPublish()
                        ->get();

        $events = Event::get();
                        
        return view($this->mainThemePath . '.news_and_events', compact( 'news', 'events'));
    }

    public function speakers()
    {

        $speakers = Person::get();
                        
        return view($this->mainThemePath . '.speakers', compact( 'speakers'));
    }

    public function showSpeaker(Person $person)
    {
        return view($this->mainThemePath . '.speaker', compact('person'));
    }
    
    public function contactUs()
    {
        $page = Page::find(6);

        return view($this->mainThemePath . '.contact_us',compact('page'));
    }
    
  public function search(Request $request){

        $posts= Post::whereStatus(1)->whereTranslationLike('title',"%{$request->search}%")->get();

        $pages=Page::whereStatus(1)->whereTranslationLike('title',"%{$request->search}%")->get();

        $events=Event::whereStatus(1)->whereTranslationLike('title',"%{$request->search}%")->get();

        return view($this->mainThemePath.'.main_search',compact("posts","pages","events"));
    }
    public function gallery()
    {
        $gallery = Gallery::whereShortCode('main-gallery')->get()->first();

        return view($this->mainThemePath . '.gallery',compact('gallery'));
    }
   
}
