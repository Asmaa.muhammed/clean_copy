<?php

namespace App\Http\Controllers;

use App\Foundation\Traits\TableIndex;
use App\Http\Middleware\Admin\SetCurrentLocaleLang;
use App\Models\Module;
use Yajra\DataTables\Html\Builder;

abstract class BaseModuleController extends Controller
{
    use TableIndex;

    protected $module;
    protected $builder;

    public function __construct(Builder $builder)
    {
        $this->builder = $builder;
        $this->module = $this->module ? (Module::whereRoute($this->module)->first() ?? $this->module) : null;
        $this->middleware(SetCurrentLocaleLang::class);
    }

    public function getModule($property = null)
    {
        return $this->module instanceof Module && $property ? $this->module->$property : $this->module;
    }



    abstract protected function getDataTableColumns() : array;

}
