<?php

namespace App\Mail;

use App\Models\AdvertisementOffer;
use App\Models\Theme;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class GetOffer extends Mailable
{
    use Queueable, SerializesModels;
    protected $advertisementOffer;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(AdvertisementOffer  $advertisementOffer)
    {
        //
        $this->advertisementOffer = $advertisementOffer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view(Theme::whereStatus(1)->first()->path.'.emails.get_offer')->with(
            'advertisementOffer',$this->advertisementOffer
        );
    }
}
