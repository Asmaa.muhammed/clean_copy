<?php

namespace App\Mail;

use App\Models\Customer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResendQrCode extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public  $customer;
    public $themePath;
    public function __construct(Customer $customer,$themePath)
    {
        //
        $this->customer = $customer;
        $this->themePath = $themePath;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view($this->themePath.'.emails.resend_qr_code')->with(['customer'=>$this->customer]);
    }
}
