<?php

namespace App\Mail;

use App\Models\Theme;
use Givebutter\LaravelKeyable\Models\ApiKey;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RateAndCommentRedeemedOffer extends Mailable
{
    use Queueable, SerializesModels;

    public $redeemApiKey;

    /**
     * Create a new message instance.
     *
     * @param ApiKey $redeemApiKey
     */
    public function __construct(ApiKey $redeemApiKey)
    {
        $this->redeemApiKey = $redeemApiKey;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view(Theme::whereStatus(1)->first()->path.'.emails.rate_and_comment_offer');
    }
}
