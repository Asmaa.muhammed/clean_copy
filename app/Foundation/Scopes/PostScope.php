<?php


namespace App\Foundation\Scopes;

use App\Models\Enum;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;


class PostScope implements  Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $type = Enum::whereValue('nodetypes')->first()->children->pluck('value','name');
        return $builder->whereType($type['post']);
    }



}
