<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 4/15/2020
 * Time: 1:54 AM
 */

namespace App\Foundation\Factories;


use App\Http\Requests\Admin\CustomerRequest;
use App\Http\Requests\Admin\AdvertisementRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class FrontUserRequestFactory
{
    public static function handle(Request $frontUserRequest)
    {
        $routeParams = explode('.', $frontUserRequest->route()->getName());
        switch ($routeParams[0]){
            case 'merchants':
                return new AdvertisementRequest();
            case 'customers':
                return new CustomerRequest();
            default:
                return $frontUserRequest;
        }
    }
}
