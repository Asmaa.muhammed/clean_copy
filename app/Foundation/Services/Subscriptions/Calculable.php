<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 4/22/2020
 * Time: 4:42 AM
 */

namespace App\Foundation\Services\Subscriptions;

use App\Models\Plan;

interface Calculable
{
    public function __construct(Plan $plan);

    public function calculate() : array;
}
