<?php

namespace App\Foundation\Services\Subscriptions;

use App\Models\Plan;
use Carbon\Carbon;
use Illuminate\Support\Str;

class Period implements Calculable
{
    private $plan;
    private $startsFrom;

    public function __construct(Plan $plan)
    {
        $this->plan = $plan;
    }

    public function startingFrom($startDate = null)
    {
        if ($startDate) {
            $this->startsFrom = Carbon::parse($startDate);
        }
        $this->startsFrom = now();
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStartsFrom()
    {
        if (!$this->startsFrom) {
            return $this->startsFrom = now();
        }

        return $this->startsFrom;
    }

    private function getCalculationQuery()
    {
        $period = $this->plan->plan_period;
        $interval = ucfirst(Str::plural($this->plan->plan_interval));
        return [
            'period' => $period,
            'method' => "add{$interval}"
        ];
    }

    public function calculate(): array
    {
        $starts_at = now();
        ['method' => $method, 'period' => $period] = $this->getCalculationQuery();

        $ends_at = Carbon::parse($starts_at)->$method($period);

        $renew_after_expiry = $this->plan->renewable;

        return compact('starts_at', 'ends_at', 'renew_after_expiry');
    }

    public function __toString()
    {
        return $this->plan->plan_period
            . " "
            . ucfirst($this->plan->plan_period > 1 ? Str::plural($this->plan->plan_interval) : $this->plan->plan_interval);
    }
}
