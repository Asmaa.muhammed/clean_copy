<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 4/22/2020
 * Time: 5:28 AM
 */

namespace App\Foundation\Services\Subscriptions;


use App\Models\Plan;

class SubscriptionBuilder implements Calculable
{
    private $plan;
    private $subscriptionCalculators = [];

    public function __construct(Plan $plan)
    {
        $this->plan = $plan;
        $this->addCalculator(new Period($plan))
            ->addCalculator(new Payment($plan))
            ->addCalculator($this);
    }

    public function addCalculator(Calculable $calculable)
    {
        $this->subscriptionCalculators[] = $calculable;
        return $this;
    }

    public function build() : array
    {
        return array_merge(...array_map(function (Calculable $calculable){
            return $calculable->calculate();
        }, $this->subscriptionCalculators));
    }


    public function calculate(): array
    {
        $plan_id = $this->plan->id;
        $status = 1;

        return compact('plan_id','status');
    }
}
