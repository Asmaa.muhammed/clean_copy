<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 4/22/2020
 * Time: 4:45 AM
 */

namespace App\Foundation\Services\Subscriptions;


use App\Models\Plan;

class Payment implements Calculable
{

    private $plan;

    private $currency;


    public function __construct(Plan $plan)
    {
        $this->plan = $plan;
    }

    private function fixedDiscountPrice(){
        return $this->plan->discount_value;
    }

    private function percentageDiscountPrice(){
        return ($this->plan->price * ($this->plan->discount_value/100));
    }

    public function getDiscountAmount()
    {
        return $this->plan->discount_type
            ? $this->percentageDiscountPrice()
            : $this->fixedDiscountPrice();
    }

    public function getFinalPrice()
    {
        return $this->plan->price - $this->getDiscountAmount();
    }

    public function calculate() : array
    {
        $paid_amount = $this->getFinalPrice();

        return compact('paid_amount');
    }
}
