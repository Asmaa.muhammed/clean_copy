<?php

namespace App\Foundation\Annotations;

use M3assy\LaravelAnnotations\Foundation\Types\MiddlewareAnnotation;

/**
 * @Annotation
 */
class HasHigherRole extends MiddlewareAnnotation
{

}
