<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 4/14/2020
 * Time: 7:02 PM
 */

namespace App\Foundation\Observers;


use App\Models\CustomerInfo;
use Keygen\Keygen;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class CustomerInfoObserver
{
    /**
     * Handle the customer "creating" event.
     *
     * @param CustomerInfo $customerInfo
     * @return void
     */
    public function creating(CustomerInfo $customerInfo)
    {
        $customerInfo->qr_token = CustomerInfo::generateQRToken(10);
        QrCode::format('png')->size(209)->generate( $customerInfo->qr_token, storage_path('qrCodes/'. $customerInfo->qr_token.'.png'));
    }
}
