<?php

namespace App\Foundation\Observers;

use App\Models\Theme;

class ThemeObserver
{
    const STATUS_COLUMN = 'status';

    /**
     * @param Theme $theme
     */
    protected function disableRemainingThemes(Theme $theme)
    {
        if ($theme->status) {
            Theme::where('id', '!=', $theme->id)->get()->each(function ($activeTheme) {
                $activeTheme->update([self::STATUS_COLUMN => 0]);
            });
        }
    }

    /**
     * @param Theme $theme
     */
    protected function handleIfActiveThemeDeleted(Theme $theme)
    {
        if ($theme->status)
            Theme::first()->update([self::STATUS_COLUMN => 1]);
    }

    /**
     * Handle the theme "created" event.
     *
     * @param  \App\Models\Theme $theme
     * @return void
     */
    public function created(Theme $theme)
    {
        $this->disableRemainingThemes($theme);
    }

    /**
     * Handle the theme "updated" event.
     *
     * @param  \App\Models\Theme $theme
     * @return void
     */
    public function updated(Theme $theme)
    {
        $this->disableRemainingThemes($theme);
    }

    /**
     * Handle the theme "deleted" event.
     *
     * @param  \App\Models\Theme $theme
     * @return void
     */
    public function deleted(Theme $theme)
    {
        $this->handleIfActiveThemeDeleted($theme);
    }

    /**
     * Handle the theme "force deleted" event.
     *
     * @param  \App\Models\Theme $theme
     * @return void
     */
    public function forceDeleted(Theme $theme)
    {
        $this->handleIfActiveThemeDeleted($theme);
    }
}
