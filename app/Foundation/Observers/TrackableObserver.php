<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 4/19/2020
 * Time: 9:50 PM
 */

namespace App\Foundation\Observers;


use Illuminate\Database\Eloquent\Model;

class TrackableObserver
{
    private function trackAction(string $action ,Model $model)
    {
        $model->$action = auth()->check() ? auth()->id() : 1;
    }

    /**
     * Handle the theme "creating" event.
     *
     * @param Model $model
     * @return void
     */
    public function creating(Model $model)
    {
        $this->trackAction('created_by', $model);
    }

    /**
     * Handle the theme "updating" event.
     *
     * @param Model $model
     * @return void
     */
    public function updating(Model $model)
    {
        $this->trackAction('modified_by', $model);
    }
}
