<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 4/16/2020
 * Time: 7:42 PM
 */

namespace App\Foundation\Observers;


use App\Models\Offer;

class OfferObserver
{
    /**
     * Handle the merchant "creating" event.
     *
     * @param Offer $offer
     * @return void
     */
    public function creating(Offer $offer)
    {
        $offer->code = Offer::generateCode(5);
    }
}
