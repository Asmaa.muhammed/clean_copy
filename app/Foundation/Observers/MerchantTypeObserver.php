<?php

namespace App\Foundation\Observers;

use App\Models\Merchant;

class MerchantTypeObserver
{
    /**
     * Handle the merchant "creating" event.
     *
     * @param  \App\Models\Merchant  $merchant
     * @return void
     */
    public function creating(Merchant $merchant)
    {
        $merchant->type = 1;
    }

    /**
     * Handle the merchant "deleting" event.
     *
     * @param  \App\Models\Merchant  $merchant
     * @return void
     */
    public function deleting(Merchant $merchant)
    {
        $merchant->info()->delete();
        $merchant->socialLinks()->delete();
    }
}
