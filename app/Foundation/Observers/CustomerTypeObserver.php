<?php

namespace App\Foundation\Observers;

use App\Models\Customer;

class CustomerTypeObserver
{
    /**
     * Handle the customer "creating" event.
     *
     * @param  \App\Models\Customer  $customer
     * @return void
     */
    public function creating(Customer $customer)
    {
        $customer->type = 0;
    }

    /**
     * Handle the customer "deleting" event.
     *
     * @param  \App\Models\Customer  $customer
     * @return void
     */
    public function deleting(Customer $customer)
    {
        $customer->info()->delete();
        $customer->interests()->delete();
    }
}
