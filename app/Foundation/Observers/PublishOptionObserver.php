<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 1/4/2019
 * Time: 2:30 AM
 */

namespace App\Foundation\Observers;


use App\Models\PublishOption;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class PublishOptionObserver
{
    /**
     * Observe action on creating model has the trait.
     * @param PublishOption $publishOption
     */
    public function creating(PublishOption $publishOption)
    {

        if(!isset($publishOption->publish_by)){
            if(Auth::check()){
                $publishOption->publish_by= Auth::id();
            }
        }
    }




}
