<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 3/6/2020
 * Time: 2:30 AM
 */

namespace App\Foundation\Traits;

use Closure;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

trait TableIndex
{
    public function makeDatatable($query,
                                  $module,
                                  Closure $customCallback = null,
                                  $customRawColumns = [],
                                  $moduleParams = [],
                                  $actionButtonsView = 'admin.layout.components.action_button',
                                  $displayActions = true)
    {
        $dataTable =  DataTables::of($query)
            ->editColumn('status', function ($record) {
                return view('admin.layout.includes.status_column', ['model' => $record]);

            })
            ->editColumn('created_at', function ($record) {
                return $record->created_at->format('d-m-Y');
            });

        if ($displayActions == true){
            $dataTable =  $dataTable
                ->addColumn('actions', function ($record) use ($module,$moduleParams, $actionButtonsView) {
                $moduleParams[Str::singular($module)] = $record;
                return view($actionButtonsView, ['module' => $module, 'model' => $moduleParams]);
            });
        }
        if($customCallback){
            $dataTable = $customCallback($dataTable);
        }
        if ($displayActions == true) {
            $dataTable = $dataTable->rawColumns(array_merge(['actions'], $customRawColumns));
        }else
        {
          $dataTable = $dataTable->rawColumns($customRawColumns);
        }

        return $dataTable->make(true);
    }

    public function tableHtmlBuilder($htmlBuilder, $columns = null,$displayActions = true)
    {

        array_walk($columns,function ($options,$data) use ($htmlBuilder){
            $htmlBuilder->addColumn(['data' => $data, 'name' => $data, 'title' => $options['title'], 'searchable' => $options['searchable'], 'orderable' => $options['orderable']]);
        });
        $htmlBuilder
            ->addColumn(['data' => 'created_at', 'name' => 'created_at', 'title' => 'Created At', 'searchable' => true, 'orderable' => true]);
        if($displayActions == true){
            $htmlBuilder = $htmlBuilder ->addColumn(['data' => 'actions', 'name' => 'actions', 'title' => __('translation::translation.actions'), 'searchable' => false, 'orderable' => false]);

        }
        $htmlBuilder = $htmlBuilder->parameters([
                'paging' => true,
                'searching' => true,
                'searchDelay' => 350,
                 'language'=>  [
                     'url' =>(app()->getLocale() == 'ar') ? "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json" : ''
                     ]
            ]);
        return $htmlBuilder;

    }
}
