<?php


namespace App\Foundation\Traits;


trait ResolveRouteBinding
{

    public function resolveRouteBinding($value, $field = null)
    {
        if (is_numeric($value)){
            return $this->where('id',$value)->firstOrFail();
        }else{
            return $this->whereTranslation('slug', $value)->firstOrFail();
        }
    }
    public function getLocalizedRouteKey($locale)
    {
        return (!empty($this->translate($locale))) ?  $this->translate($locale)->slug :  $this->translate('en')->slug;
    }

}
