<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 4/19/2020
 * Time: 10:43 PM
 */

namespace App\Foundation\Traits;


use App\Models\Subscription;

trait Subscribable
{
    public function subscriptions()
    {
        return $this->morphMany(Subscription::class, 'user');
    }

    public function subscription($renew_after_expiry  = null)
    {
        $nowDateTimeString = now()->toDateTimeString();
        $subscription = $this->morphOne(Subscription::class, 'user')
            ->where('starts_at', '<=', $nowDateTimeString)
            ->where('ends_at', '>', $nowDateTimeString)
            ->where('status', 1);

        if (!empty($renew_after_expiry)){
            $subscription = $subscription->where('renew_after_expiry','=',1);
        }
        return $subscription;
    }
    public function cancelActiveSubscription()
    {
        $nowDateTimeString = now()->toDateTimeString();
        return $this->morphOne(Subscription::class, 'user')
            ->where('starts_at', '<=', $nowDateTimeString)
            ->where('ends_at', '>', $nowDateTimeString)
            ->where('renew_after_expiry','=',0);

    }

    public function isSubscribed()
    {
        return (boolean) $this->subscription;
    }
}
