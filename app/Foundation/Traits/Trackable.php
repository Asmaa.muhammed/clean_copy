<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 4/19/2020
 * Time: 9:46 PM
 */

namespace App\Foundation\Traits;


use App\Foundation\Observers\TrackableObserver;
use App\Models\User;

trait Trackable
{
    public static function bootTrackable(){
        static::observe(TrackableObserver::class);
    }
    
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function modifiedBy()
    {
        return $this->belongsTo(User::class, 'modified_by');
    }
}
