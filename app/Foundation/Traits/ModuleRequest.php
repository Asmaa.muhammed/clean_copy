<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 3/6/2020
 * Time: 2:30 AM
 */

namespace App\Foundation\Traits;


use Illuminate\Validation\Rule;

trait ModuleRequest
{
    public function uniqueWithIgnore($table, $routeParam = null)
    {
        return $routeParam ? Rule::unique($table)->ignore($routeParam) : Rule::unique($table);
    }
}
