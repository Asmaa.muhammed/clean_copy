<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 3/10/2020
 * Time: 11:29 PM
 */

namespace App\Foundation\Traits;


use JoeDixon\Translation\Language;

trait RequestWithTranslatable
{
    protected function getDbLanguages()
    {
        return Language::whereStatus(1)->get()->pluck('language')->toArray();
    }

    public function resolveTranslatableRules()
    {
        $rules = [];
        $languageAliases = $this->getDbLanguages();
        foreach ($this->translatableRules() as $key => $value) {
            foreach($languageAliases as $languageAlias){
                $rules["$key:$languageAlias"] = $value;
            }
        }
        return $rules;
    }

    abstract public function translatableRules();

}
