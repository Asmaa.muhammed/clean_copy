<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 4/22/2020
 * Time: 5:41 PM
 */

namespace App\Foundation\Traits;


use App\Models\SocialTypeOwner;

trait HasSocialLinks
{
    public function socialLinks()
    {
        return $this->morphMany(SocialTypeOwner::class, 'sociable');
    }
}
