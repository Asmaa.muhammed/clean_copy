<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 3/24/2020
 * Time: 10:32 PM
 */

namespace App\Foundation\Traits;


use App\Foundation\Observers\PublishOptionObserver;
use App\Models\PublishOption;
use Carbon\Carbon;

trait HasPublishOptions
{
    public function publishOptions()
    {
        PublishOption::observe(PublishOptionObserver::class);
        return $this->morphOne(PublishOption::class, 'publishable');
    }

    public  function   scopeFrontPublish($query)
    {
        return $query->whereStatus(1)->whereHas('publishOptions', function ($query) {
            $query->where('start_publishing', '<=', Carbon::now())
                ->where('end_publishing', '>=', Carbon::now())
            ->orderBy('start_publishing','DESC');
        });
    }
    public  function scopeShowFrontHomePage($query){
        return $query->   whereHas('publishOptions', function ($query) {
            $query->whereShow_in_main_page(1);
        });

    }


}
