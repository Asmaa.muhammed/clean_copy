<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 4/18/2020
 * Time: 11:38 PM
 */

namespace App\Foundation\Traits;


trait NeedFilteredUpdateData
{
    public function getData()
    {
        return array_filter($this->all(), function ($datum){
            return $datum != null;
        });
    }
}
