<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 4/9/2020
 * Time: 1:06 PM
 */

namespace App\Foundation\Traits;


trait HasScopesOrObservers
{
    public static function bootHasScopesOrObservers()
    {
        if(property_exists(static::class, 'customGlobalScopes')){
            foreach (static::$customGlobalScopes as $customGlobalScope){
                static::addGlobalScope(new $customGlobalScope);
            }
        }

        if(property_exists(static::class, 'customObserver')){
            static::observe(new static::$customObserver);
        }
    }
}
