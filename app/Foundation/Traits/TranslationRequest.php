<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 1/3/2019
 * Time: 9:33 PM
 */

namespace App\Foundation\Traits;


use JoeDixon\Translation\Language;
use Illuminate\Support\Facades\App;

trait TranslationRequest
{

    /**
     * Get final translation rules.
     * @return array
     */
    public function getTranslationRules()
    {
        $languages=$this->getLanguages();
        return $this->validateLanguagesRules($languages);
    }

    /**
     * Get languages that exists in database.
     * @return array
     */
    public function getLanguages()
    {
        return Language::all( 'language')->pluck('language')->toArray();
    }

    /**
     * Validate on the fields as required fields.
     * @param $languages
     * @return mixed
     */
    public function validateLanguagesRules($languages)
    {
        $infoRules= [];
        if(count($languages) > 0){
            foreach($languages as $language){
                foreach ($this->getRequiredFields() as $requiredField => $validations){
                    $infoRules["$requiredField:$language"]= $validations;
                }
            }
            return $infoRules;
        }
        return false;
    }

    /**
     * Getting fields to be validated.
     * @return mixed
     */
    public function getRequiredFields()
    {
        return isset($this->translatedFields) ? $this->translatedFields : [];
    }
}
