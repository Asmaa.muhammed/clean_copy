<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 3/15/2020
 * Time: 11:44 PM
 */

namespace App\Foundation\Composers;

use App\Models\Role;
use Illuminate\View\View;

class UserComposer implements ComposerInterface
{
    public function compose(View $view)
    {
        $minimumRole = auth()->user()->roles()->min('id');
        $view->with([
            'roles' => Role::where('id', in_array($minimumRole
                , config('app.sorted_privileged_admins_ids')) ? '>=' : '>'
                , $minimumRole)
                ->get()
                ->pluck('display_name', 'id')
        ]);
    }
}
