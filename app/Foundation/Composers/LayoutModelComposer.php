<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 3/31/2020
 * Time: 8:51 PM
 */

namespace App\Foundation\Composers;


use App\Models\Enum;
use App\Models\Plugin;
use App\Models\Theme;
use Illuminate\View\View;

class LayoutModelComposer implements ComposerInterface
{

    /**
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $pluginGroups = Plugin::all()->groupBy('position');
        $view->with([
            'layout_types' => Enum::whereValue('layout_types')->first()->children,
            'layout_sections' => Enum::whereValue('layout_positions')->first()->children,
            'types' => Enum::whereValue('nodeTypes')->first()->children->pluck('name', 'value'),
            'themes' => Theme::all()->pluck('name', 'id'),
            'pluginGroups' => $pluginGroups,
        ]);
    }
}
