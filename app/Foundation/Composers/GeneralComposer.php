<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 1/2/2019
 * Time: 3:29 PM
 */

namespace App\Foundation\Composers;

use App\Models\Enum;
use Illuminate\View\View;
use JoeDixon\Translation\Language;

class GeneralComposer implements ComposerInterface
{
    /**
     * Compose (Languages) module provided data.
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with([
            'languages'=>Language::whereStatus(1)->orderBy('sorting','asc')->get()
        ]);
    }
}
