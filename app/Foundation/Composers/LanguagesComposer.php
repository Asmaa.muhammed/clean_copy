<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 1/2/2019
 * Time: 3:29 PM
 */

namespace App\Foundation\Composers;

use App\Models\Enum;
use Illuminate\View\View;

class LanguagesComposer implements ComposerInterface
{
    /**
     * Compose (Languages) module provided data.
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with([
            'directions'=>Enum::find(1)->children->pluck('name')
        ]);
    }
}
