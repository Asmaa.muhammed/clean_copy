<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 1/2/2019
 * Time: 3:29 PM
 */

namespace App\Foundation\Composers;

use App\Models\Enum;
use Illuminate\View\View;
use JoeDixon\Translation\Language;

class EnumsComposer implements ComposerInterface
{
    /**
     * Compose (Languages) module provided data.
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with([
            'parent'=>array_replace([__("enums.root")],Enum::whereParent_id(0)->get()->pluck('name','id')->toArray())
        ]);
    }
}
