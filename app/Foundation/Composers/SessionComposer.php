<?php

namespace App\Foundation\Composers;

use App\Models\Event;
use App\Models\Person;
use Illuminate\View\View;
use App\Models\LayoutModel;

class SessionComposer implements ComposerInterface
{

    /**
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([
            'persons' => Person::withTranslation()->get()->pluck('name', 'id'),
            'events'=>Event::withTranslation()->get()->pluck('title', 'id'),
        ]);
    }
}
