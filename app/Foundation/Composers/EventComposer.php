<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 3/31/2020
 * Time: 8:51 PM
 */

namespace App\Foundation\Composers;


use App\Models\Tag;
use App\Models\Enum;
use App\Models\Minor;
use App\Models\Slider;
use App\Models\Gallery;
use App\Models\Category;
use Illuminate\View\View;
use App\Models\LayoutModel;

class EventComposer implements ComposerInterface
{

    /**
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([
            'models' => LayoutModel::whereType(3)->get()->pluck('name','id'),
            'categories' => Category::withTranslation()->get()->pluck('name', 'id'),
            'tags'=>Tag::withTranslation()->get()->pluck('name', 'id'),
            'galleries'=>Gallery::all()->pluck('title','id'),
            'minors'=>Minor::all()->pluck('title','id'),
        ]);
    }
}
