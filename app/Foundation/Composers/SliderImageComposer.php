<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 3/25/2020
 * Time: 7:10 PM
 */

namespace App\Foundation\Composers;


use App\Models\Enum;
use App\Models\MenuLink;
use Illuminate\View\View;

class SliderImageComposer implements ComposerInterface
{
    /**
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {

        $view->with([
            'target' => Enum::whereValue('menu_link_target')->first()->children->pluck('name', 'value'),
            'targetTypes' => Enum::whereValue('supported_internal_targets')->first()->children->pluck('name', 'value'),
        ]);
    }
}
