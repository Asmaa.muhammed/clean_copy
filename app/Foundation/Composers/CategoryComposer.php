<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 1/2/2019
 * Time: 3:29 PM
 */

namespace App\Foundation\Composers;

use App\Models\Category;
use App\Models\Gallery;
use Illuminate\View\View;

class CategoryComposer implements ComposerInterface
{
    /**
     * Compose (Languages) module provided data.
     * @param View $view
     */
    public function compose(View $view)
    {


        $view->with([
            'parent'=> array_replace([__('enums.root')],
                Category::all()->pluck('name', 'id')->toArray()),
            'galleries'=>Gallery::all()->pluck('title','id'),
        ]);
    }
}
