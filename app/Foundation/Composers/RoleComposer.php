<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 3/12/2020
 * Time: 1:18 AM
 */

namespace App\Foundation\Composers;


use App\Models\Permission;
use Illuminate\View\View;

class RoleComposer implements ComposerInterface
{
    public function compose(View $view)
    {
        $view->with([
            'permissions' => Permission::whereIn('id', auth()->user()->allPermissions()->pluck('id')->toArray())
                ->get()
                ->pluck('display_name', 'id'),
        ]);
    }
}
