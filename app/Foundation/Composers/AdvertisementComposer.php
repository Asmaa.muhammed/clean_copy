<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 1/2/2019
 * Time: 3:29 PM
 */

namespace App\Foundation\Composers;

use App\Models\Category;
use App\Models\Customer;
use App\Models\Gallery;
use Illuminate\View\View;
use m3assy\nationals\Country;

class AdvertisementComposer implements ComposerInterface
{
    /**
     * Compose (Languages) module provided data.
     * @param View $view
     */
    public function compose(View $view)
    {
        $allCategories  = Category::find(2)->children;
        $categoriesWithChildren =[];
        foreach ($allCategories as  $category){
            $categoriesWithChildren[$category->name] = $category->children()->withTranslation()->whereStatus(1)->get()->pluck('name','id')->toArray();

        }
        $view->with([
            'categories'=>$categoriesWithChildren,
            'customers'=>Customer::all()->pluck('full_name','id'),
            'cities' => Country::whereCode('AE')->first()->regions->pluck('region', 'id'),
        ]);
    }
}
