<?php

namespace App\Foundation\Composers;

use App\Models\Enum;
use App\Models\MenuGroup;
use App\Models\Module;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Illuminate\View\View;
use JoeDixon\Translation\Language;
use function test\Mockery\Fixtures\HHVMString;

class AdminComposer implements ComposerInterface
{
    public function compose(View $view)
    {
        $directions = Enum::find(1)->children->pluck('name');
        $appLanguage = Language::whereLanguage(app()->getLocale())->first();
        //dd($appLanguage);
        $view->with([
            'languages' => Language::whereStatus(1)->orderBy('sorting','ASC')->get(),
           //'modules' => Module::whereStatus(1)->get(),
            'modules' => MenuGroup::whereShortCode('admin_menu')
                ->first()
                ->links()
                ->whereParent(0)
                ->orderBy('sorting','ASC')
                ->get(),
                 'langDirection'=>strtolower($directions[$appLanguage->direction]),
                 'styleDirection' => ($appLanguage->direction == 1 ) ? ".".strtolower($directions[$appLanguage->direction]): ''
        ]);
    }
}
