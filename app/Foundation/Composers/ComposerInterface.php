<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 3/25/2020
 * Time: 7:11 PM
 */

namespace App\Foundation\Composers;
use Illuminate\View\View;

interface ComposerInterface
{
    /**
     * @param View $view
     * @return void
     */
    public function compose(View $view);
}
