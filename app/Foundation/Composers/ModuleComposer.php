<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 3/25/2020
 * Time: 7:10 PM
 */

namespace App\Foundation\Composers;


use App\Models\Enum;
use App\Models\MenuLink;
use Illuminate\View\View;

class ModuleComposer implements ComposerInterface
{
    /**
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $menuGroup = request()->route()->parameter('menuGroup');
        $view->with([
            'groups' => Enum::whereValue('adminnavgroup')->first()->children->pluck('name', 'value'),

        ]);
    }
}
