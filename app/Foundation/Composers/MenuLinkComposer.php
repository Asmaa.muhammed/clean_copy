<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 3/25/2020
 * Time: 7:10 PM
 */

namespace App\Foundation\Composers;


use App\Models\Enum;
use App\Models\Gallery;
use App\Models\MenuLink;
use Illuminate\View\View;

class MenuLinkComposer implements ComposerInterface
{
    /**
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $menuGroup = request()->route()->parameter('menuGroup');
        $view->with([
            'parent' => array_replace([__('enums.root')],
                $menuGroup->links->pluck('name', 'id')->toArray()),
            'target' => Enum::whereValue('menu_link_target')->first()->children->pluck('name', 'value'),
            'targetTypes' => Enum::whereValue('supported_internal_targets')->first()->children->pluck('name', 'value'),
            'galleries'=>Gallery::all()->pluck('title','id'),
        ]);
    }
}
