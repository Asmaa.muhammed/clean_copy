<?php


namespace App\Foundation\Composers\Front;

use App\Foundation\Composers\ComposerInterface;
use App\Models\Category;
use App\Models\MenuGroup;
use App\Models\Theme;
use Illuminate\View\View;
use m3assy\nationals\Country;


class FrontGeneralComposer implements ComposerInterface
{
    /**
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $mainTheme = Theme::whereStatus(1)->get()->first();
        $view->with([
            'themePath' => $mainTheme->path,
            'Gateway' => (MenuGroup::whereShort_code('gateway')->first())?MenuGroup::whereShort_code('gateway')->first()
                ->links()
                ->whereParent(0)
                ->whereStatus(1)
                ->orderBy('sorting', 'ASC')
                ->get():[],
            'StaffMenu' => (MenuGroup::whereShort_code('Staff-main-menu')->first())?MenuGroup::whereShort_code('Staff-main-menu')->first()
                ->links()
                ->whereParent(0)
                ->whereStatus(1)
                ->orderBy('sorting', 'ASC')
                ->get():[],
            'MainMenu' =>( MenuGroup::whereShort_code('main-menu')->first())? MenuGroup::whereShort_code('main-menu')->first()
                ->links()
                ->whereParent(0)
                ->whereStatus(1)
                ->orderBy('sorting', 'ASC')
                ->get():[],
            'footerMenu' =>( MenuGroup::whereShort_code('footer-menu')->first())? MenuGroup::whereShort_code('footer-menu')->first()
                ->links()
                ->whereParent(0)
                ->whereStatus(1)
                ->orderBy('sorting', 'ASC')
                ->get():[],
            'studentMenu' =>( MenuGroup::whereShort_code('student-main-menu')->first())? MenuGroup::whereShort_code('student-main-menu')->first()
                ->links()
                ->whereParent(0)
                ->whereStatus(1)
                ->orderBy('sorting', 'ASC')
                ->get():[],
            "HeaderMenu"=>( MenuGroup::whereShort_code('header-main-menu')->first())? MenuGroup::whereShort_code('header-main-menu')->first()
            ->links()
            ->whereParent(0)
            ->whereStatus(1)
            ->orderBy('sorting', 'ASC')
            ->get():[],
        ]);

    }
}
