<?php


namespace App\Foundation\Composers\Front;

use App\Foundation\Composers\ComposerInterface;
use App\Models\Category;
use App\Models\MenuGroup;
use App\Models\Theme;
use Illuminate\View\View;
use m3assy\nationals\Country;


class StudentFrontGeneralComposer implements ComposerInterface
{
    /**
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $mainTheme = Theme::whereStatus(1)->get()->first();
        $view->with([
            'themePath' => $mainTheme->path,
            'studentMenu' => MenuGroup::whereShort_code('student-main-menu')->first()
                ->links()
                ->whereParent(0)
                ->whereStatus(1)
                ->orderBy('sorting', 'ASC')
                ->get(),
            'MainMenu' => MenuGroup::whereShort_code('main-menu')->first()
                ->links()
                ->whereParent(0)
                ->whereStatus(1)
                ->orderBy('sorting', 'ASC')
                ->get(),
            'footerMenu' => MenuGroup::whereShort_code('footer-menu')->first()
                ->links()
                ->whereParent(0)
                ->whereStatus(1)
                ->orderBy('sorting', 'ASC')
                ->get(),
        ]);

    }
}
