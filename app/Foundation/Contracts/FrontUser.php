<?php
/**
 * Created by PhpStorm.
 * User: yoyoy
 * Date: 4/14/2020
 * Time: 12:36 AM
 */

namespace App\Foundation\Contracts;


use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\Relation;

interface FrontUser
{
    public function info() : HasOne;

    public function offers() : Relation;
}
