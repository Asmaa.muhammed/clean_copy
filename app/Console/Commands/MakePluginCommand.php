<?php

namespace App\Console\Commands;

use App\Models\Enum;
use App\Models\Plugin;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;

class MakePluginCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:plugin {name} {--async}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new plugin';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $position = $this->askForPosition();
        $type = $this->askForType();
        Plugin::create([
            'name' => ucwords(str_replace('_',' ',Str::snake($this->argument('name')))),
            'short_code' => Str::camel($this->argument('name')),
            'path' => clean_copy,
            'type' => $type,
            'position' => $position,
            'status' => 1,
        ]);

        Artisan::call('make:widget', [
            'name' => $this->argument('name'),
        ]);
        Artisan::call('make:shortcode', [
            'name' => $this->argument('name'),
            '--async' => $this->option('async'),
        ]);
        $this->info('Plugin Components Created Successfully');
    }

    private function askForPosition()
    {
        $positions = Enum::whereValue('layout_positions')->first()->children->pluck('name', 'value');
        return $positions
            ->flip()[$this->choice("Choose Position", $positions->values()->toArray(), '0')];
    }

    private function askForType()
    {
        $types = collect(Relation::$morphMap)->prepend('None', 0);
        $choice = $this->choice("Choose Plugin Target Type", $types->values()->toArray(), null);
        return $choice != 'None' ? $types->flip()[$choice] : null;
    }
}
