<?php

namespace App\Console\Commands;

use App\Models\Currency;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class AddCurrenciesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'currencies:add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add Records To Currencies Table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $currencies = json_decode(Storage::get('/public/currencies/currencies.json'), true);
        if (!empty($currencies)) {
            if (array_map(function ($currency) {
                $currency['name:en'] = $currency['name'];
                unset($currency['name']);
                if (in_array($currency['code'], config('currencies.supported_currencies'))) {
                    $currency['status'] = true;
                }
                return Currency::create($currency);
            }, $currencies)) {
                $this->info('All Currencies Added');
            } else {
                $this->error('Error Occured');
            }
        } else {
            $this->error('Currencies File Not Found.');
        }
    }
}
