<?php

namespace App\Console\Commands;

use Arrilot\Widgets\AsyncFacade;
use Arrilot\Widgets\Facade;
use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;

class MakeShortCodeCommand extends GeneratorCommand
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:shortcode {name} {--async}';
    /**
     * The name of the console command.
     *
     * @var string
     */
    protected $name = 'make:shortcode';

    protected $type = "Shortcode";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new short code';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return resource_path('stubs/shortcodes/shortcode.stub');
    }

    protected function replaceNamespace(&$stub, $name)
    {
        $stub = str_replace(
            ['DummyNamespace', 'DummyRootNamespace', 'NamespacedDummyUserModel', 'DummyWidgetUse'],
            [$this->getNamespace($name), $this->rootNamespace(), $this->userProviderModel(), $this->getWidgetUse()],
            $stub
        );

        return $this;
    }

    private function getWidgetUse()
    {
        return $this->option('async') ? AsyncFacade::class : Facade::class;
    }

    protected function getDefaultNamespace($rootNamespace)
    {
        return "App\Shortcodes";
    }

    protected function getNameInput()
    {
        return Str::studly(parent::getNameInput()."Shortcode");
    }
}
