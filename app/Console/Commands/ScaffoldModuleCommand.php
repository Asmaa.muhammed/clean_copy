<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ScaffoldModuleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scaffold:module {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scaffolds Module Views';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Scaffolding {$this->argument('name')} Module");
    }
}
