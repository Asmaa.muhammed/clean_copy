<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventDetails extends Model
{
    //
    protected  $table = "event_details";
    protected $fillable =[
        'start_date',
        'end_date',
        'place',
        'organizer',
        'node_id'
    ];
    public function event(){
        $this->belongsTo(Event::class,'node_id');
    }
}
