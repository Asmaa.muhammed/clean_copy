<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Laratrust\Models\LaratrustRole;
use Spatie\Activitylog\Traits\LogsActivity;

class Role extends LaratrustRole
{
    use Translatable , LogsActivity;

    protected $fillable = [
        'name',
    ];

    public $translatedAttributes = ['display_name', 'description'];

    public $translationModel = RoleTranslation::class;
    /**
     * activities logs attributes by model used by (Activity Log Package)
     * @var array
     */
    protected static $logAttributes = ['name', 'description','display_name'];

}
