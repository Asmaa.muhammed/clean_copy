<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class MenuGroup extends Model
{
    use LogsActivity;
    protected $fillable = [
        'name',
        'short_code',
        'status',
    ];

    public function links()
    {
        return $this->hasMany(MenuLink::class)->orderBy('sorting',"ASC");
    }
}
