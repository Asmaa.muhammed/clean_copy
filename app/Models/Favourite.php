<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{
    //
    protected $fillable = [
        'advertisement_id',
        'customer_id'
    ];
    protected $table = "favourites";
    public function advertisement(){
        return $this->belongsTo(Advertisement::class,'advertisement_id');
    }
}
