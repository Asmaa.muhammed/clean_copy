<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModuleTrasnlation extends Model
{
    protected $fillable = ['name', 'description'];

    protected $table = 'module_translations';

    public $timestamps = false;
}
