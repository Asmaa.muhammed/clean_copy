<?php

namespace App\Models;

use App\Foundation\Scopes\PageScope;
use Illuminate\Database\Eloquent\Model;

class Page extends Node
{
    //
    protected static function boot(){
        parent::boot();
        static::addGlobalScope(new PageScope());
    }
    protected $fillable = [
        'status',
        'layout_model_id',
        'type',
    ];
    public function getForeignKey(){
        return parent::getForeignKey();
    }



}
