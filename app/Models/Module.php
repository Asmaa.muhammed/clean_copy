<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Module extends Model
{
    use Translatable ;

    protected $fillable = [
        'sorting',
        'icon',
        'status',
        'route',
        'group'
    ];

    public $translatedAttributes = ['name', 'description'];

    public $translationModel = ModuleTrasnlation::class;
    /**
     * activities logs attributes by model used by (Activity Log Package)
     * @var array
     */
    protected static $logAttributes = ['name', 'description','status','sorting','icon'];


    public static function quickCreate($route,$columns)
    {
        $attributes= [
            'sorting'=>self::max('sorting')+1,
            'route'=>$route,
            'status'=>$columns['status'],
            'icon'=>$columns['icon'],
            'group'=>$columns['group'] ?? null,
            'name:en'=>$columns['name']

        ];
        return self::create($attributes);
    }
}
