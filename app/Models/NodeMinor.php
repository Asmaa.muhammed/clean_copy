<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NodeMinor extends Model
{
    protected $table="node_minors";
    protected $fillable=[
        "node_id",
        "minor_id"
    ];
    public function page(){
        return   $this->belongsTo(Minor::class,'minor_id');
    }
    public function node (){
        return   $this->belongsTo(Node::class,'node_id');
    }
}
