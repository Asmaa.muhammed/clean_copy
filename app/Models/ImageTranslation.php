<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class ImageTranslation extends Model
{
    //

    protected $fillable = ['caption', 'image_alt'];

    protected $table = "image_translations";


}
