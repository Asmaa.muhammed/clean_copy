<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleTranslation extends Model
{
    protected $fillable = ['display_name', 'description'];

    protected $table = 'roles_translations';
}
