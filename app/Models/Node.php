<?php

namespace App\Models;

use App\Foundation\Traits\HasPublishOptions;
use App\Foundation\Traits\ResolveRouteBinding;
use Astrotomic\Translatable\Translatable;
use Cocur\Slugify\Slugify;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Mcamara\LaravelLocalization\Interfaces\LocalizedUrlRoutable;



class Node extends Model implements LocalizedUrlRoutable
{
    //
    use Translatable,LogsActivity,HasPublishOptions,ResolveRouteBinding;
    protected $table = 'nodes';
    public $translationModel = NodeTranslation::class;

    /**
     * Model fillables array
     * @var array
     */
    protected $fillable=[
        'status',
        'layout_model_id',
        'type',


    ];


    public function getForeignKey()
    {
        return 'node_id';
    }
    /**
     * Translated attributes by model used by (Translatable Package)
     * @var array
     */
    public $translatedAttributes = [
        'title',
        'summary',
        'body',
        'slug'
    ];
    protected static $logAttributes = ['title', 'summary','body','status','sorting','parent','type'];
    /**
     * Get the  image cover.
     */
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }


    /**
     * Get publish options.
     */

    public function categories (){
      return   $this->belongsToMany(Category::class,'node_taxonomies','node_id');
    }
    public function tags (){
      return   $this->belongsToMany(Tag::class,'node_taxonomies','node_id');
    }
    public function author (){
      return  $this->hasOne(NodeTaxonomy::class,'node_id');
    }
    public function gallery(){
      return  $this-> hasOne(NodeGallery::class);
    }
    public function layoutModel()
    {
        return $this->belongsTo(LayoutModel::class, 'layout_model_id');
    }
    public function minor()
    {
        return $this->hasOne(NodeMinor::class);
    }
    public function getLocalizedRouteKey($locale)
    {
      return $this->translate($locale)->slug;
    }





}
