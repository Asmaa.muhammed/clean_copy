<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Slider extends Model
{
    //
    use LogsActivity ;
    /**
     * Model fillables array
     * @var array
     */
    protected $fillable=[
        'title',
        'short_code',
        'status',
        'main'
    ];
    /**
     * Get all of the slider's images.
     */
    public function images()
    {
        return $this->morphMany(Image::class, 'imageable')->orderBy('sorting');;
    }

}
