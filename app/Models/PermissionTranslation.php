<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionTranslation extends Model
{
    protected $fillable = ['display_name', 'description'];

    protected $table = 'permissions_translations';
}
