<?php

namespace App\Models;

use App\Foundation\Traits\HasPublishOptions;
use App\Foundation\Traits\ResolveRouteBinding;
use Astrotomic\Translatable\Translatable;
use Cocur\Slugify\Slugify;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Mcamara\LaravelLocalization\Interfaces\LocalizedUrlRoutable;



class Session extends Model implements LocalizedUrlRoutable
{
    //
    use Translatable,LogsActivity,HasPublishOptions,ResolveRouteBinding;
    protected $table = 'sessions';
    public $translationModel = SessionTranslation::class;
    protected $fillable = ['day', 'date','time_from','time_to','status','node_id'];

    public function getForeignKey()
    {
        return 'session_id';
    }
    /**
     * Translated attributes by model used by (Translatable Package)
     * @var array
     */
    public $translatedAttributes = [
        'subject',
        'description',
        'place',
        'location',
    ];
    protected static $logAttributes = [ 'day', 'date','time_from','time_to','subject', 'description','location','status','place','node_id'];
    

    public function getLocalizedRouteKey($locale)
    {
      return $this->translate($locale)->slug;
    }

    public function event(){
        $this->belongsTo(Event::class,'node_id');
    }

    public function persons(){
        return   $this->belongsToMany(Person::class,'session_persons','session_id');
      }
}
