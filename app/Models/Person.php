<?php

namespace App\Models;

use App\Foundation\Traits\HasPublishOptions;
use App\Foundation\Traits\ResolveRouteBinding;
use Astrotomic\Translatable\Translatable;
use Cocur\Slugify\Slugify;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Mcamara\LaravelLocalization\Interfaces\LocalizedUrlRoutable;



class Person extends Model implements LocalizedUrlRoutable
{
    //
    use Translatable,LogsActivity,ResolveRouteBinding;
    protected $table = 'persons';
    public $translationModel = PersonTranslation::class;

    protected $fillable = [
        'status',
        'portfolio_link',
    ];

    public function getForeignKey()
    {
        return 'person_id';
    }
    /**
     * Translated attributes by model used by (Translatable Package)
     * @var array
     */
    public $translatedAttributes = [
        'title',
        'name',
        'position',
        'about',
        'description',
        'slug'
    ];
    protected static $logAttributes = ['title', 'name','position','status','about','description','portfolio_link'];
    /**
     * Get the  image cover.
     */
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function getLocalizedRouteKey($locale)
    {
      return $this->translate($locale)->slug;
    }

    public function persons(){
        return   $this->belongsToMany(Person::class,'session_persons','person_id');
    }
}
