<?php

namespace App\Models;

use App\Foundation\Scopes\EventScope;
use Illuminate\Database\Eloquent\Model;

class Event extends Node
{
    //
    protected static function boot(){
        parent::boot();
        static::addGlobalScope(new EventScope());
    }
    protected $fillable = [
        'status',
        'layout_model_id',
        'type',
    ];
    public function getForeignKey()
    {
        return parent::getForeignKey();
    }
    public function  eventDetails(){
       return $this->hasOne(EventDetails::class,'node_id');
    }

    public function  sessions(){
        return $this->hasMany(Session::class,'session_id');
     }

}
