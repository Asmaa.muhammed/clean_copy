<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class   ExternalLink extends Model
{
    protected $fillable = [
        'link'
    ];

    public $timestamps = false;

    public function getTargetAttribute()
    {
        return $this->link;
    }
}
