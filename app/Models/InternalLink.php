<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InternalLink extends Model
{
    protected $fillable = [
        'target_type',
        'target_id',
    ];

    public $timestamps = false;

    public function target()
    {
        return $this->morphTo();
    }
}
