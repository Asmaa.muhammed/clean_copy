<?php

namespace App\Models;

use App\Foundation\Traits\ResolveRouteBinding;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Mcamara\LaravelLocalization\Interfaces\LocalizedUrlRoutable;
use Spatie\Activitylog\Traits\LogsActivity;

class Taxonomy extends Model implements LocalizedUrlRoutable
{
    //

    use Translatable ,LogsActivity,ResolveRouteBinding;
    protected $table = 'taxonomies';
    public $translationModel = TaxonomyTranslation::class;

    /**
     * Model fillables array
     * @var array
     */
    protected $fillable=[
        'status',
        'sorting',
        'parent',
        'type',
        'gallery_id',
        'icon'

    ];


    public function getForeignKey()
    {
        return 'taxonomy_id';
    }
    /**
     * Translated attributes by model used by (Translatable Package)
     * @var array
     */
    public $translatedAttributes = [
        'name',
        'description',
        'slug'
    ];

    protected static $logAttributes = ['name', 'description','status','sorting','parent','type'];







}
