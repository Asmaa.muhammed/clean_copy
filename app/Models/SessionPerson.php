<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SessionPerson extends Model
{
    //
    protected $table = "session_persons";
    public $timestamps = false;

    protected $fillable = [
        'session_id',
        'person_id'
    ];

    public function session (){
        $this->belongsTo(Session::class,'session_id');
    }
    public function person (){
        $this->belongsTo(Person::class,'person_id');
    }
}
