<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Minor extends Model
{
    protected $table="minors";
    protected $fillable=[
        'title',
        'status'
    ];
}
