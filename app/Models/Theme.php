<?php

namespace App\Models;

use App\Foundation\Observers\ThemeObserver;
use App\Foundation\Traits\HasScopesOrObservers;
use Illuminate\Database\Eloquent\Model;

class Theme extends Model
{
    use HasScopesOrObservers;

    public static $customObserver = ThemeObserver::class;

    protected $fillable = [
        "name",
        "path",
        'thumbnail',
        "status",
    ];
}
