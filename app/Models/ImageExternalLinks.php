<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImageExternalLinks extends Model
{
    //
    protected $fillable = [
        'link'
    ];
    protected $table ="image_external_links";

    public $timestamps = false;

    public function getTargetAttribute()
    {
        return $this->link;
    }
}
