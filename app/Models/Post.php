<?php

namespace App\Models;

use App\Foundation\Scopes\PostScope;
use Illuminate\Database\Eloquent\Model;

class Post extends Node
{
    //
    protected static function boot(){
        parent::boot();
        static::addGlobalScope(new PostScope());
    }
    protected $fillable = [
        'status',
        'layout_model_id',
        'type',
    ];
    public function getForeignKey()
    {
        return parent::getForeignKey();
    }

}
