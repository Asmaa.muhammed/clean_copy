<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Gallery extends Model
{
    //
    use LogsActivity;
    /**
     * Model fillables array
     * @var array
     */
    protected $fillable=[
        'title',
        'short_code',
        'status'
    ];
    /**
     * Get all of the gallery's images.
     */
    public function images()
    {
        return $this->morphMany(Image::class, 'imageable')->orderBy('sorting');
    }

}
