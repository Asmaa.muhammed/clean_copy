<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NodeTaxonomy extends Model
{
    //
    protected $table = "node_taxonomies";
    public $timestamps = false;

    protected $fillable = [
        'node_id',
        'taxonomy_id'
    ];

    public function node (){
        $this->belongsTo(Node::class,'node_id');
    }
    public function taxonomy (){
        $this->belongsTo(Taxonomy::class,'taxonomy_id');
    }
}
