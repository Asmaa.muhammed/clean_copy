<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImageInternalLinks extends Model
{
    //
    protected $fillable = [
        'target_type',
        'target_id',
    ];
    protected $table ="image_internal_links";

    public $timestamps = false;

    public function target()
    {
        return $this->morphTo();
    }
}
