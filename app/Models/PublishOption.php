<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PublishOption extends Model
{

    protected $fillable = [
        'start_publishing',
        'end_publishing',
        'show_in_main_page'
    ];
    public function publishable()
    {
        return $this->morphTo();
    }
}
