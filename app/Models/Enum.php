<?php

namespace App\Models;

use App\Foundation\Traits\TranslationRequest;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Spatie\Activitylog\Traits\LogsActivity;

class Enum extends Model
{

    use Translatable  , LogsActivity;

    public $translationModel = EnumTranslation::class;
    /**
     * Model fillables array
     * @var array
     */
    protected $fillable=[
        'status',
        'value',
        'parent_id',
    ];

    /**
     * Translated attributes by model used by (Translatable Package)
     * @var array
     */
    public $translatedAttributes = [
        'name',
        'description',
    ];
    /**
     * activities logs attributes by model used by (Activity Log Package)
     * @var array
     */
    protected static $logAttributes = ['name', 'description','status','value','parent_id'];

    /**
     * Enum nested children
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(Enum::class, 'parent_id');
    }
    /**
     * Checks if the enum has a children
     * @return bool
     */
    public function hasChildren()
    {
        return $this->children()->exists();
    }
    /**
     * Get the child enum's parent, returns null if root enum
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Enum::class, 'parent_id');
    }
    /**
     * quick create for seeder
     * @return  array
     */

    public static function quickCreate($name=null, $parent=0, $value=null){
        $enumArr=['name:en'=>$name, 'parent_id'=>$parent, 'status'=>1, 'value'=>$value];
        return static::create($enumArr);
    }
    /**
     * quick create enum Children for seeder
     * @return  array
     */


    public static function quickCreateGroup($groupName, $children)
    {
        $enumGroup= static::quickCreate($groupName,0,Str::slug($groupName));
        $instances= [];
        foreach ($children as $child => $value){
            $instances[]=static::quickCreate($child, $enumGroup->id, $value);
        }
        return collect($instances);
    }

}
