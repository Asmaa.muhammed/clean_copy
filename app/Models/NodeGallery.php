<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NodeGallery extends Model
{
    //
    protected $table = "node_galleries";
    public $timestamps = false;
    protected $fillable = [
        'node_id',
        'gallery_id'
    ];

    public function gallery (){
        $this->belongsTo(Gallery::class,'gallery_id');
    }
    public function node (){
        $this->belongsTo(Node::class,'node_id');
    }

}
