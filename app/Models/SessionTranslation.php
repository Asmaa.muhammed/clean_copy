<?php

namespace App\Models;

use Cocur\Slugify\Slugify;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class SessionTranslation extends Model
{
    use Sluggable;

    protected $fillable = ['subject', 'place','location', 'description','session_id'];

    protected $table = 'session_translations';


    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable():array
    {
        return [
            'slug' => [
                'source' => 'subject',
                'onUpdate' => true,

            ]
        ];


    }

    public function getRouteKeyName()
    {
        return 'slug';
    }


}

