<?php

namespace App\Models;

use App\Foundation\Scopes\AuthorScope;
use Illuminate\Database\Eloquent\Model;

class Author extends Taxonomy
{
    //
    protected static function boot(){
        parent::boot();
        static::addGlobalScope(new AuthorScope());
    }
}
