<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EnumTranslation extends Model
{
    //
    /**
     * Model fillables
     * @var array
     */
    protected $fillable=[
        'name',
        'description',
    ];

    protected $table = 'enum_translations';
}
