<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use  Translatable ;
    public $translationModel = ImageTranslation::class;
    /**
     * Model fillables array
     * @var array
     */
    protected $fillable=[
        'name',
        'sorting',
        'image_url',
        'directory_name',
        'target'
    ];
    /**
     * Translated attributes by model used by (Translatable Package)
     * @var array
     */
    public $translatedAttributes =  ['caption', 'image_alt'];

    //
    /**
     * Get the owning Imageable model.
     */
    public function imageable()
    {
        return $this->morphTo();
    }

    public function link()
    {
        return $this->hasOne($this->target ? ImageExternalLinks::class : ImageInternalLinks::class);
    }
}
