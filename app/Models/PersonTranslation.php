<?php

namespace App\Models;

use Cocur\Slugify\Slugify;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class PersonTranslation extends Model
{
    //
    use Sluggable;


    /**
     * Model fillables
     * @var array
     */

    protected $guarded = [];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable():array
    {
        return [
            'slug' => [
                'source' => 'name',
                'onUpdate' => true,

            ]
        ];


    }

    public function getRouteKeyName()
    {
        return 'slug';
    }


    protected $table = 'person_translations';


}

