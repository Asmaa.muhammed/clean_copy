<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuLinkTranslation extends Model
{
    protected $fillable = ['name', 'description'];
}
