<?php

namespace App\Models;

use Cocur\Slugify\Slugify;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class NodeTranslation extends Model
{
    //
    use Sluggable;


    /**
     * Model fillables
     * @var array
     */

    protected $fillable = [
        'title',
        'summary',
        'body',
        'slug'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable():array
    {
        return [
            'slug' => [
                'source' => 'title',
                'onUpdate' => true,

            ]
        ];


    }

    public function getRouteKeyName()
    {
        return 'slug';
    }


    protected $table = 'node_translations';


}

