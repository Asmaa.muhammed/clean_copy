<?php

namespace App\Models;

use App\Foundation\Scopes\TagScope;
use Illuminate\Database\Eloquent\Model;

class Tag extends Taxonomy
{
    //
    protected static function boot(){
        parent::boot();
        static::addGlobalScope(new TagScope());
    }

}
