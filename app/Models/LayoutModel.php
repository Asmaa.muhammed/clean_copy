<?php

namespace App\Models;

use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class LayoutModel extends Model
{
    use FormAccessible;

    protected $fillable = [
        "name",
        "layout_type",
        "type",
        "theme_id",
        "status",
    ];

    public function layoutType()
    {
        return $this->belongsTo(Enum::class, 'layout_type');
    }

    public function theme()
    {
        return $this->belongsTo(Theme::class);
    }

    public function plugins()
    {
        return $this->belongsToMany(Plugin::class, 'layout_model_plugins');
    }

    public function getPluginGroupsAttribute()
    {
        return $this->plugins()->get()->mapToGroups(function ($plugin){
            return [$plugin->position => $plugin->id];
        })->toArray();
    }
}
