<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class MenuLink extends Model
{
    use Translatable;

    protected $fillable = [
        'status',
        'sorting',
        'parent',
        'target',
        'gallery_id',
        'icon',
    ];

    public $translatedAttributes = ['name', 'description'];

    public $translationModel = MenuLinkTranslation::class;

    public function group()
    {
        return $this->belongsTo(MenuGroup::class);
    }

    public function link()
    {
        return $this->hasOne($this->target ? ExternalLink::class : InternalLink::class);
    }

    /**
     * menu-link nested children
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(MenuLink::class, 'parent')->orderBy('sorting',"ASC");
    }
    /**
     * Checks if the menu-link has a children
     * @return bool
     */
    public function hasChildren()
    {
        return $this->children()->exists();
    }
    /**
     * Get the child menu-link's parent, returns null if root enum
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(MenuLink::class, 'parent');
    }

    public function gallery()
    {
        return $this->belongsTo(Gallery::class, 'gallery_id');
    }
}
