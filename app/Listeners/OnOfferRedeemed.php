<?php

namespace App\Listeners;

use App\Events\OfferRedeemed;
use App\Mail\RateAndCommentRedeemedOffer;
use App\Notifications\GetOffer;
use App\Notifications\GetOfferNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;

class OnOfferRedeemed
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OfferRedeemed  $event
     * @return void
     */
    public function handle(OfferRedeemed $event)
    {

        Notification::send($event->offer->advertisement->customer,New GetOfferNotification($event->offer));
        Mail::to($event->offer->advertisement->customer->email)->send(new \App\Mail\GetOffer($event->offer));
    }
}
