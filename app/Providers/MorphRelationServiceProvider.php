<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class MorphRelationServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $models = Finder::create()->files()->in(app_path('Models'))->getIterator();
        $models = collect($models)->mapWithKeys(function (SplFileInfo $model){
            return [
                Str::camel(Str::plural($model->getFilenameWithoutExtension()))
                => app()->getNamespace()."Models\\".$model->getFilenameWithoutExtension(),
            ];
        });
        Relation::morphMap($models->toArray());
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
