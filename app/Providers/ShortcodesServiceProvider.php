<?php

namespace App\Providers;

use App\Models\Plugin;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Webwizo\Shortcodes\Facades\Shortcode;

class ShortcodesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        if(Schema::hasTable('plugins')){
            Shortcode::enable();
            $plugins = Plugin::all();
            foreach ($plugins as $plugin){
                Shortcode::register($plugin->short_code, "App\Shortcodes\\".Str::studly($plugin->name)."Shortcode");
            }
        }
    }
}
