<?php

namespace App\Providers;

use App\Foundation\Contracts\FrontUser;
use App\Foundation\Factories\FrontUserRequestFactory;
use App\Http\Controllers\Admin\CustomerController;
use App\Http\Controllers\Admin\MerchantController;
use App\Http\Requests\Admin\FrontUserRequest;
use App\Models\Customer;
use App\Models\Merchant;
use Illuminate\Support\ServiceProvider;

class ModuleBindingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerFrontUserTypesModulesBindings();
    }

    protected function registerFrontUserTypesModulesBindings()
    {
        $bindings = [
            CustomerController::class => Customer::class,
            MerchantController::class => Merchant::class,
        ];

        foreach ($bindings as $controller => $binding) {
            $this->app
                ->when($controller)
                ->needs(FrontUser::class)
                ->give($binding);
        }
        $this->app->bind(FrontUser::class, \App\Models\FrontUser::class);
        $this->app->bind(FrontUserRequest::class, function ($app){
            return FrontUserRequestFactory::handle($app->request);
        });
    }
}
