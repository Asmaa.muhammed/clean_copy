<?php

namespace App\Providers;

use App\Foundation\Composers\Front\FrontGeneralComposer;
use App\Foundation\Composers\Front\ProfileComposer;
use App\Models\Theme;
use Illuminate\Support\ServiceProvider;

class FrontServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

        view()->composer('*',FrontGeneralComposer::class);

    }
}
