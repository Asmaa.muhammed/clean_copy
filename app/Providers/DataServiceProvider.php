<?php

namespace App\Providers;


use App\Foundation\Composers\AdvertisementComposer;
use App\Foundation\Composers\CategoryComposer;
use App\Foundation\Composers\EnumsComposer;
use App\Foundation\Composers\EventComposer;
use App\Foundation\Composers\FrontUserComposer;
use App\Foundation\Composers\GeneralComposer;
use App\Foundation\Composers\LanguagesComposer;
use App\Foundation\Composers\AdminComposer;
use App\Foundation\Composers\LayoutModelComposer;
use App\Foundation\Composers\MenuLinkComposer;
use App\Foundation\Composers\ModuleComposer;
use App\Foundation\Composers\OfferComposer;
use App\Foundation\Composers\PageComposer;
use App\Foundation\Composers\PlanComposer;
use App\Foundation\Composers\PostComposer;
use App\Foundation\Composers\RoleComposer;
use App\Foundation\Composers\SliderImageComposer;
use App\Foundation\Composers\UserComposer;

use Illuminate\Support\ServiceProvider;

class DataServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

        //
        view()->composer('*',LanguagesComposer::class);
        view()->composer('admin.layout.components.basic_tab',GeneralComposer::class);
        view()->composer("admin.enums.*",EnumsComposer::class);
        view()->composer("admin.categories.*",CategoryComposer::class);
        view()->composer('admin.*', AdminComposer::class);
        view()->composer('admin.acl.roles.*', RoleComposer::class);
        view()->composer('admin.users.*', UserComposer::class);
        view()->composer('admin.menuLinks.*', MenuLinkComposer::class);
        view()->composer('admin.slider_images.*', SliderImageComposer::class);
        view()->composer('admin.modules.*', ModuleComposer::class);
        view()->composer('admin.layoutModels.*', LayoutModelComposer::class);
        view()->composer('admin.pages.*', PageComposer::class);
        view()->composer('admin.posts.*', PostComposer::class);
        view()->composer('admin.events.*', EventComposer::class);
        view()->composer('admin.advertisements.*', AdvertisementComposer::class);

    }
}
