<div class="admission-apply">
    <span class="admission-apply__img" style="background-image: url('{{asset('public/'.$themePath."/")}}/assets/media/image2.png')"></span>
    <h6 class="big-title big-title--secondary">UNIVERSITY ADMISSION NOW OPEN</h6>
    <button type="button" class="btn btn btn-danger text-white">APPLY NOW</button>
</div>
