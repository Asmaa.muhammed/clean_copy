<?php

/*
|--------------------------------------------------------------------------
| front Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Artisan;

Route::get("test",function(){
    try{
        Artisan::call("optimize:clear");
        Artisan::call("config:clear");
    }catch(Exception $e){

    }
    return 2;
});


Route::group(['middleware' => ['web']], function () {
    Route::group(
        [
            'prefix' => LaravelLocalization::setLocale(),
            'middleware' => ['localeSessionRedirect', 'localeViewPath']
        ], function () {
        Route::get('/', 'Front\FrontMainController@index')->name('index');
        Route::get('/news-and-events', 'Front\FrontMainController@newsAndEvents')->name('newsAndEvents');
        Route::get('/speakers', 'Front\FrontMainController@speakers')->name('speakers');
        Route::get('/speakers/{person}', 'Front\FrontMainController@showSpeaker')->name('person');

        Route::get(LaravelLocalization::transRoute('routes.page'), 'Front\FrontMainController@PageDetails')->name('page');
        Route::get(LaravelLocalization::transRoute('routes.post'), 'Front\FrontMainController@PostDetails')->name('post');
        Route::get(LaravelLocalization::transRoute('routes.event'), 'Front\FrontMainController@EventDetails')->name('event');
        Route::get(LaravelLocalization::transRoute('routes.news'), 'Front\FrontMainController@NewsDetails')->name('news');

        Route::get('/search', 'Front\FrontMainController@search')->name('search');
        Route::get('/search/content', 'Front\FrontMainController@search_content')->name('search.content');
    });
});







