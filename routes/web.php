<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['prefix' => 'admin-dashboard/', 'middleware' => 'web'], function () {
    Route::get('change-language/{language}', "Admin\DashboardController@changeLanguage")->name('change_language');
    Route::resource("users", "Admin\\UserController");
    Route::get('users/{user}/change-password', "Admin\\UserController@changePassword")
        ->name('users.changePassword');
    Route::post('users/{user}/change-password', "Admin\\UserController@submitChangePassword")
        ->name('users.submitChangePassword');
    Route::resource("roles", "Admin\\RoleController");
    Route::resource("permissions", "Admin\\PermissionController");
    Route::resource("modules", "Admin\\ModuleController");
    Auth::routes(['register' => false]);
    Route::resource("enums", "Admin\\EnumController");
    Route::get('enums/show/tree', "Admin\\EnumController@indexTree")->name('enums.tree');
     Route::post('enums/show/tree', "Admin\\EnumController@indexTree")->name('enums.tree');
    Route::resource("categories", "Admin\\CategoryController");
    Route::get('categories/show/tree', "Admin\\CategoryController@indexTree")->name('categories.tree');
    Route::resource("tags", "Admin\\TagController");
    Route::resource("authors", "Admin\\AuthorController");
    Route::resource("customers", "Admin\\CustomerController");
    Route::resource("advertisements", "Admin\\AdvertisementController");
    Route::get('advertisements/{image}/delete', "Admin\\AdvertisementController@deleteImage")->name('advertisements.delete_image');

    Route::get('/', function () {
        return view('auth.login');
    });
    Route::resource('/dashboard', 'Admin\DashboardController')->only([
        'index'
    ]);
    //languages routes
    Route::resource("languages", "Admin\LanguageController");

    Route::resource('menuGroups', "Admin\MenuGroupController");
    Route::get('menuGroups/{menuGroup}/menuLinks/tree', "Admin\MenuLinkController@tree")->name('menuLinks.tree');
    Route::resource('menuGroups/{menuGroup}/menuLinks', "Admin\MenuLinkController");
    Route::post('ajax/getTargetTypeData', "Admin\AjaxController@getTargetTypeData")->name('ajax.get_target_type_data');

    Route::group(['prefix' => 'filemanager'], function () {
        \UniSharp\LaravelFilemanager\Lfm::routes();
    });
    Route::resource('galleries', "Admin\GalleryController");
    Route::resource('minors', "Admin\MinorController");
    Route::resource('galleries/{gallery}/galleryImages', "Admin\GalleryImageController");
    Route::resource('sliders', "Admin\SliderController");
    Route::resource('sliders/{slider}/sliderImages', "Admin\SliderImageController");

    Route::resource('plugins', "Admin\PluginController")->only(['index', 'create']);
    Route::resource('themes', "Admin\ThemeController");


    Route::resource('pages', "Admin\PageController");
    Route::resource('posts', "Admin\PostController");
    Route::resource('events', "Admin\EventController");
    Route::resource('persons', "Admin\PersonController");
    Route::resource('sessions', "Admin\SessionController");

    Route::resource('feedbacks', "Admin\FeedbackController")->only(['index']);

    Route::resource('newsletterSubscribers', "Admin\NewsletterSubscriberController")->only(['index']);



    Route::resource('layoutModels', "Admin\LayoutModelController");


    Route::group(['namespace' => '\JoeDixon\\Translation\\Http\\Controllers'], function ($router) {

        $router->get(config('translation.ui_url') . '/{language}/translations', 'LanguageTranslationController@index')
            ->name('languages.translations.index');

        $router->post(config('translation.ui_url') . '/{language}', 'LanguageTranslationController@update')
            ->name('languages.translations.update');

        $router->get(config('translation.ui_url') . '/{language}/translations/create', 'LanguageTranslationController@create')
            ->name('languages.translations.create');

        $router->post(config('translation.ui_url') . '/{language}/translations', 'LanguageTranslationController@store')
            ->name('languages.translations.store');


    });
});




