<?php

use Illuminate\Http\Request;
use Laravel\Passport\Passport;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function (){

    Passport::routes();

    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user()->load('info');
    });

    Route::resource('offers', 'OfferController')->only(['index', 'show']);

    Route::get('statistics', 'MerchantStatisticsController@get');

    Route::post('scanCustomer', 'ScanUserController@get');
    Route::post('offers/redeem', 'RedeemOfferController@get');
});
