<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sessions', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->boolean("status")->default(0);
            $table->mediumInteger('node_id')->unsigned();
            $table->foreign('node_id')->references('id')->on('nodes')->onDelete('CASCADE');
            $table->integer("day");
            $table->date("date");
            $table->dateTime("time_from");
            $table->dateTime("time_to");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('persons');
    }
}
