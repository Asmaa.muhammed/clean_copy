<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModuleImageTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_translations', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->string("caption",250)->nullable();
            $table->string("image_alt",250)->nullable();
            $table->string('language')->index();
            $table->mediumInteger('image_id')->unsigned();
            $table->foreign('image_id')->references('id')->on('images')->onDelete('CASCADE');
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image_translations');
    }
}
