<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModuleTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_translations', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->string('name',250);
            $table->string('language')->index();
            $table->longText('description')->nullable();
            $table->mediumInteger('module_id')->unsigned();
            $table->foreign('module_id')->references('id')->on('modules')->onDelete('CASCADE');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_translations');
    }
}
