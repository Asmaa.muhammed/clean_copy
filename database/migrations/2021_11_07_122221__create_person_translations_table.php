<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('person_translations', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->string("title",250);
            $table->string("slug",250);
            $table->string("name",250);
            $table->string("position",250);
            $table->string("description",300)->nullable();
            $table->longText("about")->nullable();
            $table->string('language')->index();
            $table->mediumInteger('person_id')->unsigned();
            $table->foreign('person_id')->references('id')->on('persons')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('person_translations');
    }
}
