<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnumTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enum_translations', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->string('name',200);
            $table->text('description')->nullable();
            $table->string('language')->index();
            $table->mediumInteger('enum_id')->unsigned();
            $table->foreign('enum_id')->references('id')->on('enums')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enum_translations');
    }
}
