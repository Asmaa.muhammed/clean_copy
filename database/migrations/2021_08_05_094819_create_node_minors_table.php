<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNodeMinorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('node_minors', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->mediumInteger('node_id')->unsigned();
            $table->foreign('node_id')->references('id')->on('nodes')->onDelete('CASCADE');

            $table->mediumInteger('minor_id')->unsigned();
            $table->foreign('minor_id')->references('id')->on('minors')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('node_minors');
    }
}
