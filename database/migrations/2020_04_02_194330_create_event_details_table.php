<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_details', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('place')->nullable();
            $table->string('organizer')->nullable();
            $table->mediumInteger('node_id')->unsigned();
            $table->foreign('node_id')->references('id')->on('nodes')->onDelete('CASCADE');


            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_details');
    }
}
