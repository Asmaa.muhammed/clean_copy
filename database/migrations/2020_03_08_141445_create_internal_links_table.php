<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInternalLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('internal_links', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->mediumInteger('menu_link_id')->unsigned();
            $table->foreign('menu_link_id')->references('id')->on('menu_links')->onDelete('CASCADE');
            $table->string("target_type");
            $table->integer("target_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('internal_links');
    }
}
