<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLayoutModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('layout_models', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->string("name");
            $table->mediumInteger('layout_type')->unsigned();
            $table->foreign('layout_type')->references('id')->on('enums')->onDelete('CASCADE');
            $table->mediumInteger("type");
            $table->mediumInteger('theme_id')->unsigned();
            $table->foreign('theme_id')->references('id')->on('themes')->onDelete('CASCADE');
            $table->boolean('status')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('layout_models');
    }
}
