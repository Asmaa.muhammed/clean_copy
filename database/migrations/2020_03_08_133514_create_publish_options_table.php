<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePublishOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publish_options', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->string("publishable_type");
            $table->integer("publishable_id");
            $table->dateTime("start_publishing");
            $table->dateTime("end_publishing");
            $table->mediumInteger("publish_by")->unsigned();
            $table->foreign('publish_by')->references('id')->on('users')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publish_options');
    }
}
