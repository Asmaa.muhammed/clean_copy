<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNodeGalleriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('node_galleries', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->mediumInteger('node_id')->unsigned();
            $table->foreign('node_id')->references('id')->on('nodes')->onDelete('CASCADE');
            $table->mediumInteger('gallery_id')->unsigned();
            $table->foreign('gallery_id')->references('id')->on('galleries')->onDelete('CASCADE');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('node_galleries');
    }
}
