<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuLinkTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_link_translations', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->string("name",250);
            $table->longText("description")->nullable();
            $table->string('language')->index();
            $table->mediumInteger('menu_link_id')->unsigned();
            $table->foreign('menu_link_id')->references('id')->on('menu_links')->onDelete('CASCADE');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_link_translations');
    }
}
