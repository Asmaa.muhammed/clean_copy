<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImageExternalLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_external_links', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->mediumInteger('image_id')->unsigned();
            $table->foreign('image_id')->references('id')->on('images')->onDelete('CASCADE');
            $table->string("link");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image_external_links');
    }
}
