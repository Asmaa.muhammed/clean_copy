<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_links', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->boolean("status");
            $table->integer("sorting");
            $table->integer("parent")->default(0);
            $table->boolean("target");
            $table->string("icon");
            $table->mediumInteger('menu_group_id')->unsigned();
            $table->foreign('menu_group_id')->references('id')->on('menu_groups')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_links');
    }
}
