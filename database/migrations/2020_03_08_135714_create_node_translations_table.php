<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNodeTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('node_translations', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->string("title",250);
            $table->string("slug",250);
            $table->string("summary",300)->nullable();
            $table->longText("body")->nullable();
            $table->string('language')->index();
            $table->mediumInteger('node_id')->unsigned();
            $table->foreign('node_id')->references('id')->on('nodes')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('node_translations');
    }
}
