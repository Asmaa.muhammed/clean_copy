<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImageInternalLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_internal_links', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->mediumInteger('image_id')->unsigned();
            $table->foreign('image_id')->references('id')->on('images')->onDelete('CASCADE');
            $table->string("target_type");
            $table->integer("target_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image_internal_links');
    }
}
