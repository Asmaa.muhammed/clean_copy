<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLayoutModelPluginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('layout_model_plugins', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->mediumInteger('plugin_id')->unsigned();
            $table->foreign('plugin_id')->references('id')->on('plugins')->onDelete('CASCADE');
            $table->mediumInteger('layout_model_id')->unsigned();
            $table->foreign('layout_model_id')->references('id')->on('layout_models')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('layout_model_plugins');
    }
}
