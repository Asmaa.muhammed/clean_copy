<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(LaratrustSeeder::class);
        $this->call(LanguagesTableSeeder::class);
        $this->call(EnumsTableSeeder::class);
        $this->call(ModulesTableSeeder::class);
        $this->call(MenuLinkEnumsSeeder::class);
        $this->call(MenuGroupSeeder::class);
        $this->call(LayoutModelSeeder::class);
        $this->call(FrontMainThemeSeeder::class);

    }
}
