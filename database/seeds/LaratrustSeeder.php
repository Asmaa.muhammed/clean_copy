<?php

use Faker\Provider\Person;
use Faker\Provider\PhoneNumber;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class LaratrustSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        activity()->disableLogging();
        $this->command->info('Truncating User, Role and Permission tables');
        $this->truncateLaratrustTables();
        $userFaker = new \Faker\Generator();
        $userFaker->addProvider(new Person($userFaker));
        $userFaker->addProvider(new PhoneNumber($userFaker));
        $config = config('laratrust_seeder.role_structure');
        $userPermission = config('laratrust_seeder.permission_structure');
        $mapPermission = collect(config('laratrust_seeder.permissions_map'));

        foreach ($config as $key => $modules) {

            // Create a new role
            $role = \App\Models\Role::create([
                'name' => $key,
                'display_name:en' => ucwords(str_replace('_', ' ', $key)),
                'description:en' => ucwords(str_replace('_', ' ', $key)),
            ]);
            $permissions = [];

            $this->command->info('Creating Role '. strtoupper($key));

            // Reading role permission modules
            foreach ($modules as $module => $value) {

                foreach (explode(',', $value) as $p => $perm) {

                    $permissionValue = $mapPermission->get($perm);

                    $permission = \App\Models\Permission::where('name', $permissionValue . '-' . $module);
                    $permissions[] = $permission->exists() ? $permission->first()->id : \App\Models\Permission::create([
                        'name' => $permissionValue . '-' . $module,
                        'display_name:en' => ucfirst($permissionValue) . ' ' . ucfirst($module),
                        'description:en' => ucfirst($permissionValue) . ' ' . ucfirst($module),
                    ])->id;

                    $this->command->info('Creating Permission to '.$permissionValue.' for '. $module);
                }
            }

            // Attach all permissions to the role
            $role->permissions()->sync($permissions);

            $this->command->info("Creating '{$key}' user");

            // Create default user for each role
            $user = \App\Models\User::create([
                'first_name' => ucwords(str_replace('_', ' ', $key)),
                'email' => $key.'@app.com',
                'last_name' => $userFaker->lastName,
                'phone' => $userFaker->phoneNumber,
                'password' => 'password'
            ]);
            $user->attachRole($role);
        }

        // Creating user with permissions
        if (!empty($userPermission)) {
            foreach ($userPermission as $key => $modules) {

                foreach ($modules as $module => $value) {

                    // Create default user for each permission set
                    $user = \App\Models\User::create([
                        'first_name' => ucwords(str_replace('_', ' ', $key)),
                        'last_name' => $userFaker->lastName,
                        'phone' => $userFaker->phoneNumber,
                        'email' => $key.'@app.com',
                        'password' => 'password',
                        'remember_token' => Str::random(10),
                    ]);
                    $permissions = [];

                    foreach (explode(',', $value) as $p => $perm) {

                        $permissionValue = $mapPermission->get($perm);
                        $permission = \App\Models\Permission::where('name', $permissionValue . '-' . $module);
                        $permissions[] = $permission->exists() ? $permission->first()->id : \App\Models\Permission::create([
                            'name' => $permissionValue . '-' . $module,
                            'display_name:en' => ucfirst($permissionValue) . ' ' . ucfirst($module),
                            'description:en' => ucfirst($permissionValue) . ' ' . ucfirst($module),
                        ])->id;

                        $this->command->info('Creating Permission to '.$permissionValue.' for '. $module);
                    }
                }

                // Attach all permissions to the user
                $user->permissions()->sync($permissions);
            }
        }
    }

    /**
     * Truncates all the laratrust tables and the users table
     *
     * @return    void
     */
    public function truncateLaratrustTables()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('permission_role')->truncate();
        DB::table('permission_user')->truncate();
        DB::table('role_user')->truncate();
        \App\Models\User::truncate();
        \App\Models\Role::truncate();
        \App\Models\Permission::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
