<?php

use Illuminate\Database\Seeder;
use App\Models\Enum;

class EnumsTableSeeder extends Seeder
{
    /**
     * Table name
     * @var string
     */
    protected $table = 'enums';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $enums = [
            'Directions' => ['LTR' => 0, 'RTL' => 1],
            'Gender' => ['Male' => 0, 'Female' => 1],
            'Status' => ['Disabled' => 0, 'Enabled' => 1],
            'AdminNavGroup' => [
                'Content' => 1,
                'Taxonomies' => 2,
                'Users' => '3',
                'Tools' => 4,
                'Merchants' => 5,
                'Subscriptions' => 6,
                'Inquiries' => 7,
            ],
            'taxonomiesTypes' => ['category' => 1, 'tag' => 2, 'author' => 3],
            'nodeTypes' => ['page' => 1, 'post' => 2, 'event' => 3, 'offer' => 4, 'category' => 5],
            'type' => ['Customer' => 0, 'Merchant' => 1],
            'planIntervals' => ['Month' => 'month', 'Day' => 'day', 'Year' => 'year'],
            'discountTypes' => ['Fixed' => 0, 'Percentage' => 1],
            'polarAnswers' => ['No' => 0, 'Yes' => 1],

        ];

        foreach ($enums as $group => $enum) {
            Enum::quickCreateGroup($group, $enum);
        }
    }
}
