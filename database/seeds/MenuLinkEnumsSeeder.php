<?php

use App\Models\Enum;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class MenuLinkEnumsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $records = [
            [
                'name:en' => 'Menu Link Targets',
                'value' => 'menu_link_target',
                'status' => 1,
                'children' => [
                    [
                        'name:en' => 'Internal Target',
                        'value' => 0,
                        'status' => 1,
                    ],
                    [
                        'name:en' => 'External Target',
                        'value' => 1,
                        'status' => 1,
                    ]
                ],
            ],
            [
                'name:en' => 'Internal Targets',
                'value' => 'supported_internal_targets',
                'status' => 1,
                'children' => [
                    [
                        'name:en' => 'Categories',
                        'value' => 'categories',
                        'status' => 1,
                    ],
                    [
                        'name:en' => 'Authors',
                        'value' => 'authors',
                        'status' => 1,
                    ],
                    [
                        'name:en' => 'Pages',
                        'value' => 'pages',
                        'status' => 1,
                    ],
                    [
                        'name:en' => 'Posts',
                        'value' => 'posts',
                        'status' => 1,
                    ],
                    [
                        'name:en' => 'Offers',
                        'value' => 'offers',
                        'status' => 1,
                    ]

                ],
            ],
        ];


        foreach ($records as $record) {
            $parent = Enum::create(Arr::except($record, 'children'));
            $parent->children()->createMany($record['children']);
        }
    }
}
