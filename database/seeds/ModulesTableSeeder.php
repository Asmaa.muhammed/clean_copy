<?php

use Illuminate\Database\Seeder;


class ModulesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $modules =[
            "menuGroups"=>["name"=>"Menus Groups","icon"=>'flaticon-grid-menu',"status"=>1,"group"=>null],
            "menuLinks"=>["name"=>"Menus Links","icon"=>'flaticon-grid-menu',"status"=>0,"group"=>null],
            "pages"=>["name"=>"Pages","icon"=>'fa fa-pager',"status"=>1,"group"=>1],
            "minors"=>["name"=>"Minors","icon"=>'fa fa-images',"status"=>1,"group"=>1],
            "posts"=>["name"=>"Posts","icon"=>'la la-newspaper-o',"status"=>1,"group"=>1],
            "events"=>["name"=>"Events","icon"=>'la la-calendar',"status"=>1,"group"=>1],
            "persons"=>["name"=>"Persons","icon"=>'la la-user',"status"=>1,"group"=>1],
            "sessions"=>["name"=>"Sessions","icon"=>'la la-calendar',"status"=>1,"group"=>1],
            "galleries"=>["name"=>"Galleries","icon"=>'fa fa-images',"status"=>1,"group"=>1],
            "galleryImages"=>["name"=>"Gallery Images","icon"=>'fa fa-images',"status"=>0,"group"=>1],
            "sliders"=>["name"=>"Sliders","icon"=>'fa fa-images',"status"=>1,"group"=>1],
            "sliderImages"=>["name"=>"Slider Images","icon"=>'fa fa-images',"status"=>0,"group"=>1],
            "plugins" => ["name"=>"Plugins","icon"=>'fa fa-plug',"status"=>1, 'group' => 4],
            "categories"=>["name"=>"Categories","icon"=>'la la-list-ol',"status"=>1,"group"=>2],
            "tags"=>["name"=>"Tags","icon"=>'la la-tags',"status"=>1,"group"=>2],
            "authors"=>["name"=>"Authors","icon"=>'flaticon-users-1',"status"=>1,"group"=>2],
            'users'=>["name"=>"Users","icon"=>'la la-users',"status"=>1,"group"=>3],
            "roles"=>["name"=>"ACL","icon"=>'la la-angle-right',"status"=>1,"group"=>3],
            "permissions"=>["name"=>"Permissions","icon"=>'la la-angle-right',"status"=>0,"group"=>3],
            'languages'=>["name"=>"Languages","icon"=>'la la-language',"status"=>1,"group"=>4],
            "modules"=>["name"=>"CMS Modules","icon"=>'flaticon-layers',"status"=>1,"group"=>4],
            "enums"=>["name"=>"ENUMS","icon"=>'flaticon-list',"status"=>1,"group"=>4],
            "themes"=>["name"=>"Themes","icon"=>'flaticon-list',"status"=>1,"group"=>4],
            "layoutModels"=>["name"=>"Layout Models","icon"=>'flaticon-list',"status"=>1,"group"=>4],
            'newsletterSubscribers' => ['name' => 'Newsletter Subscribers', 'icon' => 'las la-newspaper', 'status' => 1, 'group' => 7],
            'feedbacks' => ['name' => 'FeedBack', 'icon' => 'las la-newspaper', 'status' => 1, 'group' => 7],


        ];

        foreach($modules as $route=>$columns){
            \App\Models\Module::quickCreate($route, $columns);

        }

    }
}
