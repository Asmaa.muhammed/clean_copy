<?php

use Illuminate\Database\Seeder;

class FrontMainThemeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\Models\Theme::create([
            'name'=>"clean_copy",
            'path'=>"clean_copy",
            'thumbnail'=>"",
            'status'=>1
        ]);

        \App\Models\Slider::create([
            'title'=>"Main Slider",
            'short_code'=>"main-slider",
            'status'=>1,
            'main'=>'1'
        ]);
        \App\Models\MenuGroup::create([
            'name'=>"Main Menu",
            "short_code"=>"main-menu",
            'status'=>1
        ]);

        $homePageLayout  =  \App\Models\LayoutModel::create([
           'name'=>"Home Page",
            'layout_type'=>43,
            'type'=>1,
            'theme_id'=>1,
            'status'=>1
        ]);
        \App\Models\Page::create([
            'title:en'=> "HomePage",
            'type'=>1,
            'status'=>1,
            'layout_model_id'=>$homePageLayout->id

        ]);


    }
}
