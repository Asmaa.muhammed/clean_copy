<?php

use App\Models\Enum;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class LayoutModelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            [
                'name:en' => 'Layout Positions',
                'value' => 'layout_positions',
                'status' => 1,
                'children' => [
                    [
                        'name:en' => 'Content',
                        'value' => 0,
                        'status' => 1,
                    ],
                    [
                        'name:en' => 'Right Side',
                        'value' => 1,
                        'status' => 1,
                    ],
                    [
                        'name:en' => 'Left Side',
                        'value' => 2,
                        'status' => 1,
                    ],
                ],
            ],
            [
                'name:en' => 'Layout Types',
                'value' => 'layout_types',
                'status' => 1,
                'children' => [
                    [
                        'name:en' => 'Content-Right Side',
                        'value' => json_encode(["sections" => [
                            0,
                            1,
                        ]], true),
                        'status' => 1,
                    ],
                    [
                        'name:en' => 'Left Side-Content',
                        'value' => json_encode(["sections" => [
                            2,
                            0,
                        ]], true),
                        'status' => 1,
                    ],
                    [
                        'name:en' => 'Left Side-Content-Right Side',
                        'value' => json_encode(["sections" => [
                            2,
                            0,
                            1,
                        ]], true),
                        'status' => 1,
                    ],
                    [
                        'name:en' => 'Content',
                        'value' => json_encode(["sections" => [
                            0,
                        ]], true),
                        'status' => 1,
                    ],
                ],
            ],
        ];


        foreach ($records as $record) {
            $parent = Enum::create(Arr::except($record, 'children'));
            $parent->children()->createMany($record['children']);
        }
    }
}
