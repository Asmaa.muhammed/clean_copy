<?php

use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $languages = \Illuminate\Support\Facades\File::directories(resource_path("lang"));
        $iteration = 1;
        foreach ($languages as $language){
            if ( basename($language) != "vendor" && !empty(basename($language))){
                \JoeDixon\Translation\Language::create([
                    'name'=>basename($language),
                    'language'=>basename($language),
                    "status"=>1,
                    'sorting'=>$iteration,
                    'direction' => basename($language) == 'ar' ? 1 :0
                ]);
                $iteration++;
            }
        }

    }
}
