<?php

use App\Models\Enum;
use App\Models\MenuGroup;
use App\Models\Module;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class MenuGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modules = Module::where('status', 1)->with('translations')->get()->groupBy('group');
        $groups =  Enum::whereValue('adminnavgroup')->first()->children->pluck('name', 'value');
        $icons =[
            'Content'=>'flaticon2-website',
            'Taxonomies'=>'flaticon2-tag',
            'Users'=>'flaticon-users-1',
            'Tools'=>'flaticon-tool-1',
            'Inquiries' => 'flaticon-comment',
        ];

        $adminMenu = [
            'name' => 'Admin Menu',
            'short_code' => 'admin_menu',
            'status' => 1,
        ];
        $adminMenu = MenuGroup::create($adminMenu);
        $adminMenuChildren = [];

        foreach ($modules as $group => $module) {
                if (!empty($group)) {
                    $groupName = $groups[$group];
                    $parentLink = [
                        'status' => 1,
                        'sorting' => $group+1,
                        'parent' => 0,
                        'target' => 1,
                        'icon' => $icons[$groupName],
                        'name:en' => $groupName,
                        'link' => [
                            'link' => '#',
                        ]
                    ];
                    $parent = $adminMenu->links()->create(Arr::except($parentLink, 'link'));
                    $parent->link()->create($parentLink['link']);
                    foreach ($module as $singleModule) {
                        $adminMenuChildren[] = [
                            'status' => 1,
                            'sorting' => $singleModule->id,
                            'parent' => $parent->id,
                            'target' => 0,
                            'icon' => $singleModule->icon,
                            'name:en' => $singleModule->name,
                            'link' => [
                                'target_type' => 'modules',
                                'target_id' => $singleModule->id,
                            ]
                        ];
                    }
                }else{
                    foreach ($module as $singleModule){
                        $adminMenuChildren[] = [
                            'status' => 1,
                            'sorting' => $singleModule->id,
                            'parent' => 0,
                            'target' => 0,
                            'icon' => $singleModule->icon,
                            'name:en' => $singleModule->name,
                            'link' => [
                                'target_type' => 'modules',
                                'target_id' => $singleModule->id,
                            ]
                        ];
                    }


                }
            }

        $adminMenuChildren[] = [
            'status' => 1,
            'sorting' => $modules->count() + 1,
            'parent' => 0,
            'target' => 1,
            'icon' => "fas fa-question-circle",
            'name:en' => "Diva Support",
            'link' => [
                'link' => "mailto:support@diva-lab.com",
            ]
        ];



        foreach ($adminMenuChildren as $adminMenuChild){
            $menuLink = $adminMenu->links()->create(Arr::except($adminMenuChild, 'link'));
            $menuLink->link()->create($adminMenuChild['link']);
        }


    }
}
