<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
$factory->define(\App\Models\Category::class, function (Faker $faker) {
    return [
        //
        'name:en'=> $faker->name,
        'status'=>1,
        'type'=>1,
        'parent'=>rand(1,10),
        'sorting'=>rand(0,50)

    ];
});
