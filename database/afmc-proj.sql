-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 13, 2021 at 12:41 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `afmc-proj`
--

-- --------------------------------------------------------

--
-- Table structure for table `diva_activity_log`
--

CREATE TABLE `diva_activity_log` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `log_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `subject_id` mediumint(8) UNSIGNED DEFAULT NULL,
  `subject_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `causer_id` mediumint(8) UNSIGNED DEFAULT NULL,
  `causer_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `properties` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_activity_log`
--

INSERT INTO `diva_activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `causer_id`, `causer_type`, `properties`, `created_at`, `updated_at`) VALUES
(1, 'default', 'created', 3, 'menuGroups', 1, 'users', '[]', '2021-07-11 13:40:19', '2021-07-11 13:40:19'),
(2, 'default', 'updated', 1, 'users', 1, 'users', '{\"attributes\":{\"first_name\":\"Superadministrator\",\"last_name\":\"Doe\",\"email\":\"superadministrator@app.com\",\"phone\":\"644-897-385\",\"password\":\"$2y$10$5RimdJKm.kde5qy8Cb..iO4.n\\/892DVFsyBEWzlaz7lvSHdLHbWqO\",\"status\":1},\"old\":{\"first_name\":\"Superadministrator\",\"last_name\":\"Doe\",\"email\":\"superadministrator@app.com\",\"phone\":\"644-897-385\",\"password\":\"$2y$10$5RimdJKm.kde5qy8Cb..iO4.n\\/892DVFsyBEWzlaz7lvSHdLHbWqO\",\"status\":1}}', '2021-07-11 17:13:38', '2021-07-11 17:13:38'),
(3, 'default', 'created', 2, 'pages', 1, 'users', '{\"attributes\":{\"title\":null,\"summary\":null,\"body\":null,\"status\":1,\"sorting\":null,\"parent\":null,\"type\":1}}', '2021-07-12 07:31:41', '2021-07-12 07:31:41'),
(4, 'default', 'created', 3, 'pages', 1, 'users', '{\"attributes\":{\"title\":null,\"summary\":null,\"body\":null,\"status\":1,\"sorting\":null,\"parent\":null,\"type\":1}}', '2021-07-12 07:42:35', '2021-07-12 07:42:35'),
(5, 'default', 'created', 4, 'pages', 1, 'users', '{\"attributes\":{\"title\":null,\"summary\":null,\"body\":null,\"status\":1,\"sorting\":null,\"parent\":null,\"type\":1}}', '2021-07-12 08:09:39', '2021-07-12 08:09:39'),
(6, 'default', 'created', 2, 'categories', 1, 'users', '{\"attributes\":{\"name\":null,\"description\":null,\"status\":1,\"sorting\":1,\"parent\":0,\"type\":1}}', '2021-07-12 09:12:29', '2021-07-12 09:12:29'),
(7, 'default', 'created', 5, 'pages', 1, 'users', '{\"attributes\":{\"title\":null,\"summary\":null,\"body\":null,\"status\":1,\"sorting\":null,\"parent\":null,\"type\":1}}', '2021-07-12 09:41:18', '2021-07-12 09:41:18'),
(8, 'default', 'created', 6, 'pages', 1, 'users', '{\"attributes\":{\"title\":null,\"summary\":null,\"body\":null,\"status\":1,\"sorting\":null,\"parent\":null,\"type\":1}}', '2021-07-12 09:47:06', '2021-07-12 09:47:06'),
(9, 'default', 'created', 3, 'categories', 1, 'users', '{\"attributes\":{\"name\":null,\"description\":null,\"status\":1,\"sorting\":3,\"parent\":0,\"type\":1}}', '2021-07-12 13:02:09', '2021-07-12 13:02:09'),
(10, 'default', 'created', 7, 'posts', 1, 'users', '{\"attributes\":{\"title\":null,\"summary\":null,\"body\":null,\"status\":1,\"sorting\":null,\"parent\":null,\"type\":2}}', '2021-07-13 05:08:02', '2021-07-13 05:08:02'),
(11, 'default', 'created', 8, 'posts', 1, 'users', '{\"attributes\":{\"title\":null,\"summary\":null,\"body\":null,\"status\":1,\"sorting\":null,\"parent\":null,\"type\":2}}', '2021-07-13 05:09:11', '2021-07-13 05:09:11'),
(12, 'default', 'created', 9, 'posts', 1, 'users', '{\"attributes\":{\"title\":null,\"summary\":null,\"body\":null,\"status\":1,\"sorting\":null,\"parent\":null,\"type\":2}}', '2021-07-13 05:10:38', '2021-07-13 05:10:38'),
(13, 'default', 'created', 10, 'events', 1, 'users', '{\"attributes\":{\"title\":null,\"summary\":null,\"body\":null,\"status\":1,\"sorting\":null,\"parent\":null,\"type\":3}}', '2021-07-13 05:42:40', '2021-07-13 05:42:40'),
(14, 'default', 'created', 11, 'events', 1, 'users', '{\"attributes\":{\"title\":null,\"summary\":null,\"body\":null,\"status\":1,\"sorting\":null,\"parent\":null,\"type\":3}}', '2021-07-13 05:50:20', '2021-07-13 05:50:20'),
(15, 'default', 'created', 12, 'pages', 1, 'users', '{\"attributes\":{\"title\":null,\"summary\":null,\"body\":null,\"status\":1,\"sorting\":null,\"parent\":null,\"type\":1}}', '2021-07-13 08:35:47', '2021-07-13 08:35:47'),
(16, 'default', 'created', 13, 'pages', 1, 'users', '{\"attributes\":{\"title\":null,\"summary\":null,\"body\":null,\"status\":1,\"sorting\":null,\"parent\":null,\"type\":1}}', '2021-07-13 08:37:52', '2021-07-13 08:37:52'),
(17, 'default', 'created', 14, 'pages', 1, 'users', '{\"attributes\":{\"title\":null,\"summary\":null,\"body\":null,\"status\":1,\"sorting\":null,\"parent\":null,\"type\":1}}', '2021-07-13 08:40:25', '2021-07-13 08:40:25');

-- --------------------------------------------------------

--
-- Table structure for table `diva_advertisements`
--

CREATE TABLE `diva_advertisements` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL,
  `display_email` tinyint(1) NOT NULL DEFAULT 0,
  `display_phone` tinyint(1) NOT NULL,
  `enable_swap` tinyint(1) NOT NULL,
  `category_id` mediumint(8) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `location` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_advertisement_offers`
--

CREATE TABLE `diva_advertisement_offers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `advertisement_id` bigint(20) UNSIGNED NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_advertisement_translations`
--

CREATE TABLE `diva_advertisement_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `advertisement_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_api_keys`
--

CREATE TABLE `diva_api_keys` (
  `id` int(10) UNSIGNED NOT NULL,
  `keyable_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyable_id` bigint(20) UNSIGNED DEFAULT NULL,
  `key` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `last_used_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_countries`
--

CREATE TABLE `diva_countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_currencies`
--

CREATE TABLE `diva_currencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `symbol` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_customers`
--

CREATE TABLE `diva_customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `profile_picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `created_by` mediumint(8) UNSIGNED DEFAULT NULL,
  `modified_by` mediumint(8) UNSIGNED DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_customer_addresses`
--

CREATE TABLE `diva_customer_addresses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `country_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_enums`
--

CREATE TABLE `diva_enums` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `value` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_enums`
--

INSERT INTO `diva_enums` (`id`, `status`, `value`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'directions', 0, '2021-07-11 13:34:54', '2021-07-11 13:34:54'),
(2, 1, '0', 1, '2021-07-11 13:34:55', '2021-07-11 13:34:55'),
(3, 1, '1', 1, '2021-07-11 13:34:55', '2021-07-11 13:34:55'),
(4, 1, 'gender', 0, '2021-07-11 13:34:55', '2021-07-11 13:34:55'),
(5, 1, '0', 4, '2021-07-11 13:34:56', '2021-07-11 13:34:56'),
(6, 1, '1', 4, '2021-07-11 13:34:56', '2021-07-11 13:34:56'),
(7, 1, 'status', 0, '2021-07-11 13:34:56', '2021-07-11 13:34:56'),
(8, 1, '0', 7, '2021-07-11 13:34:56', '2021-07-11 13:34:56'),
(9, 1, '1', 7, '2021-07-11 13:34:57', '2021-07-11 13:34:57'),
(10, 1, 'adminnavgroup', 0, '2021-07-11 13:34:57', '2021-07-11 13:34:57'),
(11, 1, '1', 10, '2021-07-11 13:34:57', '2021-07-11 13:34:57'),
(12, 1, '2', 10, '2021-07-11 13:34:57', '2021-07-11 13:34:57'),
(13, 1, '3', 10, '2021-07-11 13:34:57', '2021-07-11 13:34:57'),
(14, 1, '4', 10, '2021-07-11 13:34:58', '2021-07-11 13:34:58'),
(15, 1, '5', 10, '2021-07-11 13:34:58', '2021-07-11 13:34:58'),
(16, 1, '6', 10, '2021-07-11 13:34:58', '2021-07-11 13:34:58'),
(17, 1, '7', 10, '2021-07-11 13:34:58', '2021-07-11 13:34:58'),
(18, 1, 'taxonomiestypes', 0, '2021-07-11 13:34:58', '2021-07-11 13:34:58'),
(19, 1, '1', 18, '2021-07-11 13:34:58', '2021-07-11 13:34:58'),
(20, 1, '2', 18, '2021-07-11 13:34:59', '2021-07-11 13:34:59'),
(21, 1, '3', 18, '2021-07-11 13:34:59', '2021-07-11 13:34:59'),
(22, 1, 'nodetypes', 0, '2021-07-11 13:34:59', '2021-07-11 13:34:59'),
(23, 1, '1', 22, '2021-07-11 13:34:59', '2021-07-11 13:34:59'),
(24, 1, '2', 22, '2021-07-11 13:34:59', '2021-07-11 13:34:59'),
(25, 1, '3', 22, '2021-07-11 13:34:59', '2021-07-11 13:34:59'),
(26, 1, '4', 22, '2021-07-11 13:34:59', '2021-07-11 13:34:59'),
(27, 1, '5', 22, '2021-07-11 13:34:59', '2021-07-11 13:34:59'),
(28, 1, 'type', 0, '2021-07-11 13:34:59', '2021-07-11 13:34:59'),
(29, 1, '0', 28, '2021-07-11 13:35:00', '2021-07-11 13:35:00'),
(30, 1, '1', 28, '2021-07-11 13:35:00', '2021-07-11 13:35:00'),
(31, 1, 'planintervals', 0, '2021-07-11 13:35:00', '2021-07-11 13:35:00'),
(32, 1, 'month', 31, '2021-07-11 13:35:00', '2021-07-11 13:35:00'),
(33, 1, 'day', 31, '2021-07-11 13:35:00', '2021-07-11 13:35:00'),
(34, 1, 'year', 31, '2021-07-11 13:35:00', '2021-07-11 13:35:00'),
(35, 1, 'discounttypes', 0, '2021-07-11 13:35:01', '2021-07-11 13:35:01'),
(36, 1, '0', 35, '2021-07-11 13:35:01', '2021-07-11 13:35:01'),
(37, 1, '1', 35, '2021-07-11 13:35:01', '2021-07-11 13:35:01'),
(38, 1, 'polaranswers', 0, '2021-07-11 13:35:01', '2021-07-11 13:35:01'),
(39, 1, '0', 38, '2021-07-11 13:35:01', '2021-07-11 13:35:01'),
(40, 1, '1', 38, '2021-07-11 13:35:01', '2021-07-11 13:35:01'),
(41, 1, 'menu_link_target', 0, '2021-07-11 13:35:11', '2021-07-11 13:35:11'),
(42, 1, '0', 41, '2021-07-11 13:35:11', '2021-07-11 13:35:11'),
(43, 1, '1', 41, '2021-07-11 13:35:11', '2021-07-11 13:35:11'),
(44, 1, 'supported_internal_targets', 0, '2021-07-11 13:35:12', '2021-07-11 13:35:12'),
(45, 1, 'categories', 44, '2021-07-11 13:35:12', '2021-07-11 13:35:12'),
(46, 1, 'authors', 44, '2021-07-11 13:35:12', '2021-07-11 13:35:12'),
(47, 1, 'pages', 44, '2021-07-11 13:35:12', '2021-07-11 13:35:12'),
(48, 1, 'posts', 44, '2021-07-11 13:35:13', '2021-07-11 13:35:13'),
(49, 1, 'offers', 44, '2021-07-11 13:35:13', '2021-07-11 13:35:13'),
(50, 1, 'layout_positions', 0, '2021-07-11 13:35:19', '2021-07-11 13:35:19'),
(51, 1, '0', 50, '2021-07-11 13:35:20', '2021-07-11 13:35:20'),
(52, 1, '1', 50, '2021-07-11 13:35:20', '2021-07-11 13:35:20'),
(53, 1, '2', 50, '2021-07-11 13:35:20', '2021-07-11 13:35:20'),
(54, 1, 'layout_types', 0, '2021-07-11 13:35:20', '2021-07-11 13:35:20'),
(55, 1, '{\"sections\":[0,1]}', 54, '2021-07-11 13:35:20', '2021-07-11 13:35:20'),
(56, 1, '{\"sections\":[2,0]}', 54, '2021-07-11 13:35:20', '2021-07-11 13:35:20'),
(57, 1, '{\"sections\":[2,0,1]}', 54, '2021-07-11 13:35:21', '2021-07-11 13:35:21'),
(58, 1, '{\"sections\":[0]}', 54, '2021-07-11 13:35:21', '2021-07-11 13:35:21');

-- --------------------------------------------------------

--
-- Table structure for table `diva_enum_translations`
--

CREATE TABLE `diva_enum_translations` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enum_id` mediumint(8) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_enum_translations`
--

INSERT INTO `diva_enum_translations` (`id`, `name`, `description`, `language`, `enum_id`, `created_at`, `updated_at`) VALUES
(1, 'Directions', NULL, 'en', 1, '2021-07-11 13:34:54', '2021-07-11 13:34:54'),
(2, 'LTR', NULL, 'en', 2, '2021-07-11 13:34:55', '2021-07-11 13:34:55'),
(3, 'RTL', NULL, 'en', 3, '2021-07-11 13:34:55', '2021-07-11 13:34:55'),
(4, 'Gender', NULL, 'en', 4, '2021-07-11 13:34:56', '2021-07-11 13:34:56'),
(5, 'Male', NULL, 'en', 5, '2021-07-11 13:34:56', '2021-07-11 13:34:56'),
(6, 'Female', NULL, 'en', 6, '2021-07-11 13:34:56', '2021-07-11 13:34:56'),
(7, 'Status', NULL, 'en', 7, '2021-07-11 13:34:56', '2021-07-11 13:34:56'),
(8, 'Disabled', NULL, 'en', 8, '2021-07-11 13:34:56', '2021-07-11 13:34:56'),
(9, 'Enabled', NULL, 'en', 9, '2021-07-11 13:34:57', '2021-07-11 13:34:57'),
(10, 'AdminNavGroup', NULL, 'en', 10, '2021-07-11 13:34:57', '2021-07-11 13:34:57'),
(11, 'Content', NULL, 'en', 11, '2021-07-11 13:34:57', '2021-07-11 13:34:57'),
(12, 'Taxonomies', NULL, 'en', 12, '2021-07-11 13:34:57', '2021-07-11 13:34:57'),
(13, 'Users', NULL, 'en', 13, '2021-07-11 13:34:57', '2021-07-11 13:34:57'),
(14, 'Tools', NULL, 'en', 14, '2021-07-11 13:34:58', '2021-07-11 13:34:58'),
(15, 'Merchants', NULL, 'en', 15, '2021-07-11 13:34:58', '2021-07-11 13:34:58'),
(16, 'Subscriptions', NULL, 'en', 16, '2021-07-11 13:34:58', '2021-07-11 13:34:58'),
(17, 'Inquiries', NULL, 'en', 17, '2021-07-11 13:34:58', '2021-07-11 13:34:58'),
(18, 'taxonomiesTypes', NULL, 'en', 18, '2021-07-11 13:34:58', '2021-07-11 13:34:58'),
(19, 'category', NULL, 'en', 19, '2021-07-11 13:34:59', '2021-07-11 13:34:59'),
(20, 'tag', NULL, 'en', 20, '2021-07-11 13:34:59', '2021-07-11 13:34:59'),
(21, 'author', NULL, 'en', 21, '2021-07-11 13:34:59', '2021-07-11 13:34:59'),
(22, 'nodeTypes', NULL, 'en', 22, '2021-07-11 13:34:59', '2021-07-11 13:34:59'),
(23, 'page', NULL, 'en', 23, '2021-07-11 13:34:59', '2021-07-11 13:34:59'),
(24, 'post', NULL, 'en', 24, '2021-07-11 13:34:59', '2021-07-11 13:34:59'),
(25, 'event', NULL, 'en', 25, '2021-07-11 13:34:59', '2021-07-11 13:34:59'),
(26, 'offer', NULL, 'en', 26, '2021-07-11 13:34:59', '2021-07-11 13:34:59'),
(27, 'category', NULL, 'en', 27, '2021-07-11 13:34:59', '2021-07-11 13:34:59'),
(28, 'type', NULL, 'en', 28, '2021-07-11 13:35:00', '2021-07-11 13:35:00'),
(29, 'Customer', NULL, 'en', 29, '2021-07-11 13:35:00', '2021-07-11 13:35:00'),
(30, 'Merchant', NULL, 'en', 30, '2021-07-11 13:35:00', '2021-07-11 13:35:00'),
(31, 'planIntervals', NULL, 'en', 31, '2021-07-11 13:35:00', '2021-07-11 13:35:00'),
(32, 'Month', NULL, 'en', 32, '2021-07-11 13:35:00', '2021-07-11 13:35:00'),
(33, 'Day', NULL, 'en', 33, '2021-07-11 13:35:00', '2021-07-11 13:35:00'),
(34, 'Year', NULL, 'en', 34, '2021-07-11 13:35:01', '2021-07-11 13:35:01'),
(35, 'discountTypes', NULL, 'en', 35, '2021-07-11 13:35:01', '2021-07-11 13:35:01'),
(36, 'Fixed', NULL, 'en', 36, '2021-07-11 13:35:01', '2021-07-11 13:35:01'),
(37, 'Percentage', NULL, 'en', 37, '2021-07-11 13:35:01', '2021-07-11 13:35:01'),
(38, 'polarAnswers', NULL, 'en', 38, '2021-07-11 13:35:01', '2021-07-11 13:35:01'),
(39, 'No', NULL, 'en', 39, '2021-07-11 13:35:01', '2021-07-11 13:35:01'),
(40, 'Yes', NULL, 'en', 40, '2021-07-11 13:35:01', '2021-07-11 13:35:01'),
(41, 'Menu Link Targets', NULL, 'en', 41, '2021-07-11 13:35:11', '2021-07-11 13:35:11'),
(42, 'Internal Target', NULL, 'en', 42, '2021-07-11 13:35:11', '2021-07-11 13:35:11'),
(43, 'External Target', NULL, 'en', 43, '2021-07-11 13:35:12', '2021-07-11 13:35:12'),
(44, 'Internal Targets', NULL, 'en', 44, '2021-07-11 13:35:12', '2021-07-11 13:35:12'),
(45, 'Categories', NULL, 'en', 45, '2021-07-11 13:35:12', '2021-07-11 13:35:12'),
(46, 'Authors', NULL, 'en', 46, '2021-07-11 13:35:12', '2021-07-11 13:35:12'),
(47, 'Pages', NULL, 'en', 47, '2021-07-11 13:35:13', '2021-07-11 13:35:13'),
(48, 'Posts', NULL, 'en', 48, '2021-07-11 13:35:13', '2021-07-11 13:35:13'),
(49, 'Offers', NULL, 'en', 49, '2021-07-11 13:35:13', '2021-07-11 13:35:13'),
(50, 'Layout Positions', NULL, 'en', 50, '2021-07-11 13:35:20', '2021-07-11 13:35:20'),
(51, 'Content', NULL, 'en', 51, '2021-07-11 13:35:20', '2021-07-11 13:35:20'),
(52, 'Right Side', NULL, 'en', 52, '2021-07-11 13:35:20', '2021-07-11 13:35:20'),
(53, 'Left Side', NULL, 'en', 53, '2021-07-11 13:35:20', '2021-07-11 13:35:20'),
(54, 'Layout Types', NULL, 'en', 54, '2021-07-11 13:35:20', '2021-07-11 13:35:20'),
(55, 'Content-Right Side', NULL, 'en', 55, '2021-07-11 13:35:20', '2021-07-11 13:35:20'),
(56, 'Left Side-Content', NULL, 'en', 56, '2021-07-11 13:35:20', '2021-07-11 13:35:20'),
(57, 'Left Side-Content-Right Side', NULL, 'en', 57, '2021-07-11 13:35:21', '2021-07-11 13:35:21'),
(58, 'Content', NULL, 'en', 58, '2021-07-11 13:35:21', '2021-07-11 13:35:21');

-- --------------------------------------------------------

--
-- Table structure for table `diva_event_details`
--

CREATE TABLE `diva_event_details` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `place` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `organizer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `node_id` mediumint(8) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_event_details`
--

INSERT INTO `diva_event_details` (`id`, `start_date`, `end_date`, `place`, `organizer`, `node_id`, `created_at`, `updated_at`) VALUES
(1, '2021-07-12', '2021-07-15', NULL, NULL, 10, '2021-07-13 05:42:41', '2021-07-13 05:42:41'),
(2, '2021-07-13', '2021-07-08', NULL, NULL, 11, '2021-07-13 05:50:21', '2021-07-13 05:50:21');

-- --------------------------------------------------------

--
-- Table structure for table `diva_extensions`
--

CREATE TABLE `diva_extensions` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_external_links`
--

CREATE TABLE `diva_external_links` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `menu_link_id` mediumint(8) UNSIGNED NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_external_links`
--

INSERT INTO `diva_external_links` (`id`, `menu_link_id`, `link`) VALUES
(1, 1, '#'),
(2, 2, '#'),
(3, 3, '#'),
(4, 4, '#'),
(5, 5, '#'),
(6, 25, 'mailto:support@diva-lab.com'),
(7, 26, '/AFMC-project/'),
(8, 27, '#'),
(9, 28, 'whyAFMC'),
(10, 29, '#'),
(11, 30, '#'),
(12, 31, '#'),
(13, 32, '#'),
(14, 33, 'vissionMission'),
(15, 34, 'history'),
(16, 35, 'factsFigures'),
(17, 36, 'collegeBoard'),
(18, 37, 'stratgicPlane'),
(19, 38, 'organization'),
(20, 39, 'futureProject'),
(21, 40, 'contactUs'),
(22, 41, '#'),
(23, 42, 'undergraduate'),
(24, 43, '#'),
(25, 44, 'programs'),
(26, 45, 'admissionRequirements'),
(27, 46, 'howToApply'),
(28, 47, 'researches'),
(29, 48, '#'),
(30, 49, 'newsEvents'),
(31, 50, 'collegeMagazine'),
(32, 51, 'pressRelease'),
(33, 52, 'gallery');

-- --------------------------------------------------------

--
-- Table structure for table `diva_failed_jobs`
--

CREATE TABLE `diva_failed_jobs` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_favourites`
--

CREATE TABLE `diva_favourites` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `advertisement_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_feedbacks`
--

CREATE TABLE `diva_feedbacks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `feedback` smallint(6) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_galleries`
--

CREATE TABLE `diva_galleries` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_images`
--

CREATE TABLE `diva_images` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `sorting` int(11) NOT NULL DEFAULT 0,
  `image_url` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `directory_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `imageable_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `imageable_id` mediumint(9) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `target` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_images`
--

INSERT INTO `diva_images` (`id`, `name`, `sorting`, `image_url`, `directory_name`, `imageable_type`, `imageable_id`, `created_at`, `updated_at`, `target`) VALUES
(1, 'image.png', 0, '/storage/photos/all/image.png', 'all', 'pages', 2, '2021-07-12 07:31:42', '2021-07-12 07:31:42', NULL),
(7, 'image.png', 0, '/storage/photos/all/image.png', 'all', 'posts', 7, '2021-07-13 05:26:32', '2021-07-13 05:26:32', NULL),
(8, 'image.png', 0, '/storage/photos/all/image.png', 'all', 'posts', 8, '2021-07-13 05:26:52', '2021-07-13 05:26:52', NULL),
(9, 'image.png', 0, '/storage/photos/all/image.png', 'all', 'posts', 9, '2021-07-13 05:27:16', '2021-07-13 05:27:16', NULL),
(10, 'item01.jpg', 0, '/storage/photos/all/item01.jpg', 'all', 'events', 10, '2021-07-13 05:42:41', '2021-07-13 05:42:41', NULL),
(11, 'item02.jpg', 0, '/storage/photos/all/item02.jpg', 'all', 'events', 11, '2021-07-13 05:50:21', '2021-07-13 05:50:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `diva_image_external_links`
--

CREATE TABLE `diva_image_external_links` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `image_id` mediumint(8) UNSIGNED NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_image_internal_links`
--

CREATE TABLE `diva_image_internal_links` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `image_id` mediumint(8) UNSIGNED NOT NULL,
  `target_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `target_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_image_translations`
--

CREATE TABLE `diva_image_translations` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `caption` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_alt` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_id` mediumint(8) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_internal_links`
--

CREATE TABLE `diva_internal_links` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `menu_link_id` mediumint(8) UNSIGNED NOT NULL,
  `target_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `target_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_internal_links`
--

INSERT INTO `diva_internal_links` (`id`, `menu_link_id`, `target_type`, `target_id`) VALUES
(1, 6, 'modules', 1),
(2, 7, 'modules', 3),
(3, 8, 'modules', 4),
(4, 9, 'modules', 5),
(5, 10, 'modules', 6),
(6, 11, 'modules', 8),
(7, 12, 'modules', 10),
(8, 13, 'modules', 17),
(9, 14, 'modules', 18),
(10, 15, 'modules', 19),
(11, 16, 'modules', 20),
(12, 17, 'modules', 21),
(13, 18, 'modules', 11),
(14, 19, 'modules', 12),
(15, 20, 'modules', 13),
(16, 21, 'modules', 14),
(17, 22, 'modules', 15),
(18, 23, 'modules', 22),
(19, 24, 'modules', 23);

-- --------------------------------------------------------

--
-- Table structure for table `diva_languages`
--

CREATE TABLE `diva_languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sorting` int(11) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `direction` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_languages`
--

INSERT INTO `diva_languages` (`id`, `name`, `language`, `created_at`, `updated_at`, `sorting`, `status`, `direction`, `deleted_at`) VALUES
(1, NULL, 'en', '2021-07-11 13:32:03', '2021-07-11 13:32:03', 0, 0, 0, NULL),
(2, 'ar', 'ar', '2021-07-11 13:34:54', '2021-07-11 13:34:54', 1, 1, 1, NULL),
(3, 'en', 'en', '2021-07-11 13:34:54', '2021-07-11 13:34:54', 2, 1, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `diva_layout_models`
--

CREATE TABLE `diva_layout_models` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `layout_type` mediumint(8) UNSIGNED NOT NULL,
  `type` mediumint(9) NOT NULL,
  `theme_id` mediumint(8) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_layout_models`
--

INSERT INTO `diva_layout_models` (`id`, `name`, `layout_type`, `type`, `theme_id`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Home Page', 43, 1, 1, 1, NULL, '2021-07-11 13:35:22', '2021-07-11 13:35:22'),
(2, 'Posts', 58, 2, 1, 1, NULL, '2021-07-13 05:01:25', '2021-07-13 05:01:25'),
(3, 'Events', 58, 3, 1, 1, NULL, '2021-07-13 05:01:42', '2021-07-13 05:01:42');

-- --------------------------------------------------------

--
-- Table structure for table `diva_layout_model_plugins`
--

CREATE TABLE `diva_layout_model_plugins` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `plugin_id` mediumint(8) UNSIGNED NOT NULL,
  `layout_model_id` mediumint(8) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_menu_groups`
--

CREATE TABLE `diva_menu_groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `short_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_menu_groups`
--

INSERT INTO `diva_menu_groups` (`id`, `name`, `short_code`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Admin Menu', 'admin_menu', 1, '2021-07-11 13:35:13', '2021-07-11 13:35:13'),
(2, 'Main Menu', 'main-menu', 1, '2021-07-11 13:35:22', '2021-07-11 13:35:22'),
(3, 'Gateway', 'gateway', 1, '2021-07-11 13:40:19', '2021-07-11 13:40:19');

-- --------------------------------------------------------

--
-- Table structure for table `diva_menu_links`
--

CREATE TABLE `diva_menu_links` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sorting` int(11) NOT NULL,
  `parent` int(11) NOT NULL DEFAULT 0,
  `target` tinyint(1) NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `menu_group_id` mediumint(8) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `gallery_id` mediumint(8) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_menu_links`
--

INSERT INTO `diva_menu_links` (`id`, `status`, `sorting`, `parent`, `target`, `icon`, `menu_group_id`, `created_at`, `updated_at`, `gallery_id`) VALUES
(1, 1, 2, 0, 1, 'flaticon2-website', 1, '2021-07-11 13:35:13', '2021-07-11 13:35:13', NULL),
(2, 1, 5, 0, 1, 'flaticon-tool-1', 1, '2021-07-11 13:35:13', '2021-07-11 13:35:13', NULL),
(3, 1, 3, 0, 1, 'flaticon2-tag', 1, '2021-07-11 13:35:14', '2021-07-11 13:35:14', NULL),
(4, 1, 4, 0, 1, 'flaticon-users-1', 1, '2021-07-11 13:35:14', '2021-07-11 13:35:14', NULL),
(5, 1, 8, 0, 1, 'flaticon-comment', 1, '2021-07-11 13:35:15', '2021-07-11 13:35:15', NULL),
(6, 1, 1, 0, 0, 'flaticon-grid-menu', 1, '2021-07-11 13:35:15', '2021-07-11 13:35:15', NULL),
(7, 1, 3, 1, 0, 'fa fa-pager', 1, '2021-07-11 13:35:15', '2021-07-11 13:35:15', NULL),
(8, 1, 4, 1, 0, 'la la-newspaper-o', 1, '2021-07-11 13:35:16', '2021-07-11 13:35:16', NULL),
(9, 1, 5, 1, 0, 'la la-calendar', 1, '2021-07-11 13:35:16', '2021-07-11 13:35:16', NULL),
(10, 1, 6, 1, 0, 'fa fa-images', 1, '2021-07-11 13:35:16', '2021-07-11 13:35:16', NULL),
(11, 1, 8, 1, 0, 'fa fa-images', 1, '2021-07-11 13:35:16', '2021-07-11 13:35:16', NULL),
(12, 1, 10, 2, 0, 'fa fa-plug', 1, '2021-07-11 13:35:17', '2021-07-11 13:35:17', NULL),
(13, 1, 17, 2, 0, 'la la-language', 1, '2021-07-11 13:35:17', '2021-07-11 13:35:17', NULL),
(14, 1, 18, 2, 0, 'flaticon-layers', 1, '2021-07-11 13:35:17', '2021-07-11 13:35:17', NULL),
(15, 1, 19, 2, 0, 'flaticon-list', 1, '2021-07-11 13:35:17', '2021-07-11 13:35:17', NULL),
(16, 1, 20, 2, 0, 'flaticon-list', 1, '2021-07-11 13:35:17', '2021-07-11 13:35:17', NULL),
(17, 1, 21, 2, 0, 'flaticon-list', 1, '2021-07-11 13:35:18', '2021-07-11 13:35:18', NULL),
(18, 1, 11, 3, 0, 'la la-list-ol', 1, '2021-07-11 13:35:18', '2021-07-11 13:35:18', NULL),
(19, 1, 12, 3, 0, 'la la-tags', 1, '2021-07-11 13:35:18', '2021-07-11 13:35:18', NULL),
(20, 1, 13, 3, 0, 'flaticon-users-1', 1, '2021-07-11 13:35:18', '2021-07-11 13:35:18', NULL),
(21, 1, 14, 4, 0, 'la la-users', 1, '2021-07-11 13:35:18', '2021-07-11 13:35:18', NULL),
(22, 1, 15, 4, 0, 'la la-angle-right', 1, '2021-07-11 13:35:18', '2021-07-11 13:35:18', NULL),
(23, 1, 22, 5, 0, 'las la-newspaper', 1, '2021-07-11 13:35:19', '2021-07-11 13:35:19', NULL),
(24, 1, 23, 5, 0, 'las la-newspaper', 1, '2021-07-11 13:35:19', '2021-07-11 13:35:19', NULL),
(25, 1, 7, 0, 1, 'fas fa-question-circle', 1, '2021-07-11 13:35:19', '2021-07-11 13:35:19', NULL),
(26, 1, 1, 0, 1, 'fas fa-align-center', 2, '2021-07-11 13:37:29', '2021-07-11 13:37:29', NULL),
(27, 1, 2, 0, 1, 'fas fa-align-center', 2, '2021-07-11 13:38:03', '2021-07-11 13:38:03', NULL),
(28, 1, 1, 27, 1, 'fas fa-align-center', 2, '2021-07-11 13:39:54', '2021-07-11 13:39:54', NULL),
(29, 1, 1, 0, 1, 'fas fa-align-justify', 3, '2021-07-11 13:41:42', '2021-07-11 13:41:42', NULL),
(30, 1, 2, 0, 1, 'fas fa-align-center', 3, '2021-07-11 14:09:28', '2021-07-11 14:09:28', NULL),
(31, 1, 3, 0, 1, 'fas fa-align-center', 3, '2021-07-11 14:10:50', '2021-07-11 14:10:50', NULL),
(32, 1, 4, 0, 1, 'fas fa-align-center', 3, '2021-07-11 14:11:52', '2021-07-11 14:11:52', NULL),
(33, 1, 2, 27, 1, 'fas fa-align-justify', 2, '2021-07-11 14:24:40', '2021-07-11 14:24:40', NULL),
(34, 1, 3, 27, 1, 'fas fa-align-justify', 2, '2021-07-11 14:25:41', '2021-07-11 14:25:41', NULL),
(35, 1, 4, 27, 1, 'fas fa-align-justify', 2, '2021-07-11 14:29:28', '2021-07-11 14:29:28', NULL),
(36, 1, 5, 27, 1, 'fas fa-align-justify', 2, '2021-07-11 14:31:01', '2021-07-11 14:31:01', NULL),
(37, 1, 6, 27, 1, 'fas fa-align-justify', 2, '2021-07-11 14:32:34', '2021-07-11 14:32:34', NULL),
(38, 1, 7, 27, 1, 'fas fa-align-justify', 2, '2021-07-11 14:33:37', '2021-07-11 14:33:37', NULL),
(39, 1, 8, 27, 1, 'fas fa-align-justify', 2, '2021-07-11 14:34:52', '2021-07-11 14:34:52', NULL),
(40, 1, 9, 27, 1, 'fas fa-align-justify', 2, '2021-07-11 14:35:21', '2021-07-11 14:35:21', NULL),
(41, 1, 3, 0, 1, 'fas fa-align-justify', 2, '2021-07-11 14:37:01', '2021-07-11 14:37:01', NULL),
(42, 1, 1, 41, 1, 'fas fa-align-justify', 2, '2021-07-11 14:38:24', '2021-07-11 14:38:24', NULL),
(43, 1, 2, 41, 1, 'fas fa-align-justify', 2, '2021-07-11 14:39:13', '2021-07-11 14:39:13', NULL),
(44, 1, 1, 43, 1, 'fas fa-align-justify', 2, '2021-07-11 14:40:17', '2021-07-11 14:40:17', NULL),
(45, 1, 2, 43, 1, 'fas fa-align-justify', 2, '2021-07-11 14:44:54', '2021-07-11 14:44:54', NULL),
(46, 1, 3, 43, 1, 'fas fa-align-justify', 2, '2021-07-11 14:46:40', '2021-07-11 14:46:40', NULL),
(47, 1, 4, 0, 1, 'fas fa-align-justify', 2, '2021-07-11 14:48:25', '2021-07-11 15:00:49', NULL),
(48, 1, 5, 0, 1, 'fas fa-align-justify', 2, '2021-07-11 14:49:19', '2021-07-11 15:01:47', NULL),
(49, 1, 1, 48, 1, 'fas fa-align-justify', 2, '2021-07-11 15:03:07', '2021-07-11 15:03:07', NULL),
(50, 1, 2, 48, 1, 'fas fa-align-justify', 2, '2021-07-11 15:03:08', '2021-07-12 10:35:34', NULL),
(51, 1, 3, 48, 1, 'fas fa-align-justify', 2, '2021-07-12 10:42:08', '2021-07-12 10:42:08', NULL),
(52, 1, 4, 48, 1, 'fas fa-align-justify', 2, '2021-07-12 10:56:00', '2021-07-12 10:56:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `diva_menu_link_translations`
--

CREATE TABLE `diva_menu_link_translations` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `menu_link_id` mediumint(8) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_menu_link_translations`
--

INSERT INTO `diva_menu_link_translations` (`id`, `name`, `description`, `language`, `menu_link_id`, `created_at`, `updated_at`) VALUES
(1, 'Content', NULL, 'en', 1, '2021-07-11 13:35:13', '2021-07-11 13:35:13'),
(2, 'Tools', NULL, 'en', 2, '2021-07-11 13:35:14', '2021-07-11 13:35:14'),
(3, 'Taxonomies', NULL, 'en', 3, '2021-07-11 13:35:14', '2021-07-11 13:35:14'),
(4, 'Users', NULL, 'en', 4, '2021-07-11 13:35:14', '2021-07-11 13:35:14'),
(5, 'Inquiries', NULL, 'en', 5, '2021-07-11 13:35:15', '2021-07-11 13:35:15'),
(6, 'Menus Groups', NULL, 'en', 6, '2021-07-11 13:35:15', '2021-07-11 13:35:15'),
(7, 'Pages', NULL, 'en', 7, '2021-07-11 13:35:15', '2021-07-11 13:35:15'),
(8, 'Posts', NULL, 'en', 8, '2021-07-11 13:35:16', '2021-07-11 13:35:16'),
(9, 'Events', NULL, 'en', 9, '2021-07-11 13:35:16', '2021-07-11 13:35:16'),
(10, 'Galleries', NULL, 'en', 10, '2021-07-11 13:35:16', '2021-07-11 13:35:16'),
(11, 'Sliders', NULL, 'en', 11, '2021-07-11 13:35:17', '2021-07-11 13:35:17'),
(12, 'Plugins', NULL, 'en', 12, '2021-07-11 13:35:17', '2021-07-11 13:35:17'),
(13, 'Languages', NULL, 'en', 13, '2021-07-11 13:35:17', '2021-07-11 13:35:17'),
(14, 'CMS Modules', NULL, 'en', 14, '2021-07-11 13:35:17', '2021-07-11 13:35:17'),
(15, 'ENUMS', NULL, 'en', 15, '2021-07-11 13:35:17', '2021-07-11 13:35:17'),
(16, 'Themes', NULL, 'en', 16, '2021-07-11 13:35:17', '2021-07-11 13:35:17'),
(17, 'Layout Models', NULL, 'en', 17, '2021-07-11 13:35:18', '2021-07-11 13:35:18'),
(18, 'Categories', NULL, 'en', 18, '2021-07-11 13:35:18', '2021-07-11 13:35:18'),
(19, 'Tags', NULL, 'en', 19, '2021-07-11 13:35:18', '2021-07-11 13:35:18'),
(20, 'Authors', NULL, 'en', 20, '2021-07-11 13:35:18', '2021-07-11 13:35:18'),
(21, 'Users', NULL, 'en', 21, '2021-07-11 13:35:18', '2021-07-11 13:35:18'),
(22, 'ACL', NULL, 'en', 22, '2021-07-11 13:35:18', '2021-07-11 13:35:18'),
(23, 'Newsletter Subscribers', NULL, 'en', 23, '2021-07-11 13:35:19', '2021-07-11 13:35:19'),
(24, 'FeedBack', NULL, 'en', 24, '2021-07-11 13:35:19', '2021-07-11 13:35:19'),
(25, 'Diva Support', NULL, 'en', 25, '2021-07-11 13:35:19', '2021-07-11 13:35:19'),
(26, 'الرئيسية', NULL, 'ar', 26, '2021-07-11 13:37:29', '2021-07-11 13:37:29'),
(27, 'HOME', NULL, 'en', 26, '2021-07-11 13:37:30', '2021-07-11 13:37:30'),
(28, 'من نحن', NULL, 'ar', 27, '2021-07-11 13:38:03', '2021-07-11 13:38:03'),
(29, 'ABOUT US', NULL, 'en', 27, '2021-07-11 13:38:03', '2021-07-11 13:38:03'),
(30, 'لماذا AFMC?', NULL, 'ar', 28, '2021-07-11 13:39:54', '2021-07-11 13:39:54'),
(31, 'why AFMC?', NULL, 'en', 28, '2021-07-11 13:39:54', '2021-07-11 13:39:54'),
(32, 'العاملين', NULL, 'ar', 29, '2021-07-11 13:41:42', '2021-07-11 14:10:12'),
(33, 'Staff', NULL, 'en', 29, '2021-07-11 13:41:42', '2021-07-11 14:10:13'),
(34, 'الطلاب', NULL, 'ar', 30, '2021-07-11 14:09:28', '2021-07-11 14:09:28'),
(35, 'Students', NULL, 'en', 30, '2021-07-11 14:09:28', '2021-07-11 14:09:28'),
(36, 'الزائرين', NULL, 'ar', 31, '2021-07-11 14:10:50', '2021-07-11 14:10:50'),
(37, 'Visitors', NULL, 'en', 31, '2021-07-11 14:10:50', '2021-07-11 14:10:50'),
(38, 'تواصل معنا', NULL, 'ar', 32, '2021-07-11 14:11:52', '2021-07-11 14:11:52'),
(39, 'CONTACT US', NULL, 'en', 32, '2021-07-11 14:11:52', '2021-07-11 14:11:52'),
(40, 'الرؤية والرسالة', NULL, 'ar', 33, '2021-07-11 14:24:40', '2021-07-11 14:24:40'),
(41, 'vision & mission', NULL, 'en', 33, '2021-07-11 14:24:40', '2021-07-11 14:24:40'),
(42, 'التاريخ', NULL, 'ar', 34, '2021-07-11 14:25:41', '2021-07-11 14:25:41'),
(43, 'history', NULL, 'en', 34, '2021-07-11 14:25:41', '2021-07-11 14:25:41'),
(44, 'حقائق وارقام', NULL, 'ar', 35, '2021-07-11 14:29:28', '2021-07-11 14:29:28'),
(45, 'facts & figures', NULL, 'en', 35, '2021-07-11 14:29:28', '2021-07-11 14:29:28'),
(46, 'سجل الكلية', NULL, 'ar', 36, '2021-07-11 14:31:01', '2021-07-11 14:31:01'),
(47, 'college board', NULL, 'en', 36, '2021-07-11 14:31:01', '2021-07-11 14:31:01'),
(48, 'خطة استراتيجية', NULL, 'ar', 37, '2021-07-11 14:32:34', '2021-07-11 14:32:34'),
(49, 'strategic plane', NULL, 'en', 37, '2021-07-11 14:32:34', '2021-07-11 14:32:34'),
(50, 'المنظمة', NULL, 'ar', 38, '2021-07-11 14:33:37', '2021-07-11 14:33:37'),
(51, 'organization', NULL, 'en', 38, '2021-07-11 14:33:37', '2021-07-11 14:33:37'),
(52, 'المشاريع المستقبلية', NULL, 'ar', 39, '2021-07-11 14:34:52', '2021-07-11 14:34:52'),
(53, 'future projects', NULL, 'en', 39, '2021-07-11 14:34:52', '2021-07-11 14:34:52'),
(54, 'تواصل معنا', NULL, 'ar', 40, '2021-07-11 14:35:21', '2021-07-11 14:35:21'),
(55, 'Contact Us', NULL, 'en', 40, '2021-07-11 14:35:21', '2021-07-11 14:35:21'),
(56, 'القبول', NULL, 'ar', 41, '2021-07-11 14:37:01', '2021-07-11 14:37:01'),
(57, 'ADMISSION', NULL, 'en', 41, '2021-07-11 14:37:01', '2021-07-11 14:37:01'),
(58, 'الجامعية', NULL, 'ar', 42, '2021-07-11 14:38:24', '2021-07-11 14:38:24'),
(59, 'undergraduate', NULL, 'en', 42, '2021-07-11 14:38:24', '2021-07-11 14:38:24'),
(60, 'الدراسات العليا', NULL, 'ar', 43, '2021-07-11 14:39:13', '2021-07-11 14:39:13'),
(61, 'post graduate studies', NULL, 'en', 43, '2021-07-11 14:39:13', '2021-07-11 14:39:13'),
(62, 'البرامج (Master - PHD)', NULL, 'ar', 44, '2021-07-11 14:40:17', '2021-07-11 14:40:17'),
(63, 'programs (Master - PHD)', NULL, 'en', 44, '2021-07-11 14:40:17', '2021-07-11 14:40:17'),
(64, 'شروط القبول', NULL, 'ar', 45, '2021-07-11 14:44:54', '2021-07-11 14:44:54'),
(65, 'admission requirements', NULL, 'en', 45, '2021-07-11 14:44:54', '2021-07-11 14:44:54'),
(66, 'كيفية التقديم', NULL, 'ar', 46, '2021-07-11 14:46:40', '2021-07-11 14:46:40'),
(67, 'how to apply', NULL, 'en', 46, '2021-07-11 14:46:40', '2021-07-11 14:46:40'),
(68, 'الابحاث', NULL, 'ar', 47, '2021-07-11 14:48:25', '2021-07-11 14:48:25'),
(69, 'RESEARCHES', NULL, 'en', 47, '2021-07-11 14:48:25', '2021-07-11 14:48:25'),
(70, 'المركز الاعلامي', NULL, 'ar', 48, '2021-07-11 14:49:19', '2021-07-11 14:49:19'),
(71, 'MEDIA CENTER', NULL, 'en', 48, '2021-07-11 14:49:20', '2021-07-11 14:49:20'),
(72, 'الاخبار والفاعليات', NULL, 'ar', 49, '2021-07-11 15:03:07', '2021-07-12 10:38:17'),
(73, 'News & events', NULL, 'en', 49, '2021-07-11 15:03:08', '2021-07-12 10:38:17'),
(74, 'مجلة الكلية', NULL, 'ar', 50, '2021-07-11 15:03:08', '2021-07-12 10:39:20'),
(75, 'college Magazine', NULL, 'en', 50, '2021-07-11 15:03:08', '2021-07-12 10:39:20'),
(76, 'الاخبار الصحفية', NULL, 'ar', 51, '2021-07-12 10:42:08', '2021-07-12 10:42:08'),
(77, 'Press Release', NULL, 'en', 51, '2021-07-12 10:42:08', '2021-07-12 10:42:08'),
(78, 'معرض الصور والفيديو', NULL, 'ar', 52, '2021-07-12 10:56:00', '2021-07-12 10:56:00'),
(79, 'Photo & Video Gallery', NULL, 'en', 52, '2021-07-12 10:56:00', '2021-07-12 10:56:00');

-- --------------------------------------------------------

--
-- Table structure for table `diva_migrations`
--

CREATE TABLE `diva_migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_migrations`
--

INSERT INTO `diva_migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2018_06_04_144038_create_countries_table', 1),
(9, '2018_06_04_145256_create_regions_table', 1),
(10, '2018_08_10_013418_create_currencies_table', 1),
(11, '2018_08_29_200844_create_languages_table', 1),
(12, '2018_08_29_205156_create_translations_table', 1),
(13, '2019_04_09_225232_create_api_keys_table', 1),
(14, '2019_08_19_000000_create_failed_jobs_table', 1),
(15, '2020_03_08_122115_create_enums_table', 1),
(16, '2020_03_08_122232_create_enum_translations_table', 1),
(17, '2020_03_08_122315_create_modules_table', 1),
(18, '2020_03_08_123603_create_module_translations_table', 1),
(19, '2020_03_08_125438_create_module_galleries_table', 1),
(20, '2020_03_08_125636_create_module_images_table', 1),
(21, '2020_03_08_132738_create_module_image_translations_table', 1),
(22, '2020_03_08_133514_create_publish_options_table', 1),
(23, '2020_03_08_134152_create_taxonomies_table', 1),
(24, '2020_03_08_134247_create_taxonomy_translations_table', 1),
(25, '2020_03_08_135648_create_nodes_table', 1),
(26, '2020_03_08_135714_create_node_translations_table', 1),
(27, '2020_03_08_140305_create_node_taxonomies_table', 1),
(28, '2020_03_08_140536_create_themes_table', 1),
(29, '2020_03_08_140642_create_layout_models_table', 1),
(30, '2020_03_08_140713_create_layout_plugins_table', 1),
(31, '2020_03_08_140852_create_menu_groups_table', 1),
(32, '2020_03_08_141020_create_menu_links_table', 1),
(33, '2020_03_08_141216_create_menu_link_translations_table', 1),
(34, '2020_03_08_141412_create_external_links_table', 1),
(35, '2020_03_08_141445_create_internal_links_table', 1),
(36, '2020_03_08_141615_create_extensions_table', 1),
(37, '2020_03_08_144400_create_layout_model_plugins_table', 1),
(38, '2020_03_08_154311_modify_languages_table', 1),
(39, '2020_03_09_103914_laratrust_setup_tables', 1),
(40, '2020_03_24_093434_create_activity_log_table', 1),
(41, '2020_03_26_144527_add_icon_to_taxonomies', 1),
(42, '2020_03_30_081303_create_sliders_table', 1),
(43, '2020_03_30_104409_add_target_to_images', 1),
(44, '2020_03_30_115607_create_image_external_links_table', 1),
(45, '2020_03_30_115649_create_image_internal_links_table', 1),
(46, '2020_03_30_133241_add_group_to_modules', 1),
(47, '2020_04_02_194330_create_event_details_table', 1),
(48, '2020_04_02_194912_create_node_galleries_table', 1),
(49, '2020_04_08_190257_create_customers_table', 1),
(50, '2020_04_16_105637_add_gallery_id_to_menu_links_table', 1),
(51, '2020_04_17_231642_add_gallery_id_to_taxonomies_table', 1),
(52, '2020_04_18_004735_add_show_in_main_page_to_publish_options_table', 1),
(53, '2020_04_19_195914_create_feedbacks_table', 1),
(54, '2020_04_19_201910_create_newsletter_subscription_table', 1),
(55, '2020_06_07_093000_modify_taxonomies_table', 1),
(56, '2020_06_07_093232_modify_nodes_table', 1),
(57, '2020_09_28_065750_create_customer_addresses_table', 1),
(58, '2020_09_28_210438_create_advertisements_table', 1),
(59, '2020_09_28_210840_create_advertisement_translations_table', 1),
(60, '2020_09_28_210907_create_advertisement_offers_table', 1),
(61, '2020_09_28_210940_create_swap_items_table', 1),
(62, '2020_09_28_211027_create_favourites_table', 1),
(63, '2020_09_28_220502_create_swap_item_translations_table', 1),
(64, '2020_09_29_224823_add_location_to_advertisements_table', 1),
(65, '2020_10_16_094908_create_social_providers_table', 1),
(66, '2020_11_02_093043_create_notifications_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `diva_modules`
--

CREATE TABLE `diva_modules` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `sorting` int(11) NOT NULL DEFAULT 0,
  `route` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `group` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_modules`
--

INSERT INTO `diva_modules` (`id`, `sorting`, `route`, `icon`, `status`, `deleted_at`, `created_at`, `updated_at`, `group`) VALUES
(1, 1, 'menuGroups', 'flaticon-grid-menu', 1, NULL, '2021-07-11 13:35:03', '2021-07-11 13:35:03', NULL),
(2, 2, 'menuLinks', 'flaticon-grid-menu', 0, NULL, '2021-07-11 13:35:04', '2021-07-11 13:35:04', NULL),
(3, 3, 'pages', 'fa fa-pager', 1, NULL, '2021-07-11 13:35:05', '2021-07-11 13:35:05', 1),
(4, 4, 'posts', 'la la-newspaper-o', 1, NULL, '2021-07-11 13:35:05', '2021-07-11 13:35:05', 1),
(5, 5, 'events', 'la la-calendar', 1, NULL, '2021-07-11 13:35:05', '2021-07-11 13:35:05', 1),
(6, 6, 'galleries', 'fa fa-images', 1, NULL, '2021-07-11 13:35:05', '2021-07-11 13:35:05', 1),
(7, 7, 'galleryImages', 'fa fa-images', 0, NULL, '2021-07-11 13:35:06', '2021-07-11 13:35:06', 1),
(8, 8, 'sliders', 'fa fa-images', 1, NULL, '2021-07-11 13:35:06', '2021-07-11 13:35:06', 1),
(9, 9, 'sliderImages', 'fa fa-images', 0, NULL, '2021-07-11 13:35:06', '2021-07-11 13:35:06', 1),
(10, 10, 'plugins', 'fa fa-plug', 1, NULL, '2021-07-11 13:35:07', '2021-07-11 13:35:07', 4),
(11, 11, 'categories', 'la la-list-ol', 1, NULL, '2021-07-11 13:35:07', '2021-07-11 13:35:07', 2),
(12, 12, 'tags', 'la la-tags', 1, NULL, '2021-07-11 13:35:07', '2021-07-11 13:35:07', 2),
(13, 13, 'authors', 'flaticon-users-1', 1, NULL, '2021-07-11 13:35:08', '2021-07-11 13:35:08', 2),
(14, 14, 'users', 'la la-users', 1, NULL, '2021-07-11 13:35:09', '2021-07-11 13:35:09', 3),
(15, 15, 'roles', 'la la-angle-right', 1, NULL, '2021-07-11 13:35:09', '2021-07-11 13:35:09', 3),
(16, 16, 'permissions', 'la la-angle-right', 0, NULL, '2021-07-11 13:35:09', '2021-07-11 13:35:09', 3),
(17, 17, 'languages', 'la la-language', 1, NULL, '2021-07-11 13:35:09', '2021-07-11 13:35:09', 4),
(18, 18, 'modules', 'flaticon-layers', 1, NULL, '2021-07-11 13:35:10', '2021-07-11 13:35:10', 4),
(19, 19, 'enums', 'flaticon-list', 1, NULL, '2021-07-11 13:35:10', '2021-07-11 13:35:10', 4),
(20, 20, 'themes', 'flaticon-list', 1, NULL, '2021-07-11 13:35:10', '2021-07-11 13:35:10', 4),
(21, 21, 'layoutModels', 'flaticon-list', 1, NULL, '2021-07-11 13:35:11', '2021-07-11 13:35:11', 4),
(22, 22, 'newsletterSubscribers', 'las la-newspaper', 1, NULL, '2021-07-11 13:35:11', '2021-07-11 13:35:11', 7),
(23, 23, 'feedbacks', 'las la-newspaper', 1, NULL, '2021-07-11 13:35:11', '2021-07-11 13:35:11', 7);

-- --------------------------------------------------------

--
-- Table structure for table `diva_module_translations`
--

CREATE TABLE `diva_module_translations` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `module_id` mediumint(8) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_module_translations`
--

INSERT INTO `diva_module_translations` (`id`, `name`, `language`, `description`, `module_id`, `created_at`, `updated_at`) VALUES
(1, 'Menus Groups', 'en', NULL, 1, NULL, NULL),
(2, 'Menus Links', 'en', NULL, 2, NULL, NULL),
(3, 'Pages', 'en', NULL, 3, NULL, NULL),
(4, 'Posts', 'en', NULL, 4, NULL, NULL),
(5, 'Events', 'en', NULL, 5, NULL, NULL),
(6, 'Galleries', 'en', NULL, 6, NULL, NULL),
(7, 'Gallery Images', 'en', NULL, 7, NULL, NULL),
(8, 'Sliders', 'en', NULL, 8, NULL, NULL),
(9, 'Slider Images', 'en', NULL, 9, NULL, NULL),
(10, 'Plugins', 'en', NULL, 10, NULL, NULL),
(11, 'Categories', 'en', NULL, 11, NULL, NULL),
(12, 'Tags', 'en', NULL, 12, NULL, NULL),
(13, 'Authors', 'en', NULL, 13, NULL, NULL),
(14, 'Users', 'en', NULL, 14, NULL, NULL),
(15, 'ACL', 'en', NULL, 15, NULL, NULL),
(16, 'Permissions', 'en', NULL, 16, NULL, NULL),
(17, 'Languages', 'en', NULL, 17, NULL, NULL),
(18, 'CMS Modules', 'en', NULL, 18, NULL, NULL),
(19, 'ENUMS', 'en', NULL, 19, NULL, NULL),
(20, 'Themes', 'en', NULL, 20, NULL, NULL),
(21, 'Layout Models', 'en', NULL, 21, NULL, NULL),
(22, 'Newsletter Subscribers', 'en', NULL, 22, NULL, NULL),
(23, 'FeedBack', 'en', NULL, 23, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `diva_newsletter_subscribers`
--

CREATE TABLE `diva_newsletter_subscribers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_nodes`
--

CREATE TABLE `diva_nodes` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `type` smallint(6) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `layout_model_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_nodes`
--

INSERT INTO `diva_nodes` (`id`, `status`, `type`, `deleted_at`, `created_at`, `updated_at`, `layout_model_id`) VALUES
(1, 1, 1, NULL, '2021-07-11 13:35:22', '2021-07-11 13:35:22', 1),
(2, 1, 1, NULL, '2021-07-12 07:31:41', '2021-07-12 07:31:41', 1),
(3, 1, 1, NULL, '2021-07-12 07:42:35', '2021-07-12 07:42:35', 1),
(4, 1, 1, NULL, '2021-07-12 08:09:39', '2021-07-12 08:09:39', 1),
(5, 1, 1, NULL, '2021-07-12 09:41:17', '2021-07-12 09:41:17', 1),
(6, 1, 1, NULL, '2021-07-12 09:47:05', '2021-07-12 09:47:05', 1),
(7, 1, 2, NULL, '2021-07-13 05:08:02', '2021-07-13 05:08:02', 2),
(8, 1, 2, NULL, '2021-07-13 05:09:11', '2021-07-13 05:09:11', 2),
(9, 1, 2, NULL, '2021-07-13 05:10:37', '2021-07-13 05:10:37', 2),
(10, 1, 3, NULL, '2021-07-13 05:42:40', '2021-07-13 05:42:40', 3),
(11, 1, 3, NULL, '2021-07-13 05:50:20', '2021-07-13 05:50:20', 3),
(12, 1, 1, NULL, '2021-07-13 08:35:47', '2021-07-13 08:35:47', 1),
(13, 1, 1, NULL, '2021-07-13 08:37:52', '2021-07-13 08:37:52', 1),
(14, 1, 1, NULL, '2021-07-13 08:40:24', '2021-07-13 08:40:24', 1);

-- --------------------------------------------------------

--
-- Table structure for table `diva_node_galleries`
--

CREATE TABLE `diva_node_galleries` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `node_id` mediumint(8) UNSIGNED NOT NULL,
  `gallery_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_node_taxonomies`
--

CREATE TABLE `diva_node_taxonomies` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `node_id` mediumint(8) UNSIGNED NOT NULL,
  `taxonomy_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_node_taxonomies`
--

INSERT INTO `diva_node_taxonomies` (`id`, `node_id`, `taxonomy_id`) VALUES
(1, 8, 3),
(2, 7, 3),
(3, 9, 2),
(4, 9, 3),
(5, 8, 2);

-- --------------------------------------------------------

--
-- Table structure for table `diva_node_translations`
--

CREATE TABLE `diva_node_translations` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `summary` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `node_id` mediumint(8) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_node_translations`
--

INSERT INTO `diva_node_translations` (`id`, `title`, `slug`, `summary`, `body`, `language`, `node_id`, `created_at`, `updated_at`) VALUES
(1, 'HomePage', 'HomePage', NULL, NULL, 'en', 1, '2021-07-11 13:35:22', '2021-07-11 13:35:22'),
(2, 'لماذا  AFMC?', 'لماذا_AFMC_', NULL, '<article>\r\n  <p>قيم في التاسع من يناير 2018 تحت رعاية اللواء أيمن الشافعي مدير المركز العربي لطب العيون ، بإشراف الأستاذة مها حمدي رئيس قسم علم الأحياء الدقيقة والمناعة بالمركز الطبي ، ونظمه أعضاء طاقم طبي ...\r\n                                        أقيم في 9 يناير 2018 تحت رعاية اللواء البحر المتوسط ​​أيمن الشافعي مدير الاتحاد الآسيوي لكرة القدم.</p>\r\n                                </article>', 'ar', 2, '2021-07-12 07:31:41', '2021-07-12 07:31:41'),
(3, 'Why AFCM?', 'Why_AFCM_', NULL, '<article>\r\n                                    <p>It was held on 9th of JAN 2018 under patronage of Maj.Gen.Med.Ayman Shafei director of the AFCM, directed by Prof. Maha Hamdy head of medical microbiology and immunology department, AFCM and organized by staff members of Medical…\r\n                                        It was held on 9th of JAN 2018 under patronage of Maj.Gen.Med.Ayman Shafei director of the AFCM</p>\r\n                                </article>', 'en', 2, '2021-07-12 07:31:41', '2021-07-12 07:31:41'),
(4, 'حقائق وارقام', 'حقائق_وارقام', NULL, '<div class=\"video-presentation\">\r\n              <div class=\"video-presentation__iframe\">\r\n                  <iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/SMQd32bevDI\"\r\n                          title=\"YouTube video player\" frameborder=\"0\"\r\n                          allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\"\r\n                          allowfullscreen></iframe>\r\n              </div>\r\n\r\n              <div class=\"video-presentation__dis\">\r\n                  <span class=\"video-presentation__float\">\r\n                      <span>At a Glance</span>\r\n                  </span>\r\n\r\n                  <div class=\"row\">\r\n                      <div class=\"col-lg-4\">\r\n                          <div class=\"video-presentation__num\">\r\n                              <span>5.038</span>\r\n                              <p>Total Enrollment</p>\r\n                          </div>\r\n                      </div>\r\n                      <div class=\"col-lg-4\">\r\n                          <div class=\"video-presentation__num\">\r\n                              <span>10000+</span>\r\n                              <p>Graduates</p>\r\n                          </div>\r\n                      </div>\r\n                      <div class=\"col-lg-4\">\r\n                          <div class=\"video-presentation__num\">\r\n                              <span>500+</span>\r\n                              <p>Post Graduates Students</p>\r\n                          </div>\r\n                      </div>\r\n                  </div>\r\n              </div>\r\n          </div>\r\n      </div>\r\n  </section>\r\n  <section class=\"section section--padding\">\r\n      <div class=\"container\">\r\n          <div class=\"section__header\">\r\n              <div class=\"section__title\">\r\n                  <span>(Updated as of February 21, 2020)</span>\r\n                  <h2 class=\"section-title\">2019-2020 Quick Facts</h2>\r\n              </div>\r\n          </div>\r\n\r\n          <div class=\"accordion accordion-flush main-accordion\" id=\"accordionFlushExample\">\r\n              <div class=\"accordion-item\">\r\n                  <h2 class=\"accordion-header\" id=\"flush-headingOne\">\r\n                      <button class=\"accordion-button collapsed\" type=\"button\" data-bs-toggle=\"collapse\"\r\n                              data-bs-target=\"#flush-collapseOne\" aria-expanded=\"false\"\r\n                              aria-controls=\"flush-collapseOne\">\r\n                          students and faculty\r\n                      </button>\r\n                  </h2>\r\n                  <div id=\"flush-collapseOne\" class=\"accordion-collapse collapse\"\r\n                          aria-labelledby=\"flush-headingOne\" data-bs-parent=\"#accordionFlushExample\">\r\n                      <div class=\"accordion-body\">Placeholder content for this accordion, which is intended to\r\n                          demonstrate the <code>.accordion-flush</code> class. This is the first item\'s accordion\r\n                          body.\r\n                      </div>\r\n                  </div>\r\n              </div>\r\n              <div class=\"accordion-item\">\r\n                  <h2 class=\"accordion-header\" id=\"flush-headingTwo\">\r\n                      <button class=\"accordion-button collapsed\" type=\"button\" data-bs-toggle=\"collapse\"\r\n                              data-bs-target=\"#flush-collapseTwo\" aria-expanded=\"false\"\r\n                              aria-controls=\"flush-collapseTwo\">\r\n                          students and faculty\r\n                      </button>\r\n                  </h2>\r\n                  <div id=\"flush-collapseTwo\" class=\"accordion-collapse collapse\"\r\n                          aria-labelledby=\"flush-headingTwo\" data-bs-parent=\"#accordionFlushExample\">\r\n                      <div class=\"accordion-body\">Placeholder content for this accordion, which is intended to\r\n                          demonstrate the <code>.accordion-flush</code> class. This is the second item\'s accordion\r\n                          body. Let\'s imagine this being filled with some actual content.\r\n                      </div>\r\n                  </div>\r\n              </div>\r\n              <div class=\"accordion-item\">\r\n                  <h2 class=\"accordion-header\" id=\"flush-headingThree\">\r\n                      <button class=\"accordion-button collapsed\" type=\"button\" data-bs-toggle=\"collapse\"\r\n                              data-bs-target=\"#flush-collapseThree\" aria-expanded=\"false\"\r\n                              aria-controls=\"flush-collapseThree\">\r\n                          students and faculty\r\n                      </button>\r\n                  </h2>\r\n                  <div id=\"flush-collapseThree\" class=\"accordion-collapse collapse\"\r\n                          aria-labelledby=\"flush-headingThree\" data-bs-parent=\"#accordionFlushExample\">\r\n                      <div class=\"accordion-body\">Placeholder content for this accordion, which is intended to\r\n                          demonstrate the <code>.accordion-flush</code> class. This is the third item\'s accordion\r\n                          body. Nothing more exciting happening here in terms of content, but just filling up the\r\n                          space to make it look, at least at first glance, a bit more representative of how this\r\n                          would look in a real-world application.\r\n                      </div>\r\n                  </div>\r\n              </div>\r\n          </div>', 'ar', 3, '2021-07-12 07:42:35', '2021-07-12 07:42:35'),
(5, 'facts & figures', 'facts_figures', NULL, '<div class=\"video-presentation\">\r\n              <div class=\"video-presentation__iframe\">\r\n                  <iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/SMQd32bevDI\"\r\n                          title=\"YouTube video player\" frameborder=\"0\"\r\n                          allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\"\r\n                          allowfullscreen></iframe>\r\n              </div>\r\n\r\n              <div class=\"video-presentation__dis\">\r\n                  <span class=\"video-presentation__float\">\r\n                      <span>At a Glance</span>\r\n                  </span>\r\n\r\n                  <div class=\"row\">\r\n                      <div class=\"col-lg-4\">\r\n                          <div class=\"video-presentation__num\">\r\n                              <span>5.038</span>\r\n                              <p>Total Enrollment</p>\r\n                          </div>\r\n                      </div>\r\n                      <div class=\"col-lg-4\">\r\n                          <div class=\"video-presentation__num\">\r\n                              <span>10000+</span>\r\n                              <p>Graduates</p>\r\n                          </div>\r\n                      </div>\r\n                      <div class=\"col-lg-4\">\r\n                          <div class=\"video-presentation__num\">\r\n                              <span>500+</span>\r\n                              <p>Post Graduates Students</p>\r\n                          </div>\r\n                      </div>\r\n                  </div>\r\n              </div>\r\n          </div>\r\n      </div>\r\n  </section>\r\n  <section class=\"section section--padding\">\r\n      <div class=\"container\">\r\n          <div class=\"section__header\">\r\n              <div class=\"section__title\">\r\n                  <span>(Updated as of February 21, 2020)</span>\r\n                  <h2 class=\"section-title\">2019-2020 Quick Facts</h2>\r\n              </div>\r\n          </div>\r\n\r\n          <div class=\"accordion accordion-flush main-accordion\" id=\"accordionFlushExample\">\r\n              <div class=\"accordion-item\">\r\n                  <h2 class=\"accordion-header\" id=\"flush-headingOne\">\r\n                      <button class=\"accordion-button collapsed\" type=\"button\" data-bs-toggle=\"collapse\"\r\n                              data-bs-target=\"#flush-collapseOne\" aria-expanded=\"false\"\r\n                              aria-controls=\"flush-collapseOne\">\r\n                          students and faculty\r\n                      </button>\r\n                  </h2>\r\n                  <div id=\"flush-collapseOne\" class=\"accordion-collapse collapse\"\r\n                          aria-labelledby=\"flush-headingOne\" data-bs-parent=\"#accordionFlushExample\">\r\n                      <div class=\"accordion-body\">Placeholder content for this accordion, which is intended to\r\n                          demonstrate the <code>.accordion-flush</code> class. This is the first item\'s accordion\r\n                          body.\r\n                      </div>\r\n                  </div>\r\n              </div>\r\n              <div class=\"accordion-item\">\r\n                  <h2 class=\"accordion-header\" id=\"flush-headingTwo\">\r\n                      <button class=\"accordion-button collapsed\" type=\"button\" data-bs-toggle=\"collapse\"\r\n                              data-bs-target=\"#flush-collapseTwo\" aria-expanded=\"false\"\r\n                              aria-controls=\"flush-collapseTwo\">\r\n                          students and faculty\r\n                      </button>\r\n                  </h2>\r\n                  <div id=\"flush-collapseTwo\" class=\"accordion-collapse collapse\"\r\n                          aria-labelledby=\"flush-headingTwo\" data-bs-parent=\"#accordionFlushExample\">\r\n                      <div class=\"accordion-body\">Placeholder content for this accordion, which is intended to\r\n                          demonstrate the <code>.accordion-flush</code> class. This is the second item\'s accordion\r\n                          body. Let\'s imagine this being filled with some actual content.\r\n                      </div>\r\n                  </div>\r\n              </div>\r\n              <div class=\"accordion-item\">\r\n                  <h2 class=\"accordion-header\" id=\"flush-headingThree\">\r\n                      <button class=\"accordion-button collapsed\" type=\"button\" data-bs-toggle=\"collapse\"\r\n                              data-bs-target=\"#flush-collapseThree\" aria-expanded=\"false\"\r\n                              aria-controls=\"flush-collapseThree\">\r\n                          students and faculty\r\n                      </button>\r\n                  </h2>\r\n                  <div id=\"flush-collapseThree\" class=\"accordion-collapse collapse\"\r\n                          aria-labelledby=\"flush-headingThree\" data-bs-parent=\"#accordionFlushExample\">\r\n                      <div class=\"accordion-body\">Placeholder content for this accordion, which is intended to\r\n                          demonstrate the <code>.accordion-flush</code> class. This is the third item\'s accordion\r\n                          body. Nothing more exciting happening here in terms of content, but just filling up the\r\n                          space to make it look, at least at first glance, a bit more representative of how this\r\n                          would look in a real-world application.\r\n                      </div>\r\n                  </div>\r\n              </div>\r\n          </div>', 'en', 3, '2021-07-12 07:42:35', '2021-07-12 07:42:35'),
(6, 'سجل الكلية', 'سجل_الكلية', NULL, '<div class=\"tabs\">\r\n                <ul class=\"nav nav-pills\" id=\"pills-tab\" role=\"tablist\">\r\n                    <li class=\"nav-item\" role=\"presentation\">\r\n                        <button class=\"nav-link\" id=\"pills-home-tab\" data-bs-toggle=\"pill\" data-bs-target=\"#pills-home\" type=\"button\" role=\"tab\" aria-controls=\"pills-home\" aria-selected=\"true\">supreme council</button>\r\n                    </li>\r\n                    <li class=\"nav-item\" role=\"presentation\">\r\n                        <button class=\"nav-link active\" id=\"pills-profile-tab\" data-bs-toggle=\"pill\" data-bs-target=\"#pills-profile\" type=\"button\" role=\"tab\" aria-controls=\"pills-profile\" aria-selected=\"false\">educational board</button>\r\n                    </li>\r\n                    <li class=\"nav-item\" role=\"presentation\">\r\n                        <button class=\"nav-link\" id=\"pills-contact-tab\" data-bs-toggle=\"pill\" data-bs-target=\"#pills-contact\" type=\"button\" role=\"tab\" aria-controls=\"pills-contact\" aria-selected=\"false\">academic departments</button>\r\n                    </li>\r\n                </ul>\r\n                <div class=\"tab-content\" id=\"pills-tabContent\">\r\n                    <div class=\"tab-pane fade\" id=\"pills-home\" role=\"tabpanel\" aria-labelledby=\"pills-home-tab\">\r\n                        <div class=\"id-cards\">\r\n                            <div class=\"row\">\r\n                                <div class=\"col-lg-4\">\r\n                                    <div class=\"id-card__item\">\r\n                                        <span class=\"id-card__img\" style=\"background-image: url(\'{{asset(\'public/\'.$themePath.\"/\")}}/assets/images/card.png\')\"></span>\r\n                                        <div class=\"id-card__dis\">\r\n                                            <h5 class=\"semi-big-title\">Prof. Ashraf Mahmoud Ibrahim\r\n                                                Hatem</h5>\r\n                                            <p class=\"secondary-dis\">President of supreme council of AFCM</p>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-lg-4\">\r\n                                    <div class=\"id-card__item\">\r\n                                        <span class=\"id-card__img\" style=\"background-image: url(\'{{asset(\'public/\'.$themePath.\"/\")}}/assets/images/card.png\')\"></span>\r\n                                        <div class=\"id-card__dis\">\r\n                                            <h5 class=\"semi-big-title\">Prof. Ashraf Mahmoud Ibrahim\r\n                                                Hatem</h5>\r\n                                            <p class=\"secondary-dis\">President of supreme council of AFCM</p>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-lg-4\">\r\n                                    <div class=\"id-card__item\">\r\n                                        <span class=\"id-card__img\" style=\"background-image: url(\'{{asset(\'public/\'.$themePath.\"/\")}}/assets/images/card.png\')\"></span>\r\n                                        <div class=\"id-card__dis\">\r\n                                            <h5 class=\"semi-big-title\">Prof. Ashraf Mahmoud Ibrahim\r\n                                                Hatem</h5>\r\n                                            <p class=\"secondary-dis\">President of supreme council of AFCM</p>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-lg-4\">\r\n                                    <div class=\"id-card__item\">\r\n                                        <span class=\"id-card__img\" style=\"background-image: url(\'{{asset(\'public/\'.$themePath.\"/\")}}/assets/images/card.png\')\"></span>\r\n                                        <div class=\"id-card__dis\">\r\n                                            <h5 class=\"semi-big-title\">Prof. Ashraf Mahmoud Ibrahim\r\n                                                Hatem</h5>\r\n                                            <p class=\"secondary-dis\">President of supreme council of AFCM</p>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-lg-4\">\r\n                                    <div class=\"id-card__item\">\r\n                                        <span class=\"id-card__img\" style=\"background-image: url(\'{{asset(\'public/\'.$themePath.\"/\")}}/assets/images/card.png\')\"></span>\r\n                                        <div class=\"id-card__dis\">\r\n                                            <h5 class=\"semi-big-title\">Prof. Ashraf Mahmoud Ibrahim\r\n                                                Hatem</h5>\r\n                                            <p class=\"secondary-dis\">President of supreme council of AFCM</p>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-lg-4\">\r\n                                    <div class=\"id-card__item\">\r\n                                        <span class=\"id-card__img\" style=\"background-image: url(\'{{asset(\'public/\'.$themePath.\"/\")}}/assets/images/card.png\')\"></span>\r\n                                        <div class=\"id-card__dis\">\r\n                                            <h5 class=\"semi-big-title\">Prof. Ashraf Mahmoud Ibrahim\r\n                                                Hatem</h5>\r\n                                            <p class=\"secondary-dis\">President of supreme council of AFCM</p>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"tab-pane fade show active\" id=\"pills-profile\" role=\"tabpanel\" aria-labelledby=\"pills-profile-tab\">\r\n                        <div class=\"id-cards\">\r\n                            <div class=\"row\">\r\n                                <div class=\"col-lg-4\">\r\n                                    <div class=\"id-card__item\">\r\n                                        <span class=\"id-card__img\" style=\"background-image: url(\'{{asset(\'public/\'.$themePath.\"/\")}}/assets/images/card.png\')\"></span>\r\n                                        <div class=\"id-card__dis\">\r\n                                            <h5 class=\"semi-big-title\">Prof. Ashraf Mahmoud Ibrahim\r\n                                                Hatem</h5>\r\n                                            <p class=\"secondary-dis\">President of supreme council of AFCM</p>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-lg-4\">\r\n                                    <div class=\"id-card__item\">\r\n                                        <span class=\"id-card__img\" style=\"background-image: url(\'{{asset(\'public/\'.$themePath.\"/\")}}/assets/images/card.png\')\"></span>\r\n                                        <div class=\"id-card__dis\">\r\n                                            <h5 class=\"semi-big-title\">Prof. Ashraf Mahmoud Ibrahim\r\n                                                Hatem</h5>\r\n                                            <p class=\"secondary-dis\">President of supreme council of AFCM</p>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-lg-4\">\r\n                                    <div class=\"id-card__item\">\r\n                                        <span class=\"id-card__img\" style=\"background-image: url(\'{{asset(\'public/\'.$themePath.\"/\")}}/assets/images/card.png\')\"></span>\r\n                                        <div class=\"id-card__dis\">\r\n                                            <h5 class=\"semi-big-title\">Prof. Ashraf Mahmoud Ibrahim\r\n                                                Hatem</h5>\r\n                                            <p class=\"secondary-dis\">President of supreme council of AFCM</p>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"tab-pane fade\" id=\"pills-contact\" role=\"tabpanel\" aria-labelledby=\"pills-contact-tab\">\r\n                        <div class=\"id-cards\">\r\n                            <div class=\"row\">\r\n                                <div class=\"col-lg-4\">\r\n                                    <div class=\"id-card__item\">\r\n                                        <span class=\"id-card__img\" style=\"background-image: url(\'{{asset(\'public/\'.$themePath.\"/\")}}/assets/images/card.png\')\"></span>\r\n                                        <div class=\"id-card__dis\">\r\n                                            <h5 class=\"semi-big-title\">Prof. Ashraf Mahmoud Ibrahim\r\n                                                Hatem</h5>\r\n                                            <p class=\"secondary-dis\">President of supreme council of AFCM</p>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-lg-4\">\r\n                                    <div class=\"id-card__item\">\r\n                                        <span class=\"id-card__img\" style=\"background-image: url(\'{{asset(\'public/\'.$themePath.\"/\")}}/assets/images/card.png\')\"></span>\r\n                                        <div class=\"id-card__dis\">\r\n                                            <h5 class=\"semi-big-title\">Prof. Ashraf Mahmoud Ibrahim\r\n                                                Hatem</h5>\r\n                                            <p class=\"secondary-dis\">President of supreme council of AFCM</p>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>', 'ar', 4, '2021-07-12 08:09:39', '2021-07-12 09:59:32'),
(7, 'College Board', 'College_Board', NULL, '<div class=\"tabs\">\r\n                <ul class=\"nav nav-pills\" id=\"pills-tab\" role=\"tablist\">\r\n                    <li class=\"nav-item\" role=\"presentation\">\r\n                        <button class=\"nav-link\" id=\"pills-home-tab\" data-bs-toggle=\"pill\" data-bs-target=\"#pills-home\" type=\"button\" role=\"tab\" aria-controls=\"pills-home\" aria-selected=\"true\">supreme council</button>\r\n                    </li>\r\n                    <li class=\"nav-item\" role=\"presentation\">\r\n                        <button class=\"nav-link active\" id=\"pills-profile-tab\" data-bs-toggle=\"pill\" data-bs-target=\"#pills-profile\" type=\"button\" role=\"tab\" aria-controls=\"pills-profile\" aria-selected=\"false\">educational board</button>\r\n                    </li>\r\n                    <li class=\"nav-item\" role=\"presentation\">\r\n                        <button class=\"nav-link\" id=\"pills-contact-tab\" data-bs-toggle=\"pill\" data-bs-target=\"#pills-contact\" type=\"button\" role=\"tab\" aria-controls=\"pills-contact\" aria-selected=\"false\">academic departments</button>\r\n                    </li>\r\n                </ul>\r\n                <div class=\"tab-content\" id=\"pills-tabContent\">\r\n                    <div class=\"tab-pane fade\" id=\"pills-home\" role=\"tabpanel\" aria-labelledby=\"pills-home-tab\">\r\n                        <div class=\"id-cards\">\r\n                            <div class=\"row\">\r\n                                <div class=\"col-lg-4\">\r\n                                    <div class=\"id-card__item\">\r\n                                        <span class=\"id-card__img\" style=\"background-image: url(\'./assets/images/card.png\')\"></span>\r\n                                        <div class=\"id-card__dis\">\r\n                                            <h5 class=\"semi-big-title\">Prof. Ashraf Mahmoud Ibrahim\r\n                                                Hatem</h5>\r\n                                            <p class=\"secondary-dis\">President of supreme council of AFCM</p>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-lg-4\">\r\n                                    <div class=\"id-card__item\">\r\n                                        <span class=\"id-card__img\" style=\"background-image: url(\'./assets/images/card.png\')\"></span>\r\n                                        <div class=\"id-card__dis\">\r\n                                            <h5 class=\"semi-big-title\">Prof. Ashraf Mahmoud Ibrahim\r\n                                                Hatem</h5>\r\n                                            <p class=\"secondary-dis\">President of supreme council of AFCM</p>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-lg-4\">\r\n                                    <div class=\"id-card__item\">\r\n                                        <span class=\"id-card__img\" style=\"background-image: url(\'./assets/images/card.png\')\"></span>\r\n                                        <div class=\"id-card__dis\">\r\n                                            <h5 class=\"semi-big-title\">Prof. Ashraf Mahmoud Ibrahim\r\n                                                Hatem</h5>\r\n                                            <p class=\"secondary-dis\">President of supreme council of AFCM</p>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-lg-4\">\r\n                                    <div class=\"id-card__item\">\r\n                                        <span class=\"id-card__img\" style=\"background-image: url(\'./assets/images/card.png\')\"></span>\r\n                                        <div class=\"id-card__dis\">\r\n                                            <h5 class=\"semi-big-title\">Prof. Ashraf Mahmoud Ibrahim\r\n                                                Hatem</h5>\r\n                                            <p class=\"secondary-dis\">President of supreme council of AFCM</p>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-lg-4\">\r\n                                    <div class=\"id-card__item\">\r\n                                        <span class=\"id-card__img\" style=\"background-image: url(\'./assets/images/card.png\')\"></span>\r\n                                        <div class=\"id-card__dis\">\r\n                                            <h5 class=\"semi-big-title\">Prof. Ashraf Mahmoud Ibrahim\r\n                                                Hatem</h5>\r\n                                            <p class=\"secondary-dis\">President of supreme council of AFCM</p>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-lg-4\">\r\n                                    <div class=\"id-card__item\">\r\n                                        <span class=\"id-card__img\" style=\"background-image: url(\'./assets/images/card.png\')\"></span>\r\n                                        <div class=\"id-card__dis\">\r\n                                            <h5 class=\"semi-big-title\">Prof. Ashraf Mahmoud Ibrahim\r\n                                                Hatem</h5>\r\n                                            <p class=\"secondary-dis\">President of supreme council of AFCM</p>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"tab-pane fade show active\" id=\"pills-profile\" role=\"tabpanel\" aria-labelledby=\"pills-profile-tab\">\r\n                        <div class=\"id-cards\">\r\n                            <div class=\"row\">\r\n                                <div class=\"col-lg-4\">\r\n                                    <div class=\"id-card__item\">\r\n                                        <span class=\"id-card__img\" style=\"background-image: url(\'./assets/images/card.png\')\"></span>\r\n                                        <div class=\"id-card__dis\">\r\n                                            <h5 class=\"semi-big-title\">Prof. Ashraf Mahmoud Ibrahim\r\n                                                Hatem</h5>\r\n                                            <p class=\"secondary-dis\">President of supreme council of AFCM</p>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-lg-4\">\r\n                                    <div class=\"id-card__item\">\r\n                                        <span class=\"id-card__img\" style=\"background-image: url(\'./assets/images/card.png\')\"></span>\r\n                                        <div class=\"id-card__dis\">\r\n                                            <h5 class=\"semi-big-title\">Prof. Ashraf Mahmoud Ibrahim\r\n                                                Hatem</h5>\r\n                                            <p class=\"secondary-dis\">President of supreme council of AFCM</p>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-lg-4\">\r\n                                    <div class=\"id-card__item\">\r\n                                        <span class=\"id-card__img\" style=\"background-image: url(\'./assets/images/card.png\')\"></span>\r\n                                        <div class=\"id-card__dis\">\r\n                                            <h5 class=\"semi-big-title\">Prof. Ashraf Mahmoud Ibrahim\r\n                                                Hatem</h5>\r\n                                            <p class=\"secondary-dis\">President of supreme council of AFCM</p>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"tab-pane fade\" id=\"pills-contact\" role=\"tabpanel\" aria-labelledby=\"pills-contact-tab\">\r\n                        <div class=\"id-cards\">\r\n                            <div class=\"row\">\r\n                                <div class=\"col-lg-4\">\r\n                                    <div class=\"id-card__item\">\r\n                                        <span class=\"id-card__img\" style=\"background-image: url(\'./assets/images/card.png\')\"></span>\r\n                                        <div class=\"id-card__dis\">\r\n                                            <h5 class=\"semi-big-title\">Prof. Ashraf Mahmoud Ibrahim\r\n                                                Hatem</h5>\r\n                                            <p class=\"secondary-dis\">President of supreme council of AFCM</p>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-lg-4\">\r\n                                    <div class=\"id-card__item\">\r\n                                        <span class=\"id-card__img\" style=\"background-image: url(\'./assets/images/card.png\')\"></span>\r\n                                        <div class=\"id-card__dis\">\r\n                                            <h5 class=\"semi-big-title\">Prof. Ashraf Mahmoud Ibrahim\r\n                                                Hatem</h5>\r\n                                            <p class=\"secondary-dis\">President of supreme council of AFCM</p>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>', 'en', 4, '2021-07-12 08:09:39', '2021-07-12 08:09:39'),
(8, 'مجلة الجامعة', 'مجلة_الجامعة', NULL, NULL, 'ar', 5, '2021-07-12 09:41:18', '2021-07-12 09:41:18'),
(9, 'College Magazine', 'College_Magazine', NULL, NULL, 'en', 5, '2021-07-12 09:41:19', '2021-07-12 09:41:19'),
(10, 'تواصل معنا', 'تواصل_معنا', NULL, NULL, 'ar', 6, '2021-07-12 09:47:06', '2021-07-12 09:47:06'),
(11, 'Contact Us', 'Contact_Us', NULL, NULL, 'en', 6, '2021-07-12 09:47:06', '2021-07-12 09:47:06'),
(12, 'لماذا  AFMC?', 'لماذا_AFMC__1', NULL, '<article>\r\n                                    <p>It was held on 9th of JAN 2018 under patronage of Maj.Gen.Med.Ayman Shafei director of the AFCM, directed by Prof. Maha Hamdy head of medical microbiology and immunology department, AFCM and organized by staff members of Medical…It was held on 9th of JAN 2018 under patronage of Maj.Gen.Med.Ayman Shafei director of the AFCM, directed by Pro…   </p>\r\n                                </article>', 'ar', 7, '2021-07-13 05:08:02', '2021-07-13 05:26:32'),
(13, 'Why AFCM?', 'Why_AFCM__1', NULL, '<article>\r\n                                    <p>It was held on 9th of JAN 2018 under patronage of Maj.Gen.Med.Ayman Shafei director of the AFCM, directed by Prof. Maha Hamdy head of medical microbiology and immunology department, AFCM and organized by staff members of Medical…It was held on 9th of JAN 2018 under patronage of Maj.Gen.Med.Ayman Shafei director of the AFCM, directed by Pro…   </p>\r\n                                </article>', 'en', 7, '2021-07-13 05:08:02', '2021-07-13 05:26:32'),
(14, 'لماذا  AFMC?', 'لماذا_AFMC__2', NULL, '<article>\r\n                                    <p>It was held on 9th of JAN 2018 under patronage of Maj.Gen.Med.Ayman Shafei director of the AFCM, directed by Prof. Maha Hamdy head of medical microbiology and immunology department, AFCM and organized by staff members of Medical…It was held on 9th of JAN 2018 under patronage of Maj.Gen.Med.Ayman Shafei director of the AFCM, directed by Pro…   </p>\r\n                                </article>', 'ar', 8, '2021-07-13 05:09:11', '2021-07-13 05:26:51'),
(15, 'Why AFCM?', 'Why_AFCM__2', NULL, '<article>\r\n                                    <p>It was held on 9th of JAN 2018 under patronage of Maj.Gen.Med.Ayman Shafei director of the AFCM, directed by Prof. Maha Hamdy head of medical microbiology and immunology department, AFCM and organized by staff members of Medical…It was held on 9th of JAN 2018 under patronage of Maj.Gen.Med.Ayman Shafei director of the AFCM, directed by Pro…   </p>\r\n                                </article>', 'en', 8, '2021-07-13 05:09:11', '2021-07-13 05:26:52'),
(16, 'المنظمة', 'المنظمة', NULL, '<article>\r\n                                    <p>It was held on 9th of JAN 2018 under patronage of Maj.Gen.Med.Ayman Shafei director of the AFCM, directed by Prof. Maha Hamdy head of medical microbiology and immunology department, AFCM and organized by staff members of Medical…It was held on 9th of JAN 2018 under patronage of Maj.Gen.Med.Ayman Shafei director of the AFCM, directed by Pro…   </p>\r\n                                </article>', 'ar', 9, '2021-07-13 05:10:38', '2021-07-13 05:27:16'),
(17, 'Organisation', 'Organisation', NULL, '<article>\r\n                                    <p>It was held on 9th of JAN 2018 under patronage of Maj.Gen.Med.Ayman Shafei director of the AFCM, directed by Prof. Maha Hamdy head of medical microbiology and immunology department, AFCM and organized by staff members of Medical…It was held on 9th of JAN 2018 under patronage of Maj.Gen.Med.Ayman Shafei director of the AFCM, directed by Pro…   </p>\r\n                                </article>', 'en', 9, '2021-07-13 05:10:38', '2021-07-13 05:27:16'),
(18, 'توقيع بروتوكول تعاون بين القوات المسلحة', 'توقيع_بروتوكول_تعاون_بين_القوات_المسلحة', NULL, '<article>\r\n                                <p>It was held on 9th of JAN 2018 under patronage of Maj.Gen.Med.Ayman Shafei\r\n                                    director of the AFCM, directed by Prof. Maha Hamdy head of medical microbiology\r\n                                    and immunology department, AFCM and organized by staff members of Medical…</p>\r\n                            </article>', 'ar', 10, '2021-07-13 05:42:40', '2021-07-13 05:42:40'),
(19, 'Signing Of Collaboration Protocol Between Armed Forces', 'Signing_Of_Collaboration_Protocol_Between_Armed_Forces', NULL, '<article>\r\n                                <p>It was held on 9th of JAN 2018 under patronage of Maj.Gen.Med.Ayman Shafei\r\n                                    director of the AFCM, directed by Prof. Maha Hamdy head of medical microbiology\r\n                                    and immunology department, AFCM and organized by staff members of Medical…</p>\r\n                            </article>', 'en', 10, '2021-07-13 05:42:41', '2021-07-13 05:42:41'),
(20, 'توقيع بروتوكول تعاون بين القوات المسلحة', 'توقيع_بروتوكول_تعاون_بين_القوات_المسلحة_1', NULL, '<article>\r\n                                <p>It was held on 9th of JAN 2018 under patronage of Maj.Gen.Med.Ayman Shafei\r\n                                    director of the AFCM, directed by Prof. Maha Hamdy head of medical microbiology\r\n                                    and immunology department, AFCM and organized by staff members of Medical…</p>\r\n                            </article>', 'ar', 11, '2021-07-13 05:50:21', '2021-07-13 05:50:21'),
(21, 'Signing Of Collaboration Protocol Between Armed Forces', 'Signing_Of_Collaboration_Protocol_Between_Armed_Forces_1', NULL, '<article>\r\n                                <p>It was held on 9th of JAN 2018 under patronage of Maj.Gen.Med.Ayman Shafei\r\n                                    director of the AFCM, directed by Prof. Maha Hamdy head of medical microbiology\r\n                                    and immunology department, AFCM and organized by staff members of Medical…</p>\r\n                            </article>', 'en', 11, '2021-07-13 05:50:21', '2021-07-13 05:50:21'),
(22, 'الرؤية والرسالة', 'الرؤية_والرسالة', NULL, NULL, 'ar', 12, '2021-07-13 08:35:48', '2021-07-13 08:35:48'),
(23, 'Vision & Mission', 'Vision_Mission', NULL, NULL, 'en', 12, '2021-07-13 08:35:48', '2021-07-13 08:35:48'),
(24, 'التاريخ', 'التاريخ', NULL, NULL, 'ar', 13, '2021-07-13 08:37:52', '2021-07-13 08:37:52'),
(25, 'History', 'History', NULL, NULL, 'en', 13, '2021-07-13 08:37:52', '2021-07-13 08:37:52'),
(26, 'الخطة الاستراتيجية', 'الخطة_الاستراتيجية', NULL, NULL, 'ar', 14, '2021-07-13 08:40:25', '2021-07-13 08:40:25'),
(27, 'Strategic Plane', 'Strategic_Plane', NULL, NULL, 'en', 14, '2021-07-13 08:40:25', '2021-07-13 08:40:25');

-- --------------------------------------------------------

--
-- Table structure for table `diva_notifications`
--

CREATE TABLE `diva_notifications` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notifiable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_oauth_access_tokens`
--

CREATE TABLE `diva_oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_oauth_auth_codes`
--

CREATE TABLE `diva_oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_oauth_clients`
--

CREATE TABLE `diva_oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_oauth_personal_access_clients`
--

CREATE TABLE `diva_oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_oauth_refresh_tokens`
--

CREATE TABLE `diva_oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_password_resets`
--

CREATE TABLE `diva_password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_permissions`
--

CREATE TABLE `diva_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_permissions`
--

INSERT INTO `diva_permissions` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'list-users', 1, '2021-07-11 13:34:08', '2021-07-11 13:34:08'),
(2, 'create-users', 1, '2021-07-11 13:34:08', '2021-07-11 13:34:08'),
(3, 'read-users', 1, '2021-07-11 13:34:08', '2021-07-11 13:34:08'),
(4, 'update-users', 1, '2021-07-11 13:34:09', '2021-07-11 13:34:09'),
(5, 'delete-users', 1, '2021-07-11 13:34:09', '2021-07-11 13:34:09'),
(6, 'list-roles', 1, '2021-07-11 13:34:09', '2021-07-11 13:34:09'),
(7, 'create-roles', 1, '2021-07-11 13:34:10', '2021-07-11 13:34:10'),
(8, 'read-roles', 1, '2021-07-11 13:34:10', '2021-07-11 13:34:10'),
(9, 'update-roles', 1, '2021-07-11 13:34:10', '2021-07-11 13:34:10'),
(10, 'delete-roles', 1, '2021-07-11 13:34:10', '2021-07-11 13:34:10'),
(11, 'list-enums', 1, '2021-07-11 13:34:10', '2021-07-11 13:34:10'),
(12, 'create-enums', 1, '2021-07-11 13:34:10', '2021-07-11 13:34:10'),
(13, 'read-enums', 1, '2021-07-11 13:34:11', '2021-07-11 13:34:11'),
(14, 'update-enums', 1, '2021-07-11 13:34:11', '2021-07-11 13:34:11'),
(15, 'delete-enums', 1, '2021-07-11 13:34:11', '2021-07-11 13:34:11'),
(16, 'read-profile', 1, '2021-07-11 13:34:11', '2021-07-11 13:34:11'),
(17, 'update-profile', 1, '2021-07-11 13:34:11', '2021-07-11 13:34:11'),
(18, 'list-categories', 1, '2021-07-11 13:34:11', '2021-07-11 13:34:11'),
(19, 'create-categories', 1, '2021-07-11 13:34:11', '2021-07-11 13:34:11'),
(20, 'read-categories', 1, '2021-07-11 13:34:12', '2021-07-11 13:34:12'),
(21, 'update-categories', 1, '2021-07-11 13:34:12', '2021-07-11 13:34:12'),
(22, 'delete-categories', 1, '2021-07-11 13:34:12', '2021-07-11 13:34:12'),
(23, 'list-tags', 1, '2021-07-11 13:34:12', '2021-07-11 13:34:12'),
(24, 'create-tags', 1, '2021-07-11 13:34:12', '2021-07-11 13:34:12'),
(25, 'read-tags', 1, '2021-07-11 13:34:12', '2021-07-11 13:34:12'),
(26, 'update-tags', 1, '2021-07-11 13:34:12', '2021-07-11 13:34:12'),
(27, 'delete-tags', 1, '2021-07-11 13:34:13', '2021-07-11 13:34:13'),
(28, 'list-languages', 1, '2021-07-11 13:34:13', '2021-07-11 13:34:13'),
(29, 'create-languages', 1, '2021-07-11 13:34:13', '2021-07-11 13:34:13'),
(30, 'read-languages', 1, '2021-07-11 13:34:13', '2021-07-11 13:34:13'),
(31, 'update-languages', 1, '2021-07-11 13:34:13', '2021-07-11 13:34:13'),
(32, 'delete-languages', 1, '2021-07-11 13:34:14', '2021-07-11 13:34:14'),
(33, 'list-authors', 1, '2021-07-11 13:34:14', '2021-07-11 13:34:14'),
(34, 'create-authors', 1, '2021-07-11 13:34:14', '2021-07-11 13:34:14'),
(35, 'read-authors', 1, '2021-07-11 13:34:14', '2021-07-11 13:34:14'),
(36, 'update-authors', 1, '2021-07-11 13:34:14', '2021-07-11 13:34:14'),
(37, 'delete-authors', 1, '2021-07-11 13:34:14', '2021-07-11 13:34:14'),
(38, 'list-modules', 1, '2021-07-11 13:34:14', '2021-07-11 13:34:14'),
(39, 'create-modules', 1, '2021-07-11 13:34:15', '2021-07-11 13:34:15'),
(40, 'read-modules', 1, '2021-07-11 13:34:15', '2021-07-11 13:34:15'),
(41, 'update-modules', 1, '2021-07-11 13:34:15', '2021-07-11 13:34:15'),
(42, 'delete-modules', 1, '2021-07-11 13:34:15', '2021-07-11 13:34:15'),
(43, 'list-menuGroups', 1, '2021-07-11 13:34:15', '2021-07-11 13:34:15'),
(44, 'create-menuGroups', 1, '2021-07-11 13:34:15', '2021-07-11 13:34:15'),
(45, 'read-menuGroups', 1, '2021-07-11 13:34:16', '2021-07-11 13:34:16'),
(46, 'update-menuGroups', 1, '2021-07-11 13:34:16', '2021-07-11 13:34:16'),
(47, 'delete-menuGroups', 1, '2021-07-11 13:34:16', '2021-07-11 13:34:16'),
(48, 'list-menuLinks', 1, '2021-07-11 13:34:16', '2021-07-11 13:34:16'),
(49, 'create-menuLinks', 1, '2021-07-11 13:34:16', '2021-07-11 13:34:16'),
(50, 'read-menuLinks', 1, '2021-07-11 13:34:16', '2021-07-11 13:34:16'),
(51, 'update-menuLinks', 1, '2021-07-11 13:34:16', '2021-07-11 13:34:16'),
(52, 'delete-menuLinks', 1, '2021-07-11 13:34:17', '2021-07-11 13:34:17'),
(53, 'list-pages', 1, '2021-07-11 13:34:17', '2021-07-11 13:34:17'),
(54, 'create-pages', 1, '2021-07-11 13:34:17', '2021-07-11 13:34:17'),
(55, 'read-pages', 1, '2021-07-11 13:34:17', '2021-07-11 13:34:17'),
(56, 'update-pages', 1, '2021-07-11 13:34:17', '2021-07-11 13:34:17'),
(57, 'delete-pages', 1, '2021-07-11 13:34:18', '2021-07-11 13:34:18'),
(58, 'list-posts', 1, '2021-07-11 13:34:18', '2021-07-11 13:34:18'),
(59, 'create-posts', 1, '2021-07-11 13:34:18', '2021-07-11 13:34:18'),
(60, 'read-posts', 1, '2021-07-11 13:34:18', '2021-07-11 13:34:18'),
(61, 'update-posts', 1, '2021-07-11 13:34:18', '2021-07-11 13:34:18'),
(62, 'delete-posts', 1, '2021-07-11 13:34:18', '2021-07-11 13:34:18'),
(63, 'list-events', 1, '2021-07-11 13:34:18', '2021-07-11 13:34:18'),
(64, 'create-events', 1, '2021-07-11 13:34:19', '2021-07-11 13:34:19'),
(65, 'read-events', 1, '2021-07-11 13:34:19', '2021-07-11 13:34:19'),
(66, 'update-events', 1, '2021-07-11 13:34:19', '2021-07-11 13:34:19'),
(67, 'delete-events', 1, '2021-07-11 13:34:20', '2021-07-11 13:34:20'),
(68, 'list-galleries', 1, '2021-07-11 13:34:20', '2021-07-11 13:34:20'),
(69, 'create-galleries', 1, '2021-07-11 13:34:20', '2021-07-11 13:34:20'),
(70, 'read-galleries', 1, '2021-07-11 13:34:20', '2021-07-11 13:34:20'),
(71, 'update-galleries', 1, '2021-07-11 13:34:20', '2021-07-11 13:34:20'),
(72, 'delete-galleries', 1, '2021-07-11 13:34:20', '2021-07-11 13:34:20'),
(73, 'list-galleryImages', 1, '2021-07-11 13:34:21', '2021-07-11 13:34:21'),
(74, 'create-galleryImages', 1, '2021-07-11 13:34:21', '2021-07-11 13:34:21'),
(75, 'read-galleryImages', 1, '2021-07-11 13:34:21', '2021-07-11 13:34:21'),
(76, 'update-galleryImages', 1, '2021-07-11 13:34:21', '2021-07-11 13:34:21'),
(77, 'delete-galleryImages', 1, '2021-07-11 13:34:21', '2021-07-11 13:34:21'),
(78, 'list-sliders', 1, '2021-07-11 13:34:21', '2021-07-11 13:34:21'),
(79, 'create-sliders', 1, '2021-07-11 13:34:21', '2021-07-11 13:34:21'),
(80, 'read-sliders', 1, '2021-07-11 13:34:22', '2021-07-11 13:34:22'),
(81, 'update-sliders', 1, '2021-07-11 13:34:22', '2021-07-11 13:34:22'),
(82, 'delete-sliders', 1, '2021-07-11 13:34:22', '2021-07-11 13:34:22'),
(83, 'list-sliderImages', 1, '2021-07-11 13:34:22', '2021-07-11 13:34:22'),
(84, 'create-sliderImages', 1, '2021-07-11 13:34:22', '2021-07-11 13:34:22'),
(85, 'read-sliderImages', 1, '2021-07-11 13:34:22', '2021-07-11 13:34:22'),
(86, 'update-sliderImages', 1, '2021-07-11 13:34:22', '2021-07-11 13:34:22'),
(87, 'delete-sliderImages', 1, '2021-07-11 13:34:22', '2021-07-11 13:34:22'),
(88, 'list-plugins', 1, '2021-07-11 13:34:22', '2021-07-11 13:34:22'),
(89, 'list-acl', 1, '2021-07-11 13:34:23', '2021-07-11 13:34:23'),
(90, 'create-acl', 1, '2021-07-11 13:34:23', '2021-07-11 13:34:23'),
(91, 'read-acl', 1, '2021-07-11 13:34:23', '2021-07-11 13:34:23'),
(92, 'update-acl', 1, '2021-07-11 13:34:23', '2021-07-11 13:34:23'),
(93, 'delete-acl', 1, '2021-07-11 13:34:23', '2021-07-11 13:34:23'),
(94, 'list-themes', 1, '2021-07-11 13:34:23', '2021-07-11 13:34:23'),
(95, 'create-themes', 1, '2021-07-11 13:34:24', '2021-07-11 13:34:24'),
(96, 'read-themes', 1, '2021-07-11 13:34:24', '2021-07-11 13:34:24'),
(97, 'update-themes', 1, '2021-07-11 13:34:24', '2021-07-11 13:34:24'),
(98, 'delete-themes', 1, '2021-07-11 13:34:24', '2021-07-11 13:34:24'),
(99, 'list-layoutModels', 1, '2021-07-11 13:34:24', '2021-07-11 13:34:24'),
(100, 'create-layoutModels', 1, '2021-07-11 13:34:24', '2021-07-11 13:34:24'),
(101, 'read-layoutModels', 1, '2021-07-11 13:34:24', '2021-07-11 13:34:24'),
(102, 'update-layoutModels', 1, '2021-07-11 13:34:25', '2021-07-11 13:34:25'),
(103, 'delete-layoutModels', 1, '2021-07-11 13:34:25', '2021-07-11 13:34:25'),
(104, 'list-customers', 1, '2021-07-11 13:34:25', '2021-07-11 13:34:25'),
(105, 'read-customers', 1, '2021-07-11 13:34:25', '2021-07-11 13:34:25'),
(106, 'update-customers', 1, '2021-07-11 13:34:25', '2021-07-11 13:34:25'),
(107, 'delete-customers', 1, '2021-07-11 13:34:25', '2021-07-11 13:34:25'),
(108, 'list-merchants', 1, '2021-07-11 13:34:25', '2021-07-11 13:34:25'),
(109, 'create-merchants', 1, '2021-07-11 13:34:25', '2021-07-11 13:34:25'),
(110, 'read-merchants', 1, '2021-07-11 13:34:25', '2021-07-11 13:34:25'),
(111, 'update-merchants', 1, '2021-07-11 13:34:26', '2021-07-11 13:34:26'),
(112, 'delete-merchants', 1, '2021-07-11 13:34:26', '2021-07-11 13:34:26'),
(113, 'list-offers', 1, '2021-07-11 13:34:26', '2021-07-11 13:34:26'),
(114, 'create-offers', 1, '2021-07-11 13:34:26', '2021-07-11 13:34:26'),
(115, 'read-offers', 1, '2021-07-11 13:34:26', '2021-07-11 13:34:26'),
(116, 'update-offers', 1, '2021-07-11 13:34:26', '2021-07-11 13:34:26'),
(117, 'delete-offers', 1, '2021-07-11 13:34:26', '2021-07-11 13:34:26'),
(118, 'list-plans', 1, '2021-07-11 13:34:26', '2021-07-11 13:34:26'),
(119, 'create-plans', 1, '2021-07-11 13:34:27', '2021-07-11 13:34:27'),
(120, 'read-plans', 1, '2021-07-11 13:34:27', '2021-07-11 13:34:27'),
(121, 'update-plans', 1, '2021-07-11 13:34:27', '2021-07-11 13:34:27'),
(122, 'delete-plans', 1, '2021-07-11 13:34:27', '2021-07-11 13:34:27'),
(123, 'list-subscriptions', 1, '2021-07-11 13:34:27', '2021-07-11 13:34:27'),
(124, 'read-subscriptions', 1, '2021-07-11 13:34:27', '2021-07-11 13:34:27'),
(125, 'update-subscriptions', 1, '2021-07-11 13:34:27', '2021-07-11 13:34:27'),
(126, 'list-merchantRequests', 1, '2021-07-11 13:34:28', '2021-07-11 13:34:28'),
(127, 'read-merchantRequests', 1, '2021-07-11 13:34:28', '2021-07-11 13:34:28'),
(128, 'list-feedbacks', 1, '2021-07-11 13:34:28', '2021-07-11 13:34:28'),
(129, 'list-newsletterSubscribers', 1, '2021-07-11 13:34:28', '2021-07-11 13:34:28'),
(130, 'create-profile', 1, '2021-07-11 13:34:52', '2021-07-11 13:34:52');

-- --------------------------------------------------------

--
-- Table structure for table `diva_permissions_translations`
--

CREATE TABLE `diva_permissions_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_permissions_translations`
--

INSERT INTO `diva_permissions_translations` (`id`, `display_name`, `description`, `permission_id`, `language`, `created_at`, `updated_at`) VALUES
(1, 'List Users', 'List Users', 1, 'en', '2021-07-11 13:34:08', '2021-07-11 13:34:08'),
(2, 'Create Users', 'Create Users', 2, 'en', '2021-07-11 13:34:08', '2021-07-11 13:34:08'),
(3, 'Read Users', 'Read Users', 3, 'en', '2021-07-11 13:34:08', '2021-07-11 13:34:08'),
(4, 'Update Users', 'Update Users', 4, 'en', '2021-07-11 13:34:09', '2021-07-11 13:34:09'),
(5, 'Delete Users', 'Delete Users', 5, 'en', '2021-07-11 13:34:09', '2021-07-11 13:34:09'),
(6, 'List Roles', 'List Roles', 6, 'en', '2021-07-11 13:34:09', '2021-07-11 13:34:09'),
(7, 'Create Roles', 'Create Roles', 7, 'en', '2021-07-11 13:34:10', '2021-07-11 13:34:10'),
(8, 'Read Roles', 'Read Roles', 8, 'en', '2021-07-11 13:34:10', '2021-07-11 13:34:10'),
(9, 'Update Roles', 'Update Roles', 9, 'en', '2021-07-11 13:34:10', '2021-07-11 13:34:10'),
(10, 'Delete Roles', 'Delete Roles', 10, 'en', '2021-07-11 13:34:10', '2021-07-11 13:34:10'),
(11, 'List Enums', 'List Enums', 11, 'en', '2021-07-11 13:34:10', '2021-07-11 13:34:10'),
(12, 'Create Enums', 'Create Enums', 12, 'en', '2021-07-11 13:34:11', '2021-07-11 13:34:11'),
(13, 'Read Enums', 'Read Enums', 13, 'en', '2021-07-11 13:34:11', '2021-07-11 13:34:11'),
(14, 'Update Enums', 'Update Enums', 14, 'en', '2021-07-11 13:34:11', '2021-07-11 13:34:11'),
(15, 'Delete Enums', 'Delete Enums', 15, 'en', '2021-07-11 13:34:11', '2021-07-11 13:34:11'),
(16, 'Read Profile', 'Read Profile', 16, 'en', '2021-07-11 13:34:11', '2021-07-11 13:34:11'),
(17, 'Update Profile', 'Update Profile', 17, 'en', '2021-07-11 13:34:11', '2021-07-11 13:34:11'),
(18, 'List Categories', 'List Categories', 18, 'en', '2021-07-11 13:34:11', '2021-07-11 13:34:11'),
(19, 'Create Categories', 'Create Categories', 19, 'en', '2021-07-11 13:34:12', '2021-07-11 13:34:12'),
(20, 'Read Categories', 'Read Categories', 20, 'en', '2021-07-11 13:34:12', '2021-07-11 13:34:12'),
(21, 'Update Categories', 'Update Categories', 21, 'en', '2021-07-11 13:34:12', '2021-07-11 13:34:12'),
(22, 'Delete Categories', 'Delete Categories', 22, 'en', '2021-07-11 13:34:12', '2021-07-11 13:34:12'),
(23, 'List Tags', 'List Tags', 23, 'en', '2021-07-11 13:34:12', '2021-07-11 13:34:12'),
(24, 'Create Tags', 'Create Tags', 24, 'en', '2021-07-11 13:34:12', '2021-07-11 13:34:12'),
(25, 'Read Tags', 'Read Tags', 25, 'en', '2021-07-11 13:34:12', '2021-07-11 13:34:12'),
(26, 'Update Tags', 'Update Tags', 26, 'en', '2021-07-11 13:34:13', '2021-07-11 13:34:13'),
(27, 'Delete Tags', 'Delete Tags', 27, 'en', '2021-07-11 13:34:13', '2021-07-11 13:34:13'),
(28, 'List Languages', 'List Languages', 28, 'en', '2021-07-11 13:34:13', '2021-07-11 13:34:13'),
(29, 'Create Languages', 'Create Languages', 29, 'en', '2021-07-11 13:34:13', '2021-07-11 13:34:13'),
(30, 'Read Languages', 'Read Languages', 30, 'en', '2021-07-11 13:34:13', '2021-07-11 13:34:13'),
(31, 'Update Languages', 'Update Languages', 31, 'en', '2021-07-11 13:34:13', '2021-07-11 13:34:13'),
(32, 'Delete Languages', 'Delete Languages', 32, 'en', '2021-07-11 13:34:14', '2021-07-11 13:34:14'),
(33, 'List Authors', 'List Authors', 33, 'en', '2021-07-11 13:34:14', '2021-07-11 13:34:14'),
(34, 'Create Authors', 'Create Authors', 34, 'en', '2021-07-11 13:34:14', '2021-07-11 13:34:14'),
(35, 'Read Authors', 'Read Authors', 35, 'en', '2021-07-11 13:34:14', '2021-07-11 13:34:14'),
(36, 'Update Authors', 'Update Authors', 36, 'en', '2021-07-11 13:34:14', '2021-07-11 13:34:14'),
(37, 'Delete Authors', 'Delete Authors', 37, 'en', '2021-07-11 13:34:14', '2021-07-11 13:34:14'),
(38, 'List Modules', 'List Modules', 38, 'en', '2021-07-11 13:34:15', '2021-07-11 13:34:15'),
(39, 'Create Modules', 'Create Modules', 39, 'en', '2021-07-11 13:34:15', '2021-07-11 13:34:15'),
(40, 'Read Modules', 'Read Modules', 40, 'en', '2021-07-11 13:34:15', '2021-07-11 13:34:15'),
(41, 'Update Modules', 'Update Modules', 41, 'en', '2021-07-11 13:34:15', '2021-07-11 13:34:15'),
(42, 'Delete Modules', 'Delete Modules', 42, 'en', '2021-07-11 13:34:15', '2021-07-11 13:34:15'),
(43, 'List MenuGroups', 'List MenuGroups', 43, 'en', '2021-07-11 13:34:15', '2021-07-11 13:34:15'),
(44, 'Create MenuGroups', 'Create MenuGroups', 44, 'en', '2021-07-11 13:34:15', '2021-07-11 13:34:15'),
(45, 'Read MenuGroups', 'Read MenuGroups', 45, 'en', '2021-07-11 13:34:16', '2021-07-11 13:34:16'),
(46, 'Update MenuGroups', 'Update MenuGroups', 46, 'en', '2021-07-11 13:34:16', '2021-07-11 13:34:16'),
(47, 'Delete MenuGroups', 'Delete MenuGroups', 47, 'en', '2021-07-11 13:34:16', '2021-07-11 13:34:16'),
(48, 'List MenuLinks', 'List MenuLinks', 48, 'en', '2021-07-11 13:34:16', '2021-07-11 13:34:16'),
(49, 'Create MenuLinks', 'Create MenuLinks', 49, 'en', '2021-07-11 13:34:16', '2021-07-11 13:34:16'),
(50, 'Read MenuLinks', 'Read MenuLinks', 50, 'en', '2021-07-11 13:34:16', '2021-07-11 13:34:16'),
(51, 'Update MenuLinks', 'Update MenuLinks', 51, 'en', '2021-07-11 13:34:16', '2021-07-11 13:34:16'),
(52, 'Delete MenuLinks', 'Delete MenuLinks', 52, 'en', '2021-07-11 13:34:17', '2021-07-11 13:34:17'),
(53, 'List Pages', 'List Pages', 53, 'en', '2021-07-11 13:34:17', '2021-07-11 13:34:17'),
(54, 'Create Pages', 'Create Pages', 54, 'en', '2021-07-11 13:34:17', '2021-07-11 13:34:17'),
(55, 'Read Pages', 'Read Pages', 55, 'en', '2021-07-11 13:34:17', '2021-07-11 13:34:17'),
(56, 'Update Pages', 'Update Pages', 56, 'en', '2021-07-11 13:34:17', '2021-07-11 13:34:17'),
(57, 'Delete Pages', 'Delete Pages', 57, 'en', '2021-07-11 13:34:18', '2021-07-11 13:34:18'),
(58, 'List Posts', 'List Posts', 58, 'en', '2021-07-11 13:34:18', '2021-07-11 13:34:18'),
(59, 'Create Posts', 'Create Posts', 59, 'en', '2021-07-11 13:34:18', '2021-07-11 13:34:18'),
(60, 'Read Posts', 'Read Posts', 60, 'en', '2021-07-11 13:34:18', '2021-07-11 13:34:18'),
(61, 'Update Posts', 'Update Posts', 61, 'en', '2021-07-11 13:34:18', '2021-07-11 13:34:18'),
(62, 'Delete Posts', 'Delete Posts', 62, 'en', '2021-07-11 13:34:18', '2021-07-11 13:34:18'),
(63, 'List Events', 'List Events', 63, 'en', '2021-07-11 13:34:19', '2021-07-11 13:34:19'),
(64, 'Create Events', 'Create Events', 64, 'en', '2021-07-11 13:34:19', '2021-07-11 13:34:19'),
(65, 'Read Events', 'Read Events', 65, 'en', '2021-07-11 13:34:19', '2021-07-11 13:34:19'),
(66, 'Update Events', 'Update Events', 66, 'en', '2021-07-11 13:34:19', '2021-07-11 13:34:19'),
(67, 'Delete Events', 'Delete Events', 67, 'en', '2021-07-11 13:34:20', '2021-07-11 13:34:20'),
(68, 'List Galleries', 'List Galleries', 68, 'en', '2021-07-11 13:34:20', '2021-07-11 13:34:20'),
(69, 'Create Galleries', 'Create Galleries', 69, 'en', '2021-07-11 13:34:20', '2021-07-11 13:34:20'),
(70, 'Read Galleries', 'Read Galleries', 70, 'en', '2021-07-11 13:34:20', '2021-07-11 13:34:20'),
(71, 'Update Galleries', 'Update Galleries', 71, 'en', '2021-07-11 13:34:20', '2021-07-11 13:34:20'),
(72, 'Delete Galleries', 'Delete Galleries', 72, 'en', '2021-07-11 13:34:20', '2021-07-11 13:34:20'),
(73, 'List GalleryImages', 'List GalleryImages', 73, 'en', '2021-07-11 13:34:21', '2021-07-11 13:34:21'),
(74, 'Create GalleryImages', 'Create GalleryImages', 74, 'en', '2021-07-11 13:34:21', '2021-07-11 13:34:21'),
(75, 'Read GalleryImages', 'Read GalleryImages', 75, 'en', '2021-07-11 13:34:21', '2021-07-11 13:34:21'),
(76, 'Update GalleryImages', 'Update GalleryImages', 76, 'en', '2021-07-11 13:34:21', '2021-07-11 13:34:21'),
(77, 'Delete GalleryImages', 'Delete GalleryImages', 77, 'en', '2021-07-11 13:34:21', '2021-07-11 13:34:21'),
(78, 'List Sliders', 'List Sliders', 78, 'en', '2021-07-11 13:34:21', '2021-07-11 13:34:21'),
(79, 'Create Sliders', 'Create Sliders', 79, 'en', '2021-07-11 13:34:21', '2021-07-11 13:34:21'),
(80, 'Read Sliders', 'Read Sliders', 80, 'en', '2021-07-11 13:34:22', '2021-07-11 13:34:22'),
(81, 'Update Sliders', 'Update Sliders', 81, 'en', '2021-07-11 13:34:22', '2021-07-11 13:34:22'),
(82, 'Delete Sliders', 'Delete Sliders', 82, 'en', '2021-07-11 13:34:22', '2021-07-11 13:34:22'),
(83, 'List SliderImages', 'List SliderImages', 83, 'en', '2021-07-11 13:34:22', '2021-07-11 13:34:22'),
(84, 'Create SliderImages', 'Create SliderImages', 84, 'en', '2021-07-11 13:34:22', '2021-07-11 13:34:22'),
(85, 'Read SliderImages', 'Read SliderImages', 85, 'en', '2021-07-11 13:34:22', '2021-07-11 13:34:22'),
(86, 'Update SliderImages', 'Update SliderImages', 86, 'en', '2021-07-11 13:34:22', '2021-07-11 13:34:22'),
(87, 'Delete SliderImages', 'Delete SliderImages', 87, 'en', '2021-07-11 13:34:22', '2021-07-11 13:34:22'),
(88, 'List Plugins', 'List Plugins', 88, 'en', '2021-07-11 13:34:22', '2021-07-11 13:34:22'),
(89, 'List Acl', 'List Acl', 89, 'en', '2021-07-11 13:34:23', '2021-07-11 13:34:23'),
(90, 'Create Acl', 'Create Acl', 90, 'en', '2021-07-11 13:34:23', '2021-07-11 13:34:23'),
(91, 'Read Acl', 'Read Acl', 91, 'en', '2021-07-11 13:34:23', '2021-07-11 13:34:23'),
(92, 'Update Acl', 'Update Acl', 92, 'en', '2021-07-11 13:34:23', '2021-07-11 13:34:23'),
(93, 'Delete Acl', 'Delete Acl', 93, 'en', '2021-07-11 13:34:23', '2021-07-11 13:34:23'),
(94, 'List Themes', 'List Themes', 94, 'en', '2021-07-11 13:34:23', '2021-07-11 13:34:23'),
(95, 'Create Themes', 'Create Themes', 95, 'en', '2021-07-11 13:34:24', '2021-07-11 13:34:24'),
(96, 'Read Themes', 'Read Themes', 96, 'en', '2021-07-11 13:34:24', '2021-07-11 13:34:24'),
(97, 'Update Themes', 'Update Themes', 97, 'en', '2021-07-11 13:34:24', '2021-07-11 13:34:24'),
(98, 'Delete Themes', 'Delete Themes', 98, 'en', '2021-07-11 13:34:24', '2021-07-11 13:34:24'),
(99, 'List LayoutModels', 'List LayoutModels', 99, 'en', '2021-07-11 13:34:24', '2021-07-11 13:34:24'),
(100, 'Create LayoutModels', 'Create LayoutModels', 100, 'en', '2021-07-11 13:34:24', '2021-07-11 13:34:24'),
(101, 'Read LayoutModels', 'Read LayoutModels', 101, 'en', '2021-07-11 13:34:24', '2021-07-11 13:34:24'),
(102, 'Update LayoutModels', 'Update LayoutModels', 102, 'en', '2021-07-11 13:34:25', '2021-07-11 13:34:25'),
(103, 'Delete LayoutModels', 'Delete LayoutModels', 103, 'en', '2021-07-11 13:34:25', '2021-07-11 13:34:25'),
(104, 'List Customers', 'List Customers', 104, 'en', '2021-07-11 13:34:25', '2021-07-11 13:34:25'),
(105, 'Read Customers', 'Read Customers', 105, 'en', '2021-07-11 13:34:25', '2021-07-11 13:34:25'),
(106, 'Update Customers', 'Update Customers', 106, 'en', '2021-07-11 13:34:25', '2021-07-11 13:34:25'),
(107, 'Delete Customers', 'Delete Customers', 107, 'en', '2021-07-11 13:34:25', '2021-07-11 13:34:25'),
(108, 'List Merchants', 'List Merchants', 108, 'en', '2021-07-11 13:34:25', '2021-07-11 13:34:25'),
(109, 'Create Merchants', 'Create Merchants', 109, 'en', '2021-07-11 13:34:25', '2021-07-11 13:34:25'),
(110, 'Read Merchants', 'Read Merchants', 110, 'en', '2021-07-11 13:34:26', '2021-07-11 13:34:26'),
(111, 'Update Merchants', 'Update Merchants', 111, 'en', '2021-07-11 13:34:26', '2021-07-11 13:34:26'),
(112, 'Delete Merchants', 'Delete Merchants', 112, 'en', '2021-07-11 13:34:26', '2021-07-11 13:34:26'),
(113, 'List Offers', 'List Offers', 113, 'en', '2021-07-11 13:34:26', '2021-07-11 13:34:26'),
(114, 'Create Offers', 'Create Offers', 114, 'en', '2021-07-11 13:34:26', '2021-07-11 13:34:26'),
(115, 'Read Offers', 'Read Offers', 115, 'en', '2021-07-11 13:34:26', '2021-07-11 13:34:26'),
(116, 'Update Offers', 'Update Offers', 116, 'en', '2021-07-11 13:34:26', '2021-07-11 13:34:26'),
(117, 'Delete Offers', 'Delete Offers', 117, 'en', '2021-07-11 13:34:26', '2021-07-11 13:34:26'),
(118, 'List Plans', 'List Plans', 118, 'en', '2021-07-11 13:34:27', '2021-07-11 13:34:27'),
(119, 'Create Plans', 'Create Plans', 119, 'en', '2021-07-11 13:34:27', '2021-07-11 13:34:27'),
(120, 'Read Plans', 'Read Plans', 120, 'en', '2021-07-11 13:34:27', '2021-07-11 13:34:27'),
(121, 'Update Plans', 'Update Plans', 121, 'en', '2021-07-11 13:34:27', '2021-07-11 13:34:27'),
(122, 'Delete Plans', 'Delete Plans', 122, 'en', '2021-07-11 13:34:27', '2021-07-11 13:34:27'),
(123, 'List Subscriptions', 'List Subscriptions', 123, 'en', '2021-07-11 13:34:27', '2021-07-11 13:34:27'),
(124, 'Read Subscriptions', 'Read Subscriptions', 124, 'en', '2021-07-11 13:34:27', '2021-07-11 13:34:27'),
(125, 'Update Subscriptions', 'Update Subscriptions', 125, 'en', '2021-07-11 13:34:28', '2021-07-11 13:34:28'),
(126, 'List MerchantRequests', 'List MerchantRequests', 126, 'en', '2021-07-11 13:34:28', '2021-07-11 13:34:28'),
(127, 'Read MerchantRequests', 'Read MerchantRequests', 127, 'en', '2021-07-11 13:34:28', '2021-07-11 13:34:28'),
(128, 'List Feedbacks', 'List Feedbacks', 128, 'en', '2021-07-11 13:34:28', '2021-07-11 13:34:28'),
(129, 'List NewsletterSubscribers', 'List NewsletterSubscribers', 129, 'en', '2021-07-11 13:34:28', '2021-07-11 13:34:28'),
(130, 'Create Profile', 'Create Profile', 130, 'en', '2021-07-11 13:34:53', '2021-07-11 13:34:53');

-- --------------------------------------------------------

--
-- Table structure for table `diva_permission_role`
--

CREATE TABLE `diva_permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_permission_role`
--

INSERT INTO `diva_permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 2),
(3, 1),
(3, 2),
(4, 1),
(4, 2),
(5, 1),
(5, 2),
(6, 1),
(6, 2),
(7, 1),
(7, 2),
(8, 1),
(8, 2),
(9, 1),
(9, 2),
(10, 1),
(10, 2),
(11, 1),
(11, 2),
(12, 1),
(12, 2),
(13, 1),
(13, 2),
(14, 1),
(14, 2),
(15, 1),
(15, 2),
(16, 1),
(16, 2),
(16, 3),
(17, 1),
(17, 2),
(17, 3),
(18, 1),
(18, 2),
(19, 1),
(19, 2),
(20, 1),
(20, 2),
(21, 1),
(21, 2),
(22, 1),
(22, 2),
(23, 1),
(23, 2),
(24, 1),
(24, 2),
(25, 1),
(25, 2),
(26, 1),
(26, 2),
(27, 1),
(27, 2),
(28, 1),
(28, 2),
(29, 1),
(29, 2),
(30, 1),
(30, 2),
(31, 1),
(31, 2),
(32, 1),
(32, 2),
(33, 1),
(33, 2),
(34, 1),
(34, 2),
(35, 1),
(35, 2),
(36, 1),
(36, 2),
(37, 1),
(37, 2),
(38, 1),
(38, 2),
(39, 1),
(39, 2),
(40, 1),
(40, 2),
(41, 1),
(41, 2),
(42, 1),
(42, 2),
(43, 1),
(43, 2),
(44, 1),
(44, 2),
(45, 1),
(45, 2),
(46, 1),
(46, 2),
(47, 1),
(47, 2),
(48, 1),
(48, 2),
(49, 1),
(49, 2),
(50, 1),
(50, 2),
(51, 1),
(51, 2),
(52, 1),
(52, 2),
(53, 1),
(53, 2),
(54, 1),
(54, 2),
(55, 1),
(55, 2),
(56, 1),
(56, 2),
(57, 1),
(57, 2),
(58, 1),
(58, 2),
(59, 1),
(59, 2),
(60, 1),
(60, 2),
(61, 1),
(61, 2),
(62, 1),
(62, 2),
(63, 1),
(63, 2),
(64, 1),
(64, 2),
(65, 1),
(65, 2),
(66, 1),
(66, 2),
(67, 1),
(67, 2),
(68, 1),
(68, 2),
(69, 1),
(69, 2),
(70, 1),
(70, 2),
(71, 1),
(71, 2),
(72, 1),
(72, 2),
(73, 1),
(73, 2),
(74, 1),
(74, 2),
(75, 1),
(75, 2),
(76, 1),
(76, 2),
(77, 1),
(77, 2),
(78, 1),
(78, 2),
(79, 1),
(79, 2),
(80, 1),
(80, 2),
(81, 1),
(81, 2),
(82, 1),
(82, 2),
(83, 1),
(83, 2),
(84, 1),
(84, 2),
(85, 1),
(85, 2),
(86, 1),
(86, 2),
(87, 1),
(87, 2),
(88, 1),
(88, 2),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(97, 1),
(98, 1),
(99, 1),
(100, 1),
(101, 1),
(102, 1),
(103, 1),
(104, 1),
(104, 2),
(105, 1),
(105, 2),
(106, 1),
(106, 2),
(107, 1),
(107, 2),
(108, 1),
(108, 2),
(109, 1),
(109, 2),
(110, 1),
(110, 2),
(111, 1),
(111, 2),
(112, 1),
(112, 2),
(113, 1),
(113, 2),
(114, 1),
(114, 2),
(115, 1),
(115, 2),
(116, 1),
(116, 2),
(117, 1),
(117, 2),
(118, 1),
(118, 2),
(119, 1),
(119, 2),
(120, 1),
(120, 2),
(121, 1),
(121, 2),
(122, 1),
(122, 2),
(123, 1),
(123, 2),
(124, 1),
(124, 2),
(125, 1),
(125, 2),
(126, 1),
(126, 2),
(127, 1),
(127, 2),
(128, 1),
(128, 2),
(129, 1),
(129, 2);

-- --------------------------------------------------------

--
-- Table structure for table `diva_permission_user`
--

CREATE TABLE `diva_permission_user` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_permission_user`
--

INSERT INTO `diva_permission_user` (`permission_id`, `user_id`, `user_type`) VALUES
(16, 4, 'users'),
(17, 4, 'users'),
(130, 4, 'users');

-- --------------------------------------------------------

--
-- Table structure for table `diva_plugins`
--

CREATE TABLE `diva_plugins` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `short_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` smallint(6) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_publish_options`
--

CREATE TABLE `diva_publish_options` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `publishable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publishable_id` int(11) NOT NULL,
  `start_publishing` datetime NOT NULL,
  `end_publishing` datetime NOT NULL,
  `publish_by` mediumint(8) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `show_in_main_page` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_publish_options`
--

INSERT INTO `diva_publish_options` (`id`, `publishable_type`, `publishable_id`, `start_publishing`, `end_publishing`, `publish_by`, `created_at`, `updated_at`, `show_in_main_page`) VALUES
(1, 'pages', 2, '2021-07-10 11:25:00', '2021-08-07 17:15:00', 1, '2021-07-12 07:31:42', '2021-07-12 07:31:42', 1),
(2, 'pages', 3, '2021-07-11 19:30:00', '2021-09-01 14:10:00', 1, '2021-07-12 07:42:35', '2021-07-12 07:42:35', 1),
(3, 'pages', 4, '2021-07-07 22:55:00', '2021-09-01 14:10:00', 1, '2021-07-12 08:09:39', '2021-07-12 09:59:32', 1),
(4, 'pages', 5, '2021-07-11 13:05:00', '2021-07-31 22:55:00', 1, '2021-07-12 09:41:19', '2021-07-12 09:41:19', 1),
(5, 'pages', 6, '2021-07-07 22:55:00', '2021-07-31 22:55:00', 1, '2021-07-12 09:47:06', '2021-07-12 09:47:06', 1),
(6, 'posts', 7, '2021-07-10 20:00:00', '2021-08-07 22:50:00', 1, '2021-07-13 05:08:02', '2021-07-13 05:26:32', 1),
(7, 'posts', 8, '2021-07-07 23:10:00', '2021-08-07 22:50:00', 1, '2021-07-13 05:09:12', '2021-07-13 05:26:52', 1),
(8, 'posts', 9, '2021-07-07 23:10:00', '2021-09-01 14:10:00', 1, '2021-07-13 05:10:38', '2021-07-13 05:27:16', 1),
(9, 'events', 10, '2021-07-07 22:50:00', '2021-07-31 22:55:00', 1, '2021-07-13 05:42:41', '2021-07-13 05:42:41', 1),
(10, 'events', 11, '2021-07-07 23:10:00', '2021-08-07 22:50:00', 1, '2021-07-13 05:50:21', '2021-07-13 05:50:21', 1),
(11, 'pages', 12, '2021-07-12 12:00:00', '2021-08-07 17:15:00', 1, '2021-07-13 08:35:48', '2021-07-13 08:35:48', 1),
(12, 'pages', 13, '2021-07-07 22:55:00', '2021-09-01 14:10:00', 1, '2021-07-13 08:37:52', '2021-07-13 08:37:52', 1),
(13, 'pages', 14, '2021-07-07 23:10:00', '2021-08-07 22:50:00', 1, '2021-07-13 08:40:26', '2021-07-13 08:40:26', 1);

-- --------------------------------------------------------

--
-- Table structure for table `diva_regions`
--

CREATE TABLE `diva_regions` (
  `id` int(10) UNSIGNED NOT NULL,
  `region` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_roles`
--

CREATE TABLE `diva_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_roles`
--

INSERT INTO `diva_roles` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'superadministrator', 1, '2021-07-11 13:34:07', '2021-07-11 13:34:07'),
(2, 'administrator', 1, '2021-07-11 13:34:41', '2021-07-11 13:34:41'),
(3, 'user', 1, '2021-07-11 13:34:51', '2021-07-11 13:34:51');

-- --------------------------------------------------------

--
-- Table structure for table `diva_roles_translations`
--

CREATE TABLE `diva_roles_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_roles_translations`
--

INSERT INTO `diva_roles_translations` (`id`, `display_name`, `description`, `role_id`, `language`, `created_at`, `updated_at`) VALUES
(1, 'Superadministrator', 'Superadministrator', 1, 'en', '2021-07-11 13:34:08', '2021-07-11 13:34:08'),
(2, 'Administrator', 'Administrator', 2, 'en', '2021-07-11 13:34:41', '2021-07-11 13:34:41'),
(3, 'User', 'User', 3, 'en', '2021-07-11 13:34:51', '2021-07-11 13:34:51');

-- --------------------------------------------------------

--
-- Table structure for table `diva_role_user`
--

CREATE TABLE `diva_role_user` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_role_user`
--

INSERT INTO `diva_role_user` (`role_id`, `user_id`, `user_type`) VALUES
(1, 1, 'users'),
(2, 2, 'users'),
(3, 3, 'users');

-- --------------------------------------------------------

--
-- Table structure for table `diva_sliders`
--

CREATE TABLE `diva_sliders` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `main` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_sliders`
--

INSERT INTO `diva_sliders` (`id`, `title`, `short_code`, `main`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Main Slider', 'main-slider', 1, 1, '2021-07-11 13:35:22', '2021-07-11 13:35:22');

-- --------------------------------------------------------

--
-- Table structure for table `diva_social_providers`
--

CREATE TABLE `diva_social_providers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_swap_items`
--

CREATE TABLE `diva_swap_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `advertisement_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_swap_item_translations`
--

CREATE TABLE `diva_swap_item_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `swap_item_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_taxonomies`
--

CREATE TABLE `diva_taxonomies` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL,
  `parent` mediumint(9) NOT NULL DEFAULT 0,
  `sorting` int(11) NOT NULL DEFAULT 0,
  `type` smallint(6) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gallery_id` mediumint(8) UNSIGNED DEFAULT NULL,
  `layout_model_id` mediumint(8) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_taxonomies`
--

INSERT INTO `diva_taxonomies` (`id`, `status`, `parent`, `sorting`, `type`, `deleted_at`, `created_at`, `updated_at`, `icon`, `gallery_id`, `layout_model_id`) VALUES
(1, 1, 0, 1, 1, NULL, '2021-07-11 13:35:21', '2021-07-11 13:35:21', NULL, NULL, NULL),
(2, 1, 0, 1, 1, NULL, '2021-07-12 09:12:29', '2021-07-12 09:12:29', NULL, NULL, NULL),
(3, 1, 0, 3, 1, NULL, '2021-07-12 13:02:08', '2021-07-12 13:02:08', 'fas fa-align-justify', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `diva_taxonomy_translations`
--

CREATE TABLE `diva_taxonomy_translations` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `taxonomy_id` mediumint(8) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_taxonomy_translations`
--

INSERT INTO `diva_taxonomy_translations` (`id`, `name`, `slug`, `description`, `language`, `taxonomy_id`, `created_at`, `updated_at`) VALUES
(1, 'How To Work ', 'How_To_Work_', NULL, 'en', 1, '2021-07-11 13:35:22', '2021-07-11 13:35:22'),
(2, 'الأبحاث', 'الأبحاث_1', NULL, 'ar', 2, '2021-07-12 09:12:30', '2021-07-12 09:13:00'),
(3, 'researches', 'researches', NULL, 'en', 2, '2021-07-12 09:12:30', '2021-07-12 09:13:00'),
(4, 'مجلة الكلية', 'مجلة_الكلية', NULL, 'ar', 3, '2021-07-12 13:02:10', '2021-07-12 13:02:10'),
(5, 'college magazine', 'college_magazine', NULL, 'en', 3, '2021-07-12 13:02:10', '2021-07-12 13:02:10');

-- --------------------------------------------------------

--
-- Table structure for table `diva_themes`
--

CREATE TABLE `diva_themes` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `thumbnail` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_themes`
--

INSERT INTO `diva_themes` (`id`, `name`, `path`, `thumbnail`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'AFMC project', 'AFMC-project', '', 1, NULL, '2021-07-11 13:35:21', '2021-07-11 13:35:21');

-- --------------------------------------------------------

--
-- Table structure for table `diva_translations`
--

CREATE TABLE `diva_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `group` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `key` text COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diva_users`
--

CREATE TABLE `diva_users` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diva_users`
--

INSERT INTO `diva_users` (`id`, `first_name`, `last_name`, `email`, `email_verified_at`, `password`, `phone`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Superadministrator', 'Doe', 'superadministrator@app.com', NULL, '$2y$10$5RimdJKm.kde5qy8Cb..iO4.n/892DVFsyBEWzlaz7lvSHdLHbWqO', '644-897-385', 1, 'fhLluZHci6SwYbbUeIBcG1Ld4pfUS4YzcHERNPNhqqXT2JIi11yv6LEA4yWS', '2021-07-11 13:34:40', '2021-07-11 13:34:40'),
(2, 'Administrator', 'Doe', 'administrator@app.com', NULL, '$2y$10$pMLAiZgnVHE6PONp8eKTXetM4UcSklzIwARKIJV1dJoJGkHka93i.', '248-394-654', 1, NULL, '2021-07-11 13:34:51', '2021-07-11 13:34:51'),
(3, 'User', 'Doe', 'user@app.com', NULL, '$2y$10$frcMcGEHSTqjr30qXzKOt.lgTsI5pHEQTGHUKXpSC.ncyS3A86joq', '803-214-758', 1, NULL, '2021-07-11 13:34:52', '2021-07-11 13:34:52'),
(4, 'Cru User', 'Doe', 'cru_user@app.com', NULL, '$2y$10$ihrWOm9H2JMbJa80pHPjg.7Jflx1AJz2ZObxzpE16KfJR.xOGlAGa', '148-464-584', 1, 'uLc1pVAwjt', '2021-07-11 13:34:52', '2021-07-11 13:34:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `diva_activity_log`
--
ALTER TABLE `diva_activity_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_activity_log_log_name_index` (`log_name`),
  ADD KEY `subject` (`subject_id`,`subject_type`),
  ADD KEY `causer` (`causer_id`,`causer_type`);

--
-- Indexes for table `diva_advertisements`
--
ALTER TABLE `diva_advertisements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_advertisements_category_id_foreign` (`category_id`),
  ADD KEY `diva_advertisements_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `diva_advertisement_offers`
--
ALTER TABLE `diva_advertisement_offers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_advertisement_offers_customer_id_foreign` (`customer_id`),
  ADD KEY `diva_advertisement_offers_advertisement_id_foreign` (`advertisement_id`);

--
-- Indexes for table `diva_advertisement_translations`
--
ALTER TABLE `diva_advertisement_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_advertisement_translations_advertisement_id_foreign` (`advertisement_id`),
  ADD KEY `diva_advertisement_translations_language_index` (`language`);

--
-- Indexes for table `diva_api_keys`
--
ALTER TABLE `diva_api_keys`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_api_keys_keyable_type_keyable_id_index` (`keyable_type`,`keyable_id`),
  ADD KEY `diva_api_keys_key_index` (`key`);

--
-- Indexes for table `diva_countries`
--
ALTER TABLE `diva_countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diva_currencies`
--
ALTER TABLE `diva_currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diva_customers`
--
ALTER TABLE `diva_customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `diva_customers_email_unique` (`email`),
  ADD UNIQUE KEY `diva_customers_phone_unique` (`phone`),
  ADD KEY `diva_customers_created_by_foreign` (`created_by`),
  ADD KEY `diva_customers_modified_by_foreign` (`modified_by`);

--
-- Indexes for table `diva_customer_addresses`
--
ALTER TABLE `diva_customer_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_customer_addresses_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `diva_enums`
--
ALTER TABLE `diva_enums`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diva_enum_translations`
--
ALTER TABLE `diva_enum_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_enum_translations_enum_id_foreign` (`enum_id`),
  ADD KEY `diva_enum_translations_language_index` (`language`);

--
-- Indexes for table `diva_event_details`
--
ALTER TABLE `diva_event_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_event_details_node_id_foreign` (`node_id`);

--
-- Indexes for table `diva_extensions`
--
ALTER TABLE `diva_extensions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diva_external_links`
--
ALTER TABLE `diva_external_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_external_links_menu_link_id_foreign` (`menu_link_id`);

--
-- Indexes for table `diva_failed_jobs`
--
ALTER TABLE `diva_failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diva_favourites`
--
ALTER TABLE `diva_favourites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_favourites_advertisement_id_foreign` (`advertisement_id`),
  ADD KEY `diva_favourites_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `diva_feedbacks`
--
ALTER TABLE `diva_feedbacks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diva_galleries`
--
ALTER TABLE `diva_galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diva_images`
--
ALTER TABLE `diva_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diva_image_external_links`
--
ALTER TABLE `diva_image_external_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_image_external_links_image_id_foreign` (`image_id`);

--
-- Indexes for table `diva_image_internal_links`
--
ALTER TABLE `diva_image_internal_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_image_internal_links_image_id_foreign` (`image_id`);

--
-- Indexes for table `diva_image_translations`
--
ALTER TABLE `diva_image_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_image_translations_image_id_foreign` (`image_id`),
  ADD KEY `diva_image_translations_language_index` (`language`);

--
-- Indexes for table `diva_internal_links`
--
ALTER TABLE `diva_internal_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_internal_links_menu_link_id_foreign` (`menu_link_id`);

--
-- Indexes for table `diva_languages`
--
ALTER TABLE `diva_languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diva_layout_models`
--
ALTER TABLE `diva_layout_models`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_layout_models_layout_type_foreign` (`layout_type`),
  ADD KEY `diva_layout_models_theme_id_foreign` (`theme_id`);

--
-- Indexes for table `diva_layout_model_plugins`
--
ALTER TABLE `diva_layout_model_plugins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_layout_model_plugins_plugin_id_foreign` (`plugin_id`),
  ADD KEY `diva_layout_model_plugins_layout_model_id_foreign` (`layout_model_id`);

--
-- Indexes for table `diva_menu_groups`
--
ALTER TABLE `diva_menu_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diva_menu_links`
--
ALTER TABLE `diva_menu_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_menu_links_menu_group_id_foreign` (`menu_group_id`),
  ADD KEY `diva_menu_links_gallery_id_foreign` (`gallery_id`);

--
-- Indexes for table `diva_menu_link_translations`
--
ALTER TABLE `diva_menu_link_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_menu_link_translations_menu_link_id_foreign` (`menu_link_id`),
  ADD KEY `diva_menu_link_translations_language_index` (`language`);

--
-- Indexes for table `diva_migrations`
--
ALTER TABLE `diva_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diva_modules`
--
ALTER TABLE `diva_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diva_module_translations`
--
ALTER TABLE `diva_module_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_module_translations_module_id_foreign` (`module_id`),
  ADD KEY `diva_module_translations_language_index` (`language`);

--
-- Indexes for table `diva_newsletter_subscribers`
--
ALTER TABLE `diva_newsletter_subscribers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `diva_newsletter_subscribers_email_unique` (`email`);

--
-- Indexes for table `diva_nodes`
--
ALTER TABLE `diva_nodes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_nodes_layout_model_id_foreign` (`layout_model_id`);

--
-- Indexes for table `diva_node_galleries`
--
ALTER TABLE `diva_node_galleries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_node_galleries_node_id_foreign` (`node_id`),
  ADD KEY `diva_node_galleries_gallery_id_foreign` (`gallery_id`);

--
-- Indexes for table `diva_node_taxonomies`
--
ALTER TABLE `diva_node_taxonomies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_node_taxonomies_node_id_foreign` (`node_id`),
  ADD KEY `diva_node_taxonomies_taxonomy_id_foreign` (`taxonomy_id`);

--
-- Indexes for table `diva_node_translations`
--
ALTER TABLE `diva_node_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_node_translations_node_id_foreign` (`node_id`),
  ADD KEY `diva_node_translations_language_index` (`language`);

--
-- Indexes for table `diva_notifications`
--
ALTER TABLE `diva_notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `diva_oauth_access_tokens`
--
ALTER TABLE `diva_oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `diva_oauth_auth_codes`
--
ALTER TABLE `diva_oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `diva_oauth_clients`
--
ALTER TABLE `diva_oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `diva_oauth_personal_access_clients`
--
ALTER TABLE `diva_oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diva_oauth_refresh_tokens`
--
ALTER TABLE `diva_oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diva_password_resets`
--
ALTER TABLE `diva_password_resets`
  ADD KEY `diva_password_resets_email_index` (`email`);

--
-- Indexes for table `diva_permissions`
--
ALTER TABLE `diva_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `diva_permissions_name_unique` (`name`);

--
-- Indexes for table `diva_permissions_translations`
--
ALTER TABLE `diva_permissions_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `diva_permissions_translations_permission_id_language_unique` (`permission_id`,`language`),
  ADD KEY `diva_permissions_translations_language_index` (`language`);

--
-- Indexes for table `diva_permission_role`
--
ALTER TABLE `diva_permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `diva_permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `diva_permission_user`
--
ALTER TABLE `diva_permission_user`
  ADD PRIMARY KEY (`user_id`,`permission_id`,`user_type`),
  ADD KEY `diva_permission_user_permission_id_foreign` (`permission_id`);

--
-- Indexes for table `diva_plugins`
--
ALTER TABLE `diva_plugins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diva_publish_options`
--
ALTER TABLE `diva_publish_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_publish_options_publish_by_foreign` (`publish_by`);

--
-- Indexes for table `diva_regions`
--
ALTER TABLE `diva_regions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_regions_country_id_foreign` (`country_id`);

--
-- Indexes for table `diva_roles`
--
ALTER TABLE `diva_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `diva_roles_name_unique` (`name`);

--
-- Indexes for table `diva_roles_translations`
--
ALTER TABLE `diva_roles_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `diva_roles_translations_role_id_language_unique` (`role_id`,`language`),
  ADD KEY `diva_roles_translations_language_index` (`language`);

--
-- Indexes for table `diva_role_user`
--
ALTER TABLE `diva_role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`,`user_type`),
  ADD KEY `diva_role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `diva_sliders`
--
ALTER TABLE `diva_sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diva_social_providers`
--
ALTER TABLE `diva_social_providers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_social_providers_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `diva_swap_items`
--
ALTER TABLE `diva_swap_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_swap_items_advertisement_id_foreign` (`advertisement_id`);

--
-- Indexes for table `diva_swap_item_translations`
--
ALTER TABLE `diva_swap_item_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_swap_item_translations_swap_item_id_foreign` (`swap_item_id`),
  ADD KEY `diva_swap_item_translations_language_index` (`language`);

--
-- Indexes for table `diva_taxonomies`
--
ALTER TABLE `diva_taxonomies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_taxonomies_gallery_id_foreign` (`gallery_id`),
  ADD KEY `diva_taxonomies_layout_model_id_foreign` (`layout_model_id`);

--
-- Indexes for table `diva_taxonomy_translations`
--
ALTER TABLE `diva_taxonomy_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_taxonomy_translations_taxonomy_id_foreign` (`taxonomy_id`),
  ADD KEY `diva_taxonomy_translations_language_index` (`language`);

--
-- Indexes for table `diva_themes`
--
ALTER TABLE `diva_themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diva_translations`
--
ALTER TABLE `diva_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diva_translations_language_id_foreign` (`language_id`);

--
-- Indexes for table `diva_users`
--
ALTER TABLE `diva_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `diva_users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `diva_activity_log`
--
ALTER TABLE `diva_activity_log`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `diva_advertisements`
--
ALTER TABLE `diva_advertisements`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `diva_advertisement_offers`
--
ALTER TABLE `diva_advertisement_offers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `diva_advertisement_translations`
--
ALTER TABLE `diva_advertisement_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `diva_api_keys`
--
ALTER TABLE `diva_api_keys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `diva_countries`
--
ALTER TABLE `diva_countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `diva_currencies`
--
ALTER TABLE `diva_currencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `diva_customers`
--
ALTER TABLE `diva_customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `diva_customer_addresses`
--
ALTER TABLE `diva_customer_addresses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `diva_enums`
--
ALTER TABLE `diva_enums`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `diva_enum_translations`
--
ALTER TABLE `diva_enum_translations`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `diva_event_details`
--
ALTER TABLE `diva_event_details`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `diva_extensions`
--
ALTER TABLE `diva_extensions`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `diva_external_links`
--
ALTER TABLE `diva_external_links`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `diva_failed_jobs`
--
ALTER TABLE `diva_failed_jobs`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `diva_favourites`
--
ALTER TABLE `diva_favourites`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `diva_feedbacks`
--
ALTER TABLE `diva_feedbacks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `diva_galleries`
--
ALTER TABLE `diva_galleries`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `diva_images`
--
ALTER TABLE `diva_images`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `diva_image_external_links`
--
ALTER TABLE `diva_image_external_links`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `diva_image_internal_links`
--
ALTER TABLE `diva_image_internal_links`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `diva_image_translations`
--
ALTER TABLE `diva_image_translations`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `diva_internal_links`
--
ALTER TABLE `diva_internal_links`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `diva_languages`
--
ALTER TABLE `diva_languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `diva_layout_models`
--
ALTER TABLE `diva_layout_models`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `diva_layout_model_plugins`
--
ALTER TABLE `diva_layout_model_plugins`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `diva_menu_groups`
--
ALTER TABLE `diva_menu_groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `diva_menu_links`
--
ALTER TABLE `diva_menu_links`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `diva_menu_link_translations`
--
ALTER TABLE `diva_menu_link_translations`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `diva_migrations`
--
ALTER TABLE `diva_migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `diva_modules`
--
ALTER TABLE `diva_modules`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `diva_module_translations`
--
ALTER TABLE `diva_module_translations`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `diva_newsletter_subscribers`
--
ALTER TABLE `diva_newsletter_subscribers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `diva_nodes`
--
ALTER TABLE `diva_nodes`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `diva_node_galleries`
--
ALTER TABLE `diva_node_galleries`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `diva_node_taxonomies`
--
ALTER TABLE `diva_node_taxonomies`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `diva_node_translations`
--
ALTER TABLE `diva_node_translations`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `diva_oauth_clients`
--
ALTER TABLE `diva_oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `diva_oauth_personal_access_clients`
--
ALTER TABLE `diva_oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `diva_permissions`
--
ALTER TABLE `diva_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `diva_permissions_translations`
--
ALTER TABLE `diva_permissions_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `diva_plugins`
--
ALTER TABLE `diva_plugins`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `diva_publish_options`
--
ALTER TABLE `diva_publish_options`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `diva_regions`
--
ALTER TABLE `diva_regions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `diva_roles`
--
ALTER TABLE `diva_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `diva_roles_translations`
--
ALTER TABLE `diva_roles_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `diva_sliders`
--
ALTER TABLE `diva_sliders`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `diva_social_providers`
--
ALTER TABLE `diva_social_providers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `diva_swap_items`
--
ALTER TABLE `diva_swap_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `diva_swap_item_translations`
--
ALTER TABLE `diva_swap_item_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `diva_taxonomies`
--
ALTER TABLE `diva_taxonomies`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `diva_taxonomy_translations`
--
ALTER TABLE `diva_taxonomy_translations`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `diva_themes`
--
ALTER TABLE `diva_themes`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `diva_translations`
--
ALTER TABLE `diva_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `diva_users`
--
ALTER TABLE `diva_users`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `diva_advertisements`
--
ALTER TABLE `diva_advertisements`
  ADD CONSTRAINT `diva_advertisements_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `diva_taxonomies` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `diva_advertisements_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `diva_customers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `diva_advertisement_offers`
--
ALTER TABLE `diva_advertisement_offers`
  ADD CONSTRAINT `diva_advertisement_offers_advertisement_id_foreign` FOREIGN KEY (`advertisement_id`) REFERENCES `diva_advertisements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `diva_advertisement_offers_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `diva_customers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `diva_advertisement_translations`
--
ALTER TABLE `diva_advertisement_translations`
  ADD CONSTRAINT `diva_advertisement_translations_advertisement_id_foreign` FOREIGN KEY (`advertisement_id`) REFERENCES `diva_advertisements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `diva_customers`
--
ALTER TABLE `diva_customers`
  ADD CONSTRAINT `diva_customers_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `diva_users` (`id`),
  ADD CONSTRAINT `diva_customers_modified_by_foreign` FOREIGN KEY (`modified_by`) REFERENCES `diva_users` (`id`);

--
-- Constraints for table `diva_customer_addresses`
--
ALTER TABLE `diva_customer_addresses`
  ADD CONSTRAINT `diva_customer_addresses_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `diva_customers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `diva_enum_translations`
--
ALTER TABLE `diva_enum_translations`
  ADD CONSTRAINT `diva_enum_translations_enum_id_foreign` FOREIGN KEY (`enum_id`) REFERENCES `diva_enums` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `diva_event_details`
--
ALTER TABLE `diva_event_details`
  ADD CONSTRAINT `diva_event_details_node_id_foreign` FOREIGN KEY (`node_id`) REFERENCES `diva_nodes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `diva_external_links`
--
ALTER TABLE `diva_external_links`
  ADD CONSTRAINT `diva_external_links_menu_link_id_foreign` FOREIGN KEY (`menu_link_id`) REFERENCES `diva_menu_links` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `diva_favourites`
--
ALTER TABLE `diva_favourites`
  ADD CONSTRAINT `diva_favourites_advertisement_id_foreign` FOREIGN KEY (`advertisement_id`) REFERENCES `diva_advertisements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `diva_favourites_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `diva_customers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `diva_image_external_links`
--
ALTER TABLE `diva_image_external_links`
  ADD CONSTRAINT `diva_image_external_links_image_id_foreign` FOREIGN KEY (`image_id`) REFERENCES `diva_images` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `diva_image_internal_links`
--
ALTER TABLE `diva_image_internal_links`
  ADD CONSTRAINT `diva_image_internal_links_image_id_foreign` FOREIGN KEY (`image_id`) REFERENCES `diva_images` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `diva_image_translations`
--
ALTER TABLE `diva_image_translations`
  ADD CONSTRAINT `diva_image_translations_image_id_foreign` FOREIGN KEY (`image_id`) REFERENCES `diva_images` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `diva_internal_links`
--
ALTER TABLE `diva_internal_links`
  ADD CONSTRAINT `diva_internal_links_menu_link_id_foreign` FOREIGN KEY (`menu_link_id`) REFERENCES `diva_menu_links` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `diva_layout_models`
--
ALTER TABLE `diva_layout_models`
  ADD CONSTRAINT `diva_layout_models_layout_type_foreign` FOREIGN KEY (`layout_type`) REFERENCES `diva_enums` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `diva_layout_models_theme_id_foreign` FOREIGN KEY (`theme_id`) REFERENCES `diva_themes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `diva_layout_model_plugins`
--
ALTER TABLE `diva_layout_model_plugins`
  ADD CONSTRAINT `diva_layout_model_plugins_layout_model_id_foreign` FOREIGN KEY (`layout_model_id`) REFERENCES `diva_layout_models` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `diva_layout_model_plugins_plugin_id_foreign` FOREIGN KEY (`plugin_id`) REFERENCES `diva_plugins` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `diva_menu_links`
--
ALTER TABLE `diva_menu_links`
  ADD CONSTRAINT `diva_menu_links_gallery_id_foreign` FOREIGN KEY (`gallery_id`) REFERENCES `diva_galleries` (`id`),
  ADD CONSTRAINT `diva_menu_links_menu_group_id_foreign` FOREIGN KEY (`menu_group_id`) REFERENCES `diva_menu_groups` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `diva_menu_link_translations`
--
ALTER TABLE `diva_menu_link_translations`
  ADD CONSTRAINT `diva_menu_link_translations_menu_link_id_foreign` FOREIGN KEY (`menu_link_id`) REFERENCES `diva_menu_links` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `diva_module_translations`
--
ALTER TABLE `diva_module_translations`
  ADD CONSTRAINT `diva_module_translations_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `diva_modules` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `diva_nodes`
--
ALTER TABLE `diva_nodes`
  ADD CONSTRAINT `diva_nodes_layout_model_id_foreign` FOREIGN KEY (`layout_model_id`) REFERENCES `diva_layout_models` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `diva_node_galleries`
--
ALTER TABLE `diva_node_galleries`
  ADD CONSTRAINT `diva_node_galleries_gallery_id_foreign` FOREIGN KEY (`gallery_id`) REFERENCES `diva_galleries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `diva_node_galleries_node_id_foreign` FOREIGN KEY (`node_id`) REFERENCES `diva_nodes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `diva_node_taxonomies`
--
ALTER TABLE `diva_node_taxonomies`
  ADD CONSTRAINT `diva_node_taxonomies_node_id_foreign` FOREIGN KEY (`node_id`) REFERENCES `diva_nodes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `diva_node_taxonomies_taxonomy_id_foreign` FOREIGN KEY (`taxonomy_id`) REFERENCES `diva_taxonomies` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `diva_node_translations`
--
ALTER TABLE `diva_node_translations`
  ADD CONSTRAINT `diva_node_translations_node_id_foreign` FOREIGN KEY (`node_id`) REFERENCES `diva_nodes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `diva_permissions_translations`
--
ALTER TABLE `diva_permissions_translations`
  ADD CONSTRAINT `diva_permissions_translations_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `diva_permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `diva_permission_role`
--
ALTER TABLE `diva_permission_role`
  ADD CONSTRAINT `diva_permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `diva_permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `diva_permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `diva_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `diva_permission_user`
--
ALTER TABLE `diva_permission_user`
  ADD CONSTRAINT `diva_permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `diva_permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `diva_publish_options`
--
ALTER TABLE `diva_publish_options`
  ADD CONSTRAINT `diva_publish_options_publish_by_foreign` FOREIGN KEY (`publish_by`) REFERENCES `diva_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `diva_regions`
--
ALTER TABLE `diva_regions`
  ADD CONSTRAINT `diva_regions_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `diva_countries` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `diva_roles_translations`
--
ALTER TABLE `diva_roles_translations`
  ADD CONSTRAINT `diva_roles_translations_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `diva_roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `diva_role_user`
--
ALTER TABLE `diva_role_user`
  ADD CONSTRAINT `diva_role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `diva_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `diva_social_providers`
--
ALTER TABLE `diva_social_providers`
  ADD CONSTRAINT `diva_social_providers_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `diva_customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `diva_swap_items`
--
ALTER TABLE `diva_swap_items`
  ADD CONSTRAINT `diva_swap_items_advertisement_id_foreign` FOREIGN KEY (`advertisement_id`) REFERENCES `diva_advertisements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `diva_swap_item_translations`
--
ALTER TABLE `diva_swap_item_translations`
  ADD CONSTRAINT `diva_swap_item_translations_swap_item_id_foreign` FOREIGN KEY (`swap_item_id`) REFERENCES `diva_swap_items` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `diva_taxonomies`
--
ALTER TABLE `diva_taxonomies`
  ADD CONSTRAINT `diva_taxonomies_gallery_id_foreign` FOREIGN KEY (`gallery_id`) REFERENCES `diva_galleries` (`id`),
  ADD CONSTRAINT `diva_taxonomies_layout_model_id_foreign` FOREIGN KEY (`layout_model_id`) REFERENCES `diva_layout_models` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `diva_taxonomy_translations`
--
ALTER TABLE `diva_taxonomy_translations`
  ADD CONSTRAINT `diva_taxonomy_translations_taxonomy_id_foreign` FOREIGN KEY (`taxonomy_id`) REFERENCES `diva_taxonomies` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `diva_translations`
--
ALTER TABLE `diva_translations`
  ADD CONSTRAINT `diva_translations_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `diva_languages` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
