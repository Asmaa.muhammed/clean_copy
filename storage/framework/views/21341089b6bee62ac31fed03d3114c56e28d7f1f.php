<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="TemplateMo">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">

    <title>Event Mangment</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo e(asset('public/'.$themePath."/")); ?>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">


    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="<?php echo e(asset('public/'.$themePath."/")); ?>/assets/css/fontawesome.css">
    <link rel="stylesheet" href="<?php echo e(asset('public/'.$themePath."/")); ?>/assets/css/templatemo-stand-blog.css">
    <link rel="stylesheet" href="<?php echo e(asset('public/'.$themePath."/")); ?>/assets/css/owl.css">
<!--

TemplateMo 551 Stand Blog

https://templatemo.com/tm-551-stand-blog

-->
  </head>

  <body>

    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>  
    <!-- ***** Preloader End ***** -->

    <!-- Header -->
    <header class="">
      <nav class="navbar navbar-expand-lg">
        <div class="container">
          <a class="navbar-brand" href="index.html"><h2>Speakers<em>.</em></h2></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item active">
                <a class="nav-link" href="index.html">Home
                  <span class="sr-only">(current)</span>
                </a>
              </li> 
              <li class="nav-item">
                <a class="nav-link" href="about.html">About Us</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="blog.html">Blog Entries</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="post-details.html">Post Details</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="contact.html">Contact Us</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>

    <!-- Page Content -->
    <section class="blog-posts">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <div class="all-blog-posts">
              <div class="row">
                <?php $__currentLoopData = $speakers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $speaker): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <div class="col-lg-12">
                    <div class="blog-post">
                      <div class="blog-thumb">
                        <img src="<?php echo e($speaker->image->image_url); ?>" alt="">
                      </div>
                      <div class="down-content">
                        <a href="<?php echo e(route('person',$speaker->slug)); ?>"><h4><?php echo e($speaker->name); ?></h4></a>
                        <a href="<?php echo e(route('person',$speaker->slug)); ?>"><h4><?php echo e($speaker->title); ?></h4></a>
                        <ul class="speaker-info">
                          <li><a href="#"><?php echo e($speaker->portfolio_link); ?></a></li>
                        </ul>
                        <p><?php echo e($speaker->about); ?></p>
                        <p><?php echo e($speaker->description); ?></p>
                      </div>
                    </div>
                  </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
               
              </div>
            </div>
          </div>
          
        </div>
      </div>
    </section>

    
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <ul class="social-icons">
              <li><a href="#">Facebook</a></li>
              <li><a href="#">Twitter</a></li>
              <li><a href="#">Behance</a></li>
              <li><a href="#">Linkedin</a></li>
              <li><a href="#">Dribbble</a></li>
            </ul>
          </div>
          <div class="col-lg-12">
            <div class="copyright-text">
              <p>Copyright 2020 Stand Blog Co.
                    
                 | Design: <a rel="nofollow" href="https://templatemo.com" target="_parent">TemplateMo</a></p>
            </div>
          </div>
        </div>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo e(asset('public/'.$themePath."/")); ?>/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo e(asset('public/'.$themePath."/")); ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Additional Scripts -->
    <script src="<?php echo e(asset('public/'.$themePath."/")); ?>/assets/js/custom.js"></script>
    <script src="<?php echo e(asset('public/'.$themePath."/")); ?>/assets/js/owl.js"></script>
    <script src="<?php echo e(asset('public/'.$themePath."/")); ?>/assets/js/slick.js"></script>
    <script src="<?php echo e(asset('public/'.$themePath."/")); ?>/assets/js/isotope.js"></script>
    <script src="<?php echo e(asset('public/'.$themePath."/")); ?>/assets/js/accordions.js"></script>

    <script language = "text/Javascript"> 
      cleared[0] = cleared[1] = cleared[2] = 0; //set a cleared flag for each field
      function clearField(t){                   //declaring the array outside of the
      if(! cleared[t.id]){                      // function makes it static and global
          cleared[t.id] = 1;  // you could use true and false, but that's more typing
          t.value='';         // with more chance of typos
          t.style.color='#fff';
          }
      }
    </script>

  </body>
</html><?php /**PATH D:\xampp\htdocs\Event_mangnent\resources\views/event_mangment/speakers.blade.php ENDPATH**/ ?>