<?php $__env->startSection('content'); ?>

    <?php if($page->layoutModel->name != "Home Page"): ?>
        <?php $__currentLoopData = $page->layoutModel->plugins; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $plugin): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php echo app('arrilot.widget')->run($plugin->short_code,['page' => $page]); ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php else: ?>
        <section class="section section--padding">
            <div class="container">
                <div class="row">
                    <?php echo $page->body; ?>

                </div>
                <?php if($page->image): ?>
                    <div class="col-lg-9">
                        <img src="<?php echo e($page->image->image_url); ?>" class="img-fluid" alt="">
                    </div>
                <?php endif; ?>
                <div class="admission-apply">
                <span class="admission-apply__img" style="background-image: url('<?php echo e(asset('public/'.$themePath."/")); ?>/assets/media/image2.png')"></span>
                <h6 class="big-title big-title--secondary">UNIVERSITY ADMISSION NOW OPEN</h6>
                <button type="button" class="btn btn btn-danger text-white">APPLY NOW</button>
            </div>
            </div>
        </section>
    <?php endif; ?>


</section>

<?php $__env->stopSection(); ?>


<?php echo $__env->make($themePath.'.layouts.mainPage',["title"=>$page->title,"minor"=>($page->minor->page->title != "Main Page")? true:false,"MenuLinks"=>($page->minor->page->title == "Student")? $studentMenu:$StaffMenu], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/AFCM/resources/views/AFMC-project/page.blade.php ENDPATH**/ ?>