<div class="row">
    <div class="col-md-7">
        <div class="kt-section">
            <div class="kt-section__body">
                <?php $__env->startComponent("admin.components.translatable_section"); ?>
                    <?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $__env->slot("translatable_{$language->language}"); ?>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label><?php echo e(__('persons.subject')); ?></label>
                                    <?php echo Form::text("subject:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                                    <?php if($errors->has("subject:{$language->language}")): ?>
                                        <span
                                            class="form-text text-danger"><?php echo e($errors->first("subject:{$language->language}")); ?></span>
                                    <?php endif; ?>
                                </div>
                                <div class="col-lg-6">
                                    <label class=""><?php echo e(__('persons.description')); ?></label>
                                    <?php echo Form::textarea("description:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                                    <?php if($errors->has("description:{$language->language}")): ?>
                                        <span
                                            class="form-text text-danger"><?php echo e($errors->first("description:{$language->language}")); ?></span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label><?php echo e(__('persons.place')); ?></label>
                                    <?php echo Form::text("place:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                                    <?php if($errors->has("place:{$language->language}")): ?>
                                        <span
                                            class="form-text text-danger"><?php echo e($errors->first("place:{$language->language}")); ?></span>
                                    <?php endif; ?>
                                </div>
                                <div class="col-lg-6">
                                    <label><?php echo e(__('persons.location')); ?></label>
                                    <?php echo Form::text("location:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                                    <?php if($errors->has("location:{$language->language}")): ?>
                                        <span
                                            class="form-text text-danger"><?php echo e($errors->first("location:{$language->language}")); ?></span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            
                            <?php $__env->endSlot(); ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php echo $__env->renderComponent(); ?>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label><?php echo e(__('sessions.day')); ?></label>
                                    <?php echo Form::number("day", null, ['class'=>'form-control',  'min' => '1',  'max' => '8' ,'disabled'=>$disabled]); ?>

                                    <?php if($errors->has("day")): ?>
                                        <span
                                            class="form-text text-danger"><?php echo e($errors->first("day")); ?></span>
                                    <?php endif; ?>
                                </div>
                                <div class="col-lg-6">
                                    <label><?php echo e(__('sessions.date')); ?></label>
                                    <?php echo Form::date('date', null, ['class'=>'form-control', 'disabled'=>$disabled], 'required'); ?>

                                    <?php if($errors->has("date")): ?>
                                        <span class="form-text text-danger"><?php echo e($errors->first('date')); ?></span>
                                    <?php endif; ?>
                                </div>
                               
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label><?php echo e(__('sessions.time_from')); ?></label>
                                    <?php echo Form::time('time_from', null, ['class'=>'form-control', 'disabled'=>$disabled], 'required'); ?>

                                    <?php if($errors->has("time_from")): ?>
                                        <span class="form-text text-danger"><?php echo e($errors->first('time_from')); ?></span>
                                    <?php endif; ?>
                                </div>
                                <div class="col-lg-6">
                                    <label><?php echo e(__('sessions.time_to')); ?></label>
                                    <?php echo Form::time('time_to', null, ['class'=>'form-control', 'disabled'=>$disabled], 'required'); ?>

                                    <?php if($errors->has("time_to")): ?>
                                        <span class="form-text text-danger"><?php echo e($errors->first('time_to')); ?></span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label><?php echo e(__('sessions.event')); ?></label>
                                    <?php echo Form::select('node_id', $events, null, ['class'=>'form-control', 'id'=>'node_id', 'disabled'=>$disabled]); ?>

                                    <?php if($errors->has("node_id")): ?>
                                        <span class="form-text text-danger"><?php echo e($errors->first('node_id')); ?></span>
                                    <?php endif; ?>
                                </div>
                                <div class="col-lg-6">
                                    <label><?php echo e(__('sessions.persons')); ?></label>
                                    <?php echo Form::select('persons[]', $persons, null, ['class'=>'form-control multi', 'id'=>'persons','multiple', 'disabled'=>$disabled]); ?>

                                    <?php if($errors->has("persons")): ?>
                                        <span class="form-text text-danger"><?php echo e($errors->first('persons')); ?></span>
                                    <?php endif; ?>
                                </div>
                               
                            </div>
                            
            </div>
        </div>
    
    </div>
    <div class="col-md-1"></div>
    <div class="col-lg-4">
        <div class="kt-section">
            <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>
            <div class="kt-section__body">
                <?php echo $__env->make('admin.layout.form_components.status', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                
            </div>
        </div>
    </div>
</div>



<?php /**PATH D:\xampp\htdocs\clean_copy\resources\views/admin/sessions/form.blade.php ENDPATH**/ ?>