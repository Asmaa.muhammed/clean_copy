<?php $__env->startSection('subheader'); ?> <?php echo $__env->make('admin.layout.components.breadcrumb',['pageTitle'=>__('common.moduleTitle', ['module'=>$cmsModule->name]),'moduleName'=>$cmsModule->route,'pageName'=>__('common.editPageName', ['module'=>__($cmsModule->route.".singularModuleName")]) ], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile"
                 id="kt_page_portlet">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title"><?php echo e(__('common.edit', ['module'=>__('pages.singularModuleName')])); ?></h3>
                    </div>
                    <?php echo $__env->make("admin.layout.includes.form_button",['disabled'=>false,"module"=>$cmsModule->route], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>
                <div class="kt-portlet__body">
                    <?php echo Form::model($event,['route'=>['events.update', $event->id], 'method'=>'patch', 'class'=>'kt-form kt-form--labe-right','id'=>'kt_form']); ?>

                    <?php echo $__env->make('admin.events.form', ['disabled'=>false], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('additionalScripts'); ?>
    <!--begin::Page Scripts(used by this page) -->
    <?php echo $__env->make('admin.layout.includes.filemanger_scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('admin.layout.includes.tinyMCE_config', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <script>
        $(function () {
            $(".multi").select2();
        });
    </script>
    <!--end::Page Scripts -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', ['pageTitle'=>__('common.editPageTitle', ['module'=>$cmsModule->name])], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\clean_copy\resources\views/admin/events/edit.blade.php ENDPATH**/ ?>