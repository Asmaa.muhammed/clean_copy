<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo $__env->yieldContent('metaTags'); ?>

    <?php echo $__env->make($themePath.'.students.layouts.includes.styles', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->yieldContent('additionalStyles'); ?>

    <link rel="icon" href="<?php echo e(asset('public/'.$themePath."/")); ?>/assets/images/logo.png" type="image/x-icon">
    <title><?php echo e($title); ?></title>
</head>
<body>
<div id="root">

<?php echo $__env->yieldContent('header'); ?>
<main>
    <?php echo $__env->yieldContent('content'); ?>
</main>

<?php echo $__env->yieldContent('footer'); ?>
<?php echo $__env->make($themePath.'.students.layouts.includes.scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->yieldContent('additionalScripts'); ?>

</body>
</html>
<?php /**PATH /var/www/html/AFCM/resources/views/AFMC-project/layouts/main.blade.php ENDPATH**/ ?>