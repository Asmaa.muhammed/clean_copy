<?php $__env->startSection('subheader'); ?> <?php echo $__env->make('admin.layout.components.breadcrumb',['pageTitle'=>'Categories','moduleName'=>'categories','pageName'=> __('taxonomies.show_all_categories') ], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label"> <i class="flaticon-list"></i> &nbsp;<h3 class="kt-portlet__head-title">  <?php echo e(__('taxonomies.show_all_categories')); ?>

                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <div class="dropdown dropdown-inline">
                            <button type="button" class="btn btn-default btn-icon-sm dropdown-toggle"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="la la-download"></i> Export
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__section kt-nav__section--first">
                                        <span class="kt-nav__section-text">Choose an option</span>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-print"></i>
                                            <span class="kt-nav__link-text">Print</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-copy"></i>
                                            <span class="kt-nav__link-text">Copy</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-excel-o"></i>
                                            <span class="kt-nav__link-text">Excel</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-text-o"></i>
                                            <span class="kt-nav__link-text">CSV</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-pdf-o"></i>
                                            <span class="kt-nav__link-text">PDF</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        &nbsp;
                        <a href="<?php echo e(route('categories.create')); ?>" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            <?php echo e(__('translation::translation.add')); ?>

                        </a>
                        <a href="<?php echo e(route('categories.tree')); ?>" class="btn btn-warning btn-elevate btn-icon-sm">
                            <i class="la la-list-ul"></i>
                            <?php echo e(__('taxonomies.show_tree')); ?>

                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
        <?php echo $__env->make('admin.layout.components.flash_messages', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

            <!--begin: Datatable -->

        <?php echo $html->table(['class' => ' table table-striped- table-bordered table-hover table-checkable dataTable no-footer categories'], true); ?>

        <!--end: Datatable -->
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('additionalScripts'); ?>

    <?php echo $html->scripts(); ?>

    <script src="<?php echo e(asset('/public/admin')); ?>/assets/js/modules.js" type="text/javascript"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', ['pageTitle'=>'Categories |Index'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\clean_copy\resources\views/admin/categories/index.blade.php ENDPATH**/ ?>