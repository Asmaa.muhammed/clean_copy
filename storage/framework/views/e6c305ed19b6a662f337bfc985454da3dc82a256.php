<!-- end::Global Config -->
<script>
    var url = '<?php echo e(url('/')); ?>';
</script>
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": [
                    "#c5cbe3",
                    "#a1a8c3",
                    "#3d4465",
                    "#3e4466"
                ],
                "shape": [
                    "#f0f3ff",
                    "#d9dffa",
                    "#afb4d4",
                    "#646c9a"
                ]
            }
        }
    };
</script>

<!--begin::Global Theme Bundle(used by all pages) -->
<script src="<?php echo e(asset('/public/admin')); ?>/assets/plugins/global/plugins.bundle.js" type="text/javascript"></script>
<script src="<?php echo e(asset('/public/admin')); ?>/assets/js/scripts.bundle.js" type="text/javascript"></script>

<!--end::Global Theme Bundle -->

<!--begin::Page Vendors(used by this page) -->
<script src="<?php echo e(asset('/public/admin')); ?>/assets/plugins/custom/datatables/datatables.bundle.js" type="text/javascript"></script>

<!--end::Page Vendors -->

<!--begin::Page Scripts(used by this page) -->
<script src="<?php echo e(asset('/public/admin')); ?>/assets/js/pages/crud/datatables/basic/scrollable.js" type="text/javascript"></script>
<script src="<?php echo e(asset('/public/admin')); ?>/assets/plugins/custom/gmaps/gmaps.js" type="text/javascript"></script>

<!--end::Page Vendors -->

<!--begin::Page Scripts(used by this page) -->
<script src="<?php echo e(asset('/public/admin')); ?>/assets/js/pages/dashboard.js" type="text/javascript"></script>
<script src="<?php echo e(asset("public/admin/assets/js/fontawesome-iconpicker.js")); ?>"></script>
<script src="<?php echo e(asset('/public/admin')); ?>/assets/js/pages/components/extended/sweetalert2.js" type="text/javascript"></script>

<script>
    $("#submitButton").click(function () {

        $("#kt_form").submit()
        //console.log(  $("#kt_form").serializeArray() );


    })
</script>
<script src="<?php echo e(asset('public/admin')); ?>/assets/js/pages/crud/forms/widgets/bootstrap-datetimepicker.js" type="text/javascript"></script>
<?php /**PATH /var/www/html/AFCM/resources/views/admin/layout/includes/scripts.blade.php ENDPATH**/ ?>