<div class="row">
    <div class="col-md-7">
        <div class="kt-section">
            <div class="kt-section__body">
                <?php $__env->startComponent("admin.components.translatable_section"); ?>
                    <?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $__env->slot("translatable_{$language->language}"); ?>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label><?php echo e(__('events.title')); ?></label>
                                    <?php echo Form::text("title:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                                    <?php if($errors->has("title:{$language->language}")): ?>
                                        <span
                                            class="form-text text-danger"><?php echo e($errors->first("title:{$language->language}")); ?></span>
                                    <?php endif; ?>
                                </div>
                                <div class="col-lg-6">
                                    <label class=""><?php echo e(__('events.summary')); ?></label>
                                    <?php echo Form::textarea("summary:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                                    <?php if($errors->has("summary:{$language->language}")): ?>
                                        <span
                                            class="form-text text-danger"><?php echo e($errors->first("summary:{$language->language}")); ?></span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-8">
                                    <label class=""><?php echo e(__('events.body')); ?></label>
                                    <?php echo Form::textarea("body:{$language->language}", null, ['class'=>'form-control editor', 'disabled'=>$disabled]); ?>

                                    <?php if($errors->has("body:{$language->language}")): ?>
                                        <span
                                            class="form-text text-danger"><?php echo e($errors->first("body:{$language->language}")); ?></span>
                                    <?php endif; ?>
                                </div>


                            </div>
                        <?php $__env->endSlot(); ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php echo $__env->renderComponent(); ?>
            </div>
        </div>
        <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>
        <div class="kt-section">
            <h4 class="kt-section__title kt-section__title-lg"><?php echo e(__('events.image_and_galleries')); ?>:</h4>
            <div class="kt-section__body">
                <div class="form-group row">
                    <?php echo $__env->make('admin.layout.form_components.select_image', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <div class="col-lg-6">
                        <label><?php echo e(__('events.gallery')); ?></label>
                        <?php echo Form::select('gallery',$galleries,(isset($event) )? "" :'', ['class'=>'form-control ', 'id'=>'galleries','placeholder'=> __('events.gallery'), 'disabled'=>$disabled]); ?>

                        <?php if($errors->has("gallery")): ?>
                            <span class="form-text text-danger"><?php echo e($errors->first('gallery')); ?></span>
                        <?php endif; ?>
                    </div>

                </div>
            </div>
        </div>
        <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>
        <div class="kt-section">
            <h4 class="kt-section__title kt-section__title-lg"><?php echo e(__('pages.minor')); ?>:</h4>
            <div class="kt-section__body">
                <div class="form-group row">
                    <div class="col-lg-6">
                        <label><?php echo e(__('pages.minor')); ?></label>
                        <?php echo Form::select('minor',$minors,(isset($page) && $page->minor)? $page->minor->minor_id :'', ['class'=>'form-control ', 'id'=>'minors','placeholder'=> __('pages.minor'), 'disabled'=>$disabled]); ?>

                        <?php if($errors->has("minor")): ?>
                            <span class="form-text text-danger"><?php echo e($errors->first('minor')); ?></span>
                        <?php endif; ?>
                    </div>

                </div>
            </div>
        </div>
        <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>
        <div class="kt-section">
            <h4 class="kt-section__title kt-section__title-lg"><?php echo e(__('events.taxonomies')); ?>:</h4>
            <div class="kt-section__body">
                <div class="form-group row">
                    <div class="col-lg-6">
                        <label><?php echo e(__('events.categories')); ?></label>
                        <?php echo Form::select('categories[]',$categories,null, ['class'=>'form-control multi', 'id'=>'categories','multiple', 'disabled'=>$disabled]); ?>

                        <?php if($errors->has("categories")): ?>
                            <span class="form-text text-danger"><?php echo e($errors->first('categories')); ?></span>
                        <?php endif; ?>
                    </div>
                    <div class="col-lg-6">
                        <label><?php echo e(__('events.tags')); ?></label>
                        <?php echo Form::select('tags[]',$tags,null, ['class'=>'form-control multi', 'id'=>'tags','multiple', 'disabled'=>$disabled]); ?>

                        <?php if($errors->has("tags")): ?>
                            <span class="form-text text-danger"><?php echo e($errors->first('tags')); ?></span>
                        <?php endif; ?>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-lg-4">
        <div class="kt-section">
            <h4 class="kt-section__title kt-section__title-lg"><?php echo e(__('events.publish_options')); ?>:</h4>
            <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>
            <div class="kt-section__body">
                <?php echo $__env->make('admin.layout.form_components.status', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <div class="form-group row">
                    <div class="col-lg-12">
                        <label class=""><?php echo e(__('events.model')); ?></label>
                        <?php echo Form::select("layout_model_id",$models, null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                        <?php if($errors->has("layout_model_id")): ?>
                            <span
                                class="form-text text-danger"><?php echo e($errors->first("layout_model_id")); ?></span>
                        <?php endif; ?>
                    </div>

                </div>
                <?php echo $__env->make('admin.layout.form_components.publish_option',["module"=>$cmsModule->route], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>



            </div>
        </div>
        <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>

        <div class="kt-section">
            <h4 class="kt-section__title kt-section__title-lg"><?php echo e(__('events.event_details')); ?>:</h4>
            <div class="kt-section__body">
                <div class="form-group row">
                    <div class="col-lg-12">
                        <label class=""><?php echo e(__('events.start_date')); ?></label>
                        <div class="input-group date">
                            <?php echo Form::text("eventDetails[start_date]", null, ['class'=>'form-control','id'=>'kt_datetimepicker_4_4', 'disabled'=>$disabled]); ?>

                            <div class="input-group-append">
            <span class="input-group-text">
                	<i class="la la-calendar glyphicon-th"></i>
            </span>
                                <?php if($errors->has("eventDetails.start_date")): ?>
                                    <span
                                        class="form-text text-danger"><?php echo e($errors->first("eventDetails.start_date")); ?>

                    </span>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-12">
                        <label class=""><?php echo e(__('events.end_date')); ?></label>
                        <div class="input-group date">
                            <?php echo Form::text("eventDetails[end_date]", null, ['class'=>'form-control','id'=>'kt_datetimepicker_4_1', 'disabled'=>$disabled]); ?>

                            <div class="input-group-append">
            <span class="input-group-text">
                	<i class="la la-calendar glyphicon-th"></i>
            </span>
                                <?php if($errors->has("eventDetails.end_date")): ?>
                                    <span
                                        class="form-text text-danger"><?php echo e($errors->first("eventDetails.end_date")); ?>

                    </span>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-12">
                        <label><?php echo e(__('events.place')); ?></label>
                        <?php echo Form::text('eventDetails[place]', null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                        <?php if($errors->has("eventDetails.place")): ?>
                            <span class="form-text text-danger"><?php echo e($errors->first('eventDetails.place')); ?></span>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-12">
                        <label><?php echo e(__('events.organizer')); ?></label>
                        <?php echo Form::text('eventDetails[organizer]', null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                        <?php if($errors->has("eventDetails.organizer")): ?>
                            <span class="form-text text-danger"><?php echo e($errors->first('eventDetails.organizer')); ?></span>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php /**PATH D:\xampp\htdocs\Event_mangnent\resources\views/admin/events/form.blade.php ENDPATH**/ ?>