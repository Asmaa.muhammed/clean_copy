
<header class="header ">

    <div class="header__top">
        <div class="container">
            <ul class="header__top__sites">
                <?php $__currentLoopData = $Gateway; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $link): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($link->link->target != "#"): ?>
                        <?php if($link->target == 0): ?>
                            <?php if(!empty($link->link->target)): ?>
                                <li>
                                    <a class="link link--default link--nav" href="<?php echo e(route(\Illuminate\Support\Str::singular($link->link->target_type),$link->link->target->slug)); ?>"><?php echo e($link->name); ?></a>
                                </li>
                                <?php endif; ?>
                            <?php else: ?>
                                <?php if(!empty($link->link->target)): ?>
                                    <li>
                                        <a class="link link--default link--nav" href="<?php echo e($link->link->target); ?>" > <?php echo e($link->name); ?></a>
                                    </li>
                                <?php endif; ?>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        <div class="header__top__lang">
            <div class="dropdown">
                <ul class="nav-bar__nav lang">
                    <li class="nav-item">
                        <a class="link link--default link--nav" role="button" hreflang="<?php echo e((LaravelLocalization::getCurrentLocale() == "en") ? "ar":"en"); ?>"
                                onclick="window.location.href='<?php echo e(LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale() == "en" ? "ar":"en",null, [], true)); ?>'"
                        ><i class="bi bi-translate"></i> <span><?php echo e((LaravelLocalization::getCurrentLocale() == "en") ? "العربية":"English"); ?> </span>
                        </a>

                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="header__bottom">
    <nav class="navbar navbar-expand-lg">
        <div class="container">
            <div class="header__bottom__up">
                <div class="logo">
                <a class="logo__img" href="<?php echo e(route('index')); ?>">
                        <img src="<?php echo e(asset('public/'.$themePath."/")); ?>/assets/images/logo.png" alt="Logo">
                    </a>
                    <a class="logo__dis" href="<?php echo e(route('index')); ?>">
                       <?php echo __("ARMED FORCES <span> COLLEGE OF MEDICINE </span>"); ?>

                    </a>
                </div>
                <form class="input-btn d-flex" method="GET"  action="<?php echo e(route("search")); ?>">
                    <input class="input" type="search" name="search" placeholder="<?php echo e(__("front.search")); ?>" aria-label="Search">
                    <button class="btn btn--secondary" type="submit">
                        <i class="bi bi-search"></i>
                    </button>
                </form>
                <button class="navbar-toggle" id="navbarOpen" type="button">
                    <i class="bi bi-list"></i>
                </button>
            </div>
        </div>

        <div class="collapse-navbar">
            <div class="container">
            <ul class="navbar-nav">
                <?php $__currentLoopData = $MainMenu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$link): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li class="nav-item dropdown">
                    <?php if($link->hasChildren()): ?>
                        <a class="link link--main-clr link--nav nav-link dropdown-toggle" href="#"
                            id="navbarDropdown2<?php echo e($key); ?>" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <?php echo e($link->name); ?>

                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown2<?php echo e($key); ?>">
                            <?php $__currentLoopData = $link->children; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key2=>$child): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($child->hasChildren()): ?>
                                <li class="sub-dropdown">
                                        <span class="dropdown-item double-chevron--before">   <?php echo e($child->name); ?></span>
                                        <ul class="sub-dropdown__menu">
                                            <?php $__currentLoopData = $child->children; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key2=>$data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($data->target == 0): ?>
                                                    <?php if(!empty($data->link->target)): ?>
                                                        <li>
                                                            <a class="dropdown-item " href="<?php echo e(route(\Illuminate\Support\Str::singular($data->link->target_type),$data->link->target->slug)); ?>"><?php echo e($data->name); ?></a>
                                                        </li>
                                                    <?php endif; ?>
                                                <?php else: ?>
                                                    <?php if(!empty($data->link->target)): ?>
                                                    <li>
                                                        <a class="dropdown-item " href="<?php echo e($data->link->target); ?>" > <?php echo e($data->name); ?></a>
                                                    </li>
                                                    <?php endif; ?>
                                                <?php endif; ?>


                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    </li>
                                <?php else: ?>

                                    <?php if($child->target == 0): ?>
                                        <?php if(!empty($child->link->target)): ?>
                                            <li>
                                                <a class="dropdown-item " href="<?php echo e(route(\Illuminate\Support\Str::singular($child->link->target_type),$child->link->target->slug)); ?>"><?php echo e($child->name); ?></a>
                                            </li>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <?php if(!empty($child->link->target)): ?>
                                        <li>
                                            <a class="dropdown-item " href="<?php echo e($child->link->target); ?>">
                                                <?php echo e($child->name); ?></a>
                                        </li>
                                        <?php endif; ?>
                                    <?php endif; ?>

                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    <?php else: ?>
                        <?php if($link->target == 0): ?>
                            <?php if(!empty($link->link->target)): ?>
                                <a class="link link--main-clr link--nav nav-link " href="<?php echo e(route(\Illuminate\Support\Str::singular($link->link->target_type),$link->link->target->slug)); ?>"><?php echo e($link->name); ?></a>

                            <?php endif; ?>
                        <?php else: ?>
                            <?php if(!empty($link->link->target)): ?>
                                <a class="link link--main-clr link--nav nav-link " href="<?php echo e($link->link->target); ?>" target="_self">
                                     <?php echo e($link->name); ?></a>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </ul>
            </div>
        </div>

    <!--navbar mobile-->
    <div class="collapse-navbar--mobile" id="collapseNavbar" style='position:absolute'>
        <button class="navbar-toggle" id="navbarClose" type="button">
          <i class="bi bi-x-lg"></i>
        </button>
        <div class="nav-accordion" id="navAccordion">
        <?php $__currentLoopData = $MainMenu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$link): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="accordion-item">
                <h2 class="accordion-header" id="heading<?php echo e($key); ?>">
                <?php if($link->hasChildren()): ?>
                    <button class="link collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse<?php echo e($key); ?>" aria-expanded="false" aria-controls="collapse<?php echo e($key); ?>">
                        <?php echo e($link->name); ?>

                    </button>
                    <?php $__currentLoopData = $link->children; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key2=>$child): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($child->hasChildren()): ?>

                            <button class="link collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse<?php echo e($key); ?>" aria-expanded="false" aria-controls="collapse<?php echo e($key); ?>">
                                <?php echo e($child->name); ?>

                            </button>

                          <?php $__currentLoopData = $child->children; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($data->target == 0): ?>
                                <?php if(!empty($data->link->target)): ?>
                                <a class="link double-chevron--before" href="<?php echo e(route(\Illuminate\Support\Str::singular($data->link->target_type),$data->link->target->slug)); ?>">
                                    <?php echo e($data->name); ?>

                                </a>
                            <?php endif; ?>
                                <?php else: ?>
                                    <?php if(!empty($data->link->target)): ?>
                                    <a class="link double-chevron--before" href="<?php echo e($data->link->target); ?>" > <?php echo e($data->name); ?></a>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php else: ?>
                            <?php if($child->target == 0): ?>
                                <?php if(!empty($child->link->target)): ?>
                                    <a class="link double-chevron--before" href="<?php echo e(route(\Illuminate\Support\Str::singular($child->link->target_type),$child->link->target->slug)); ?>">
                                        <?php echo e($child->name); ?>

                                    </a>
                                <?php endif; ?>
                                <?php else: ?>
                                    <?php if(!empty($child->link->target)): ?>
                                    <a class="link double-chevron--before" href="<?php echo e($child->link->target); ?>" target="_self">
                                            <?php echo e($child->name); ?></a>
                                    <?php endif; ?>

                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?>
                    <?php if($link->target == 0): ?>
                        <?php if(!empty($link->link->target)): ?>
                            <a class="link double-chevron--before" href="<?php echo e(route(\Illuminate\Support\Str::singular($link->link->target_type),$link->link->target->slug)); ?>">
                                <?php echo e($link->name); ?>

                            </a>
                        <?php endif; ?>
                        <?php else: ?>
                            <?php if(!empty($link->link->target)): ?>
                            <a class="link double-chevron--before" href="<?php echo e($link->link->target); ?>" target="_self">
                                    <?php echo e($link->name); ?></a>
                            <?php endif; ?>

                    <?php endif; ?>
                <?php endif; ?>
                </h2>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>
      </div>
      <!--End of navbar mobile-->

      <!--End of navbar desktop-->
            </div>
        </div>
        <!--End of navbar desktop-->


    </nav>
</div>
</header>

            <!--End of navbar mobile-->
        </nav>
    </div>
</header>
<?php /**PATH /var/www/html/AFCM/resources/views/AFMC-project/layouts/includes/mainheader.blade.php ENDPATH**/ ?>