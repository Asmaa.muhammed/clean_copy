<?php if($model->hasChildren()): ?>
    <ul role="group" class="jstree-children">
        <?php $__empty_1 = true; $__currentLoopData = $model->children; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $child): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            <li role="treeitem" data-jstree=""
                aria-selected="true" aria-level="<?php echo e($level); ?>"
                aria-labelledby="j1_<?php echo e($child->id); ?>_anchor" id="j1_<?php echo e($child->id); ?>"
                class="jstree-node  <?php echo e($child->hasChildren() ? "jstree-open" : "jstree-leaf"); ?> <?php echo e(($loop->last) ? 'jstree-last' :''); ?>">
                <i class="jstree-icon jstree-ocl" role="presentation"></i>
                <a
                    class="jstree-anchor  " href="<?php echo e(route('menuLinks.show', ['menuGroup'=>$menuGroup->id, 'menuLink'=> $child->id])); ?>"
                    tabindex="<?php echo e($child->id); ?>"
                    id="j1_<?php echo e($child->id); ?>_anchor"><i
                        class="jstree-icon jstree-themeicon fa fa-li jstree-themeicon-custom"
                        role="presentation"></i>
                    <?php echo e($child->name); ?>

                </a>
                <?php echo $__env->make('admin.layout.components.recursive_tree', ['model' => $child, 'level' => $level++, 'menuGroup'=>$menuGroup], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
        <?php endif; ?>

    </ul>
<?php endif; ?>
<?php /**PATH D:\xampp\htdocs\AFCM\resources\views/admin/layout/components/recursive_tree.blade.php ENDPATH**/ ?>