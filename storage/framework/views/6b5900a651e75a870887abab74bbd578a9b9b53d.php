<?php $__env->startSection('content'); ?>
<div class="internals-breadcrumb">
    <div class="container">
        <h2 class="internals-title"><?php echo e(__("front.events")); ?></h2>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo e(route('index')); ?>"><?php echo e(__("front.home")); ?></a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo e(__("front.mediaCenter")); ?></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo e(__("front.events")); ?></li>
            </ol>
        </nav>
    </div>
</div>
<section>
    <div class="container">
        <div class="section__header">
            <div class="section__title">
                <span><?php echo e(__("our events")); ?></span>
                <h2 class="section-title"><?php echo e(__("our upcoming Events")); ?></h2>
            </div>
            <!-- <a class="btn btn--default" href="<?php echo e(route('moreEvents')); ?>"><?php echo e(__("more events")); ?></a> -->
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="data-block data-block--no-border mb-3 mb-lg-0">
                    <div class="data-block__item">
                        <a href="<?php echo e(route('event',$event->id)); ?>" class="data-block__img"
                            style="background-image: url('<?php echo e($event->image->image_url); ?>"></a>
                        <div class="data-block__dis">
                        <span class="data-block__date">
                            <span><?php echo e($event->eventDetails->start_date); ?></span>
                        </span>
                            <a href="<?php echo e(route('event',$event->id)); ?>" class="block-title link link--main-clr">
                            <?php echo e($event->title); ?>

                            </a>
                            <?php echo $event->body; ?>

                            <div class="data-block__link">
                                <a class="link link--red-clr double-chevron--after" href="<?php echo e(route('event',$event->id)); ?>">
                                    <?php echo e(__("read more")); ?>

                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="data-block data-block--no-border data-block--no-img">
                    <?php $__empty_1 = true; $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <div class="data-block__item">
                        <div class="data-block__dis">
                        <span class="data-block__date">
                            <span><?php echo e($event->eventDetails->start_date); ?></span>
                        </span>
                            <a href="<?php echo e(route('event',$event->id)); ?>" class="block-title link link--main-clr">
                            <?php echo e($event->title); ?>

                            </a>
                            <?php echo $event->body; ?>

                            
                            <div class="data-block__link">
                                <a class="link link--red-clr double-chevron--after" href="<?php echo e(route('event',$event->id)); ?>">
                                    <?php echo e(__("read more")); ?>

                                </a>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make($themePath.'.layouts.mainPage',["title"=>__("front.events")], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/AFCM/resources/views/AFMC-project/more_events.blade.php ENDPATH**/ ?>