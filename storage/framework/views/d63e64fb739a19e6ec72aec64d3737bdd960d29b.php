<div class="form-group row">
    <div class="col-lg-12">
        <label class=""><?php echo e(__($module.'.start_publishing')); ?></label>
        <div class="input-group date">

            <?php echo Form::text("publishOptions[start_publishing]", null, ['class'=>'form-control','id'=>'kt_datetimepicker_4_3', 'disabled'=>$disabled]); ?>

            <div class="input-group-append">
            <span class="input-group-text">
                	<i class="la la-calendar glyphicon-th"></i>
            </span>
                <?php if($errors->has("publishOptions.start_publishing")): ?>
                    <span
                        class="form-text text-danger"><?php echo e($errors->first("publishOptions.start_publishing")); ?>

                    </span>
                <?php endif; ?>

            </div>
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-lg-12">
        <label class=""><?php echo e(__($module.'.end_publishing')); ?></label>
        <div class="input-group date">
            <?php echo Form::text("publishOptions[end_publishing]", null, ['class'=>'form-control','id'=>'kt_datetimepicker_4_2', 'disabled'=>$disabled]); ?>

            <div class="input-group-append">
            <span class="input-group-text">
                	<i class="la la-calendar glyphicon-th"></i>
            </span>
                <?php if($errors->has("publishOptions.end_publishing")): ?>
                    <span
                        class="form-text text-danger"><?php echo e($errors->first("publishOptions.end_publishing")); ?>

                    </span>
                <?php endif; ?>

            </div>
        </div>
    </div>
</div>
<div class="form-group">
    <label><?php echo e(__('translation::translation.show_in_main_page')); ?></label>
    <div class="kt-radio-list">
        <label class="kt-radio kt-radio--success">
            <?php echo e(Form::radio('publishOptions[show_in_main_page]',1, null,['id' => 'input-radio-17' ,$disabled ? 'disabled': ''])); ?> <?php echo e(__('translation::translation.status_enable')); ?>

            <span></span>
        </label>
        <label class="kt-radio kt-radio--danger">
            <?php echo e(Form::radio('publishOptions[show_in_main_page]',0, null,['id' => 'input-radio-16' ,$disabled ? 'disabled': ''])); ?> <?php echo e(__('translation::translation.status_disable')); ?>

            <span></span>

        </label>
        <?php $__errorArgs = ['publishOptions.show_in_main_page'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
        <span class="form-text text-danger"><?php echo e($message); ?></span>
        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
    </div>

</div>




<?php /**PATH /var/www/html/AFCM/resources/views/admin/layout/form_components/publish_option.blade.php ENDPATH**/ ?>