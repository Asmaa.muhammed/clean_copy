<div class="row">
    <div class="col-md-7">

        <div class="kt-section">
            <div class="kt-section__body">
                <div class="form-group row">
                    <div class="col-lg-8">
                        <label>    <?php echo e(__('galleryImages.image')); ?>:</label>
                        <div class="input-group">
                             <span class="input-group-prepend">
                                  <a data-input="image" data-preview="image_preview" class="btn btn-primary lfm ">  <i
                                          class="la la-image  text-white "></i>
                                       </a>
                             </span>
                            <?php echo Form::text('image', (!empty($sliderImage) ? $sliderImage->image_url : null), ['class'=>'form-control','id'=>'image','disabled'=>$disabled,'readonly']); ?>


                        </div>
                    </div>
                    <div class="col-lg-4">
                        <label><?php echo e(__('galleryImages.sorting')); ?>:</label>
                        <?php echo Form::number('sorting', null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                        <?php if($errors->has("sorting")): ?>
                            <span class="form-text text-danger"><?php echo e($errors->first('sorting')); ?></span>
                        <?php endif; ?>
                    </div>
                </div>

            </div>
        </div>
        <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>
        <div class="kt-section">
            <h4 class="kt-section__title kt-section__title-lg"><?php echo e(__('sliderImages.targets_and_links')); ?>:</h4>
            <div class="kt-section__body">
                <div class="form-group row">
                    <div class="col-lg-6">
                        <label><?php echo e(__('sliderImages.target')); ?></label>
                        <?php echo Form::select('target',$target,null, ['id'=>'target','class'=>'form-control', 'disabled'=>$disabled]); ?>

                        <?php if($errors->has("target")): ?>
                            <span class="form-text text-danger"><?php echo e($errors->first('target')); ?></span>
                        <?php endif; ?>
                    </div>
                    <div class="col-lg-6">
                        <label class=""><?php echo e(__('sliderImages.link')); ?></label>
                        <div id="linkComponent">
                        </div>
                        <?php if($errors->has("target_type")): ?>
                            <span class="form-text text-danger"><?php echo e($errors->first('target_type')); ?></span>
                        <?php endif; ?>
                        <?php if($errors->has("target_id")): ?>
                            <span class="form-text text-danger"><?php echo e($errors->first('target_id')); ?></span>
                        <?php endif; ?>
                        <?php if($errors->has("link")): ?>
                            <span class="form-text text-danger"><?php echo e($errors->first('link')); ?></span>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>


        <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>
        <div class="kt-section">
            <h4 class="kt-section__title kt-section__title-lg"><?php echo e(__('sliderImages.captions_and_alts')); ?>:</h4>
            <div class="kt-section__body">
                <?php $__env->startComponent("admin.components.translatable_section"); ?>
                    <?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $__env->slot("translatable_{$language->language}"); ?>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label><?php echo e(__('sliderImages.caption')); ?></label>
                                    <?php echo Form::text("caption:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                                    <?php if($errors->has("caption:{$language->language}")): ?>
                                        <span
                                            class="form-text text-danger"><?php echo e($errors->first("caption:{$language->language}")); ?></span>
                                    <?php endif; ?>
                                </div>
                                <div class="col-lg-6">
                                    <label class=""><?php echo e(__('sliderImages.image_alt')); ?></label>
                                    <?php echo Form::text("image_alt:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                                    <?php if($errors->has("image_alt:{$language->language}")): ?>
                                        <span
                                            class="form-text text-danger"><?php echo e($errors->first("image_alt:{$language->language}")); ?></span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php $__env->endSlot(); ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php echo $__env->renderComponent(); ?>
            </div>
        </div>

    </div>
    <div class="col-md-1"></div>
    <div class="col-lg-4">
        <div id="image_preview" style="margin-top:15px;max-height:150px;">
            <?php if(!empty($sliderImage)): ?>
                <img src="<?php echo e($sliderImage->image_url); ?>" width="150px">
            <?php endif; ?>
        </div>

    </div>
</div>
<?php $__env->startSection('additionalScripts'); ?>
    <!--begin::Page Scripts(used by this page) -->

    <?php echo $__env->make('admin.layout.includes.filemanger_scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <script src="https://cdn.jsdelivr.net/npm/handlebars@latest/dist/handlebars.js"></script>
    <script>

        Handlebars.registerHelper('link', function (value) {
            value = parseInt(value);
            if (value) {
                return `<?php echo Form::text('link',( isset($sliderImage) && $sliderImage->target == 1) ? ($sliderImage->link->link ?? null) : old('link'), ['class'=>'form-control',"id"=>"external_link", 'disabled'=>$disabled]); ?>`;
            }
            return `<?php echo Form::select('target_type',$targetTypes, null, ["id"=>"target_type",'placeholder' => 'Choose Target Type','class'=>'form-control mb-2', 'disabled'=>$disabled]); ?>

            <?php echo Form::select('target_id',[],null, ['id'=>"target_id",'class'=>'form-control', 'disabled'=>true]); ?>`;
        });

        $('#target').change(function () {
            $("#linkComponent").empty().html(Handlebars.compile(`{{{link ${$(this).val()}}}}`)());
        }).change();

        $(document).on('change', '#target_type', function () {
            $.ajax({
                method: 'post',
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    model: $(this).val(),
                },
                url: `<?php echo e(route('ajax.get_target_type_data')); ?>`,
                success: function (data) {
                    $("#target_id").removeAttr('disabled').empty().html(data.data.map(function (element) {
                        return `<option value="${element.id}">${element.name}</option>`;
                    }).join("\n"));
                    <?php if(old('target_id')): ?>
                    $(document).find('#target_id').val(`<?php echo e(old('target_id')); ?>`).change();
                    <?php endif; ?>
                    <?php if(isset($sliderImage)): ?>
                    $(document).find('#target_id').val(`<?php echo e($sliderImage->link->target_id); ?>`).change().attr('disabled', <?php echo e($disabled); ?>);
                    <?php endif; ?>
                }
            });
        }).change();
        <?php if(old('target_type')): ?>
        $(document).find("#target_type").change();
        <?php endif; ?>
        <?php if(isset($sliderImage)): ?>
        $(document).find("#target_type").val('<?php echo e($sliderImage->link->target_type); ?>').change();
        <?php endif; ?>
    </script>

    <!--end::Page Scripts -->
<?php $__env->stopSection(); ?>

<?php /**PATH /var/www/html/AFCM/resources/views/admin/slider_images/form.blade.php ENDPATH**/ ?>