<a href="<?php echo e(route('sliderImages.index',$slider)); ?>" class="btn btn-success btn-icon" >
    <i class="la la-eye text-white"></i>
</a>
<a href="<?php echo e(route('sliderImages.create',$slider)); ?>" class="btn btn-success btn-icon" >
    <i class="la la-plus-circle"></i>
</a>
<?php /**PATH /var/www/html/AFCM/resources/views/admin/sliders/image_actions.blade.php ENDPATH**/ ?>