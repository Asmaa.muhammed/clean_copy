<div class="row">
    <div class="col-md-7">
        <div class="kt-section">
            <div class="kt-section__body">
                <?php $__env->startComponent("admin.components.translatable_section"); ?>
                    <?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $__env->slot("translatable_{$language->language}"); ?>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label><?php echo e(__('posts.title')); ?></label>
                                    <?php echo Form::text("title:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                                    <?php if($errors->has("title:{$language->language}")): ?>
                                        <span
                                            class="form-text text-danger"><?php echo e($errors->first("title:{$language->language}")); ?></span>
                                    <?php endif; ?>
                                </div>
                                <div class="col-lg-6">
                                    <label class=""><?php echo e(__('posts.summary')); ?></label>
                                    <?php echo Form::textarea("summary:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                                    <?php if($errors->has("summary:{$language->language}")): ?>
                                        <span
                                            class="form-text text-danger"><?php echo e($errors->first("summary:{$language->language}")); ?></span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-8">
                                    <label class=""><?php echo e(__('posts.body')); ?></label>
                                    <?php echo Form::textarea("body:{$language->language}", null, ['class'=>'form-control editor', 'disabled'=>$disabled]); ?>

                                    <?php if($errors->has("body:{$language->language}")): ?>
                                        <span
                                            class="form-text text-danger"><?php echo e($errors->first("body:{$language->language}")); ?></span>
                                    <?php endif; ?>
                                </div>


                            </div>
                        <?php $__env->endSlot(); ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php echo $__env->renderComponent(); ?>
            </div>
        </div>
        <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>
        <div class="kt-section">
            <h4 class="kt-section__title kt-section__title-lg"><?php echo e(__('posts.image_and_galleries')); ?>:</h4>
            <div class="kt-section__body">
                <div class="form-group row">
                 <?php echo $__env->make('admin.layout.form_components.select_image', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <div class="col-lg-6">
                        <label><?php echo e(__('posts.gallery')); ?></label>
                        <?php echo Form::select('gallery',$galleries,(isset($post) && $post->gallery)? $post->gallery->gallery_id :'', ['class'=>'form-control ', 'id'=>'galleries','placeholder'=> __('posts.gallery'), 'disabled'=>$disabled]); ?>

                        <?php if($errors->has("gallery")): ?>
                            <span class="form-text text-danger"><?php echo e($errors->first('gallery')); ?></span>
                        <?php endif; ?>
                    </div>

                </div>
            </div>
        </div>
         <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>
        <div class="kt-section">
            <h4 class="kt-section__title kt-section__title-lg"><?php echo e(__('pages.minor')); ?>:</h4>
            <div class="kt-section__body">
                <div class="form-group row">
                    <div class="col-lg-6">
                        <label><?php echo e(__('pages.minor')); ?></label>
                        <?php echo Form::select('minor',$minors,(isset($post) && $post->minor)? $post->minor->minor_id :'', ['class'=>'form-control ', 'id'=>'minors','placeholder'=> __('pages.minor'), 'disabled'=>$disabled]); ?>

                        <?php if($errors->has("minor")): ?>
                            <span class="form-text text-danger"><?php echo e($errors->first('minor')); ?></span>
                        <?php endif; ?>
                    </div>

                </div>
            </div>
        </div>
        <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>
        <div class="kt-section">
            <h4 class="kt-section__title kt-section__title-lg"><?php echo e(__('posts.taxonomies')); ?>:</h4>
            <div class="kt-section__body">
                <div class="form-group row">
                    <div class="col-lg-6">
                        <label><?php echo e(__('posts.categories')); ?></label>
                        <?php echo Form::select('categories[]',$categories, null, ['class'=>'form-control multi', 'id'=>'categories','multiple', 'disabled'=>$disabled]); ?>

                        <?php if($errors->has("categories")): ?>
                            <span class="form-text text-danger"><?php echo e($errors->first('categories')); ?></span>
                        <?php endif; ?>
                    </div>
                    <div class="col-lg-6">
                        <label><?php echo e(__('posts.tags')); ?></label>
                        <?php echo Form::select('tags[]',$tags, null, ['class'=>'form-control multi', 'id'=>'tags','multiple', 'disabled'=>$disabled]); ?>

                        <?php if($errors->has("tags")): ?>
                            <span class="form-text text-danger"><?php echo e($errors->first('tags')); ?></span>
                        <?php endif; ?>
                    </div>


                </div>
                <div class="form-group row">
                        <div class="col-lg-6">
                            <label class=""><?php echo e(__('posts.author')); ?></label>
                            <?php echo Form::select("author",$authors, null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                            <?php if($errors->has("author")): ?>
                                <span class="form-text text-danger"><?php echo e($errors->first("author")); ?></span>
                            <?php endif; ?>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-lg-4">
        <div class="kt-section">
            <h4 class="kt-section__title kt-section__title-lg"><?php echo e(__('posts.publish_options')); ?>:</h4>
            <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>
            <div class="kt-section__body">
                <?php echo $__env->make('admin.layout.form_components.status', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <div class="form-group row">
                    <div class="col-lg-12">
                        <label class=""><?php echo e(__('posts.model')); ?></label>
                        <?php echo Form::select("layout_model_id",$models, null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                        <?php if($errors->has("layout_model_id")): ?>
                            <span class="form-text text-danger"><?php echo e($errors->first("layout_model_id")); ?></span>
                        <?php endif; ?>
                    </div>
                </div>
                <?php echo $__env->make('admin.layout.form_components.publish_option',["module"=>$cmsModule->route], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
        </div>
    </div>
</div>

<?php /**PATH D:\xampp\htdocs\clean_copy\resources\views/admin/posts/form.blade.php ENDPATH**/ ?>