<?php $__env->startSection('subheader'); ?> <?php echo $__env->make('admin.layout.components.breadcrumb',['pageTitle'=>__('common.moduleTitle', ['module'=>$cmsModule->name]),'moduleName'=>$cmsModule->route,'pageName'=>__('common.indexPageName', ['module'=>$cmsModule->name]) ], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    <?php echo e(__('common.all', ['module'=>$cmsModule->name])); ?>

                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="<?php echo e(route('events.create')); ?>" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            <?php echo e(__('common.newRecord')); ?>

                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
        <?php echo $__env->make('admin.layout.components.flash_messages', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <!--begin: Datatable -->
        <?php echo $html->table(['class' => ' table table-striped- table-bordered table-hover table-checkable dataTable no-footer events'], true); ?>


        <!--end: Datatable -->
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('additionalScripts'); ?>

    <?php echo $html->scripts(); ?>

    <script src="<?php echo e(asset('/public/admin')); ?>/assets/js/modules.js" type="text/javascript"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', ['pageTitle'=>__('common.indexPageTitle', ['module' => $cmsModule->name])], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\clean_copy\resources\views/admin/events/index.blade.php ENDPATH**/ ?>