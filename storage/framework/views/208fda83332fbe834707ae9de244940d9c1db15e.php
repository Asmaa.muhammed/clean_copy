<!--begin::Fonts -->
<link rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

<!--end::Fonts -->

<!--begin::Page Vendors Styles(used by this page) -->
<link href="<?php echo e(asset('/public/admin')); ?>/assets/plugins/custom/datatables/datatables.bundle<?php echo e($styleDirection); ?>.css" rel="stylesheet" type="text/css" />

<!--end::Page Vendors Styles -->

<!--begin::Global Theme Styles(used by all pages) -->
<link href="<?php echo e(asset('/public/admin')); ?>/assets/plugins/global/plugins.bundle<?php echo e($styleDirection); ?>.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo e(asset('/public/admin')); ?>/assets/css/style.bundle<?php echo e($styleDirection); ?>.css" rel="stylesheet" type="text/css"/>

<!--end::Global Theme Styles -->

<!--begin::Layout Skins(used by all pages) -->
<link href="<?php echo e(asset('/public/admin')); ?>/assets/css/skins/header/base/dark<?php echo e($styleDirection); ?>.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo e(asset('/public/admin')); ?>/assets/css/skins/header/menu/dark<?php echo e($styleDirection); ?>.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo e(asset('/public/admin')); ?>/assets/css/skins/brand/dark<?php echo e($styleDirection); ?>.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo e(asset('/public/admin')); ?>/assets/css/skins/aside/light<?php echo e($styleDirection); ?>.css" rel="stylesheet" type="text/css"/>


<!--end::Layout Skins -->
<link rel="shortcut icon" href="<?php echo e(asset('/public/admin')); ?>/assets/media/logos/diva-ico.png"/>
<link rel="stylesheet" href="<?php echo e(asset("public/admin/assets/css/fontawesome-iconpicker.css")); ?>">
<?php /**PATH /var/www/html/AFCM/resources/views/admin/layout/includes/head.blade.php ENDPATH**/ ?>