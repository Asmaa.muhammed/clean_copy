

<div class="internals-breadcrumb">
    <div class="container">
        <h2 class="internals-title"><?php echo e($pageHeading); ?></h2>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo e(route('index')); ?>"><?php echo e(__("front.home")); ?></a></li>
                <?php if(isset($parentLink)): ?>
                    <li class="breadcrumb-item"><a href="<?php echo e($parentLink['route']); ?>"><?php echo e($parentLink['title']); ?></a></li>
                <?php endif; ?>
                <li class="breadcrumb-item active" aria-current="page"><?php echo e($pageTitle); ?></li>
            </ol>
        </nav>
    </div>
</div>
<?php /**PATH /var/www/html/AFCM/resources/views/AFMC-project/layouts/includes/breadcrumb.blade.php ENDPATH**/ ?>