<div class="row">
    <div class="col-md-7">
        <?php $__env->startComponent("admin.components.translatable_section"); ?>
            <?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php $__env->slot("translatable_{$language->language}"); ?>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label><?php echo e(__('menuLinks.name')); ?></label>
                            <?php echo Form::text("name:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                            <?php if($errors->has("name:{$language->language}")): ?>
                                <span
                                    class="form-text text-danger"><?php echo e($errors->first("name:{$language->language}")); ?></span>
                            <?php endif; ?>
                        </div>
                        <div class="col-lg-6">
                            <label class=""><?php echo e(__('menuLinks.description')); ?></label>
                            <?php echo Form::textarea("description:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                            <?php if($errors->has("description:{$language->language}")): ?>
                                <span
                                    class="form-text text-danger"><?php echo e($errors->first("description:{$language->language}")); ?></span>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php $__env->endSlot(); ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php echo $__env->renderComponent(); ?>
        <div class="kt-portlet__head mb-4 row">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <?php echo e(__('common.moduleInfo', ['module'=>__('menuLinks.singularModuleName')])); ?>

                </h3>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-6">
                <label><?php echo e(__('menuLinks.target')); ?></label>
                <?php echo Form::select('target',$target,null, ['id'=>'target','class'=>'form-control', 'disabled'=>$disabled]); ?>

                <?php if($errors->has("target")): ?>
                    <span class="form-text text-danger"><?php echo e($errors->first('target')); ?></span>
                <?php endif; ?>
            </div>
            <div class="col-lg-6">
                <label class=""><?php echo e(__('menuLinks.link')); ?></label>
                <div id="linkComponent">
                </div>
                <?php if($errors->has("target_type")): ?>
                    <span class="form-text text-danger"><?php echo e($errors->first('target_type')); ?></span>
                <?php endif; ?>
                <?php if($errors->has("target_id")): ?>
                    <span class="form-text text-danger"><?php echo e($errors->first('target_id')); ?></span>
                <?php endif; ?>
                <?php if($errors->has("link")): ?>
                    <span class="form-text text-danger"><?php echo e($errors->first('link')); ?></span>
                <?php endif; ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-6">
                <label class=""><?php echo e(__('menuLinks.sorting')); ?></label>
                <?php echo Form::number('sorting', null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                <?php if($errors->has("sorting")): ?>
                    <span class="form-text text-danger"><?php echo e($errors->first('sorting')); ?></span>
                <?php endif; ?>
            </div>
            <div class="col-lg-6">
                <label><?php echo e(__('menuLinks.icon')); ?></label>
                <button type="button"
                        id="icon_picker"
                        value=""
                        data-selected=""
                        data-toggle="dropdown"
                        class="btn btn-secondary form-control dropdown-toggle iconpicker-component"
                        <?php echo e($disabled ? "disabled" : ""); ?>

                        name="icon">
                    <?php echo e(__('menuLinks.icon')); ?> <i class="" id="drop-down-selected-icon"></i>
                    <span class="caret"></span>
                </button>
                <?php echo Form::hidden('icon', null, ["id"=>"icon"]); ?>

                <div class="dropdown-menu iconpicker-container"></div>
                <?php if($errors->has("icon")): ?>
                    <span class="form-text text-danger"><?php echo e($errors->first('icon')); ?></span>
                <?php endif; ?>
            </div>
        </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label><?php echo e(__('pages.gallery')); ?></label>
                    <?php echo Form::select('gallery_id',$galleries,null, ['class'=>'form-control ', 'id'=>'gallery_id','placeholder'=> __('pages.gallery'), 'disabled'=>$disabled]); ?>

                    <?php if($errors->has("gallery_id")): ?>
                        <span class="form-text text-danger"><?php echo e($errors->first('gallery_id')); ?></span>
                    <?php endif; ?>
                </div>
            <div class="col-lg-6">
                <label><?php echo e(__('menuLinks.parent')); ?></label>
                <?php echo Form::select('parent',$parent,null, ['id'=>'target','class'=>'form-control', 'disabled'=>$disabled]); ?>

                <?php if($errors->has("parent")): ?>
                    <span class="form-text text-danger"><?php echo e($errors->first('parent')); ?></span>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-md-4">
        <?php echo $__env->make('admin.layout.form_components.status', ['disabled' => $disabled], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="row">
            <div id="kt_tree_1" class="tree-demo jstree jstree-1 jstree-default row" role="tree"
                 aria-multiselectable="true" tabindex="0" aria-activedescendant="j1_<?php echo e($menuGroup->id); ?>"
                 aria-busy="false">
                <ul class="jstree-container-ul jstree-children" role="group">
                    <li role="treeitem" aria-selected="true" aria-level="<?php echo e($level = $level ?? 1); ?>"
                        aria-labelledby="j1_<?php echo e($menuGroup->id); ?>_anchor"
                        aria-expanded="true" id="j1_<?php echo e($menuGroup->id); ?>"
                        class="jstree-node  jstree-open  jstree-last"><i
                            class="jstree-icon jstree-ocl" role="presentation"></i><a class="jstree-anchor"
                                                                                      href="<?php echo e(route('menuGroups.show', $menuGroup->id)); ?>"
                                                                                      tabindex="<?php echo e($menuGroup->id); ?>"
                                                                                      id="j1_<?php echo e($menuGroup->id); ?>_anchor"><i
                                class="jstree-icon jstree-themeicon fa fa-list-ul jstree-themeicon-custom"
                                role="presentation"></i>
                            <?php echo e($menuGroup->name); ?>

                        </a>
                        <ul role="group" class="jstree-children">
                            <?php $__empty_1 = true; $__currentLoopData = $menuGroup->links()->whereParent(0)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $link): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                <li role="treeitem" aria-selected="true" aria-level="<?php echo e($level); ?>"
                                    aria-labelledby="j1_<?php echo e($link->id); ?>_anchor"
                                    aria-expanded="true" id="j1_<?php echo e($link->id); ?>"
                                    class="jstree-node  <?php echo e($link->hasChildren() ? "jstree-open" : "jstree-leaf"); ?>  <?php echo e(($loop->last) ? 'jstree-last' :''); ?>">
                                    <i
                                        class="jstree-icon jstree-ocl" role="presentation"></i><a
                                        class="jstree-anchor"
                                        href="<?php echo e(route('menuLinks.show', ['menuGroup'=>$menuGroup->id, 'menuLink'=> $link->id])); ?>"
                                        tabindex="<?php echo e($link->id); ?>"
                                        id="j1_<?php echo e($link->id); ?>_anchor"><i
                                            class="jstree-icon jstree-themeicon fa fa-list-ul jstree-themeicon-custom"
                                            role="presentation"></i>
                                        <?php echo e($link->name); ?>

                                    </a>
                                    <?php echo $__env->make('admin.layout.components.recursive_tree', ['model' => $link, 'level' => $level++, 'menuGroup'=>$menuGroup], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                            <?php endif; ?>
                        </ul>

                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php $__env->startSection("additionalScripts"); ?>
    <script src="https://cdn.jsdelivr.net/npm/handlebars@latest/dist/handlebars.js"></script>
    <script>
        $(function () {
            $("#icon_picker").iconpicker({
                title: "Pick Icon"
            });
            $('#icon_picker').on('iconpickerSelected', function (event) {
                $("#icon").val(event.iconpickerValue);
            });
            $.iconpicker.batch("#icon_picker", "update", $("#icon").val(), false);
        });
    </script>
    <script>

        Handlebars.registerHelper('link', function (value) {
            value = parseInt(value);
            if (value) {
                return `<?php echo Form::text('link', isset($menuLink) ? ($menuLink->link->link ?? null) : old('link'), ['class'=>'form-control', 'disabled'=>$disabled]); ?>`;
            }
            return `<?php echo Form::select('target_type',$targetTypes, null, ["id"=>"target_type",'placeholder' => 'Choose Target Type','class'=>'form-control mb-2', 'disabled'=>$disabled]); ?>

            <?php echo Form::select('target_id',[],null, ['id'=>"target_id",'class'=>'form-control', 'disabled'=>true]); ?>`;
        });

        $('#target').change(function () {
            $("#linkComponent").empty().html(Handlebars.compile(`{{{link ${$(this).val()}}}}`)());
        }).change();

        $(document).on('change', '#target_type', function () {
            $.ajax({
                method: 'post',
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    model: $(this).val(),
                },
                url: `<?php echo e(route('ajax.get_target_type_data')); ?>`,
                success: function (data) {
                    $("#target_id").removeAttr('disabled').empty().html(data.data.map(function (element) {
                        return `<option value="${element.id}">${element.name}</option>`;
                    }).join("\n"));
                    <?php if(old('target_id')): ?>
                    $(document).find('#target_id').val(`<?php echo e(old('target_id')); ?>`).change();
                    <?php endif; ?>
                    <?php if(isset($menuLink)): ?>
                    $(document).find('#target_id').val(`<?php echo e($menuLink->link->target_id); ?>`).change().attr('disabled', <?php echo e($disabled); ?>);
                    <?php endif; ?>
                }
            });
        }).change();
        <?php if(old('target_type')): ?>
        $(document).find("#target_type").change();
        <?php endif; ?>
        <?php if(isset($menuLink)): ?>
        $(document).find("#target_type").val('<?php echo e($menuLink->link->target_type); ?>').change();
        <?php endif; ?>
    </script>
<?php $__env->stopSection(); ?>
<?php /**PATH D:\xampp\htdocs\AFCM\resources\views/admin/menuLinks/form.blade.php ENDPATH**/ ?>