<div class="dropdown dropdown-inline ">
    <button type="button"
            class="btn btn-hover-warning btn-elevate-hover btn-icon btn-sm btn-icon-md btn-circle"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="flaticon-more-1"></i>
    </button>
    <div class="dropdown-menu dropdown-menu-right <?php echo e($module); ?>" x-placement="top-end"
         style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-149px, -189px, 0px);">
        <?php if (app('laratrust')->can("update-{$module}")) : ?>
        <a class="dropdown-item"
           href="<?php echo e(route($module."."."edit", $model)); ?>"><i
                class="la la-pencil-square-o"></i> <?php echo e(__('translation::translation.edit')); ?>

        </a>
        <?php endif; // app('laratrust')->can ?>
       
        
        <?php if (app('laratrust')->can("delete-{$module}")) : ?>
            <button type="submit" class="delete_btn dropdown-item btn-clean delete_data"
                    data="<?php echo e($model[Str::singular($module)]->id); ?>" 
                    data_name="<?php echo e($model[Str::singular($module)]->name); ?>">
                <i class="la la-trash-o"></i> <?php echo e(__('translation::translation.delete')); ?>

            </button>
        <?php endif; // app('laratrust')->can ?>
    </div>
</div>

<?php
    $id=$model[Str::singular($module)]->id;
?>
    
<script>
     $(document).ready(function() {

        var CSRF_TOKEN = $('meta[name="X-CSRF-TOKEN"]').attr('content');

        $('body').on('click', '.delete_data', function() {

            var id = $(this).attr('data');
            var swal_text = 'You want to DELETE this record , You will not be able to recover this record!';
            var swal_title = 'Are you sure?';
            swal({
                title: swal_title,
                text: swal_text,
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: "btn-warning",
                confirmButtonText: "Confirm",
                cancelButtonText: "Cancel"
            }, function() {
            
                $.ajax({
                    url: "<?php echo e(url('/')); ?>" + "/admin-dashboard/persons/delete" ,
                    type: "POST",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {_token: CSRF_TOKEN, 'id' : id},
                })
                    .done(function(reseived_data) {
                        var parsed_data = $.parseJSON(reseived_data);

                        if(parsed_data.code === '1'){
                            swal({
                                type: 'success',
                                title: 'Person deleted successfully',
                                confirmButtonClass: 'btn-success'
                            }, function() {
                                window.location.href = parsed_data.url;
                            });
                        }
                        else{
                            swal(
                                "Something Went Wrong",
                                parsed_data.message ,
                                "error"
                            );
                        }
                    });
                });

            });
        });
</script><?php /**PATH D:\xampp\htdocs\clean_copy\resources\views/admin/persons/custom_action_buttons.blade.php ENDPATH**/ ?>