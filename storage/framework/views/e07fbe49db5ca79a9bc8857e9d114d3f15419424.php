
    <footer class="footer">
        <div class="footer__top">
            <div class="container">
                <h4 class="section-title section-title--white"><?php echo e(__("newsletter")); ?></h4>
                <p><?php echo e(__("The role of laparoscopy in diagnosis as well as therapeutic interventions has increased markedly in the last few years. In traum")); ?></p>
                    <form class="input-btn d-flex" method="POST" action="<?php echo e(route("newsletter")); ?>">
                        <?php echo csrf_field(); ?>
                        <input class="input" type="email" name="email" placeholder="<?php echo e(__("email")); ?>" aria-label="email">
                        <button class="btn btn--secondary" type="submit">
                            <i class="bi bi-cursor-fill"></i>
                        </button>
                    </form>
            </div>
        </div>
        <div class="footer__main">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="footer__block">
                            <h4 class="footer__title"><?php echo e(__("About The University")); ?></h4>
                            <div class="logo">
                                <a class="logo__img" href="<?php echo e(route('index')); ?>">
                                    <img src="<?php echo e(asset('public/'.$themePath."/")); ?>/assets/images/logo.png" alt="Logo">
                                </a>
                                <a class="logo__dis" href="<?php echo e(route('index')); ?>">
                                    <?php echo e(__("front.sitename")); ?> <span><?php echo e(__("front.sitename2")); ?>  </span>
                                </a>
                            </div>
                            <article>
                                <p>
                                    <?php echo e(__("front.celebratedEvent")); ?>


                                </p>
                            </article>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="footer__block">
                            <h4 class="footer__title"><?php echo e(__("important links")); ?></h4>
                            <ul class="footer__links" style="column-count: 2">
                            <?php $__currentLoopData = $footerMenu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $link): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                               
                                <?php if($link->target == 0): ?>
                                    <?php if(!empty($link->link->target)): ?>
                                        <li>
                                            <a class="link link__footer double-chevron--before" href="<?php echo e(route(\Illuminate\Support\Str::singular($link->link->target_type),$link->link->target->slug)); ?>"><?php echo e($link->name); ?></a>
                                        </li>
                                        <?php endif; ?>
                                    <?php else: ?>
                                    <?php if(!empty($link->link->target)): ?>
                                        <li>    
                                            <a class="link link__footer double-chevron--before" href="<?php echo e($link->link->target); ?>" target="_blank">
                                            <?php echo e($link->name); ?></a>
                                        </li>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="footer__block">
                            <h4 class="footer__title"><?php echo e(__("front.contactInformation")); ?></h4>
                            <ul class="footer__contact">
                                <li>
                                    <i class="bi bi-geo-alt-fill"></i>
                                    <span><?php echo e(__("front.theAddress")); ?></span>
                                </li>
                                <li>
                                    <i class="bi bi-phone"></i>
                                    <a class="link link__footer" href="fax:02-24032839">02-24032839</a>
                                </li>
                                <li>
                                    <i class="bi bi-telephone-fill"></i>
                                    <a class="link link__footer" href="tel:02-24032872">02-24032872 - 02-24032843</a>
                                </li>
                                <li>
                                    <i class="bi bi-envelope-fill"></i>
                                    <a class="link link__footer" href="mailto:Afcm@mod.gov.eg">Afcm@mod.gov.eg</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer__bottom">
            <div class="container">
                <ul class="footer__social-links">
                    <li>
                        <a class="link link--default" href="#">
                            <i class="bi bi-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a class="link link--default" href="#">
                            <i class="bi bi-linkedin"></i>
                        </a>
                    </li>
                    <li>
                        <a class="link link--default" href="#">
                            <i class="bi bi-instagram"></i>
                        </a>
                    </li>
                    <li>
                        <a class="link link--default" href="#">
                            <i class="bi bi-twitter"></i>
                        </a>
                    </li>
                </ul>
                <p class="footer__copyright"><?php echo e(__("copyrights reserved 2021")); ?></p>
            </div>
        </div>
    </footer>
<?php /**PATH D:\xampp\htdocs\AFCM\resources\views/AFMC-project/layouts/includes/footer.blade.php ENDPATH**/ ?>