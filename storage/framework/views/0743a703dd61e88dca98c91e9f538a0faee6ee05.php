<div class="row">
    <div class="col-md-7">
        <div class="kt-section">
            <div class="kt-section__body">
                <?php $__env->startComponent("admin.components.translatable_section"); ?>
                    <?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $__env->slot("translatable_{$language->language}"); ?>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label><?php echo e(__('persons.title')); ?></label>
                                    <?php echo Form::text("title:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                                    <?php if($errors->has("title:{$language->language}")): ?>
                                        <span
                                            class="form-text text-danger"><?php echo e($errors->first("title:{$language->language}")); ?></span>
                                    <?php endif; ?>
                                </div>
                                <div class="col-lg-6">
                                    <label><?php echo e(__('persons.name')); ?></label>
                                    <?php echo Form::text("name:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                                    <?php if($errors->has("name:{$language->language}")): ?>
                                        <span
                                            class="form-text text-danger"><?php echo e($errors->first("name:{$language->language}")); ?></span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label><?php echo e(__('persons.position')); ?></label>
                                    <?php echo Form::text("position:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                                    <?php if($errors->has("position:{$language->language}")): ?>
                                        <span
                                            class="form-text text-danger"><?php echo e($errors->first("position:{$language->language}")); ?></span>
                                    <?php endif; ?>
                                </div>
                                
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label class=""><?php echo e(__('persons.about')); ?></label>
                                    <?php echo Form::textarea("about:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                                    <?php if($errors->has("about:{$language->language}")): ?>
                                        <span
                                            class="form-text text-danger"><?php echo e($errors->first("about:{$language->language}")); ?></span>
                                    <?php endif; ?>

                                    
                                </div>
                                <div class="col-lg-6">
                                    <label class=""><?php echo e(__('persons.description')); ?></label>
                                    <?php echo Form::textarea("description:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                                    <?php if($errors->has("description:{$language->language}")): ?>
                                        <span
                                            class="form-text text-danger"><?php echo e($errors->first("description:{$language->language}")); ?></span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <?php $__env->endSlot(); ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php echo $__env->renderComponent(); ?>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label><?php echo e(__('persons.portfolio_link')); ?></label>
                                    <?php echo Form::text("portfolio_link", null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                                    <?php if($errors->has("portfolio_link")): ?>
                                        <span
                                            class="form-text text-danger"><?php echo e($errors->first("portfolio_link")); ?></span>
                                    <?php endif; ?>
                                </div>
                               
                            </div>
                        
            </div>
        </div>
        <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>
        <div class="kt-section">
            <h4 class="kt-section__title kt-section__title-lg"><?php echo e(__('person.picture')); ?>:</h4>
            <div class="kt-section__body">
                <div class="form-group row">
                 <?php echo $__env->make('admin.layout.form_components.select_image', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    
                </div>
            </div>
        </div>       
    </div>
    <div class="col-md-1"></div>
    <div class="col-lg-4">
        <div class="kt-section">
            <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>
            <div class="kt-section__body">
                <?php echo $__env->make('admin.layout.form_components.status', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                
            </div>
        </div>
    </div>
</div>



<?php /**PATH D:\xampp\htdocs\clean_copy\resources\views/admin/persons/form.blade.php ENDPATH**/ ?>