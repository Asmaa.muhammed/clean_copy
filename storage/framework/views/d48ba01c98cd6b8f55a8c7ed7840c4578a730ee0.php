<?php if(Session::has('success') && !empty(Session::get('success'))): ?>
    <ul class="alert alert-success list-unstyled" style="margin: 0">
        <div class="alert-icon"><i class="flaticon-like"></i></div>
        <?php if(is_array(Session::get('success'))): ?>
            <?php $__currentLoopData = Session::get('success'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $message): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li class="  alert-text "><?php echo e($message); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php else: ?>
            <li class="  alert-text"><?php echo Session::get('success'); ?></li>
        <?php endif; ?>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="la la-close"></i></span>
            </button>
        </div>
    </ul>
<?php endif; ?>
<?php if(Session::has('error') && !empty(Session::get('error'))): ?>
    <ul class="alert alert-danger list-group">
        <?php if(is_array(Session::get('error'))): ?>
            <?php $__currentLoopData = Session::get('error'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $message): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li  class="  alert-text " ><?php echo e($message); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php else: ?>
            <li class="  alert-text "><?php echo Session::get('error'); ?></li>
        <?php endif; ?>
            <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="la la-close"></i></span>
                </button>
            </div>
    </ul>
<?php endif; ?>
<?php /**PATH /var/www/html/AFCM/resources/views/admin/layout/components/flash_messages.blade.php ENDPATH**/ ?>