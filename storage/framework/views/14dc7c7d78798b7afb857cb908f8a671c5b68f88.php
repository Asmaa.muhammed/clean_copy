<div class="row">
    <div class="col-md-7">
        <?php $__env->startComponent("admin.components.translatable_section"); ?>
            <?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php $__env->slot("translatable_{$language->language}"); ?>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label><?php echo e(__('roles.display_name')); ?></label>
                                <?php echo Form::text("display_name:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                                <?php if($errors->has("display_name:{$language->language}")): ?>
                                    <span class="form-text text-danger"><?php echo e($errors->first("display_name:{$language->language}")); ?></span>
                                <?php endif; ?>
                            </div>
                            <div class="col-lg-6">
                                <label class=""><?php echo e(__('roles.description')); ?></label>
                                <?php echo Form::textarea("description:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                                <?php if($errors->has("description:{$language->language}")): ?>
                                    <span class="form-text text-danger"><?php echo e($errors->first("description:{$language->language}")); ?></span>
                                <?php endif; ?>
                            </div>
                        </div>
                <?php $__env->endSlot(); ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php echo $__env->renderComponent(); ?>
        <div class="kt-portlet__head mb-4 row">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <?php echo e(__('common.moduleInfo', ['module'=>__('roles.singularModuleName')])); ?>

                </h3>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-6">
                <label><?php echo e(__('roles.name')); ?></label>
                <?php echo Form::text('name', null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                <?php if($errors->has("name")): ?>
                    <span class="form-text text-danger"><?php echo e($errors->first('name')); ?></span>
                <?php endif; ?>
            </div>
            <div class="col-lg-6">
                <label><?php echo e(__('roles.permissions')); ?></label>
                <?php echo Form::select('permissions[]', $permissions,null, ['class'=>'form-control', 'id'=>'permissions','multiple', 'disabled'=>$disabled]); ?>

                <?php if($errors->has("permissions")): ?>
                    <span class="form-text text-danger"><?php echo e($errors->first('permissions')); ?></span>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-md-4">
        <?php echo $__env->make('admin.layout.form_components.status', ['disabled' => $disabled], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
</div>
<?php $__env->startSection("additionalScripts"); ?>
    <script>
        $(function () {
           $("#permissions").select2();
        });
    </script>
<?php $__env->stopSection(); ?>
<?php /**PATH /var/www/html/AFCM/resources/views/admin/acl/roles/form.blade.php ENDPATH**/ ?>