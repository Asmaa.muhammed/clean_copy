<div class="row">
    <div class="col-md-7">
        <div class="form-group row">
            <div class="col-lg-6">
                <label><?php echo e(__('layoutModels.name')); ?></label>
                <?php echo Form::text('name', null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                <?php if($errors->has("name")): ?>
                    <span class="form-text text-danger"><?php echo e($errors->first('name')); ?></span>
                <?php endif; ?>
            </div>
            <div class="col-lg-6">
                <label class=""><?php echo e(__('layoutModels.layout_type')); ?></label>
                <?php echo Form::select('layout_type', $layout_types->pluck('name', 'id'), null, ['class'=>'form-control','id'=>'layout_type','placeholder'=>__('common.please_choose', ['name' => __('layoutModels.layout_type')]), 'disabled'=>$disabled]); ?>

                <?php if($errors->has("layout_type")): ?>
                    <span class="form-text text-danger"><?php echo e($errors->first('layout_type')); ?></span>
                <?php endif; ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-6">
                <label class=""><?php echo e(__('layoutModels.type')); ?></label>
                <?php echo Form::select('type', $types, null, ['class'=>'form-control','placeholder'=>__('common.please_choose', ['name'=>__('layoutModels.type')]), 'disabled'=>$disabled]); ?>

                <?php if($errors->has("type")): ?>
                    <span class="form-text text-danger"><?php echo e($errors->first('type')); ?></span>
                <?php endif; ?>
            </div>
            <div class="col-lg-6">
                <label class=""><?php echo e(__('layoutModels.theme')); ?></label>
                <?php echo Form::select('theme_id', $themes, null, ['class'=>'form-control','placeholder'=>__('common.please_choose', ['name' => __('layoutModels.theme')]), 'disabled'=>$disabled]); ?>

                <?php if($errors->has("theme_id")): ?>
                    <span class="form-text text-danger"><?php echo e($errors->first('theme_id')); ?></span>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-lg-4">
        <?php echo $__env->make('admin.layout.form_components.status', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
</div>
<div class="form-group row">
    <div class="col-lg-12" id="pluginsLists">

    </div>
</div>
<?php $__env->startSection('additionalScripts'); ?>
    <script src="https://cdn.jsdelivr.net/npm/handlebars@latest/dist/handlebars.js"></script>
    <script src="<?php echo e(asset('public/admin/assets/js/pages/components/extended/dual-listbox.js')); ?>"></script>
    <script>
        let layoutTypes = (<?php echo json_encode($layout_types, 15, 512) ?>).map(function (item) {
            item['value'] = JSON.parse(item['value']);
            return item;
        });
        let layoutSections = <?php echo json_encode($layout_sections, 15, 512) ?>;
        <?php $__currentLoopData = $layout_sections; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $layout_section): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        Handlebars.registerHelper('plugins-<?php echo e($layout_section->value); ?>', function (value) {

            return `<?php echo Form::select('pluginGroups['.$layout_section->value.'][]'
                            ,$pluginGroups->has($layout_section->value)
                            ? $pluginGroups->get($layout_section->value)->pluck('name', 'id')
                            : []
                            , null
                            , [
                                "id"=>"plugins-".$layout_section->value,
                                'class'=>'form-control mb-2 kt-dual-listbox',
                                'multiple' => "multiple",
                                'disabled'=>$disabled,
                                'data-available-title' => "Available {$layout_section->name} Plugins",
                                'data-selected-title'=>"Selected {$layout_section->name} Plugins",
                                'data-search' => "false",
                             ]
                         ); ?>`;
        });
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        $('#layout_type').change(function (event) {
            let query = "";
            let selectedType = layoutTypes.find((layoutType) => {
                return layoutType.id == $(this).val();
            });
            let selectedSections = layoutSections.filter(function (item) {
                return selectedType ? selectedType.value.sections.includes(parseInt(item.value)) : false;
            });
            for (let i in selectedSections) {
                query += `{{{plugins-${selectedSections[i].value}}}}`;
            }
            console.log(query);
            if(query.length > 0){
                query = "<hr>" + query;
                $("#pluginsLists").empty().html(Handlebars.compile(query)());
            }
            KTDualListbox.init();
        }).change();
    </script>
<?php $__env->stopSection(); ?>
<?php /**PATH D:\xampp\htdocs\AFCM\resources\views/admin/layoutModels/form.blade.php ENDPATH**/ ?>