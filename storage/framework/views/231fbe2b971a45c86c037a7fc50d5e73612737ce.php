<?php if($model->status == 1): ?>
    <span class="kt-badge  kt-badge--primary kt-badge--inline kt-badge--pill"><?php echo e(__('translation::translation.status_enable')); ?></span>
<?php else: ?>
    <span
        class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill"><?php echo e(__('translation::translation.status_disable')); ?></span>
<?php endif; ?>
<?php /**PATH D:\xampp\htdocs\clean_copy\resources\views/admin/layout/includes/status_column.blade.php ENDPATH**/ ?>