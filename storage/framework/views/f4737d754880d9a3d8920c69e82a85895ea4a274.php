<?php $__env->startSection('subheader'); ?> <?php echo $__env->make('admin.layout.components.breadcrumb',['pageTitle'=>__('common.moduleTitle', ['module'=>$cmsModule->name]),'moduleName'=>$cmsModule->route,'pageName'=>__('common.createPageName', ['module'=>__('layoutModels.singularModuleName')]) ], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile"
                 id="kt_page_portlet">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title"><?php echo e(__('common.addNew', ['module'=>__('layoutModels.singularModuleName')])); ?></h3>
                    </div>
                    <?php echo $__env->make("admin.layout.includes.form_button",['disabled'=>false,"module"=>$cmsModule->route], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>
                <div class="kt-portlet__body">
                    <?php echo Form::open(['route'=>'layoutModels.store', 'method'=>'post', 'class'=>'kt-form kt-form--labe-right','id'=>'kt_form']); ?>

                    <?php echo $__env->make('admin.layoutModels.form', ['disabled'=>false], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', ['pageTitle'=>__('common.createPageTitle', ['module'=>$cmsModule->name])], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\Event_mangnent\resources\views/admin/layoutModels/create.blade.php ENDPATH**/ ?>