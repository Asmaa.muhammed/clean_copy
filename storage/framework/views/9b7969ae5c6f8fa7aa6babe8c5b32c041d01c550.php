<?php $__env->startSection('content'); ?>
<div class="internals-breadcrumb">
    <div class="container">
        <h2 class="internals-title"><?php echo e(__("front.medicalNews")); ?></h2>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo e(route('staff')); ?>"><?php echo e(__("front.staff")); ?></a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo e(__("front.medicalNews")); ?></li>
            </ol>
        </nav>
    </div>
</div>

<section class="section section--padding">
    <div class="container">
        <div class="section__header">
            <!-- <div class="section__title">
                <span><?php echo e(__("follow our news")); ?></span>
                <h2 class="section-title"><?php echo e(__("latest news")); ?></h2>
            </div>
            <a class="btn btn--default" href="<?php echo e(route('staffMoreNews')); ?>"><?php echo e(__("more news")); ?></a> -->
        </div>
        <div class="data-block">
            <div class="row">
                <?php $__empty_1 = true; $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <div class="col-lg-4">
                    <div class="data-block__item">
                        <a href="<?php echo e(route('news',$post->id)); ?>" class="data-block__img"
                            style="background-image: url('<?php echo e($post->image->image_url); ?>">
                        </a>
                        <div class="data-block__dis">
                            <a href="<?php echo e(route('news',$post->id)); ?>" class="block-title link link--main-clr">
                                <?php echo e(substr($post->title, 0, 50)); ?>

                            </a>
                                <?php echo $post->body; ?>

                            <div class="data-block__link">
                                <a class="link link--red-clr double-chevron--after" href="<?php echo e(route('news',$post->id)); ?>">
                                    read more
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make($themePath.'.students.layouts.mainPage',["title"=>__("front.medicalNews"),"minor"=>true,"MenuLinks"=>$StaffMenu], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/AFCM/resources/views/AFMC-project/staff/medical_news.blade.php ENDPATH**/ ?>