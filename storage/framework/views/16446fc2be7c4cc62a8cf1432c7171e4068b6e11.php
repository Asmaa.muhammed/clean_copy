<div class="form-group">
    <label><?php echo e(__('translation::translation.status')); ?></label>
    <div class="kt-radio-list">
        <label class="kt-radio kt-radio--success">
            <?php echo e(Form::radio('status', 1, null,['id' => 'input-radio-16' ,$disabled ? 'disabled': ''])); ?> <?php echo e(__('translation::translation.status_enable')); ?>

            <span></span>

        </label>
        <label class="kt-radio kt-radio--danger">
            <?php echo e(Form::radio('status', 0, null,['id' => 'input-radio-17' ,$disabled ? 'disabled': ''])); ?> <?php echo e(__('translation::translation.status_disable')); ?>

            <span></span>
        </label>
        <?php $__errorArgs = ['status'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
            <span class="form-text text-danger"><?php echo e($message); ?></span>
        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
    </div>

</div>
<?php /**PATH D:\xampp\htdocs\clean_copy\resources\views/admin/layout/form_components/status.blade.php ENDPATH**/ ?>