<section class="section section--padding">
    <div class="container">
        <div class="data-block">
            <div class="row">
                <?php $__empty_1 = true; $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <div class="col-lg-4">
                    <div class="data-block__item">
                        <a href="<?php echo e(route('post',$post->id)); ?>" class="data-block__img"
                            style="background-image: url('<?php echo e($post->image->image_url); ?>">
                        </a>
                        <div class="data-block__dis">
                            <a href="<?php echo e(route('post',$post->id)); ?>" class="block-title link link--main-clr">
                                <?php echo e(substr($post->title, 0, 50)); ?>

                            </a>
                                <?php echo $post->body; ?>

                            <div class="data-block__link">
                                <a class="link link--red-clr double-chevron--after" href="<?php echo e(route('post',$post->id)); ?>">
                                    <?php echo e(__("read more")); ?>

                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<?php /**PATH D:\xampp\htdocs\AFCM\resources\views/widgets/horizontal_posts.blade.php ENDPATH**/ ?>