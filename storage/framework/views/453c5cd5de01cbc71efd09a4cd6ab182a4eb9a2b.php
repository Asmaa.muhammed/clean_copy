<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Bootstrap & Plugins -->
    <link rel="stylesheet" href="node_modules/@fortawesome/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="node_modules/select2/dist/css/select2.min.css">

    <!-- Main CSS -->
    <link rel="stylesheet" href="css/style_ltr.css">

    <!-- icon & Title -->
    <link rel="icon" href="media/icon.png" type="image/x-icon">
    <title><?php echo $__env->yieldContent('title'); ?></title>
</head>
<body>
        <div class="flex-center position-ref full-height">
            <div class="code">
                <?php echo $__env->yieldContent('code'); ?>
            </div>

            <div class="message" style="padding: 10px;">
                <?php echo $__env->yieldContent('message'); ?>
            </div>
        </div>
    </body>
</html>
<?php /**PATH D:\xampp\htdocs\clean_copy\resources\views/errors/minimal.blade.php ENDPATH**/ ?>