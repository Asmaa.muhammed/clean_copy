<?php $__env->startSection('content'); ?>
   
<div class="internals-breadcrumb">
    <div class="container">
        <h2 class="internals-title"><?php echo e($page->title); ?></h2>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo e(route('index')); ?>"><?php echo e(__("front.home")); ?></a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo e(__("front.about_us")); ?></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo e($page->title); ?></li>
            </ol>
        </nav>
    </div>
</div>
<section class="section section--padding section--bg">
    <div class="container">
        <div class="data-block">
            <div class="col-lg-4">
                <div class="footer__block">
                    <h4 class="footer__title"><?php echo e(__("front.contactInformation")); ?></h4>
                    <ul class="footer__contact">
                        <li>
                            <i class="bi bi-geo-alt-fill"></i>
                            <span><?php echo e(__("front.theAddress")); ?></span>
                        </li>
                        <li>
                            <i class="bi bi-phone"></i>
                            <a class="link link__footer" href="fax:02-24032839">02-24032839</a>
                        </li>
                        <li>
                            <i class="bi bi-telephone-fill"></i>
                            <a class="link link__footer" href="tel:02-24032872">02-24032872 - 02-24032843</a>
                        </li>
                        <li>
                            <i class="bi bi-envelope-fill"></i>
                            <a class="link link__footer" href="mailto:Afcm@mod.gov.eg">Afcm@mod.gov.eg</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                 <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3371.4233774434806!2d31.304141!3d30.084518000000003!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x55d2a4e47d79ee58!2sArmed%20Forces%20College%20of%20Medicine!5e1!3m2!1sen!2seg!4v1626552837125!5m2!1sen!2seg" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>
        </div>
    </div>
</section>

<?php $__env->stopSection(); ?>


<?php echo $__env->make($themePath.'.layouts.mainPage',["title"=>__("front.contact_us")], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\AFCM\resources\views/AFMC-project/contact_us.blade.php ENDPATH**/ ?>