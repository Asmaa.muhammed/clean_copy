<?php $__env->startSection('content'); ?>
    <div class="internals-breadcrumb">
        <div class="container">
        <h2 class="internals-title"><?php echo e(__("front.newsDetails")); ?></h2>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo e(route('index')); ?>"><?php echo e(__("front.home")); ?></a></li>
            <li class="breadcrumb-item active" aria-current="page"><?php echo e(__("front.news")); ?></li>
            </ol>
        </nav>
        </div>
    </div>
<div class='container pt-5'>
    <div class="data-block__item">
        <a href="#" class="data-block__img"
            style="background-image: url('<?php echo e($post->image->image_url); ?>')">
        </a>
        <div class="data-block__dis">
        <span class="data-block__date">
            <span><?php echo e($post->minor->titile); ?></span>
        </span>

            <a href="#" class="block-title link link--main-clr" style='font-size:30px'>
               <?php echo e($post->title); ?>

            </a>
          <span style='font-size:18px'><?php echo $post->body; ?></span>
        </div>
    </div>

</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('additionalScripts'); ?>
    <script src="<?php echo e(asset('public/'.$themePath."/")); ?>/node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js"></script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make($themePath.'.layouts.mainPage',["title"=>$post->title,"minor"=>($post->minor->page->title != "Main Page")? true:false,"MenuLinks"=>($post->minor->page->title == "Student")? $studentMenu:$StaffMenu], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\AFCM\resources\views/AFMC-project/news.blade.php ENDPATH**/ ?>