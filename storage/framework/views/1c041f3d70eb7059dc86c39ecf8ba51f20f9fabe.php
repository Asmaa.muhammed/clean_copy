<button type="button" class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal"
        data-target="#generate_<?php echo e($model['plugin']->short_code); ?>"><?php echo e(__('plugins.generateShortcode')); ?></button>
<div class="modal fade" id="generate_<?php echo e($model['plugin']->short_code); ?>" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><?php echo e(__('plugins.generateShortcode')); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <?php if($model['plugin']->type): ?>
                <div class="form-group">
                    <label for="recipient-name" class="form-control-label">Recipient:</label>
                    <?php echo Form::select($model['plugin']->short_code,
                     (\Illuminate\Database\Eloquent\Relations\Relation::getMorphedModel($model['plugin']->type))::all()
                     ->pluck('name', 'id')->toArray(), null, ['class'=>'form-control','placeholder'=>'Choose User', 'id' => $model['plugin']->short_code,]); ?>

                </div>
                <?php endif; ?>
                <div class="form-group">
                    <label for="message-text-<?php echo e($model['plugin']->short_code); ?>" class="form-control-label">
                        <?php echo e(__('plugins.short_code')); ?>:
                    </label>
                    <textarea class="form-control" disabled="disabled" id="message-text-<?php echo e($model['plugin']->short_code); ?>">
                        <?php echo e("[".$model['plugin']->short_code."]"); ?>

                    </textarea>
                </div>
                <script>
                    $("#<?php echo e($model['plugin']->short_code); ?>").change(function () {
                        $("#message-text-<?php echo e($model['plugin']->short_code); ?>")
                            .empty()
                            .val(`[<?php echo e($model['plugin']->short_code); ?> id=${$(this).val()}]`);
                    });
                </script>
            </div>
        </div>
    </div>
</div>
<?php /**PATH D:\xampp\htdocs\AFCM\resources\views/admin/plugins/generator_modal_action.blade.php ENDPATH**/ ?>