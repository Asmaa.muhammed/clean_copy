<?php $__env->startSection('content'); ?>
<div class="internals-breadcrumb">
    <div class="container">
        <h2 class="internals-title"><?php echo e(__("collegeMagazine.pageTitle")); ?></h2>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo e(route('index')); ?>"><?php echo e(__("front.home")); ?></a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo e(__("front.mediaCenter")); ?></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo e(__("collegeMagazine.pageTitle")); ?></li>
            </ol>
        </nav>
    </div>
</div>
<section class="section section--padding">
    <div class="container">
    <?php $__empty_1 = true; $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
        <div class="row-with-img">
            <div class="row-with-img__dis">
                <div class="data-block data-block--no-border data-block--no-img data-block--no-hover">
                    <div class="data-block__item">
                        <div class="data-block__dis grey-background">
                            <p class="block-title link link--main-clr">
                                <?php echo e($post->title); ?>

                            </p>
                            <?php echo $post->summary; ?>

                            <a class="link link--red-clr double-chevron--after" href="<?php echo e(route('post',$post->id)); ?>">
                                <?php echo e(__("read more")); ?>

                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-with-img__img">
                <img src="<?php echo e($post->image->image_url); ?>" class="img-fluid" alt="">
            </div>
        </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
        <?php endif; ?>
        <!--<div class="custom-pagination">-->
        <!--    <button class="btn btn--grey double-chevron-left--before">previos</button>-->
        <!--    <button class="btn btn--grey double-chevron--after">next</button>-->
        <!--</div>-->

    </div>
</section>
<?php $__env->stopSection(); ?>


<?php echo $__env->make($themePath.'.layouts.mainPage',["title"=>__("collegeMagazine.pageTitle")], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\AFCM\resources\views/AFMC-project/college_magazine.blade.php ENDPATH**/ ?>