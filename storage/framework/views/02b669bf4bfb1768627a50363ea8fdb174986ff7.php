
    <ul class="nav nav-tabs  nav-tabs-line nav-tabs-line-2x nav-tabs-line-danger" role="tablist">
        <?php $__empty_1 = true; $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            <li class="nav-item">
                <a class="nav-link <?php echo e(($loop->first) ? 'active': ''); ?>" data-toggle="tab"
                   href="#<?php echo e($language->language); ?>" role="tab"><?php echo e($language->name); ?></a>
            </li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
        <?php endif; ?>

    </ul>
    <div class="tab-content">
        <?php $__empty_1 = true; $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            <div class="tab-pane  <?php echo e(($loop->first) ? 'active': ''); ?>" id="<?php echo e($language->language); ?>" role="tabpanel">
                <div class="form-group row">
                    <div class="col-lg-6">
                        <label for="name:<?php echo e($language->language); ?>"><?php echo e(__('enums.name')); ?>:</label>
                        <?php echo Form::text("name:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                        <?php if($errors->has("name:{$language->language}")): ?>
                            <span class="form-text text-danger"><?php echo e($errors->first("name:{$language->language}")); ?></span>
                        <?php endif; ?>
                    </div>
                    <div class="col-lg-6">
                        <label for="description:<?php echo e($language->language); ?>"><?php echo e(__('enums.description')); ?>:</label>
                        <?php echo Form::textarea("description:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                        <?php if($errors->has("description:{$language->language}")): ?>
                            <span
                                class="form-text text-danger"><?php echo e($errors->first("description:{$language->language}")); ?></span>
                        <?php endif; ?>
                    </div>
                </div>

            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
        <?php endif; ?>

    </div>

<?php /**PATH D:\xampp\htdocs\Event_mangnent\resources\views/admin/layout/components/basic_tab.blade.php ENDPATH**/ ?>