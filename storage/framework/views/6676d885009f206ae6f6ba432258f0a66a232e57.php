<ul class="nav nav-tabs  nav-tabs-line nav-tabs-line-2x nav-tabs-line-danger" role="tablist">
    <?php $__empty_1 = true; $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
        <li class="nav-item">
            <a class="nav-link <?php echo e(($loop->first) ? 'active': ''); ?>" data-toggle="tab"
               href="#<?php echo e($language->language); ?>" role="tab"><?php echo e($language->name); ?></a>
        </li>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
    <?php endif; ?>

</ul>
<div class="tab-content">
    <?php $__empty_1 = true; $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
        <div class="tab-pane  <?php echo e(($loop->first) ? 'active': ''); ?>" id="<?php echo e($language->language); ?>" role="tabpanel">
            <?php echo ${"translatable_".$language->language}; ?>

        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
    <?php endif; ?>

</div>

<?php /**PATH /var/www/html/AFCM/resources/views/admin/components/translatable_section.blade.php ENDPATH**/ ?>