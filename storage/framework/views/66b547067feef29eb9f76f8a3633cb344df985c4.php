<div class="dropdown dropdown-inline ">
    <button type="button"
            class="btn btn-hover-warning btn-elevate-hover btn-icon btn-sm btn-icon-md btn-circle"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="flaticon-more-1"></i>
    </button>
    <div class="dropdown-menu dropdown-menu-right <?php echo e($module); ?>" x-placement="top-end"
         style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-149px, -189px, 0px);">
      
        <a class="dropdown-item"
           href="<?php echo e(route($module."."."edit", $model)); ?>"><i
                class="la la-pencil-square-o"></i> <?php echo e(__('translation::translation.edit')); ?>

        </a>
       

        <a class="dropdown-item" href="<?php echo e(route($module."."."show", $model)); ?>">
            <i class="la la-info"></i> <?php echo e(__('translation::translation.full_info')); ?>

        </a>
      
        <div class="dropdown-divider"></div>

        <button type="button" class="delete_btn dropdown-item btn-clean "
                id="delete-<?php echo e($model[Str::singular($module)]->id); ?>">
            <i class="la la-trash-o"></i> <?php echo e(__('translation::translation.delete')); ?>

        </button>
      


    </div>
</div>
<?php /**PATH D:\xampp\htdocs\Event_mangnent\resources\views/admin/layout/components/action_button.blade.php ENDPATH**/ ?>