<!--Scripts-->
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js"
        integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT"
        crossorigin="anonymous"></script>
<script src="<?php echo e(asset('public/'.$themePath."/")); ?>/node_modules/jquery/dist/jquery.js"></script>
<script src="<?php echo e(asset('public/'.$themePath."/")); ?>/js/jquery.nivo.slider.js"></script>
<script src="<?php echo e(asset('public/'.$themePath."/")); ?>/js/all.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script>
    $(function () {
        var $l = $('#carousel-left'),
            $c = $('#carousel-center'),
            $r = $('#carousel-right');

        $l.carouFredSel({
            auto: false,
            items: 1,
            direction: 'right',
            prev: {
                button: '#prev',
                fx: 'uncover',
                onBefore: function () {
                    setTimeout(function () {
                        $c.trigger('prev');
                    }, 100);
                }
            },
            next: {
                fx: 'cover'
            }
        });
        $c.carouFredSel({
            auto: false,
            items: 1,
            prev: {
                onBefore: function () {
                    setTimeout(function () {
                        $r.trigger('prev');
                    }, 100);
                }
            },
            next: {
                onBefore: function () {
                    setTimeout(function () {
                        $l.trigger('next');
                    }, 100);
                }
            }
        });
        $r.carouFredSel({
            auto: false,
            items: 1,
            prev: {
                fx: 'cover'
            },
            next: {
                button: '#next',
                fx: 'uncover',
                onBefore: function () {
                    setTimeout(function () {
                        $c.trigger('next');
                    }, 100);
                }
            }
        });
    });
</script>

<script src="<?php echo e(asset('public/'.$themePath."/")); ?>/js/index.js"></script>

<script src="<?php echo e(asset('public/'.$themePath."/")); ?>/node_modules/sweetalert/dist/sweetalert.min.js"></script>

<script>
    jQuery(document).ready(function ($) {
        $('.subscribe').on('click', function (e) {
            data = {
                _token: '<?php echo e(csrf_token()); ?>',
                'email': $("#email").val(),

            };
            $.ajax({
                url: '<?php echo e(route('newsletter')); ?>',
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (data) {
                    sweetAlert({
                        title: "<?php echo e(__("front.success")); ?>!",
                        text: data.message,
                        icon: "success",
                        button: {
                            text:'<?php echo e(__("front.ok")); ?>' ,
                            value: true,
                            visible: true,
                            className: "btn btn--scdBG",
                            closeModal: true

                        },
                    });

                }
            });
            return false;
        })

    });

</script>
<?php /**PATH /var/www/html/AFCM/resources/views/AFMC-project/students/layouts/includes/scripts.blade.php ENDPATH**/ ?>