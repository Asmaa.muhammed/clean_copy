<section class="section section--padding">
    <div class="container">
    <?php $__empty_1 = true; $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
        <div class="row-with-img">
            <div class="row-with-img__dis">
                <div class="data-block data-block--no-border data-block--no-img data-block--no-hover">
                    <div class="data-block__item">
                        <div class="data-block__dis grey-background">
                            <p class="block-title link link--main-clr">
                                <?php echo e($post->title); ?>

                            </p>
                            <?php echo $post->summary; ?>

                            <a class="link link--red-clr double-chevron--after" href="<?php echo e(route('post',$post->id)); ?>">
                                <?php echo e(__("read more")); ?>

                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-with-img__img">
                <img src="<?php echo e($post->image->image_url); ?>" class="img-fluid" alt="">
            </div>
        </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
        <?php endif; ?>
        <div class="custom-pagination">
            <button class="btn btn--grey double-chevron-left--before">previos</button>
            <button class="btn btn--grey double-chevron--after">next</button>
        </div>

    </div>
</section><?php /**PATH /var/www/html/AFCM/resources/views/widgets/vertical_posts.blade.php ENDPATH**/ ?>