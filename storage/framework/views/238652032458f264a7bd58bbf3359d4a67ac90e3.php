<?php $__env->startSection('content'); ?>

  <div class="internals-breadcrumb">
    <div class="container">
      <h2 class="internals-title"><?php echo e(__("front.search")); ?></h2>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo e(route('index')); ?>"><?php echo e(__("front.home")); ?></a></li>
          <li class="breadcrumb-item active" aria-current="page"><?php echo e(__("front.search")); ?></li>
        </ol>
      </nav>
    </div>
</div>

<section class="section section--padding">
    <?php if(!$pages->count() && !$events->count()): ?>
        <div class="d-flex justify-content-center">
        <div class="google-calendar">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10">
                        <div class="custom-card">
                            <p class="card-title">CAN’T FIND WHAT YOU ARE LOOKING FOR?</p>
                            <a href="#" class="btn btn--secondary">ASK QUESTION</a>
                        </div>
                    </div>

                    <div class="col-lg-8">
                        <div class="google-calendar__iframe">
                            <img src="./assets/media/calender.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    <?php endif; ?>
  
    <?php if($pages->count()): ?>
            <div class="container mt-3">
            <?php $__empty_1 = true; $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <div class="row-with-img">
                    <div class="row-with-img__dis">
                        <div class="data-block data-block--no-border data-block--no-img data-block--no-hover">
                            <div class="data-block__item">

                                <div class="data-block__dis grey-background">
                                    <p class="block-title link link--main-clr">
                                        <?php echo e($page->title); ?>

                                    </p>
                                    <?php echo e(substr($page->summary, 0, 100)); ?>

                                    <div class="data-block__link">
                                        <a class="link link--red-clr double-chevron--after" href="<?php echo e(route('page',$page->id)); ?>">
                                            <?php echo e(__("read more")); ?>

                                        </a>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row-with-img__img">
                        <img src="<?php echo e($page->image->image_url ?? "#"); ?>" class="img-fluid" alt="">
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <?php endif; ?>
            </div>
    <?php endif; ?>
    <?php if($events->count()): ?>
        <div class="container mt-3">
        <?php $__empty_1 = true; $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            <div class="row-with-img">
                <div class="row-with-img__dis">
                    <div class="data-block data-block--no-border data-block--no-img data-block--no-hover">
                        <div class="data-block__item">

                            <div class="data-block__dis grey-background">
                                <p class="block-title link link--main-clr">
                                    <?php echo e($event->title); ?>

                                </p>
                                <?php echo e(substr($event->summary, 0, 100)); ?>


                                <div class="data-block__link">
                                    <a class="link link--red-clr double-chevron--after" href="<?php echo e(route('event',$event->id)); ?>">
                                        <?php echo e(__("read more")); ?>

                                    </a>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="row-with-img__img">
                    <img src="<?php echo e($event->image->image_url ?? "#"); ?>" class="img-fluid" alt="">
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
            <?php endif; ?>

        </div>
    <?php endif; ?>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make($themePath.'.layouts.mainPage',["title"=>__("front.search")], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\AFCM\resources\views/AFMC-project/main_search.blade.php ENDPATH**/ ?>