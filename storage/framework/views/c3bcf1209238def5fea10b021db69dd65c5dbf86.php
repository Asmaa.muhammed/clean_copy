<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Bootstrap & Plugins -->
    <link rel="stylesheet" href="<?php echo e(asset('public/'.$themePath."/")); ?>/node_modules/@fortawesome/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="<?php echo e(asset('public/'.$themePath."/")); ?>/node_modules/select2/dist/css/select2.min.css">

    <!-- Main CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('public/'.$themePath."/")); ?>/css/style_ltr.css">

    <!-- icon & Title -->
    <link rel="icon" href="<?php echo e(asset('public/'.$themePath."/")); ?>/media/icon.png" type="image/x-icon">
    <title><?php echo $__env->yieldContent('title'); ?></title>
</head>
    <body>
   <main>
       <section class="section section--full-page">
           <div class="container-header">
               <a class="link link--scdClr link--angle" href="<?php echo e(route('index')); ?>"><?php echo e(__("front.back")); ?></a>
               <div class="centered-con">
                   <div class="thanks-page p404">
                       <h5 class="heading-primary"><?php echo $__env->yieldContent('code'); ?></h5>
                       <p><?php echo $__env->yieldContent('message'); ?></p>
                       <div class="social-links">
                           <a href="#" class="link link--mainClr"><i class="fab fa-facebook-f"></i></a>
                           <a href="#" class="link link--mainClr"><i class="fab fa-twitter"></i></a>
                           <a href="#" class="link link--mainClr"><i class="fab fa-google"></i></a>
                       </div>
                   </div>
               </div>
           </div>
       </section>
   </main>
    <!-- End of Main Content -->

    <!-- core scripts -->
    <script src="<?php echo e(asset('public/'.$themePath."/")); ?>/node_modules/jquery/dist/jquery.min.js"></script>
    <!--<script src="node_modules/@popperjs/core/dist/umd/popper.min.js"></script>-->
    <script src="<?php echo e(asset('public/'.$themePath."/")); ?>/https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>

    <!-- Custom bootstrap scripts -->
    <script src="<?php echo e(asset('public/'.$themePath."/")); ?>/node_modules/bootstrap/js/dist/util.js"></script>

    <!-- Main scripts -->
    <script src="<?php echo e(asset('public/'.$themePath."/")); ?>/js/mainScript.js"></script>
    </body>
</html>
<?php /**PATH /var/www/html/AFCM/resources/views/errors/layout.blade.php ENDPATH**/ ?>