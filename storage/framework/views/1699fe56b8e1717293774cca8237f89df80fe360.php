<div class="col-lg-6">
    <label >    <?php echo e(__('translation::translation.image')); ?>:</label>
    <div class="input-group">
          <span class="input-group-prepend">
            <a  data-input="thumbnail" data-preview="holder" class="btn btn-primary lfm ">
              <i class="la la-image  text-white "></i>
            </a>
          </span>
        <?php echo Form::text('image_url', (!empty($image) ? $image : null), ['class'=>'form-control','id'=>'thumbnail','disabled'=>$disabled]); ?>


    </div>
    <div id="holder" style="margin-top:15px;max-height:150px;">
        <?php if(!empty($image)): ?>
            <img src="<?php echo e($image); ?>" width="150px">
            <?php endif; ?>
    </div>
</div>

<?php /**PATH D:\xampp\htdocs\clean_copy\resources\views/admin/layout/form_components/select_image.blade.php ENDPATH**/ ?>