<div class="kt-container  kt-container--fluid ">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">
            <?php echo e($pageTitle); ?> </h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="<?php echo e(route("dashboard.index")); ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?php if(!isset($params)): ?><?php echo e(route("{$moduleName}.index" ?? null)); ?><?php else: ?> <?php echo e(route("$moduleName.index", ...$params)); ?> <?php endif; ?>" class="kt-subheader__breadcrumbs-link">
                <?php echo e(ucfirst($moduleName)); ?> </a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="#" class="kt-subheader__breadcrumbs-link">
                <?php echo e($pageName); ?> </a>

            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>
    </div>

</div>
<?php /**PATH /var/www/html/AFCM/resources/views/admin/layout/components/breadcrumb.blade.php ENDPATH**/ ?>