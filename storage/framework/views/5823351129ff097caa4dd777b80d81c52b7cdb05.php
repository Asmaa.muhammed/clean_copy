<?php $__env->startSection('content'); ?>
<?php echo $__env->make('admin.layout.components.flash_messages', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<section class="section">
<div id="homeSlider" class="carousel slide carousel-fade" data-bs-ride="carousel">
    <div class="carousel-inner">
        <?php $__empty_1 = true; $__currentLoopData = $slider->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            <div class="carousel-item <?php echo e($loop->iteration == 1 ? 'active' :""); ?>">
                <div class="grid-container">
                    <?php if(array_key_exists($key+2,$slider->images->toArray())): ?>
                    <a class="grid-container_item grid-container__item--double" href="<?php echo e(route('moreEvents')); ?>"
                        style="background-image: url('<?php echo e($slider->images[$key+2]->image_url); ?>')"></a>
                    <?php endif; ?>
                    <a class="grid-container__item" href="<?php echo e(route('moreEvents')); ?>"
                        style="background-image: url('<?php echo e($slider->images[0]->image_url); ?>')"></a>
                    <?php if(array_key_exists(1,$slider->images->toArray())): ?>
                    <a class="grid-container__item" href="<?php echo e(route('moreEvents')); ?>"
                        style="background-image: url('<?php echo e($slider->images[1]->image_url); ?>')"></a>
                    <?php endif; ?>
                </div>
                <div class="carousel-caption d-md-block">
                    <h5 class='block-title-event' ><?php echo e($image->image_alt); ?></h5>
                    <p class='block-p'><?php echo e($image->caption); ?></p>
                    <a class="btn btn--white" href="<?php echo e(route('moreEvents')); ?>"><?php echo e(__("more events")); ?></a>
                </div>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
        <?php endif; ?>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#homeSlider" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#homeSlider" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button>
</div>
</section>


<section class="section section--padding">
    <div class="container">
        <div class="section__header">
            <div class="section__title">
                <span><?php echo e(__("follow our news")); ?></span>
                <h2 class="section-title"><?php echo e(__("latest news")); ?></h2>
            </div>
            <p ></p>
            <a class="btn btn--default" href="<?php echo e(route('moreNews')); ?>"><?php echo e(__("more news")); ?></a>
        </div>
        <div class="data-block">
            <div class="row">
                <?php if($posts): ?>
                    <?php $__empty_1 = true; $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $news): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <div class="col-lg-4">
                        <div class="data-block__item">
                            <a href="<?php echo e(route('news',$news->id)); ?>" class="data-block__img"
                                style="background-image: url('<?php echo e($news->image->image_url); ?>')">
                            </a>
                            <div class="data-block__dis">
                            <span class="data-block__date">
                                <span><?php echo e($news->start_date); ?></span>
                            </span>

                                <a href="<?php echo e(route('news',$news->id)); ?>" class="block-title link link--main-clr">
                                    <?php echo e($news->title); ?>

                                </a>
                                    <?php echo $news->body; ?>

                                <div class="data-block__link">
                                    <a class="link link--red-clr double-chevron--after" href="<?php echo e(route('news',$news->id)); ?>">
                                        <?php echo e(__("read more")); ?>

                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<section class="section section--padding section--bg">
    <div class="container">
        <div class="section__header">
            <div class="section__title">
                <span><?php echo e(__("our events")); ?></span>
                <h2 class="section-title"><?php echo e(__("our upcoming Events")); ?></h2>
            </div>
            <p></p>
            <a class="btn btn--default" href="<?php echo e(route('moreEvents')); ?>"><?php echo e(__("more events")); ?></a>
        </div>
        <div class="row">
            <?php if($event): ?>
                <div class="col-lg-6">
                    <div class="data-block data-block--no-border mb-3 mb-lg-0">
                        <div class="data-block__item">
                            <a href="<?php echo e(route('event',$event->id)); ?>" class="data-block__img"
                                style="background-image: url('<?php echo e($event->image->image_url); ?>')"></a>
                            <div class="data-block__dis">
                            <span class="data-block__date">
                                <span><?php echo e($event->eventDetails->start_date); ?></span>
                            </span>
                                <a href="<?php echo e(route('event',$event->id)); ?>" class="block-title link link--main-clr">
                                <?php echo e($event->title); ?>

                                </a>
                                <?php echo $event->body; ?>

                                <div class="data-block__link">
                                    <a class="link link--red-clr double-chevron--after" href="<?php echo e(route('event',$event->id)); ?>">
                                        <?php echo e(__("read more")); ?>

                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php if($events): ?>
                <div class="col-lg-6">
                    <div class="data-block data-block--no-border data-block--no-img">
                        <?php $__empty_2 = true; $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_2 = false; ?>
                        <div class="data-block__item">
                            <div class="data-block__dis">
                            <span class="data-block__date">
                                <span><?php echo e($event->eventDetails->start_date); ?></span>
                            </span>
                                <a href="<?php echo e(route('event',$event->id)); ?>" class="block-title link link--main-clr" >
                                    <?php echo e($event->title); ?>

                                </a>
                                <article>
                                    <p>   <?php echo $event->body; ?></p>
                                </article>
                                <div class="data-block__link">
                                    <a class="link link--red-clr double-chevron--after" href="<?php echo e(route('event',$event->id)); ?>">
                                        <?php echo e(__("read more")); ?>

                                    </a>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_2): ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<section class="section section--padding">
    <div class="container">
        <div class="section__header">
            <div class="section__title">
                <span><?php echo e(__("our events")); ?></span>
                <h2 class="section-title"><?php echo e(__("meet the graduated")); ?></h2>
            </div>
            <p></p>
            <a class="btn btn--default" href="<?php echo e(route('moreEvents')); ?>"><?php echo e(__("more events")); ?></a>
        </div>
        <?php if($subSlider): ?>
            <div id="wrapper">
                <div id="carousel-left">
                    <?php $__currentLoopData = $subSlider->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="slider-img" style="background-image: url('<?php echo e($event->image_url); ?>')"></div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
                <div id="carousel-center">
                <?php $__currentLoopData = $subSlider->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="slider-img" style="background-image: url('<?php echo e($event->image_url); ?>')">
                        <div class="carousel-card">
                            <h5 class="block-title-event"><?php echo e($event->image_alt); ?></h5>
                            <div class="quote-top">
                                <i class="bi bi-chat-right-quote"></i>
                            </div>
                            <article>
                                <p  class='block-p'> <?php echo e($event->caption); ?>   </p>
                            </article>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
                <div id="carousel-right">
                    <?php $__currentLoopData = $subSlider->images->reverse(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="slider-img" style="background-image: url('<?php echo e($event->image_url); ?>')"></div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
                <a id="prev" href="#">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                </a>
                <a id="next" href="#">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                </a>
            </div>
        <?php endif; ?>
    </div>
</section>
<?php if($events_study_at): ?>
<section class="section section--padding section--bg">
    <div class="container">
        <div class="section__header">
            <div class="section__title">
                <span><?php echo e(__("our events")); ?></span>
                <h2 class="section-title"><?php echo e(__("study at AFCM")); ?></h2>
            </div>
            <p></p>
            <a class="btn btn--default" href="<?php echo e(route('moreEvents')); ?>"><?php echo e(__("more events")); ?></a>
        </div>

        <div class="grid-container grid-container--gradient">
            <?php $__currentLoopData = $events_study_at; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if($key%3 == 0): ?>
                    <a class="grid-container__item grid-container__item--double" href="<?php echo e(route('event',$event->id)); ?>"
                    style="background-image: url('<?php echo e($event->image->image_url); ?>')">
                    <span><?php echo e($event->title); ?></span>
                    </a>
                <?php else: ?>
                    <a class="grid-container__item" href="<?php echo e(route('event',$event->id)); ?>"
                    style="background-image: url('<?php echo e($event->image->image_url); ?>')">
                        <span><?php echo e($event->title); ?></span>
                    </a>
                <?php endif; ?>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


        </div>
    </div>
</section>
<?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make($themePath.'.students.layouts.mainPage',["title"=>__("front.website_title"),"minor"=>false], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/AFCM/resources/views/AFMC-project/index.blade.php ENDPATH**/ ?>