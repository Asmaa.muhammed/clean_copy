<div class="kt-portlet__head-toolbar">
    <button type="button" onclick="window.history.back();" class="btn btn-clean kt-margin-r-10">
        <i class="la la-arrow-left"></i>
        <span class="kt-hidden-mobile">   <?php echo e(__('translation::translation.back')); ?></span>
    </button >

    <div class="btn-group">
        <?php if(!$disabled): ?>
        <button type="button" class="btn btn-brand" id="submitButton">
            <i class="la la-check"></i>
            <span class="kt-hidden-mobile">   <?php echo e(__('translation::translation.save')); ?></span>
        </button>
            <?php else: ?>
            <button type="button" class="btn btn-brand" onclick="window.location.href='<?php echo e(route($module.".edit",$model)); ?>'">
                <i class="la la-pencil-square-o"></i>
                <span class="kt-hidden-mobile">   <?php echo e(__('translation::translation.edit')); ?></span>
            </button>
        <?php endif; ?>


    </div>
</div>
<?php /**PATH /var/www/html/AFCM/resources/views/admin/layout/includes/form_button.blade.php ENDPATH**/ ?>