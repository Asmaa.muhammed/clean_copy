<?php $__env->startSection('subheader'); ?> <?php echo $__env->make('admin.layout.components.breadcrumb',['pageTitle'=>__('common.moduleTitle', ['module'=>$cmsModule->name]),'moduleName'=>$cmsModule->route,'pageName'=>__('common.showPageName', ['module'=>$cmsModule->name]) ], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile"
                 id="kt_page_portlet">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title"><?php echo e(__('common.show', ['module'=>__('pages.singularModuleName')])); ?></h3>
                    </div>
                    <?php echo $__env->make("admin.layout.includes.form_button",['disabled'=>true,"module"=>$cmsModule->route, 'model'=>$event], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>
                <div class="kt-portlet__body">
                    <?php echo Form::model($event,['class'=>'kt-form kt-form--labe-right','id'=>'kt_form']); ?>

                    <?php echo $__env->make('admin.events.form', ['disabled'=>true], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', ['pageTitle'=>__('common.showPageTitle', ['module'=>$cmsModule->name])], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\clean_copy\resources\views/admin/events/show.blade.php ENDPATH**/ ?>