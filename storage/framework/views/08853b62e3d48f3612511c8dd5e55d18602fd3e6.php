<div class="row">
    <div class="col-md-7">
        <div class="form-group row">
            <div class="col-lg-6">
                <label><?php echo e(__('menuGroups.name')); ?></label>
                <?php echo Form::text('name', null, ['class'=>'form-control', 'disabled'=>$disabled]); ?>

                <?php if($errors->has("name")): ?>
                    <span class="form-text text-danger"><?php echo e($errors->first('name')); ?></span>
                <?php endif; ?>
            </div>
            <div class="col-lg-6">
                <label class=""><?php echo e(__('menuGroups.short_code')); ?></label>
                <?php echo Form::text('short_code', null, ['class'=>'form-control',  'disabled'=>$disabled]); ?>

                <?php if($errors->has("short_code")): ?>
                    <span class="form-text text-danger"><?php echo e($errors->first('short_code')); ?></span>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-lg-4">
        <?php echo $__env->make('admin.layout.form_components.status', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
</div>
<?php /**PATH /var/www/html/AFCM/resources/views/admin/menuGroups/form.blade.php ENDPATH**/ ?>