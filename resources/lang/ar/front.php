<?php

return array (
  'home' => 'الصفحة الرئيسية',
  'sitename' => 'القوات المسلحة ',
  'sitename2' => 'كلية الطب',
  'about_us' => 'من نحن',
  'contactUs'  => 'تواصل معنا',
  'contact_us' => 'تواصل معنا',
  'contactInformation' => 'معلومات الاتصال',
  'faq' =>
  array (
    'empty_page' => 'لم يتم العثور على الأسئلة الشائعة',
  ),
  'faqs' =>
  array (
    'pageHeader' => 'الأسئلة الشائعة',
  ),
  'login' =>
  array (
    'PageTitle' => 'تسجيل الدخول إلى الحساب',
    'contact_message' => 'قم بتسجيل الدخول أو إنضم لبدأ المبادلة',
    'dont_have_account' => 'ليس لديك حساب على سواب لوكو؟',
    'email' => 'البريد الإلكتروني',
    'forgot_password' => 'هل نسيت كلمة المرور؟',
    'login_btn' => 'تسجيل الدخول',
    'login_slogan' => 'استخدم بيانات اعتمادك للوصول إلى حسابك',
    'password' => 'كلمه السر',
    'phone' => 'التليفون',
    'register' => 'تسجيل',
    'register_now' => 'إضغط هنا لإنشاء واحد',
    'remember_me' => 'تذكرنى',
    'social_login_header' => 'او',
    'title' => 'سجّل الدخول أو إنضم',
  ),
  'logout' => 'تسجيل خروج',
  'search' => 'بحث',
  'send' => 'إرسال',
  'state' => 'مدن',
  'subscribe_btn' => 'الإشتراك',
  'subscribe_link' => 'الإشتراك',
  'subscription_successfully' => 'بريدك الإلكتروني الآن في النشرة الإخبارية ',
  'success' => '',
  'terms' => 'شروط الاستخدام',
  'thank_you' => 'شكرا',
  'view_all_categories' => 'عرض جميع الاقسام',
  'website_title' => 'مرحبا بكم في ',
  'works' => 'كيف تعمل',
  'eventDetails' => 'تفاصيل الحدث',
  'postDetails' => 'تفاصيل المنشور',
  'newsDetails' => 'تفاصيل الخبر',
  'pageDetails' => 'تفاصيل الصفحة',
  'post' => 'المنشور',
  'event' => 'الحدث',
  'events' => 'الفاعليات',
  'news' => 'الاخبار',
  
);
