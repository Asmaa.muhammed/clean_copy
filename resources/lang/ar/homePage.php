<?php

return array (
  'best_seller' => 'لأكثر مبيعا',
  'details' => 'تفاصيل',
  'how_to_enjoy_offers' => 'كيفية الاستمتاع بالعروض',
  'latest_deals' => 'أحدث العروض',
  'off' => 'خصم',
  'packages_starting_from' => 'تبدأ الخطة من',
  'play_a_demo' => 'تشغيل عرض فيديو',
  'today_featured_coupons' => 'عروض اليوم من كوبوون شوب',
  'view_deal' => 'عرض العرض',
);
