<?php

return array (
  'agree' => 'الرجاء تحديد موافقة لمتابعة العملية الخاصة بك',
  'agree_term' => 'الرجاء تحديد موافقة لمتابعة العملية الخاصة بك',
  'business_name_format' => ' يجب ألا يقل الاسم الأول عن 3 أحرف',
  'business_name_required' => 'من فضلك أدخل اسمك الأول',
  'business_phone_exist' => 'بريدك الإلكتروني موجود بالفعل',
  'business_phone_required' => 'يرجى إدخال هاتفك',
  'business_phone_wrong' => 'رقم هاتفك موجود بالفعل  ',
  'contact_first_name_format' => ' يجب ألا يقل الاسم الأول عن 3 أحرف',
  'contact_first_name_required' => 'من فضلك أدخل اسمك الأول',
  'contact_last_name_format' => ' يجب ألا يقل اسم العائلة عن 3 أحرف',
  'contact_last_name_required' => 'من فضلك أدخل  اسم عائلتك',
  'contact_phone_exist' => 'بريدك الإلكتروني موجود بالفعل',
  'contact_phone_required' => 'يرجى إدخال هاتفك',
  'contact_phone_wrong' => 'رقم هاتفك موجود بالفعل  ',
  'email_exist' => 'بريدك الإلكتروني موجود بالفعل',
  'email_required' => 'يرجى إدخال بريدك الإلكتروني',
  'email_wrong' => 'تنسيق البريد الإلكتروني خاطئ  ',
  'first_name_format' => ' يجب ألا يقل الاسم الأول عن 3 أحرف',
  'first_name_required' => 'من فضلك أدخل اسمك الأول',
  'last_name_format' => ' يجب ألا يقل اسم العائلة عن 3 أحرف',
  'last_name_required' => 'من فضلك أدخل  اسم عائلتك',
  'login' =>
  array (
    'not_verified' => 'لم يتم التحقق من حسابك يرجى التحقق من بريدك الإلكتروني للتفعيل',
      'verified'=>'تم التحقق من بريدك الإلكتروني شكرا لك'
  ),
  'name_format' => '',
  'name_required' => '',
  'password_required' => 'يرجى إدخال كلمة المرور الخاصة بك',
  'password_wrong' => 'لا يتطابق مع تأكيد كلمة المرور',
  'phone_exist' => 'بريدك الإلكتروني موجود بالفعل',
  'phone_required' => 'يرجى إدخال هاتفك',
  'phone_wrong' => 'رقم هاتفك موجود بالفعل  ',
  'profile' =>
  array (
    'end_subscription_confirmation_message' => 'هل أنت متأكد أنك تريد إنهاء اشتراكك',
  ),
  'subscribe' =>
  array (
    'login_before_subscribe' => '',
  ),
);
