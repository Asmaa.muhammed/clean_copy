<?php

return array(
    'name' => 'Name',
    'description' => 'Description',
    'singularModuleName' => 'Plan',
    'price' => 'Price',
    'currency' => 'Currency',
    'plan_interval' => 'Plan Interval',
    'plan_period' => 'Plan Period',
    'discount_type' => 'Discount Type',
    'discount_value' => 'Discount Value',
    'renewable' => 'Renewable',
    'sorting' => 'Sorting',
    'period' => 'Period',
    'free' => 'Free'
);
