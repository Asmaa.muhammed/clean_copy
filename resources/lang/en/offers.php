<?php

return array(
    'name' => 'Name',
    'price' => 'Price',
    'discount_percent' => 'Discount Percentage',
    'merchant_id' => 'Merchant',
    'description' => 'Description',
    'content' => 'Content',
    'singularModuleName' => 'Offer',
    'redeems' => 'Max No. of Redeems',
    'merchant' => 'Merchant',
    'category' => 'Category',
    'gallery' => 'Gallery',
    'start_publishing' => 'From',
    'end_publishing' => 'To',
    'tags' => 'Tags',
    'redeemed' => 'Offers Redeemed Successfully',
    'max_redeems' => 'Offer Succeeded Maximum Redeems Number',
);
