<?php

return array(
    'additional_info'=> 'Additional Info',
    'singularModuleName'=> 'Customer',
    'city_id'=> 'Residence City',
    'address'=> 'Address',
    'not_subscribed' => 'User Cannot Redeem, Unsubscribed',
);
