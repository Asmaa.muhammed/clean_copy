<?php


return array(
    'name' => 'Display Name',
    'description' => 'Description',
    'route' => 'Route',
    'sorting'=>'Sorting',
    'icon' => 'Module Icon',
    'singularModuleName' => 'Module',
    'group'=>'Group',
    'select_group'=>'Select Group'
);
