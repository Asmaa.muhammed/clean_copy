<?php

return array(
    'contact_full_name' => 'Full Name',
    'email' => 'Email',
    'contact_phone' => 'Contact Phone',
    'business_name' => 'Business Name',
    'redirect_to_merchants' => 'Redirect To Merchants',
    'singularModuleName' => 'Merchant Request'
);
