<?php

return array (
  'body' => '',
  'image_and_galleries' => '',
  'model' => '',
  'publish_options' => '',
  'singularModuleName' => '',
  'title' => 'Title',
  'name' => 'Name',
  'description' => 'Description',
  'subject' => 'Subject',
  'place' => 'Place',
  'location' => 'location',
  'day' => 'day',
  'date' => 'date',
  'event' => 'Event title',
  'persons'=> 'Person name',
  'time_from' => 'Time from',
  'time_to' => 'Time to',
);
