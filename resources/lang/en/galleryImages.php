<?php

return array(
    'title' => 'Title',
    'short_code' => "Short Code Alias",
    'singularModuleName' => 'Gallery Image',
    'image' =>'Select Image',
    'sorting'=>'Sorting',
    'captions_and_alts' =>"Captions And Alts",
    'caption'=>'Caption',
    'image_alt'=>'Image Alt',
    'image_preview' =>"Image Preview",
    'image_name'=>"Image Name"

);
