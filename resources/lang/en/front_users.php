<?php

return array(
    'fullName' => 'Full Name',
    'email' => 'Email',
    'phone' => 'Phone',
    'last_login' => 'Last Login',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'password' => 'Password',
    'password_confirmation' => 'Repeat Password',
    'profile_picture' => "Profile Picture",
    'website_url' => 'Website'
);
