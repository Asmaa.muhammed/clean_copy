<?php

return array (
    "page"  =>  "{page}",
    "advertisement"=>"advertisement/{advertisement}",
    "category"=>"category/{category}",
    "post"=>"post/{post}",
    "event"=>"event/{event}",
    "news"=>"news/{post}",
    "store"=>"store/{merchant}",
    "customer_login"=>"customer/login",
    "customer_register"=>"customer/register",
    "profile"=>"customer/profile",
    "categories"=>"categories",
    'faqs'=>'faqs',
    "explore"=>"explore",
    "search"=>"result",
    "terms_and_conditions"=>"terms-and-conditions",
    "thank_you"=>"thank-you",
    "place_advertisement"=>"customer/advertisement/place",
    'my_ads'=>"customer/advertisement",
    "favourite"=>'customer/favourite',
    "privacy_policy"=>"privacy-policy",




);
