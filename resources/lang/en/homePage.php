<?php
return [
    "off"=>"OFF",
    "details"=>"Details",
    "best_seller"=>"Best Seller",
    "latest_deals"=>"Latest Deals",
    "packages_starting_from"=>"Packages starting from",
    "today_featured_coupons"=>"Today Featured Coupons",
    "view_deal"=>"View Deal",
    "how_to_enjoy_offers"=>"How to enjoy offers",
    "play_a_demo"=>"Play a Demo"


];
