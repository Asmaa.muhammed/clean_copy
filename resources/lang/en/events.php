<?php

return array (
  'body' => 'Body',
  'categories' => 'Categories',
  'end_date' => 'End Date',
  'end_publishing' => 'End Publishing',
  'event_details' => '',
  'gallery' => 'Gallery',
  'image_and_galleries' => 'Images And Galleries',
  'model' => 'Layout Model',
  'organizer' => 'Organizer',
  'place' => 'Place',
  'publish_options' => 'Publish Options',
  'short_code' => 'Short Code Alias',
  'singularModuleName' => 'Event',
  'start_date' => 'Start Date',
  'start_publishing' => 'Start Publishing',
  'summary' => 'Summary',
  'tags' => 'Tags',
  'taxonomies' => 'Taxonomies',
  'title' => 'Title',
);
