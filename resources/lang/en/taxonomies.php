<?php

return array (
  'add_new_author' => 'Add New Author ',
  'add_new_category' => 'Add New category ',
  'add_new_tag' => 'Add New Tag ',
  'description' => 'Description',
  'edit_author' => 'Edit Author',
  'edit_category' => 'Edit Category',
  'edit_tag' => 'Edit Tag',
  'full_info' => 'Show Full Info',
  'icon' => '',
  'main_information' => 'Main Information',
  'name' => 'Name',
  'parent' => 'Parent',
  'root' => 'Root',
  'show_all_authors' => 'Show All Authors',
  'show_all_categories' => 'Show All Categories',
  'show_all_tags' => 'Show All Tags',
  'show_tree' => 'Show Categories Tree',
  'sorting' => 'Sorting',
);
