<?php

return array(
    'plan' => 'Plan',
    'user' => 'User',
    'starts_at' => 'Starts At',
    'ends_at' => 'Ends At',
    'cancelled_at' => 'Cancelled At',
    'gateway_payment_id' => 'Payment Gateway ID',
    'paid_amount' => 'Payment Amount',
    'renew_after_expiry' => 'Renewable',
    'singularModuleName' => 'Subscription'
);
