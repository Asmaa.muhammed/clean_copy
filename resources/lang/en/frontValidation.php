<?php

return array (
  'agree' => 'please select agree to continue your process',
  'agree_term' => 'please select agree to continue your process',
  'business_name_format' => ' The  business name must be at least 3 characters',
  'business_name_required' => 'Please enter your business name',
  'business_phone_exist' => 'Your phone already exist',
  'business_phone_required' => 'Please Insert Your phone',
  'business_phone_wrong' => 'phone is wrong  ',
  'contact_first_name_format' => ' The first name must be at least 3 characters',
  'contact_first_name_required' => 'Please enter your last name',
  'contact_last_name_format' => ' The first name must be at least 3 characters',
  'contact_last_name_required' => 'Please enter your last name',
  'contact_phone_exist' => 'Your phone already exist',
  'contact_phone_required' => 'Please Insert Your phone',
  'contact_phone_wrong' => 'phone is wrong  ',
  'email_exist' => 'Your email already exist',
  'email_required' => 'Please Insert Your email',
  'email_wrong' => 'Email Formatting wrong  ',
  'first_name_format' => ' The first name must be at least 3 characters',
  'first_name_required' => 'Please enter your first name',
  'last_name_format' => ' The last name must be at least 3 characters',
  'last_name_required' => 'Please enter your last name',
  'login' => 
  array (
    'click_here' => 'Click Here',
    'not_verified' => 'Oops! Your account has not been activated yet. Please follow the activation link sent to you by e-mail.',
    'verified' => 'Yay! Your account is now active.
Thank you for signing up.',
  ),
  'name_format' => 'The user name must be at least 3 characters',
  'name_required' => 'Please enter your user name',
  'password_required' => 'Please Insert Your Password',
  'password_wrong' => 'The password confirmation does not match!',
  'phone_exist' => 'Your phone already exist',
  'phone_required' => 'Please Insert Your phone',
  'phone_wrong' => 'phone is wrong  ',
  'profile' => 
  array (
    'end_subscription_confirmation_message' => 'Are You Sure You Want end your subscription',
  ),
  'subscribe' => 
  array (
    'login_before_subscribe' => 'Log In or Sign Up To Start Swapping!',
  ),
);
