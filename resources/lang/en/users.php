<?php

return array(
    'first_name' => 'First Name',
    'last_name' => "Last Name",
    'email' => 'Email',
    'password' => 'Password',
    'phone' => 'Phone',
    'repeat_password' => "Repeat Password",
    'roles' => 'Roles',
    'singularModuleName' => "User",
    'changePassword' => 'Change :User Password',
    'changePasswordPageTitle' => ':User | Change Password',
    'changePasswordPageName' => "Change :User Password",
);
