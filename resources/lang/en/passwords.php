<?php

return array (
  'reset' => 'Your password has been reset!',
  'sent' => 'We have e-mailed your password reset link!',
  'throttled' => 'Please wait before retrying.',
  'token' => 'This password reset token is invalid.',
  'user' => 'Sorry! We couldn\'t find a user with that email address.',
);
