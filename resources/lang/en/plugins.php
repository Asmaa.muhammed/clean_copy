<?php

return array(
    'name' => 'Plugin Name',
    'short_code' => 'Short Code',
    'type' => 'Type',
    'position' => 'Position',
    'generateShortcode' => 'Generate Shortcode',
    'generate' => 'Generate'
);
