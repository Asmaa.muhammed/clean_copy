<?php

return array (
  'body' => '',
  'image_and_galleries' => '',
  'model' => '',
  'publish_options' => '',
  'singularModuleName' => '',
  'title' => 'Title',
  'name' => 'Name',
  'position' => 'Positoin',
  'about' => 'About',
  'description' => 'Description',
  'portfolio_link' => 'Portfolio link',
  'subject' => 'Subject',
  'place' => 'Place',
  'location' => 'location',
  'day' => 'day',
  'time_from' => 'Time from',
  'time_to' => 'Time to',
);
