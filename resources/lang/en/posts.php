<?php

return array(
    'title' => 'Title',
    'short_code' => "Short Code Alias",
    'singularModuleName' => 'Post',
    'publish_options'=>'Publish Options' ,
    'model'=>'Layout Model' ,
    'start_publishing'=>'Start Publishing' ,
    'end_publishing'=>'End Publishing' ,
    'summary'=>'Summary' ,
    'body'=>'Body' ,
    'image_and_galleries'=>'Images And Galleries',
    'taxonomies'=>'Taxonomies',
    'categories'=>'Categories',
    'tags'=>'Tags',
    'gallery'=>'Gallery',
    'author'=>'Author'


);
