<?php

return array(
    'display_name' => 'Display Name',
    'description' => 'Description',
    'name' => 'Role Name',
    'permissions' => 'Permissions',
    'singularModuleName' => 'Role',
);
