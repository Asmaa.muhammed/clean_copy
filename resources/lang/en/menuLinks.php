<?php
return array(
    'name' => 'Display Name',
    'description' => 'Description',
    'route' => 'Route',
    'sorting'=>'Sorting',
    'icon' => 'Menu Link Icon',
    'singularModuleName' => 'Menu Link',
    'target' => 'Target',
    'link' => 'Link',
    'parent' => 'Parent',
);
