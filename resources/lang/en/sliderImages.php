<?php

return array(
    'title' => 'Title',
    'short_code' => "Short Code Alias",
    'singularModuleName' => 'Slider Image',
    'image' =>'Select Image',
    'sorting'=>'Sorting',
    'captions_and_alts' =>"Captions And Alts",
    'caption'=>'Caption',
    'image_alt'=>'Image Alt',
    'image_preview' =>"Image Preview",
    'image_name'=>"Image Name",
    'target' => 'Target',
    'link' => 'Link',
    "targets_and_links"=>'Targets And Links'

);
