<?php

return array(
    'frontInfo' => 'Display Info',
    'singularModuleName' => 'Merchant',
    'front_email' => 'Display Email',
    'front_phone' => 'Display Phone',
    'thumbnail' => 'Merchant Thumbnail',
    'location' => 'Merchant Location',
    'category_id' => 'Merchant Category',
    'city_id' => 'City',
    'front_name' => 'Display Name',
    'front_description' => 'Display Description',
    'social_type' => 'Social Media Platform',
    'social_type_link' => 'Social Media Link',
    'front_mail' => 'Display Email'
);
