<div class="row">
    <div class="col-md-7">
        <div class="kt-section">
            <div class="kt-section__body">
                @component("admin.components.translatable_section")
                    @foreach($languages as $language)
                        @slot("translatable_{$language->language}")
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>{{__('persons.subject')}}</label>
                                    {!! Form::text("subject:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                                    @if ($errors->has("subject:{$language->language}"))
                                        <span
                                            class="form-text text-danger">{{$errors->first("subject:{$language->language}")}}</span>
                                    @endif
                                </div>
                                <div class="col-lg-6">
                                    <label class="">{{__('persons.description')}}</label>
                                    {!! Form::textarea("description:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                                    @if ($errors->has("description:{$language->language}"))
                                        <span
                                            class="form-text text-danger">{{$errors->first("description:{$language->language}")}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>{{__('persons.place')}}</label>
                                    {!! Form::text("place:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                                    @if ($errors->has("place:{$language->language}"))
                                        <span
                                            class="form-text text-danger">{{$errors->first("place:{$language->language}")}}</span>
                                    @endif
                                </div>
                                <div class="col-lg-6">
                                    <label>{{__('persons.location')}}</label>
                                    {!! Form::text("location:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                                    @if ($errors->has("location:{$language->language}"))
                                        <span
                                            class="form-text text-danger">{{$errors->first("location:{$language->language}")}}</span>
                                    @endif
                                </div>
                            </div>
                            
                            @endslot
                    @endforeach
                @endcomponent
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>{{__('sessions.day')}}</label>
                                    {!! Form::number("day", null, ['class'=>'form-control',  'min' => '1',  'max' => '8' ,'disabled'=>$disabled]) !!}
                                    @if ($errors->has("day"))
                                        <span
                                            class="form-text text-danger">{{$errors->first("day")}}</span>
                                    @endif
                                </div>
                                <div class="col-lg-6">
                                    <label>{{__('sessions.date')}}</label>
                                    {!! Form::date('date', null, ['class'=>'form-control', 'disabled'=>$disabled], 'required') !!}
                                    @if ($errors->has("date"))
                                        <span class="form-text text-danger">{{$errors->first('date')}}</span>
                                    @endif
                                </div>
                               
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>{{__('sessions.time_from')}}</label>
                                    {!! Form::time('time_from', null, ['class'=>'form-control', 'disabled'=>$disabled], 'required') !!}
                                    @if ($errors->has("time_from"))
                                        <span class="form-text text-danger">{{$errors->first('time_from')}}</span>
                                    @endif
                                </div>
                                <div class="col-lg-6">
                                    <label>{{__('sessions.time_to')}}</label>
                                    {!! Form::time('time_to', null, ['class'=>'form-control', 'disabled'=>$disabled], 'required') !!}
                                    @if ($errors->has("time_to"))
                                        <span class="form-text text-danger">{{$errors->first('time_to')}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>{{__('sessions.event')}}</label>
                                    {!! Form::select('node_id', $events, null, ['class'=>'form-control', 'id'=>'node_id', 'disabled'=>$disabled]) !!}
                                    @if ($errors->has("node_id"))
                                        <span class="form-text text-danger">{{$errors->first('node_id')}}</span>
                                    @endif
                                </div>
                                <div class="col-lg-6">
                                    <label>{{__('sessions.persons')}}</label>
                                    {!! Form::select('persons[]', $persons, null, ['class'=>'form-control multi', 'id'=>'persons','multiple', 'disabled'=>$disabled]) !!}
                                    @if ($errors->has("persons"))
                                        <span class="form-text text-danger">{{$errors->first('persons')}}</span>
                                    @endif
                                </div>
                               
                            </div>
                            
            </div>
        </div>
    
    </div>
    <div class="col-md-1"></div>
    <div class="col-lg-4">
        <div class="kt-section">
            <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>
            <div class="kt-section__body">
                @include('admin.layout.form_components.status')
                
            </div>
        </div>
    </div>
</div>



