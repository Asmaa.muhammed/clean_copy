<div class="row">
    <div class="col-md-7">
        <div class="form-group row">
            <div class="col-lg-6">
                <label>{{__('front_users.first_name')}}</label>
                {!! Form::text('first_name', null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                @if ($errors->has("first_name"))
                    <span class="form-text text-danger">{{$errors->first('first_name')}}</span>
                @endif
            </div>
            <div class="col-lg-6">
                <label>{{__('front_users.last_name')}}</label>
                {!! Form::text('last_name', null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                @if ($errors->has("last_name"))
                    <span class="form-text text-danger">{{$errors->first('last_name')}}</span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-6">
                <label>{{__('front_users.email')}}</label>
                {!! Form::email('email', null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                @if ($errors->has("email"))
                    <span class="form-text text-danger">{{$errors->first('email')}}</span>
                @endif
            </div>
            <div class="col-lg-6">
                <label>{{__('front_users.phone')}}</label>
                {!! Form::text('phone', null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                @if ($errors->has("phone"))
                    <span class="form-text text-danger">{{$errors->first('phone')}}</span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-6">
                <label>{{__('front_users.password')}}</label>
                {!! Form::password('password', ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                @if ($errors->has("password"))
                    <span class="form-text text-danger">{{$errors->first('password')}}</span>
                @endif
            </div>
            <div class="col-lg-6">
                <label>{{__('front_users.password_confirmation')}}</label>
                {!! Form::password('password_confirmation', ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                @if ($errors->has("password_confirmation"))
                    <span class="form-text text-danger">{{$errors->first('password_confirmation')}}</span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-8">
                <label>    {{ __('front_users.profile_picture') }}:</label>
                <div class="input-group">
                             <span class="input-group-prepend">
                                  <a data-input="profile_picture" data-preview="profile_image_preview"
                                     class="btn btn-primary lfm ">  <i
                                          class="la la-image  text-white "></i>
                                       </a>
                             </span>
                    {!! Form::text('profile_picture', (!empty($user) ? $user->thumbnail : null), ['class'=>'form-control','id'=>'profile_picture','disabled'=>$disabled,'readonly']) !!}
                    @if ($errors->has("profile_picture"))
                        <span class="form-text text-danger">{{$errors->first('profile_picture')}}</span>
                    @endif
                </div>
                <div class="col-lg-4">
                    <div id="profile_image_preview" style="margin-top:15px;max-height:150px;">
                        @if(!empty($user))
                            <img src="{{$user->profile_picture}}" width="150px">
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="col-md-1"></div>
    <div class="col-md-4">
        @include('admin.layout.form_components.status', ['disabled' => $disabled])
    </div>
</div>
@section('additionalScripts')
    <!--begin::Page Scripts(used by this page) -->
    @include('admin.layout.includes.filemanger_scripts')
    <!--end::Page Scripts -->
@endsection
