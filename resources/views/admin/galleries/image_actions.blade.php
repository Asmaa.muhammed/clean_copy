<a href="{{route('galleryImages.index',$gallery)}}" class="btn btn-success btn-icon" >
    <i class="la la-eye text-white"></i>
</a>
<a href="{{route('galleryImages.create',$gallery)}}" class="btn btn-success btn-icon" >
    <i class="la la-plus-circle"></i>
</a>
