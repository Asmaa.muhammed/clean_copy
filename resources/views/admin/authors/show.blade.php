@extends('admin.layout.main', ['pageTitle'=>'author | Full Info'])
@section('subheader') @include('admin.layout.components.breadcrumb',['pageTitle'=>'authors','moduleName'=>'authors','pageName'=> __('taxonomies.full_info') ]) @endsection
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile"
                 id="kt_page_portlet">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">  {{ __('taxonomies.full_info') }}</h3>
                    </div>
                    @include("admin.layout.includes.form_button",['disabled'=>true,"module"=>"authors","model"=>$author])
                </div>
                <div class="kt-portlet__body">
                {!! Form::model($author,['route'=>['authors.update', $author->id], 'method'=>'patch']) !!}
                  @include('admin.authors.form', ['disabled'=>true,'class'=>'kt-form kt-form--labe-right','id'=>'kt_form'])
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
