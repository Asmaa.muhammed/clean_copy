<div class="dropdown dropdown-inline ">
    <button type="button"
            class="btn btn-hover-warning btn-elevate-hover btn-icon btn-sm btn-icon-md btn-circle"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="flaticon-more-1"></i>
    </button>
    <div class="dropdown-menu dropdown-menu-right {{$module}}" x-placement="top-end"
         style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-149px, -189px, 0px);">
        @permission("update-{$module}")
        <a class="dropdown-item"
           href="{{ route($module."."."edit", $model) }}"><i
                class="la la-pencil-square-o"></i> {{ __('translation::translation.edit')}}
        </a>
        @endpermission
        @permission("read-{$module}")
        <a class="dropdown-item" href="{{ route($module."."."show", $model) }}">
            <i class="la la-info"></i> {{ __('translation::translation.full_info')}}
        </a>
        @endpermission
        @role("superadministrator|administrator")
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="{{ route($module."."."changePassword", $model) }}">
            <i class="la la-lock"></i> {{ __('users.changePassword', ['user' => __('users.singularModuleName')])}}
        </a>
        @endrole
        @permission("delete-{$module}")
        <button type="button" class="delete_btn dropdown-item btn-clean "
                id="delete-{{$model[Str::singular($module)]->id}}">
            <i class="la la-trash-o"></i> {{ __('translation::translation.delete')}}
        </button>
        @endpermission
    </div>
</div>
