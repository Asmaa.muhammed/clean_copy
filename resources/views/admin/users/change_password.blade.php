@extends('admin.layout.main', ['pageTitle'=>__('users.changePasswordPageTitle', ['user'=>$cmsModule->name])])
@section('subheader') @include('admin.layout.components.breadcrumb',['pageTitle'=>__('common.moduleTitle', ['module'=>$cmsModule->name]),'moduleName'=>$cmsModule->route,'pageName'=>__('users.changePasswordPageName', ['user'=>$cmsModule->name]) ]) @endsection
@section('content')
    {{--@dd($errors)--}}
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile"
                 id="kt_page_portlet">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">{{__('users.changePassword', ['user'=>$user->fullName])}}</h3>
                    </div>
                    @include("admin.layout.includes.form_button",['disabled'=>false,"module"=>$cmsModule])
                </div>
                <div class="kt-portlet__body">
                    <div class="row">
                        <div class="col-md-7">
                            {!! Form::model($user,['route'=>['users.submitChangePassword', $user->id], 'method'=>'post', 'class'=>'kt-form kt-form--labe-right','id'=>'kt_form']) !!}
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>{{__('users.password')}}</label>
                                    <div class="kt-input-icon">
                                        {!! Form::password('password', ['class'=>'form-control']) !!}
                                        <span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                                                    class="la la-lock"></i></span></span>
                                    </div>
                                    @if ($errors->has("password"))
                                        <span class="form-text text-danger">{{$errors->first('password')}}</span>
                                    @endif
                                </div>
                                <div class="col-lg-6">
                                    <label class="">{{__('users.repeat_password')}}</label>
                                    <div class="kt-input-icon">
                                        {!! Form::password('password_confirmation', ['class'=>'form-control']) !!}
                                        <span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                                                    class="la la-lock"></i></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
