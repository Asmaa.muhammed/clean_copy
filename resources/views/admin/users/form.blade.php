<div class="row">
    <div class="col-md-7">
        <div class="form-group row">
            <div class="col-lg-6">
                <label>{{__('users.first_name')}}</label>
                {!! Form::text('first_name', null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                @if ($errors->has("first_name"))
                    <span class="form-text text-danger">{{$errors->first('first_name')}}</span>
                @endif
            </div>
            <div class="col-lg-6">
                <label class="">{{__('users.last_name')}}</label>
                {!! Form::text('last_name', null, ['class'=>'form-control',  'disabled'=>$disabled]) !!}
                @if ($errors->has("last_name"))
                    <span class="form-text text-danger">{{$errors->first('last_name')}}</span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-6">
                <label>{{__('users.email')}}</label>
                <div class="kt-input-icon">
                    {!! Form::email('email', null, ['class'=>'form-control',  'disabled'=>$disabled]) !!}
                    <span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                                class="la la-envelope"></i></span></span>
                </div>
                @if ($errors->has("email"))
                    <span class="form-text text-danger">{{$errors->first('email')}}</span>
                @endif
            </div>
            <div class="col-lg-6">
                <label class="">{{__('users.phone')}}</label>
                <div class="kt-input-icon">
                    {!! Form::text('phone', null, ['class'=>'form-control',  'disabled'=>$disabled]) !!}
                    <span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                                class="la la-phone"></i></span></span>
                </div>
                @if ($errors->has("phone"))
                    <span class="form-text text-danger">{{$errors->first('phone')}}</span>
                @endif
            </div>
        </div>
        @if(request()->routeIs('users.create'))
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>{{__('users.password')}}</label>
                    <div class="kt-input-icon">
                        {!! Form::password('password', ['class'=>'form-control',  'disabled'=>$disabled]) !!}
                        <span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                                    class="la la-lock"></i></span></span>
                    </div>
                    @if ($errors->has("password"))
                        <span class="form-text text-danger">{{$errors->first('password')}}</span>
                    @endif
                </div>
                <div class="col-lg-6">
                    <label class="">{{__('users.repeat_password')}}</label>
                    <div class="kt-input-icon">
                        {!! Form::password('password_confirmation', ['class'=>'form-control',  'disabled'=>$disabled]) !!}
                        <span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                                    class="la la-lock"></i></span></span>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-group row">
            <div class="col-lg-6">
                <label>{{__('users.roles')}}</label>
                {!! Form::select('roles[]', $roles,null, ['class'=>'form-control', 'id'=>'roles','multiple', 'disabled'=>$disabled]) !!}
                @if ($errors->has("roles"))
                    <span class="form-text text-danger">{{$errors->first('roles')}}</span>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-lg-4">
        @include('admin.layout.form_components.status')
    </div>
</div>
@section("additionalScripts")
    <script>
        $(function () {
            $("#roles").select2();
        });
    </script>
@endsection
