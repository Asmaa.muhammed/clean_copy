<div class="row">
    <div class="col-md-7">

        <div class="kt-section">
            <div class="kt-section__body">
                <div class="form-group row">
                    <div class="col-lg-8">
                        <label>    {{ __('galleryImages.image') }}:</label>
                        <div class="input-group">
                             <span class="input-group-prepend">
                                  <a data-input="image" data-preview="image_preview" class="btn btn-primary lfm ">  <i
                                          class="la la-image  text-white "></i>
                                       </a>
                             </span>
                            {!! Form::text('image', (!empty($galleryImage) ? $galleryImage->image_url : null), ['class'=>'form-control','id'=>'image','disabled'=>$disabled,'readonly']) !!}

                        </div>
                    </div>
                    <div class="col-lg-4">
                        <label>{{ __('galleryImages.sorting') }}:</label>
                        {!! Form::number('sorting', null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                        @if ($errors->has("sorting"))
                            <span class="form-text text-danger">{{$errors->first('sorting')}}</span>
                        @endif
                    </div>
                </div>

            </div>
        </div>
        <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>
        <div class="kt-section">
            <h4 class="kt-section__title kt-section__title-lg">{{ __('galleryImages.captions_and_alts') }}:</h4>
            <div class="kt-section__body">
                @component("admin.components.translatable_section")
                    @foreach($languages as $language)
                        @slot("translatable_{$language->language}")
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>{{__('galleryImages.caption')}}</label>
                                    {!! Form::text("caption:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                                    @if ($errors->has("caption:{$language->language}"))
                                        <span
                                            class="form-text text-danger">{{$errors->first("caption:{$language->language}")}}</span>
                                    @endif
                                </div>
                                <div class="col-lg-6">
                                    <label class="">{{__('galleryImages.image_alt')}}</label>
                                    {!! Form::text("image_alt:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                                    @if ($errors->has("image_alt:{$language->language}"))
                                        <span
                                            class="form-text text-danger">{{$errors->first("image_alt:{$language->language}")}}</span>
                                    @endif
                                </div>
                            </div>
                        @endslot
                    @endforeach
                @endcomponent
            </div>
        </div>

    </div>
    <div class="col-md-1"></div>
    <div class="col-lg-4">
        <div id="image_preview" style="margin-top:15px;max-height:150px;">
            @if(!empty($galleryImage))
                <img src="{{$galleryImage->image_url}}" width="150px">
            @endif
        </div>
    </div>
</div>

