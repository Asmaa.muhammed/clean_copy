<div class="row">
    <div class="col-md-7">
        <div class="kt-section">
            <div class="kt-section__body">
                @component("admin.components.translatable_section")
                    @foreach($languages as $language)
                        @slot("translatable_{$language->language}")
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>{{__('persons.title')}}</label>
                                    {!! Form::text("title:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                                    @if ($errors->has("title:{$language->language}"))
                                        <span
                                            class="form-text text-danger">{{$errors->first("title:{$language->language}")}}</span>
                                    @endif
                                </div>
                                <div class="col-lg-6">
                                    <label>{{__('persons.name')}}</label>
                                    {!! Form::text("name:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                                    @if ($errors->has("name:{$language->language}"))
                                        <span
                                            class="form-text text-danger">{{$errors->first("name:{$language->language}")}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>{{__('persons.position')}}</label>
                                    {!! Form::text("position:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                                    @if ($errors->has("position:{$language->language}"))
                                        <span
                                            class="form-text text-danger">{{$errors->first("position:{$language->language}")}}</span>
                                    @endif
                                </div>
                                
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label class="">{{__('persons.about')}}</label>
                                    {!! Form::textarea("about:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                                    @if ($errors->has("about:{$language->language}"))
                                        <span
                                            class="form-text text-danger">{{$errors->first("about:{$language->language}")}}</span>
                                    @endif

                                    
                                </div>
                                <div class="col-lg-6">
                                    <label class="">{{__('persons.description')}}</label>
                                    {!! Form::textarea("description:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                                    @if ($errors->has("description:{$language->language}"))
                                        <span
                                            class="form-text text-danger">{{$errors->first("description:{$language->language}")}}</span>
                                    @endif
                                </div>
                            </div>
                            @endslot
                    @endforeach
                @endcomponent
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>{{__('persons.portfolio_link')}}</label>
                                    {!! Form::text("portfolio_link", null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                                    @if ($errors->has("portfolio_link"))
                                        <span
                                            class="form-text text-danger">{{$errors->first("portfolio_link")}}</span>
                                    @endif
                                </div>
                               
                            </div>
                        
            </div>
        </div>
        <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>
        <div class="kt-section">
            <h4 class="kt-section__title kt-section__title-lg">{{ __('person.picture') }}:</h4>
            <div class="kt-section__body">
                <div class="form-group row">
                 @include('admin.layout.form_components.select_image')
                    
                </div>
            </div>
        </div>       
    </div>
    <div class="col-md-1"></div>
    <div class="col-lg-4">
        <div class="kt-section">
            <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>
            <div class="kt-section__body">
                @include('admin.layout.form_components.status')
                
            </div>
        </div>
    </div>
</div>



