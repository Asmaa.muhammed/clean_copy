<ul class="nav nav-tabs  nav-tabs-line nav-tabs-line-2x nav-tabs-line-danger" role="tablist">
    @forelse($languages as $language)
        <li class="nav-item">
            <a class="nav-link {{($loop->first) ? 'active': ''}}" data-toggle="tab"
               href="#{{$language->language}}" role="tab">{{$language->name}}</a>
        </li>
    @empty
    @endforelse

</ul>
<div class="tab-content">
    @forelse($languages as $language)
        <div class="tab-pane  {{($loop->first) ? 'active': ''}}" id="{{$language->language}}" role="tabpanel">
            {!! ${"translatable_".$language->language} !!}
        </div>
    @empty
    @endforelse

</div>

