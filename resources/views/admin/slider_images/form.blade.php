<div class="row">
    <div class="col-md-7">

        <div class="kt-section">
            <div class="kt-section__body">
                <div class="form-group row">
                    <div class="col-lg-8">
                        <label>    {{ __('galleryImages.image') }}:</label>
                        <div class="input-group">
                             <span class="input-group-prepend">
                                  <a data-input="image" data-preview="image_preview" class="btn btn-primary lfm ">  <i
                                          class="la la-image  text-white "></i>
                                       </a>
                             </span>
                            {!! Form::text('image', (!empty($sliderImage) ? $sliderImage->image_url : null), ['class'=>'form-control','id'=>'image','disabled'=>$disabled,'readonly']) !!}

                        </div>
                    </div>
                    <div class="col-lg-4">
                        <label>{{ __('galleryImages.sorting') }}:</label>
                        {!! Form::number('sorting', null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                        @if ($errors->has("sorting"))
                            <span class="form-text text-danger">{{$errors->first('sorting')}}</span>
                        @endif
                    </div>
                </div>

            </div>
        </div>
        <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>
        <div class="kt-section">
            <h4 class="kt-section__title kt-section__title-lg">{{ __('sliderImages.targets_and_links') }}:</h4>
            <div class="kt-section__body">
                <div class="form-group row">
                    <div class="col-lg-6">
                        <label>{{__('sliderImages.target')}}</label>
                        {!! Form::select('target',$target,null, ['id'=>'target','class'=>'form-control', 'disabled'=>$disabled]) !!}
                        @if ($errors->has("target"))
                            <span class="form-text text-danger">{{$errors->first('target')}}</span>
                        @endif
                    </div>
                    <div class="col-lg-6">
                        <label class="">{{__('sliderImages.link')}}</label>
                        <div id="linkComponent">
                        </div>
                        @if ($errors->has("target_type"))
                            <span class="form-text text-danger">{{$errors->first('target_type')}}</span>
                        @endif
                        @if ($errors->has("target_id"))
                            <span class="form-text text-danger">{{$errors->first('target_id')}}</span>
                        @endif
                        @if ($errors->has("link"))
                            <span class="form-text text-danger">{{$errors->first('link')}}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>


        <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>
        <div class="kt-section">
            <h4 class="kt-section__title kt-section__title-lg">{{ __('sliderImages.captions_and_alts') }}:</h4>
            <div class="kt-section__body">
                @component("admin.components.translatable_section")
                    @foreach($languages as $language)
                        @slot("translatable_{$language->language}")
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>{{__('sliderImages.caption')}}</label>
                                    {!! Form::text("caption:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                                    @if ($errors->has("caption:{$language->language}"))
                                        <span
                                            class="form-text text-danger">{{$errors->first("caption:{$language->language}")}}</span>
                                    @endif
                                </div>
                                <div class="col-lg-6">
                                    <label class="">{{__('sliderImages.image_alt')}}</label>
                                    {!! Form::text("image_alt:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                                    @if ($errors->has("image_alt:{$language->language}"))
                                        <span
                                            class="form-text text-danger">{{$errors->first("image_alt:{$language->language}")}}</span>
                                    @endif
                                </div>
                            </div>
                        @endslot
                    @endforeach
                @endcomponent
            </div>
        </div>

    </div>
    <div class="col-md-1"></div>
    <div class="col-lg-4">
        <div id="image_preview" style="margin-top:15px;max-height:150px;">
            @if(!empty($sliderImage))
                <img src="{{$sliderImage->image_url}}" width="150px">
            @endif
        </div>

    </div>
</div>
@section('additionalScripts')
    <!--begin::Page Scripts(used by this page) -->

    @include('admin.layout.includes.filemanger_scripts')
    <script src="https://cdn.jsdelivr.net/npm/handlebars@latest/dist/handlebars.js"></script>
    <script>

        Handlebars.registerHelper('link', function (value) {
            value = parseInt(value);
            if (value) {
                return `{!! Form::text('link',( isset($sliderImage) && $sliderImage->target == 1) ? ($sliderImage->link->link ?? null) : old('link'), ['class'=>'form-control',"id"=>"external_link", 'disabled'=>$disabled]) !!}`;
            }
            return `{!! Form::select('target_type',$targetTypes, null, ["id"=>"target_type",'placeholder' => 'Choose Target Type','class'=>'form-control mb-2', 'disabled'=>$disabled]) !!}
            {!! Form::select('target_id',[],null, ['id'=>"target_id",'class'=>'form-control', 'disabled'=>true]) !!}`;
        });

        $('#target').change(function () {
            $("#linkComponent").empty().html(Handlebars.compile(`{@{{link ${$(this).val()}}}}`)());
        }).change();

        $(document).on('change', '#target_type', function () {
            $.ajax({
                method: 'post',
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    model: $(this).val(),
                },
                url: `{{route('ajax.get_target_type_data')}}`,
                success: function (data) {
                    $("#target_id").removeAttr('disabled').empty().html(data.data.map(function (element) {
                        return `<option value="${element.id}">${element.name}</option>`;
                    }).join("\n"));
                    @if(old('target_id'))
                    $(document).find('#target_id').val(`{{old('target_id')}}`).change();
                    @endif
                    @isset($sliderImage)
                    $(document).find('#target_id').val(`{{$sliderImage->link->target_id}}`).change().attr('disabled', {{$disabled}});
                    @endisset
                }
            });
        }).change();
        @if(old('target_type'))
        $(document).find("#target_type").change();
        @endif
        @isset($sliderImage)
        $(document).find("#target_type").val('{{$sliderImage->link->target_type}}').change();
        @endisset
    </script>

    <!--end::Page Scripts -->
@endsection

