<div class="row">
    <div class="col-md-7">
        @component("admin.components.translatable_section")
            @foreach($languages as $language)
                @slot("translatable_{$language->language}")
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>{{__('roles.display_name')}}</label>
                                {!! Form::text("display_name:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                                @if ($errors->has("display_name:{$language->language}"))
                                    <span class="form-text text-danger">{{$errors->first("display_name:{$language->language}")}}</span>
                                @endif
                            </div>
                            <div class="col-lg-6">
                                <label class="">{{__('roles.description')}}</label>
                                {!! Form::textarea("description:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                                @if ($errors->has("description:{$language->language}"))
                                    <span class="form-text text-danger">{{$errors->first("description:{$language->language}")}}</span>
                                @endif
                            </div>
                        </div>
                @endslot
            @endforeach
        @endcomponent
        <div class="kt-portlet__head mb-4 row">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    {{__('common.moduleInfo', ['module'=>__('roles.singularModuleName')])}}
                </h3>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-6">
                <label>{{__('roles.name')}}</label>
                {!! Form::text('name', null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                @if ($errors->has("name"))
                    <span class="form-text text-danger">{{$errors->first('name')}}</span>
                @endif
            </div>
            <div class="col-lg-6">
                <label>{{__('roles.permissions')}}</label>
                {!! Form::select('permissions[]', $permissions,null, ['class'=>'form-control', 'id'=>'permissions','multiple', 'disabled'=>$disabled]) !!}
                @if ($errors->has("permissions"))
                    <span class="form-text text-danger">{{$errors->first('permissions')}}</span>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-md-4">
        @include('admin.layout.form_components.status', ['disabled' => $disabled])
    </div>
</div>
@section("additionalScripts")
    <script>
        $(function () {
           $("#permissions").select2();
        });
    </script>
@endsection
