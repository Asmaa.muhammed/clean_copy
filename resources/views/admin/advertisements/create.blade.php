@extends('admin.layout.main', ['pageTitle'=>'advertisements | Create'])
@section('subheader') @include('admin.layout.components.breadcrumb',['pageTitle'=>'advertisements','moduleName'=>'advertisements','pageName'=>"Create" ]) @endsection
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile"
                 id="kt_page_portlet">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title"> Add New Advertisement</h3>
                    </div>
                    @include("admin.layout.includes.form_button",['disabled'=>false,"module"=>"advertisements"])
                </div>
                <div class="kt-portlet__body">
                    {!! Form::open(['route'=>'advertisements.store', 'method'=>'post','class'=>'kt-form kt-form--labe-right','id'=>'kt_form','enctype'=>"multipart/form-data"]) !!}
                    @include('admin.advertisements.form', ['disabled'=>false])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
