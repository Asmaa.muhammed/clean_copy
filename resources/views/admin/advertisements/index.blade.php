@extends('admin.layout.main', ['pageTitle'=>'Advertisements |Index'])
@section('subheader') @include('admin.layout.components.breadcrumb',['pageTitle'=>'advertisements','moduleName'=>'advertisements','pageName'=>"Show All" ]) @endsection
@section('content')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label"> <i class="flaticon-list"></i> &nbsp;<h3 class="kt-portlet__head-title">  Show All
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        &nbsp;
                        <a href="{{route('advertisements.create')}}" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            {{ __('translation::translation.add') }}
                        </a>

                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
        @include('admin.layout.components.flash_messages')

            <!--begin: Datatable -->

        {!! $html->table(['class' => ' table table-striped- table-bordered table-hover table-checkable dataTable no-footer categories'], true) !!}
        <!--end: Datatable -->
        </div>
    </div>
@endsection
@section('additionalScripts')

    {!! $html->scripts() !!}
    <script src="{{asset('/public/admin')}}/assets/js/modules.js" type="text/javascript"></script>
@endsection
