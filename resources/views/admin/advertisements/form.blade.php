<div class="row">
    <div class="col-lg-7">
        @component("admin.components.translatable_section")
            @foreach($languages as $language)
                @slot("translatable_{$language->language}")
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label>Name</label>
                            {!! Form::text("name:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                            @if ($errors->has("name:{$language->language}"))
                                <span
                                    class="form-text text-danger">{{$errors->first("name:{$language->language}")}}</span>
                            @endif
                        </div>
                        <div class="col-lg-6">
                            <label class="">Description</label>
                            {!! Form::textarea("description:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                            @if ($errors->has("description:{$language->language}"))
                                <span
                                    class="form-text text-danger">{{$errors->first("description:{$language->language}")}}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label>Swap Item Name</label>
                            {!! Form::text("swapItem[name:{$language->language}]", null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                            @if ($errors->has("swapItem.name:{$language->language}"))
                                <span
                                    class="form-text text-danger">{{$errors->first("swapItem.name:{$language->language}")}}</span>
                            @endif
                        </div>
                        <div class="col-lg-6">
                            <label class="">Swap Item Description</label>
                            {!! Form::textarea("swapItem[description:{$language->language}]", null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                            @if ($errors->has("swapItem.description:{$language->language}"))
                                <span
                                    class="form-text text-danger">{{$errors->first("swapItem.description:{$language->language}")}}</span>
                            @endif
                        </div>
                    </div>

                @endslot
            @endforeach
        @endcomponent
        <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>
        <div class="kt-section">
            <div class="kt-section__body">
                <h4 class="kt-section__title kt-section__title-lg">{{ __('taxonomies.main_information') }}:</h4>
                <div class="form-group row">
                    <div class="col-lg-6">
                        <label>categories:</label>
                        {!! Form::select('category_id',$categories, null, ['class'=>'form-control','placeholder'=>"Select", 'disabled'=>$disabled]) !!}
                        @if ($errors->has("category_id"))
                            <span class="form-text text-danger">{{$errors->first('category_id')}}</span>
                        @endif
                    </div>
                    <div class="col-lg-6">
                        <label>customers:</label>
                        {!! Form::select('customer_id',$customers, null, ['class'=>'form-control','placeholder'=>"Select", 'disabled'=>$disabled]) !!}
                        @if ($errors->has("customer_id"))
                            <span class="form-text text-danger">{{$errors->first('customer_id')}}</span>
                        @endif
                    </div>

                </div>
                <div class="form-group row">
                    <div class="col-lg-6">
                        <label>location:</label>
                        {!! Form::select('location',$cities, null, ['class'=>'form-control','placeholder'=>"Select", 'disabled'=>$disabled]) !!}
                        @if ($errors->has("location"))
                            <span class="form-text text-danger">{{$errors->first('location')}}</span>
                        @endif
                    </div>
                    <div class="col-lg-6">
                        <label>Enable Swap:</label>
                        {!! Form::select('enable_swap',['no','yes'], null, ['class'=>'form-control','placeholder'=>"Select", 'disabled'=>$disabled]) !!}
                        @if ($errors->has("enable_swap"))
                            <span class="form-text text-danger">{{$errors->first('enable_swap')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-6">
                        <label>Display phone:</label>
                        {!! Form::select('display_phone',['no','yes'], null, ['class'=>'form-control','placeholder'=>"Select", 'disabled'=>$disabled]) !!}
                        @if ($errors->has("display_phone"))
                            <span class="form-text text-danger">{{$errors->first('display_phone')}}</span>
                        @endif
                    </div>
                    <div class="col-lg-6">
                        <label>Display Email:</label>
                        {!! Form::select('display_email',['no','yes'], null, ['class'=>'form-control','placeholder'=>"Select", 'disabled'=>$disabled]) !!}
                        @if ($errors->has("display_email"))
                            <span class="form-text text-danger">{{$errors->first('display_email')}}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>
        <div class="kt-section">
            <h4 class="kt-section__title kt-section__title-lg">{{ __('pages.image_and_galleries') }}:</h4>
            <div class="kt-section__body">
                <div class="form-group row">
                    @if(request()->routeIs('advertisements.edit') || request()->routeIs('advertisements.create'))
                        <div class="col-lg-6">
                            <label>Advertisement Images</label>
                            <div></div>
                            <div class="custom-file">
                                {!! Form::file('images[]', ['id'=>'customFile','class'=>'custom-file-input','multiple','accept'=>'.png, .jpg, .jpeg']) !!}
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                        </div>
                    @endif
                    @if(request()->routeIs('advertisements.edit') || request()->routeIs('advertisements.show') )
                        <div class="col-lg-6">
                            @forelse($advertisement->images as $image)
                                <div class="mb-2">
                                    <img src="{{$image->image_url}}" style="width: 100px">
                                    @if(request()->routeIs('advertisements.edit'))
                                        <a href="{{route('advertisements.delete_image',$image)}}"
                                           class="btn  btn-icon-sm  "><i class="fa fa-trash text-danger"></i></a>
                                    @endif
                                </div>

                            @empty
                            @endforelse
                        </div>
                    @endif
                </div>
                <div class="form-group row">
                    @if(request()->routeIs('advertisements.edit') || request()->routeIs('advertisements.create'))
                        <div class="col-lg-6">
                            <label>Swap Item Image</label>
                            <div></div>
                            <div class="custom-file">
                                {!! Form::file('swapItem[image]', ['id'=>'customFile2','class'=>'custom-file-input','accept'=>'.png, .jpg, .jpeg']) !!}
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                        </div>
                    @endif
                    @if(request()->routeIs('advertisements.edit') || request()->routeIs('advertisements.show') )
                        <div class="col-lg-6">

                            @if($advertisement->swapItem)
                                @if($advertisement->swapItem->image)
                                    <div class="mb-2">
                                        <img src="{{$advertisement->swapItem->image->image_url}}" style="width: 100px">
                                        @if(request()->routeIs('advertisements.edit'))
                                            <a href="{{route('advertisements.delete_image',$advertisement->swapItem->image->image_url)}}"
                                               class="btn  btn-icon-sm  "><i class="fa fa-trash text-danger"></i></a>
                                        @endif
                                    </div>
                                @endif
                            @endif
                        </div>
                    @endif
                </div>
            </div>
        </div>


    </div>
    <div class="col-lg-1"></div>
    <div class="col-lg-4">
        @include("admin.layout.form_components.status")

    </div>
</div>
@section('additionalScripts')
    @include('admin.layout.includes.filemanger_scripts')
@endsection


