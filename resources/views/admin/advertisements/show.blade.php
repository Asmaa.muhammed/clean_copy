@extends('admin.layout.main', ['pageTitle'=>'Advertisement | Full Info'])
@section('subheader') @include('admin.layout.components.breadcrumb',['pageTitle'=>'Categories','moduleName'=>'advertisements','pageName'=>"Show Full info" ]) @endsection
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile"
                 id="kt_page_portlet">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title"> Advertisement Full Info </h3>
                    </div>
                    @include("admin.layout.includes.form_button",['disabled'=>true,"module"=>"advertisements","model"=>$advertisement])
                </div>
                <div class="kt-portlet__body">
                {!! Form::model($advertisement,['route'=>['advertisements.update', $advertisement->id], 'method'=>'patch']) !!}
                  @include('admin.advertisements.form', ['disabled'=>true,'class'=>'kt-form kt-form--labe-right','id'=>'kt_form'])
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
