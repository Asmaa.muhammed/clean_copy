
        <div class="row">
            <div class="col-lg-7">
                <div class="kt-section">
                    <div class="kt-section__body">
                        @include("admin.layout.components.basic_tab")
                    </div>
                </div>
                <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>
                <div class="kt-section">
                    <div class="kt-section__body">
                        <h4 class="kt-section__title kt-section__title-lg">{{ __('taxonomies.main_information') }}:</h4>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>{{ __('taxonomies.sorting') }}:</label>
                                {!! Form::number('sorting', null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                                @if ($errors->has("sorting"))
                                    <span class="form-text text-danger">{{$errors->first('sorting')}}</span>
                                @endif
                            </div>
                            <div class="col-lg-6">
                                <label>{{ __('taxonomies.parent') }}:</label>
                                {!! Form::select('parent',$parent,null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                                @if ($errors->has("parent"))
                                    <span class="form-text text-danger">{{$errors->first('parent')}}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>
                <div class="kt-section">
                    <div class="kt-section__body">
                        <div class="form-group row">
                            @include('admin.layout.form_components.select_image')
                            <div class="col-lg-6">
                                <label>{{__('taxonomies.icon')}}</label>
                                <button type="button"
                                        id="icon_picker"
                                        value=""
                                        data-selected=""
                                        data-toggle="dropdown"
                                        class="btn btn-secondary form-control dropdown-toggle iconpicker-component"
                                        {{$disabled ? "disabled" : ""}}
                                        name="icon">
                                    {{__('taxonomies.icon')}} <i class="" id="drop-down-selected-icon"></i>
                                    <span class="caret"></span>
                                </button>
                                {!! Form::hidden('icon', null, ["id"=>"icon"]) !!}
                                <div class="dropdown-menu iconpicker-container"></div>
                                @if ($errors->has("icon"))
                                    <span class="form-text text-danger">{{$errors->first('icon')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>{{__('pages.gallery')}}</label>
                                {!! Form::select('gallery_id',$galleries,null, ['class'=>'form-control ', 'id'=>'gallery_id','placeholder'=> __('pages.gallery'), 'disabled'=>$disabled]) !!}
                                @if ($errors->has("gallery_id"))
                                    <span class="form-text text-danger">{{$errors->first('gallery_id')}}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="col-lg-1"></div>
            <div class="col-lg-4">
                @include("admin.layout.form_components.status")

            </div>
        </div>
        @section('additionalScripts')
            @include('admin.layout.includes.filemanger_scripts')
            <script>
                $(function () {
                    $("#icon_picker").iconpicker({
                        title: "Pick Icon"
                    });
                    $('#icon_picker').on('iconpickerSelected', function (event) {
                        $("#icon").val(event.iconpickerValue);
                    });
                    $.iconpicker.batch("#icon_picker","update",$("#icon").val(), false);
                });
            </script>

        @endsection


