@extends('admin.layout.main', ['pageTitle'=>'Tag | Edit'])
@section('breadcrumb') @include('admin.layout.components.breadcrumb',['pageTitle'=>'Tags','moduleName'=>'tags','pageName'=>"Update Tag" ]) @endsection
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile"
                 id="kt_page_portlet">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">  {{ __('taxonomies.edit_category') }}</h3>
                    </div>
                    @include("admin.layout.includes.form_button",['disabled'=>false,"module"=>"categorys","model"=>$category])
                </div>
                <div class="kt-portlet__body">
                    {!! Form::model($category,['route'=>['categories.update', $category->id], 'method'=>'patch','class'=>'kt-form kt-form--labe-right','id'=>'kt_form']) !!}
                    @include('admin.categories.form', ['disabled'=>false])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
