@extends('admin.layout.main', ['pageTitle'=>__('common.indexPageTitle', ['module' => $cmsModule->name])])
@section('subheader') @include('admin.layout.components.breadcrumb',['pageTitle'=>__('common.moduleTitle', ['module'=>$cmsModule->name]),'moduleName'=>$cmsModule->route,'pageName'=>__('common.indexPageName', ['module'=>$cmsModule->name]) ]) @endsection
@section('content')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    {{__('common.all', ['module'=>$cmsModule->name])}}
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        {{--<div class="dropdown dropdown-inline">--}}
                        {{--<button type="button" class="btn btn-default btn-icon-sm dropdown-toggle"--}}
                        {{--data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                        {{--<i class="la la-download"></i> Export--}}
                        {{--</button>--}}
                        {{--<div class="dropdown-menu dropdown-menu-right">--}}
                        {{--<ul class="kt-nav">--}}
                        {{--<li class="kt-nav__section kt-nav__section--first">--}}
                        {{--<span class="kt-nav__section-text">Choose an option</span>--}}
                        {{--</li>--}}
                        {{--<li class="kt-nav__item">--}}
                        {{--<a href="#" class="kt-nav__link">--}}
                        {{--<i class="kt-nav__link-icon la la-print"></i>--}}
                        {{--<span class="kt-nav__link-text">Print</span>--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        {{--<li class="kt-nav__item">--}}
                        {{--<a href="#" class="kt-nav__link">--}}
                        {{--<i class="kt-nav__link-icon la la-copy"></i>--}}
                        {{--<span class="kt-nav__link-text">Copy</span>--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        {{--<li class="kt-nav__item">--}}
                        {{--<a href="#" class="kt-nav__link">--}}
                        {{--<i class="kt-nav__link-icon la la-file-excel-o"></i>--}}
                        {{--<span class="kt-nav__link-text">Excel</span>--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        {{--<li class="kt-nav__item">--}}
                        {{--<a href="#" class="kt-nav__link">--}}
                        {{--<i class="kt-nav__link-icon la la-file-text-o"></i>--}}
                        {{--<span class="kt-nav__link-text">CSV</span>--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        {{--<li class="kt-nav__item">--}}
                        {{--<a href="#" class="kt-nav__link">--}}
                        {{--<i class="kt-nav__link-icon la la-file-pdf-o"></i>--}}
                        {{--<span class="kt-nav__link-text">PDF</span>--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        {{--</ul>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        &nbsp;
                        <a href="{{route('menuGroups.create')}}" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            {{__('common.newRecord')}}
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
        @include('admin.layout.components.flash_messages')

        <!--begin: Datatable -->
        {!! $html->table(['class' => ' table table-striped- table-bordered table-hover table-checkable dataTable no-footer categories'], true) !!}

        <!--end: Datatable -->
        </div>
    </div>
@endsection
@section('additionalScripts')

    {!! $html->scripts() !!}
    <script src="{{asset('/public/admin')}}/assets/js/modules.js" type="text/javascript"></script>
@endsection
