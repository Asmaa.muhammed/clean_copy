<div class="row">
    <div class="col-md-7">
        <div class="form-group row">
            <div class="col-lg-6">
                <label>{{__('menuGroups.name')}}</label>
                {!! Form::text('name', null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                @if ($errors->has("name"))
                    <span class="form-text text-danger">{{$errors->first('name')}}</span>
                @endif
            </div>
            <div class="col-lg-6">
                <label class="">{{__('menuGroups.short_code')}}</label>
                {!! Form::text('short_code', null, ['class'=>'form-control',  'disabled'=>$disabled]) !!}
                @if ($errors->has("short_code"))
                    <span class="form-text text-danger">{{$errors->first('short_code')}}</span>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-lg-4">
        @include('admin.layout.form_components.status')
    </div>
</div>
