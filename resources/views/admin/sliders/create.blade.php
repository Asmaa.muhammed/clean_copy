@extends('admin.layout.main', ['pageTitle'=>__('common.createPageTitle', ['module'=>$cmsModule->name])])
@section('subheader') @include('admin.layout.components.breadcrumb',['pageTitle'=>__('common.moduleTitle', ['module'=>$cmsModule->name]),'moduleName'=>$cmsModule->route,'pageName'=>__('common.createPageName', ['module'=>__('menuGroups.singularModuleName')]) ]) @endsection
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile"
                 id="kt_page_portlet">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">{{__('common.addNew', ['module'=>__('sliders.singularModuleName')])}}</h3>
                    </div>
                    @include("admin.layout.includes.form_button",['disabled'=>false,"module"=>$cmsModule->route])
                </div>
                <div class="kt-portlet__body">
                    {!! Form::open(['route'=>'sliders.store', 'method'=>'post', 'class'=>'kt-form kt-form--labe-right','id'=>'kt_form']) !!}
                    @include('admin.sliders.form', ['disabled'=>false])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('additionalScripts')
    <!--begin::Page Scripts(used by this page) -->
    <script src="{{asset('public/admin/')}}/assets/js/pages/crud/forms/widgets/form-repeater.js" type="text/javascript"></script>
    @include('admin.layout.includes.filemanger_scripts')

    <!--end::Page Scripts -->
    @endsection
