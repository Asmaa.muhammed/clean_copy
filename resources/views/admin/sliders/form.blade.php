<div class="row">
    <div class="col-md-7">
        <div class="form-group row">
            <div class="col-lg-8">
                <label>{{__('galleries.title')}}</label>
                {!! Form::text('title', null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                @if ($errors->has("title"))
                    <span class="form-text text-danger">{{$errors->first('title')}}</span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-8">
                <label class="">{{__('galleries.short_code')}}</label>
                {!! Form::text('short_code', null, ['class'=>'form-control',  'disabled'=>$disabled]) !!}
                @if ($errors->has("short_code"))
                    <span class="form-text text-danger">{{$errors->first('short_code')}}</span>
                @endif
            </div>
        </div>

    </div>
    <div class="col-md-1"></div>
    <div class="col-lg-4">
        @include('admin.layout.form_components.status')
        <div class="form-group">
            <label>{{__('sliders.main')}}</label>
            <div class="kt-radio-list">
                <label class="kt-radio kt-radio--success">
                    {{ Form::radio('main', 1, null,['id' => 'input-radio-18' ,$disabled ? 'disabled': '']) }} {{ __('sliders.yes')   }}
                    <span></span>

                </label>
                <label class="kt-radio kt-radio--danger">
                    {{ Form::radio('main', 0, null,['id' => 'input-radio-19' ,$disabled ? 'disabled': '']) }} {{ __('sliders.no')   }}
                    <span></span>
                </label>
                @error('main')
                <span class="form-text text-danger">{{$message}}</span>
                @enderror
            </div>

        </div>

    </div>
</div>

