<div class="row">
    <div class="col-md-7">
        @component("admin.components.translatable_section")
            @foreach($languages as $language)
                @slot("translatable_{$language->language}")
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label>{{__('menuLinks.name')}}</label>
                            {!! Form::text("name:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                            @if ($errors->has("name:{$language->language}"))
                                <span
                                    class="form-text text-danger">{{$errors->first("name:{$language->language}")}}</span>
                            @endif
                        </div>
                        <div class="col-lg-6">
                            <label class="">{{__('menuLinks.description')}}</label>
                            {!! Form::textarea("description:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                            @if ($errors->has("description:{$language->language}"))
                                <span
                                    class="form-text text-danger">{{$errors->first("description:{$language->language}")}}</span>
                            @endif
                        </div>
                    </div>
                @endslot
            @endforeach
        @endcomponent
        <div class="kt-portlet__head mb-4 row">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    {{__('common.moduleInfo', ['module'=>__('menuLinks.singularModuleName')])}}
                </h3>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-6">
                <label>{{__('menuLinks.target')}}</label>
                {!! Form::select('target',$target,null, ['id'=>'target','class'=>'form-control', 'disabled'=>$disabled]) !!}
                @if ($errors->has("target"))
                    <span class="form-text text-danger">{{$errors->first('target')}}</span>
                @endif
            </div>
            <div class="col-lg-6">
                <label class="">{{__('menuLinks.link')}}</label>
                <div id="linkComponent">
                </div>
                @if ($errors->has("target_type"))
                    <span class="form-text text-danger">{{$errors->first('target_type')}}</span>
                @endif
                @if ($errors->has("target_id"))
                    <span class="form-text text-danger">{{$errors->first('target_id')}}</span>
                @endif
                @if ($errors->has("link"))
                    <span class="form-text text-danger">{{$errors->first('link')}}</span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-6">
                <label class="">{{__('menuLinks.sorting')}}</label>
                {!! Form::number('sorting', null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                @if ($errors->has("sorting"))
                    <span class="form-text text-danger">{{$errors->first('sorting')}}</span>
                @endif
            </div>
            <div class="col-lg-6">
                <label>{{__('menuLinks.icon')}}</label>
                <button type="button"
                        id="icon_picker"
                        value=""
                        data-selected=""
                        data-toggle="dropdown"
                        class="btn btn-secondary form-control dropdown-toggle iconpicker-component"
                        {{$disabled ? "disabled" : ""}}
                        name="icon">
                    {{__('menuLinks.icon')}} <i class="" id="drop-down-selected-icon"></i>
                    <span class="caret"></span>
                </button>
                {!! Form::hidden('icon', null, ["id"=>"icon"]) !!}
                <div class="dropdown-menu iconpicker-container"></div>
                @if ($errors->has("icon"))
                    <span class="form-text text-danger">{{$errors->first('icon')}}</span>
                @endif
            </div>
        </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>{{__('pages.gallery')}}</label>
                    {!! Form::select('gallery_id',$galleries,null, ['class'=>'form-control ', 'id'=>'gallery_id','placeholder'=> __('pages.gallery'), 'disabled'=>$disabled]) !!}
                    @if ($errors->has("gallery_id"))
                        <span class="form-text text-danger">{{$errors->first('gallery_id')}}</span>
                    @endif
                </div>
            <div class="col-lg-6">
                <label>{{__('menuLinks.parent')}}</label>
                {!! Form::select('parent',$parent,null, ['id'=>'target','class'=>'form-control', 'disabled'=>$disabled]) !!}
                @if ($errors->has("parent"))
                    <span class="form-text text-danger">{{$errors->first('parent')}}</span>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-md-4">
        @include('admin.layout.form_components.status', ['disabled' => $disabled])
        <div class="row">
            <div id="kt_tree_1" class="tree-demo jstree jstree-1 jstree-default row" role="tree"
                 aria-multiselectable="true" tabindex="0" aria-activedescendant="j1_{{$menuGroup->id}}"
                 aria-busy="false">
                <ul class="jstree-container-ul jstree-children" role="group">
                    <li role="treeitem" aria-selected="true" aria-level="{{$level = $level ?? 1}}"
                        aria-labelledby="j1_{{$menuGroup->id}}_anchor"
                        aria-expanded="true" id="j1_{{$menuGroup->id}}"
                        class="jstree-node  jstree-open  jstree-last"><i
                            class="jstree-icon jstree-ocl" role="presentation"></i><a class="jstree-anchor"
                                                                                      href="{{route('menuGroups.show', $menuGroup->id)}}"
                                                                                      tabindex="{{$menuGroup->id}}"
                                                                                      id="j1_{{$menuGroup->id}}_anchor"><i
                                class="jstree-icon jstree-themeicon fa fa-list-ul jstree-themeicon-custom"
                                role="presentation"></i>
                            {{$menuGroup->name}}
                        </a>
                        <ul role="group" class="jstree-children">
                            @forelse($menuGroup->links()->whereParent(0)->get() as $link)
                                <li role="treeitem" aria-selected="true" aria-level="{{$level}}"
                                    aria-labelledby="j1_{{$link->id}}_anchor"
                                    aria-expanded="true" id="j1_{{$link->id}}"
                                    class="jstree-node  {{$link->hasChildren() ? "jstree-open" : "jstree-leaf"}}  {{($loop->last) ? 'jstree-last' :''}}">
                                    <i
                                        class="jstree-icon jstree-ocl" role="presentation"></i><a
                                        class="jstree-anchor"
                                        href="{{route('menuLinks.show', ['menuGroup'=>$menuGroup->id, 'menuLink'=> $link->id])}}"
                                        tabindex="{{$link->id}}"
                                        id="j1_{{$link->id}}_anchor"><i
                                            class="jstree-icon jstree-themeicon fa fa-list-ul jstree-themeicon-custom"
                                            role="presentation"></i>
                                        {{$link->name}}
                                    </a>
                                    @include('admin.layout.components.recursive_tree', ['model' => $link, 'level' => $level++, 'menuGroup'=>$menuGroup])
                                </li>
                            @empty
                            @endforelse
                        </ul>

                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
@section("additionalScripts")
    <script src="https://cdn.jsdelivr.net/npm/handlebars@latest/dist/handlebars.js"></script>
    <script>
        $(function () {
            $("#icon_picker").iconpicker({
                title: "Pick Icon"
            });
            $('#icon_picker').on('iconpickerSelected', function (event) {
                $("#icon").val(event.iconpickerValue);
            });
            $.iconpicker.batch("#icon_picker", "update", $("#icon").val(), false);
        });
    </script>
    <script>

        Handlebars.registerHelper('link', function (value) {
            value = parseInt(value);
            if (value) {
                return `{!! Form::text('link', isset($menuLink) ? ($menuLink->link->link ?? null) : old('link'), ['class'=>'form-control', 'disabled'=>$disabled]) !!}`;
            }
            return `{!! Form::select('target_type',$targetTypes, null, ["id"=>"target_type",'placeholder' => 'Choose Target Type','class'=>'form-control mb-2', 'disabled'=>$disabled]) !!}
            {!! Form::select('target_id',[],null, ['id'=>"target_id",'class'=>'form-control', 'disabled'=>true]) !!}`;
        });

        $('#target').change(function () {
            $("#linkComponent").empty().html(Handlebars.compile(`{@{{link ${$(this).val()}}}}`)());
        }).change();

        $(document).on('change', '#target_type', function () {
            $.ajax({
                method: 'post',
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    model: $(this).val(),
                },
                url: `{{route('ajax.get_target_type_data')}}`,
                success: function (data) {
                    $("#target_id").removeAttr('disabled').empty().html(data.data.map(function (element) {
                        return `<option value="${element.id}">${element.name}</option>`;
                    }).join("\n"));
                    @if(old('target_id'))
                    $(document).find('#target_id').val(`{{old('target_id')}}`).change();
                    @endif
                    @isset($menuLink)
                    $(document).find('#target_id').val(`{{$menuLink->link->target_id}}`).change().attr('disabled', {{$disabled}});
                    @endisset
                }
            });
        }).change();
        @if(old('target_type'))
        $(document).find("#target_type").change();
        @endif
        @isset($menuLink)
        $(document).find("#target_type").val('{{$menuLink->link->target_type}}').change();
        @endisset
    </script>
@endsection
