@extends('admin.layout.main', ['pageTitle'=>__('common.indexPageTitle', ['module'=>$cmsModule->name])])
@section('subheader') @include('admin.layout.components.breadcrumb',['pageTitle'=>__('common.moduleTitle', ['module'=>$cmsModule->name]),'params' => [$menuGroup->id],'moduleName'=>$cmsModule->route,'pageName'=>__('common.indexPageName', ['module'=>$cmsModule->name])]) @endsection
@section('additionalStyles')
    <link href="{{asset('public/admin')}}/assets/plugins/custom/jstree/jstree.bundle.css" rel="stylesheet"
          type="text/css"/>
@endsection
@section('content')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label"><i class="flaticon-list"></i> &nbsp;<h3
                    class="kt-portlet__head-title">  {{__('common.all', ['module'=>$cmsModule->name])}}
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <div class="dropdown dropdown-inline">
                            <button type="button" class="btn btn-default btn-icon-sm dropdown-toggle"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="la la-download"></i> Export
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__section kt-nav__section--first">
                                        <span class="kt-nav__section-text">Choose an option</span>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-print"></i>
                                            <span class="kt-nav__link-text">Print</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-copy"></i>
                                            <span class="kt-nav__link-text">Copy</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-excel-o"></i>
                                            <span class="kt-nav__link-text">Excel</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-text-o"></i>
                                            <span class="kt-nav__link-text">CSV</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-pdf-o"></i>
                                            <span class="kt-nav__link-text">PDF</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        &nbsp;
                        <a href="{{route('menuLinks.create', $menuGroup->id)}}"
                           class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            {{ __('translation::translation.add') }}
                        </a>
                        <a href="{{route('menuLinks.index', $menuGroup->id)}}"
                           class="btn btn-warning btn-elevate btn-icon-sm">
                            <i class="la la-table"></i>
                            {{__('common.all', ['module'=>$cmsModule->name])}}
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            {{ __('common.showTree', ['module'=>$cmsModule->name]) }}
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div id="kt_tree_1" class="tree-demo jstree jstree-1 jstree-default" role="tree"
                         aria-multiselectable="true" tabindex="0" aria-activedescendant="j1_{{$menuGroup->id}}" aria-busy="false">
                        <ul class="jstree-container-ul jstree-children" role="group">
                            <li role="treeitem" aria-selected="true" aria-level="{{$level++}}"
                                aria-labelledby="j1_{{$menuGroup->id}}_anchor"
                                aria-expanded="true" id="j1_{{$menuGroup->id}}"
                                class="jstree-node  jstree-open  jstree-last"><i
                                    class="jstree-icon jstree-ocl" role="presentation"></i><a class="jstree-anchor"
                                                                                              href="{{route('menuGroups.show', $menuGroup->id)}}" tabindex="{{$menuGroup->id}}"
                                                                                              id="j1_{{$menuGroup->id}}_anchor"><i
                                        class="jstree-icon jstree-themeicon fa fa-list-ul jstree-themeicon-custom"
                                        role="presentation"></i>
                                    {{$menuGroup->name}}
                                </a>
                                <ul role="group" class="jstree-children">
                                    @forelse($menuGroup->links()->whereParent(0)->get() as $link)
                                        <li role="treeitem" aria-selected="true" aria-level="{{$level}}"
                                            aria-labelledby="j1_{{$link->id}}_anchor"
                                            aria-expanded="true" id="j1_{{$link->id}}"
                                            class="jstree-node  {{$link->hasChildren() ? "jstree-open" : "jstree-leaf"}}  {{($loop->last) ? 'jstree-last' :''}}">
                                            <i
                                                class="jstree-icon jstree-ocl" role="presentation"></i><a
                                                class="jstree-anchor"
                                                href="{{route('menuLinks.show', ['menuGroup'=>$menuGroup->id, 'menuLink'=> $link->id])}}" tabindex="{{$link->id}}"
                                                id="j1_{{$link->id}}_anchor"><i
                                                    class="jstree-icon jstree-themeicon fa fa-list-ul jstree-themeicon-custom"
                                                    role="presentation"></i>
                                                {{$link->name}}
                                            </a>
                                            @include('admin.layout.components.recursive_tree', ['model' => $link, 'level' => $level++, 'menuGroup'=>$menuGroup])
                                        </li>
                                    @empty
                                    @endforelse
                                </ul>

                            </li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('additionalScripts')
    <script src="{{asset('public/admin')}}/assets/plugins/custom/jstree/jstree.bundle.js"
            type="text/javascript"></script>
@endsection
