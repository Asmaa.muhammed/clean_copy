<div class="form-group">
    <label>{{__('translation::translation.status')}}</label>
    <div class="kt-radio-list">
        <label class="kt-radio kt-radio--success">
            {{ Form::radio('status', 1, null,['id' => 'input-radio-16' ,$disabled ? 'disabled': '']) }} {{ __('translation::translation.status_enable')   }}
            <span></span>

        </label>
        <label class="kt-radio kt-radio--danger">
            {{ Form::radio('status', 0, null,['id' => 'input-radio-17' ,$disabled ? 'disabled': '']) }} {{ __('translation::translation.status_disable')   }}
            <span></span>
        </label>
        @error('status')
            <span class="form-text text-danger">{{$message}}</span>
        @enderror
    </div>

</div>
