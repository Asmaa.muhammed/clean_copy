<div class="col-lg-6">
    <label >    {{ __('translation::translation.image') }}:</label>
    <div class="input-group">
          <span class="input-group-prepend">
            <a  data-input="thumbnail" data-preview="holder" class="btn btn-primary lfm ">
              <i class="la la-image  text-white "></i>
            </a>
          </span>
        {!! Form::text('image_url', (!empty($image) ? $image : null), ['class'=>'form-control','id'=>'thumbnail','disabled'=>$disabled]) !!}

    </div>
    <div id="holder" style="margin-top:15px;max-height:150px;">
        @if(!empty($image))
            <img src="{{$image}}" width="150px">
            @endif
    </div>
</div>

