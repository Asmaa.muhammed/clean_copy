<div class="form-group row">
    <div class="col-lg-12">
        <label class="">{{__($module.'.start_publishing')}}</label>
        <div class="input-group date">

            {!! Form::text("publishOptions[start_publishing]", null, ['class'=>'form-control','id'=>'kt_datetimepicker_4_3', 'disabled'=>$disabled]) !!}
            <div class="input-group-append">
            <span class="input-group-text">
                	<i class="la la-calendar glyphicon-th"></i>
            </span>
                @if ($errors->has("publishOptions.start_publishing"))
                    <span
                        class="form-text text-danger">{{$errors->first("publishOptions.start_publishing")}}
                    </span>
                @endif

            </div>
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-lg-12">
        <label class="">{{__($module.'.end_publishing')}}</label>
        <div class="input-group date">
            {!! Form::text("publishOptions[end_publishing]", null, ['class'=>'form-control','id'=>'kt_datetimepicker_4_2', 'disabled'=>$disabled]) !!}
            <div class="input-group-append">
            <span class="input-group-text">
                	<i class="la la-calendar glyphicon-th"></i>
            </span>
                @if ($errors->has("publishOptions.end_publishing"))
                    <span
                        class="form-text text-danger">{{$errors->first("publishOptions.end_publishing")}}
                    </span>
                @endif

            </div>
        </div>
    </div>
</div>
<div class="form-group">
    <label>{{__('translation::translation.show_in_main_page')}}</label>
    <div class="kt-radio-list">
        <label class="kt-radio kt-radio--success">
            {{ Form::radio('publishOptions[show_in_main_page]',1, null,['id' => 'input-radio-17' ,$disabled ? 'disabled': '']) }} {{ __('translation::translation.status_enable')   }}
            <span></span>
        </label>
        <label class="kt-radio kt-radio--danger">
            {{ Form::radio('publishOptions[show_in_main_page]',0, null,['id' => 'input-radio-16' ,$disabled ? 'disabled': '']) }} {{ __('translation::translation.status_disable')   }}
            <span></span>

        </label>
        @error('publishOptions.show_in_main_page')
        <span class="form-text text-danger">{{$message}}</span>
        @enderror
    </div>

</div>




