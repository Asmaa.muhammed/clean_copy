@if($model->status == 1)
    <span class="kt-badge  kt-badge--primary kt-badge--inline kt-badge--pill">{{ __('translation::translation.status_enable')   }}</span>
@else
    <span
        class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill">{{ __('translation::translation.status_disable') }}</span>
@endif
