<div class="kt-portlet__head-toolbar">
    <button type="button" onclick="window.history.back();" class="btn btn-clean kt-margin-r-10">
        <i class="la la-arrow-left"></i>
        <span class="kt-hidden-mobile">   {{ __('translation::translation.back') }}</span>
    </button >

    <div class="btn-group">
        @if(!$disabled)
        <button type="button" class="btn btn-brand" id="submitButton">
            <i class="la la-check"></i>
            <span class="kt-hidden-mobile">   {{ __('translation::translation.save') }}</span>
        </button>
            @else
            <button type="button" class="btn btn-brand" onclick="window.location.href='{{route($module.".edit",$model)}}'">
                <i class="la la-pencil-square-o"></i>
                <span class="kt-hidden-mobile">   {{ __('translation::translation.edit') }}</span>
            </button>
        @endif


    </div>
</div>
