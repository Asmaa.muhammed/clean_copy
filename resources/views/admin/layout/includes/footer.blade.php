<!-- begin:: Footer -->

    <div class="kt-container  kt-container--fluid ">
        <div class="kt-footer__copyright">
            2020&nbsp;&copy;&nbsp;<a href="https://diva-lab.com" target="_blank" class="kt-link">DivaLab</a>
        </div>

    </div>


<!-- end:: Footer -->
