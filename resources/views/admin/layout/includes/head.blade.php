<!--begin::Fonts -->
<link rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

<!--end::Fonts -->

<!--begin::Page Vendors Styles(used by this page) -->
<link href="{{asset('/public/admin')}}/assets/plugins/custom/datatables/datatables.bundle{{$styleDirection}}.css" rel="stylesheet" type="text/css" />

<!--end::Page Vendors Styles -->

<!--begin::Global Theme Styles(used by all pages) -->
<link href="{{asset('/public/admin')}}/assets/plugins/global/plugins.bundle{{$styleDirection}}.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('/public/admin')}}/assets/css/style.bundle{{$styleDirection}}.css" rel="stylesheet" type="text/css"/>

<!--end::Global Theme Styles -->

<!--begin::Layout Skins(used by all pages) -->
<link href="{{asset('/public/admin')}}/assets/css/skins/header/base/dark{{$styleDirection}}.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('/public/admin')}}/assets/css/skins/header/menu/dark{{$styleDirection}}.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('/public/admin')}}/assets/css/skins/brand/dark{{$styleDirection}}.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('/public/admin')}}/assets/css/skins/aside/light{{$styleDirection}}.css" rel="stylesheet" type="text/css"/>


<!--end::Layout Skins -->
<link rel="shortcut icon" href="{{asset('/public/admin')}}/assets/media/logos/diva-ico.png"/>
<link rel="stylesheet" href="{{asset("public/admin/assets/css/fontawesome-iconpicker.css")}}">
