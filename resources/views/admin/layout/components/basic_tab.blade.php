
    <ul class="nav nav-tabs  nav-tabs-line nav-tabs-line-2x nav-tabs-line-danger" role="tablist">
        @forelse($languages as $language)
            <li class="nav-item">
                <a class="nav-link {{($loop->first) ? 'active': ''}}" data-toggle="tab"
                   href="#{{$language->language}}" role="tab">{{$language->name}}</a>
            </li>
        @empty
        @endforelse

    </ul>
    <div class="tab-content">
        @forelse($languages as $language)
            <div class="tab-pane  {{($loop->first) ? 'active': ''}}" id="{{$language->language}}" role="tabpanel">
                <div class="form-group row">
                    <div class="col-lg-6">
                        <label for="name:{{$language->language}}">{{__('enums.name')}}:</label>
                        {!! Form::text("name:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                        @if ($errors->has("name:{$language->language}"))
                            <span class="form-text text-danger">{{$errors->first("name:{$language->language}")}}</span>
                        @endif
                    </div>
                    <div class="col-lg-6">
                        <label for="description:{{$language->language}}">{{__('enums.description')}}:</label>
                        {!! Form::textarea("description:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                        @if ($errors->has("description:{$language->language}"))
                            <span
                                class="form-text text-danger">{{$errors->first("description:{$language->language}")}}</span>
                        @endif
                    </div>
                </div>

            </div>
        @empty
        @endforelse

    </div>

