<div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Dashboard</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
               {{-- <a href="#" class="btn kt-subheader__btn-secondary">Today</a>
                <a href="#" class="btn kt-subheader__btn-secondary">Month</a>
                <a href="#" class="btn kt-subheader__btn-secondary">Year</a>--}}
                <a href="#" class="btn kt-subheader__btn-daterange" id="kt_dashboard_daterangepicker" data-toggle="kt-tooltip" title="Select dashboard daterange" data-placement="left">
                    <span class="kt-subheader__btn-daterange-title" id="kt_dashboard_daterangepicker_title">Today</span>&nbsp;
                    <span class="kt-subheader__btn-daterange-date" id="kt_dashboard_daterangepicker_date">{{\Illuminate\Support\Carbon::today()->toDayDateTimeString()}}</span>
                    <i class="flaticon2-calendar-1"></i>
                </a>

            </div>
        </div>
    </div>
