@if($model->hasChildren())
    <ul role="group" class="jstree-children">
        @forelse($model->children as $child)
            <li role="treeitem" data-jstree=""
                aria-selected="true" aria-level="{{$level}}"
                aria-labelledby="j1_{{$child->id}}_anchor" id="j1_{{$child->id}}"
                class="jstree-node  {{$child->hasChildren() ? "jstree-open" : "jstree-leaf"}} {{($loop->last) ? 'jstree-last' :''}}">
                <i class="jstree-icon jstree-ocl" role="presentation"></i>
                <a
                    class="jstree-anchor  " href="{{route('menuLinks.show', ['menuGroup'=>$menuGroup->id, 'menuLink'=> $child->id])}}"
                    tabindex="{{$child->id}}"
                    id="j1_{{$child->id}}_anchor"><i
                        class="jstree-icon jstree-themeicon fa fa-li jstree-themeicon-custom"
                        role="presentation"></i>
                    {{$child->name}}
                </a>
                @include('admin.layout.components.recursive_tree', ['model' => $child, 'level' => $level++, 'menuGroup'=>$menuGroup])
            </li>
        @empty
        @endforelse

    </ul>
@endif
