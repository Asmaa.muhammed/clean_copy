@if(Session::has('success') && !empty(Session::get('success')))
    <ul class="alert alert-success list-unstyled" style="margin: 0">
        <div class="alert-icon"><i class="flaticon-like"></i></div>
        @if(is_array(Session::get('success')))
            @foreach(Session::get('success') as $message)
                <li class="  alert-text ">{{$message}}</li>
            @endforeach
        @else
            <li class="  alert-text">{!! Session::get('success') !!}</li>
        @endif
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="la la-close"></i></span>
            </button>
        </div>
    </ul>
@endif
@if(Session::has('error') && !empty(Session::get('error')))
    <ul class="alert alert-danger list-group">
        @if(is_array(Session::get('error')))
            @foreach(Session::get('error') as $message)
                <li  class="  alert-text " >{{$message}}</li>
            @endforeach
        @else
            <li class="  alert-text ">{!! Session::get('error') !!}</li>
        @endif
            <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="la la-close"></i></span>
                </button>
            </div>
    </ul>
@endif
