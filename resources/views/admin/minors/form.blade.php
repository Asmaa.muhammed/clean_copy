<div class="row">
    <div class="col-md-7">
        <div class="form-group row">
            <div class="col-lg-6">
                <label>{{__('galleries.title')}}</label>
                {!! Form::text('title', null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                @if ($errors->has("title"))
                    <span class="form-text text-danger">{{$errors->first('title')}}</span>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-lg-4">
        @include('admin.layout.form_components.status')
    </div>
</div>

