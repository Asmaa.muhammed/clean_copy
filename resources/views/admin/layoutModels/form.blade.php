<div class="row">
    <div class="col-md-7">
        <div class="form-group row">
            <div class="col-lg-6">
                <label>{{__('layoutModels.name')}}</label>
                {!! Form::text('name', null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                @if ($errors->has("name"))
                    <span class="form-text text-danger">{{$errors->first('name')}}</span>
                @endif
            </div>
            <div class="col-lg-6">
                <label class="">{{__('layoutModels.layout_type')}}</label>
                {!! Form::select('layout_type', $layout_types->pluck('name', 'id'), null, ['class'=>'form-control','id'=>'layout_type','placeholder'=>__('common.please_choose', ['name' => __('layoutModels.layout_type')]), 'disabled'=>$disabled]) !!}
                @if ($errors->has("layout_type"))
                    <span class="form-text text-danger">{{$errors->first('layout_type')}}</span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-6">
                <label class="">{{__('layoutModels.type')}}</label>
                {!! Form::select('type', $types, null, ['class'=>'form-control','placeholder'=>__('common.please_choose', ['name'=>__('layoutModels.type')]), 'disabled'=>$disabled]) !!}
                @if ($errors->has("type"))
                    <span class="form-text text-danger">{{$errors->first('type')}}</span>
                @endif
            </div>
            <div class="col-lg-6">
                <label class="">{{__('layoutModels.theme')}}</label>
                {!! Form::select('theme_id', $themes, null, ['class'=>'form-control','placeholder'=>__('common.please_choose', ['name' => __('layoutModels.theme')]), 'disabled'=>$disabled]) !!}
                @if ($errors->has("theme_id"))
                    <span class="form-text text-danger">{{$errors->first('theme_id')}}</span>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-lg-4">
        @include('admin.layout.form_components.status')
    </div>
</div>
<div class="form-group row">
    <div class="col-lg-12" id="pluginsLists">

    </div>
</div>
@section('additionalScripts')
    <script src="https://cdn.jsdelivr.net/npm/handlebars@latest/dist/handlebars.js"></script>
    <script src="{{asset('public/admin/assets/js/pages/components/extended/dual-listbox.js')}}"></script>
    <script>
        let layoutTypes = (@json($layout_types)).map(function (item) {
            item['value'] = JSON.parse(item['value']);
            return item;
        });
        let layoutSections = @json($layout_sections);
        @foreach($layout_sections as $layout_section)
        Handlebars.registerHelper('plugins-{{$layout_section->value}}', function (value) {

            return `{!! Form::select('pluginGroups['.$layout_section->value.'][]'
                            ,$pluginGroups->has($layout_section->value)
                            ? $pluginGroups->get($layout_section->value)->pluck('name', 'id')
                            : []
                            , null
                            , [
                                "id"=>"plugins-".$layout_section->value,
                                'class'=>'form-control mb-2 kt-dual-listbox',
                                'multiple' => "multiple",
                                'disabled'=>$disabled,
                                'data-available-title' => "Available {$layout_section->name} Plugins",
                                'data-selected-title'=>"Selected {$layout_section->name} Plugins",
                                'data-search' => "false",
                             ]
                         ) !!}`;
        });
        @endforeach
        $('#layout_type').change(function (event) {
            let query = "";
            let selectedType = layoutTypes.find((layoutType) => {
                return layoutType.id == $(this).val();
            });
            let selectedSections = layoutSections.filter(function (item) {
                return selectedType ? selectedType.value.sections.includes(parseInt(item.value)) : false;
            });
            for (let i in selectedSections) {
                query += `{@{{plugins-${selectedSections[i].value}}}}`;
            }
            console.log(query);
            if(query.length > 0){
                query = "<hr>" + query;
                $("#pluginsLists").empty().html(Handlebars.compile(query)());
            }
            KTDualListbox.init();
        }).change();
    </script>
@endsection
