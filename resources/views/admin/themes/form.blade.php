<div class="row">
    <div class="col-md-7">
        <div class="form-group row">
            <div class="col-lg-6">
                <label>{{__('themes.name')}}</label>
                {!! Form::text('name', null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                @if ($errors->has("name"))
                    <span class="form-text text-danger">{{$errors->first('name')}}</span>
                @endif
            </div>
            <div class="col-lg-6">
                <label class="">{{__('themes.path')}}</label>
                {!! Form::text('path', null, ['class'=>'form-control',  'disabled'=>$disabled]) !!}
                @if ($errors->has("path"))
                    <span class="form-text text-danger">{{$errors->first('path')}}</span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-8">
                <label>    {{ __('themes.thumbnail') }}:</label>
                <div class="input-group">
                             <span class="input-group-prepend">
                                  <a data-input="thumbnail" data-preview="image_preview" class="btn btn-primary lfm ">  <i
                                          class="la la-image  text-white "></i>
                                       </a>
                             </span>
                    {!! Form::text('thumbnail', (!empty($theme) ? $theme->thumbnail : null), ['class'=>'form-control','id'=>'thumbnail','disabled'=>$disabled,'readonly']) !!}

                </div>
            </div>
            <div class="col-lg-4">
                <div id="image_preview" style="margin-top:15px;max-height:150px;">
                    @if(!empty($thumbnail))
                        <img src="{{$thumbnail->image_url}}" width="150px">
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-lg-4">
        @include('admin.layout.form_components.status')
    </div>
</div>
@section('additionalScripts')
    <!--begin::Page Scripts(used by this page) -->
    @include('admin.layout.includes.filemanger_scripts')

    <!--end::Page Scripts -->
@endsection
