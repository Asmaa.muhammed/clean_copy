<div class="row">
    <div class="col-md-7">
        @component("admin.components.translatable_section")
            @foreach($languages as $language)
                @slot("translatable_{$language->language}")
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>{{__('modules.name')}}</label>
                                {!! Form::text("name:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                                @if ($errors->has("name:{$language->language}"))
                                    <span
                                        class="form-text text-danger">{{$errors->first("name:{$language->language}")}}</span>
                                @endif
                            </div>
                            <div class="col-lg-6">
                                <label class="">{{__('modules.description')}}</label>
                                {!! Form::textarea("description:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                                @if ($errors->has("description:{$language->language}"))
                                    <span
                                        class="form-text text-danger">{{$errors->first("description:{$language->language}")}}</span>
                                @endif
                            </div>
                        </div>
                @endslot
            @endforeach
        @endcomponent
        <div class="kt-portlet__head mb-4 row">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    {{__('common.moduleInfo', ['module'=>__('modules.singularModuleName')])}}
                </h3>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-6">
                <label>{{__('modules.route')}}</label>
                {!! Form::text('route', null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                @if ($errors->has("route"))
                    <span class="form-text text-danger">{{$errors->first('route')}}</span>
                @endif
            </div>
            <div class="col-lg-6">
                <label class="">{{__('modules.sorting')}}</label>
                {!! Form::number('sorting', null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                @if ($errors->has("sorting"))
                    <span class="form-text text-danger">{{$errors->first('sorting')}}</span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-6">
                <label>{{__('modules.group')}}</label>
                {!! Form::select('group',$groups, null, ['class'=>'form-control','placeholder'=>__('modules.select_group'), 'disabled'=>$disabled]) !!}
                @if ($errors->has("group"))
                    <span class="form-text text-danger">{{$errors->first('group')}}</span>
                @endif
            </div>
            <div class="col-lg-6">
                <label>{{__('modules.icon')}}</label>
                <button type="button"
                        id="icon_picker"
                        value=""
                        data-selected=""
                        data-toggle="dropdown"
                        class="btn btn-secondary form-control dropdown-toggle iconpicker-component"
                        {{$disabled ? "disabled" : ""}}
                        name="icon">
                    {{__('modules.icon')}} <i class="" id="drop-down-selected-icon"></i>
                    <span class="caret"></span>
                </button>
                {!! Form::hidden('icon', null, ["id"=>"icon"]) !!}
                <div class="dropdown-menu iconpicker-container"></div>
                @if ($errors->has("icon"))
                    <span class="form-text text-danger">{{$errors->first('icon')}}</span>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-md-4">
            @include('admin.layout.form_components.status', ['disabled' => $disabled])
    </div>
</div>
@section("additionalScripts")
    <script>
        $(function () {
            $("#icon_picker").iconpicker({
                title: "Pick Icon"
            });
            $('#icon_picker').on('iconpickerSelected', function (event) {
                $("#icon").val(event.iconpickerValue);
            });
            $.iconpicker.batch("#icon_picker","update",$("#icon").val(), false);
        });
    </script>
@endsection
