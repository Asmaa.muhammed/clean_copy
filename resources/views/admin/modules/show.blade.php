@extends('admin.layout.main', ['pageTitle'=>__('common.showPageTitle', ['module'=>$cmsModule->name])])
@section('subheader') @include('admin.layout.components.breadcrumb',['pageTitle'=>__('common.moduleTitle', ['module'=>$cmsModule->name]),'moduleName'=>$cmsModule->route,'pageName'=>__('common.showPageName', ['module'=>$cmsModule->name])]) @endsection
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile"
                 id="kt_page_portlet">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">{{__('common.show', ['module'=>__('modules.singularModuleName')])}}</h3>
                    </div>
                    @include("admin.layout.includes.form_button",['disabled'=>true,"module"=>$cmsModule->route, 'model' => $module])
                </div>
                <div class="kt-portlet__body">
                    {!! Form::model($module,['class'=>'kt-form kt-form--labe-right','id'=>'kt_form']) !!}
                        @include('admin.modules.form', ['disabled'=>true])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
