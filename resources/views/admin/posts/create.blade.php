@extends('admin.layout.main', ['pageTitle'=>__('common.createPageTitle', ['module'=>$cmsModule->name])])
@section('subheader') @include('admin.layout.components.breadcrumb',['pageTitle'=>__('common.moduleTitle', ['module'=>$cmsModule->name]),'moduleName'=>$cmsModule->route,'pageName'=>__('common.createPageName', ['module'=>__('posts.singularModuleName')]) ]) @endsection
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile"
                 id="kt_page_portlet">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">{{__('common.addNew', ['module'=>__('posts.singularModuleName')])}}</h3>
                    </div>
                    @include("admin.layout.includes.form_button",['disabled'=>false,"module"=>$cmsModule->route])
                </div>
                <div class="kt-portlet__body">
                    {!! Form::open(['route'=>'posts.store', 'method'=>'post', 'class'=>'kt-form kt-form--labe-right','id'=>'kt_form']) !!}
                    @include('admin.posts.form', ['disabled'=>false])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('additionalScripts')
    @include('admin.layout.includes.filemanger_scripts')
    @include('admin.layout.includes.tinyMCE_config')
    <script>
        $(function () {
            $(".multi").select2();
        });
    </script>
    <!--end::Page Scripts -->
   @endsection
