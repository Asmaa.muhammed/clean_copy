@extends('admin.layout.main', ['pageTitle'=>__('common.indexPageTitle', ['module' => $cmsModule->name])])
@section('subheader') @include('admin.layout.components.breadcrumb',['pageTitle'=>__('common.moduleTitle', ['module'=>$cmsModule->name]),'moduleName'=>$cmsModule->route,'pageName'=>__('common.indexPageName', ['module'=>$cmsModule->name]) ]) @endsection
@section('content')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    {{__('common.all', ['module'=>$cmsModule->name])}}
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="{{route('posts.create')}}" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            {{__('common.newRecord')}}
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
        @include('admin.layout.components.flash_messages')

        <!--begin: Datatable -->
        {!! $html->table(['class' => ' table table-striped- table-bordered table-hover table-checkable dataTable no-footer pages'], true) !!}

        <!--end: Datatable -->
        </div>
    </div>
@endsection
@section('additionalScripts')

    {!! $html->scripts() !!}
    <script src="{{asset('/public/admin')}}/assets/js/modules.js" type="text/javascript"></script>
@endsection
