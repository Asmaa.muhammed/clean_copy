@extends('admin.layout.main', ['pageTitle'=>'Enum | Full Info'])
@section('subheader') @include('admin.layout.components.breadcrumb',['pageTitle'=>'Enums','moduleName'=>'enums','pageName'=>"Show Full info" ]) @endsection
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile"
                 id="kt_page_portlet">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">  {{ __('enums.add_new_enum') }}</h3>
                    </div>
                    @include("admin.layout.includes.form_button",['disabled'=>true,"module"=>"enums","model"=>$enum])
                </div>
                <div class="kt-portlet__body">
                {!! Form::model($enum,['route'=>['enums.update', $enum->id], 'method'=>'patch']) !!}
                  @include('admin.enums.form', ['disabled'=>true,'class'=>'kt-form kt-form--labe-right','id'=>'kt_form'])
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
