
        <div class="row">
            <div class="col-lg-7">
                <div class="kt-section">
                    <div class="kt-section__body">
                        @include("admin.layout.components.basic_tab")
                    </div>
                </div>
                <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>
                <div class="kt-section">
                    <div class="kt-section__body">
                        <h4 class="kt-section__title kt-section__title-lg">{{ __('enums.main_information') }}:</h4>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>{{ __('enums.value') }}:</label>
                                {!! Form::text('value', null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                                @if ($errors->has("value"))
                                    <span class="form-text text-danger">{{$errors->first('value')}}</span>
                                @endif
                            </div>
                            <div class="col-lg-6">
                                <label>{{ __('enums.parent') }}:</label>
                                {!! Form::select('parent_id',$parent,null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                                @if ($errors->has("parent_id"))
                                    <span class="form-text text-danger">{{$errors->first('parent_id')}}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="col-lg-1"></div>
            <div class="col-lg-4">
                @include("admin.layout.form_components.status")

            </div>
        </div>


