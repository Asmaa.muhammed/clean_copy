<div class="row">
    <div class="col-md-7">
        <div class="kt-section">
            <div class="kt-section__body">
                @component("admin.components.translatable_section")
                    @foreach($languages as $language)
                        @slot("translatable_{$language->language}")
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>{{__('events.title')}}</label>
                                    {!! Form::text("title:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                                    @if ($errors->has("title:{$language->language}"))
                                        <span
                                            class="form-text text-danger">{{$errors->first("title:{$language->language}")}}</span>
                                    @endif
                                </div>
                                <div class="col-lg-6">
                                    <label class="">{{__('events.summary')}}</label>
                                    {!! Form::textarea("summary:{$language->language}", null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                                    @if ($errors->has("summary:{$language->language}"))
                                        <span
                                            class="form-text text-danger">{{$errors->first("summary:{$language->language}")}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-8">
                                    <label class="">{{__('events.body')}}</label>
                                    {!! Form::textarea("body:{$language->language}", null, ['class'=>'form-control editor', 'disabled'=>$disabled]) !!}
                                    @if ($errors->has("body:{$language->language}"))
                                        <span
                                            class="form-text text-danger">{{$errors->first("body:{$language->language}")}}</span>
                                    @endif
                                </div>


                            </div>
                        @endslot
                    @endforeach
                @endcomponent
            </div>
        </div>
        <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>
        <div class="kt-section">
            <h4 class="kt-section__title kt-section__title-lg">{{ __('events.image_and_galleries') }}:</h4>
            <div class="kt-section__body">
                <div class="form-group row">
                    @include('admin.layout.form_components.select_image')
                    <div class="col-lg-6">
                        <label>{{__('events.gallery')}}</label>
                        {!! Form::select('gallery',$galleries,(isset($event) )? "" :'', ['class'=>'form-control ', 'id'=>'galleries','placeholder'=> __('events.gallery'), 'disabled'=>$disabled]) !!}
                        @if ($errors->has("gallery"))
                            <span class="form-text text-danger">{{$errors->first('gallery')}}</span>
                        @endif
                    </div>

                </div>
            </div>
        </div>
        <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>
        <div class="kt-section">
            <h4 class="kt-section__title kt-section__title-lg">{{ __('pages.minor') }}:</h4>
            <div class="kt-section__body">
                <div class="form-group row">
                    <div class="col-lg-6">
                        <label>{{__('pages.minor')}}</label>
                        {!! Form::select('minor',$minors,(isset($page) && $page->minor)? $page->minor->minor_id :'', ['class'=>'form-control ', 'id'=>'minors','placeholder'=> __('pages.minor'), 'disabled'=>$disabled]) !!}
                        @if ($errors->has("minor"))
                            <span class="form-text text-danger">{{$errors->first('minor')}}</span>
                        @endif
                    </div>

                </div>
            </div>
        </div>
        <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>
        <div class="kt-section">
            <h4 class="kt-section__title kt-section__title-lg">{{ __('events.taxonomies') }}:</h4>
            <div class="kt-section__body">
                <div class="form-group row">
                    <div class="col-lg-6">
                        <label>{{__('events.categories')}}</label>
                        {!! Form::select('categories[]',$categories,null, ['class'=>'form-control multi', 'id'=>'categories','multiple', 'disabled'=>$disabled]) !!}
                        @if ($errors->has("categories"))
                            <span class="form-text text-danger">{{$errors->first('categories')}}</span>
                        @endif
                    </div>
                    <div class="col-lg-6">
                        <label>{{__('events.tags')}}</label>
                        {!! Form::select('tags[]',$tags,null, ['class'=>'form-control multi', 'id'=>'tags','multiple', 'disabled'=>$disabled]) !!}
                        @if ($errors->has("tags"))
                            <span class="form-text text-danger">{{$errors->first('tags')}}</span>
                        @endif
                    </div>


                </div>
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-lg-4">
        <div class="kt-section">
            <h4 class="kt-section__title kt-section__title-lg">{{ __('events.publish_options') }}:</h4>
            <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>
            <div class="kt-section__body">
                @include('admin.layout.form_components.status')
                <div class="form-group row">
                    <div class="col-lg-12">
                        <label class="">{{__('events.model')}}</label>
                        {!! Form::select("layout_model_id",$models, null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                        @if ($errors->has("layout_model_id"))
                            <span
                                class="form-text text-danger">{{$errors->first("layout_model_id")}}</span>
                        @endif
                    </div>

                </div>
                @include('admin.layout.form_components.publish_option',["module"=>$cmsModule->route])



            </div>
        </div>
        <div class="kt-separator kt-separator--border-2x kt-separator--space-lg"></div>

        <div class="kt-section">
            <h4 class="kt-section__title kt-section__title-lg">{{ __('events.event_details') }}:</h4>
            <div class="kt-section__body">
                <div class="form-group row">
                    <div class="col-lg-12">
                        <label class="">{{__('events.start_date')}}</label>
                        <div class="input-group date">
                            {!! Form::text("eventDetails[start_date]", null, ['class'=>'form-control','id'=>'kt_datetimepicker_4_4', 'disabled'=>$disabled]) !!}
                            <div class="input-group-append">
            <span class="input-group-text">
                	<i class="la la-calendar glyphicon-th"></i>
            </span>
                                @if ($errors->has("eventDetails.start_date"))
                                    <span
                                        class="form-text text-danger">{{$errors->first("eventDetails.start_date")}}
                    </span>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-12">
                        <label class="">{{__('events.end_date')}}</label>
                        <div class="input-group date">
                            {!! Form::text("eventDetails[end_date]", null, ['class'=>'form-control','id'=>'kt_datetimepicker_4_1', 'disabled'=>$disabled]) !!}
                            <div class="input-group-append">
            <span class="input-group-text">
                	<i class="la la-calendar glyphicon-th"></i>
            </span>
                                @if ($errors->has("eventDetails.end_date"))
                                    <span
                                        class="form-text text-danger">{{$errors->first("eventDetails.end_date")}}
                    </span>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-12">
                        <label>{{__('events.place')}}</label>
                        {!! Form::text('eventDetails[place]', null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                        @if ($errors->has("eventDetails.place"))
                            <span class="form-text text-danger">{{$errors->first('eventDetails.place')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-12">
                        <label>{{__('events.organizer')}}</label>
                        {!! Form::text('eventDetails[organizer]', null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
                        @if ($errors->has("eventDetails.organizer"))
                            <span class="form-text text-danger">{{$errors->first('eventDetails.organizer')}}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



