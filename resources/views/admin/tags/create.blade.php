@extends('admin.layout.main', ['pageTitle'=>'tags | Create'])
@section('subheader') @include('admin.layout.components.breadcrumb',['pageTitle'=>'tags','moduleName'=>'tags','pageName'=>"Create" ]) @endsection
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile"
                 id="kt_page_portlet">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">  {{ __('taxonomies.add_new_tag') }}</h3>
                    </div>
                    @include("admin.layout.includes.form_button",['disabled'=>false,"module"=>"tags"])
                </div>
                <div class="kt-portlet__body">
                    {!! Form::open(['route'=>'tags.store', 'method'=>'post','class'=>'kt-form kt-form--labe-right','id'=>'kt_form']) !!}
                    @include('admin.tags.form', ['disabled'=>false])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
