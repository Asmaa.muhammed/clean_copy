@extends('admin.layout.main', ['pageTitle'=>'tag | Full Info'])
@section('subheader') @include('admin.layout.components.breadcrumb',['pageTitle'=>'tags','moduleName'=>'tags','pageName'=> __('taxonomies.full_info') ]) @endsection
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile"
                 id="kt_page_portlet">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">  {{ __('taxonomies.full_info') }}</h3>
                    </div>
                    @include("admin.layout.includes.form_button",['disabled'=>true,"module"=>"tags","model"=>$tag])
                </div>
                <div class="kt-portlet__body">
                {!! Form::model($tag,['route'=>['tags.update', $tag->id], 'method'=>'patch']) !!}
                  @include('admin.tags.form', ['disabled'=>true,'class'=>'kt-form kt-form--labe-right','id'=>'kt_form'])
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
