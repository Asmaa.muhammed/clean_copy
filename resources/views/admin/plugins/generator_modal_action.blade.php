<button type="button" class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal"
        data-target="#generate_{{$model['plugin']->short_code}}">{{__('plugins.generateShortcode')}}</button>
<div class="modal fade" id="generate_{{$model['plugin']->short_code}}" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{__('plugins.generateShortcode')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                @if($model['plugin']->type)
                <div class="form-group">
                    <label for="recipient-name" class="form-control-label">Recipient:</label>
                    {!! Form::select($model['plugin']->short_code,
                     (\Illuminate\Database\Eloquent\Relations\Relation::getMorphedModel($model['plugin']->type))::all()
                     ->pluck('name', 'id')->toArray(), null, ['class'=>'form-control','placeholder'=>'Choose User', 'id' => $model['plugin']->short_code,]) !!}
                </div>
                @endif
                <div class="form-group">
                    <label for="message-text-{{$model['plugin']->short_code}}" class="form-control-label">
                        {{__('plugins.short_code')}}:
                    </label>
                    <textarea class="form-control" disabled="disabled" id="message-text-{{$model['plugin']->short_code}}">
                        {{"[".$model['plugin']->short_code."]"}}
                    </textarea>
                </div>
                <script>
                    $("#{{$model['plugin']->short_code}}").change(function () {
                        $("#message-text-{{$model['plugin']->short_code}}")
                            .empty()
                            .val(`[{{$model['plugin']->short_code}} id=${$(this).val()}]`);
                    });
                </script>
            </div>
        </div>
    </div>
</div>
