<div class="col-lg-6">
    <label for="{{ $field }}">
        {{ $label }}
    </label>
    {!! Form::text($field,null, ['class'=>($errors->has($field)? "error": "")." form-control", 'placeholder'=>isset($placeholder) ? $placeholder : '', 'id'=>$field]) !!}
    @if($errors->has($field))
        @foreach($errors->get($field) as $error)
            <p class="error-text">{!! $error !!}</p>
        @endforeach
    @endif
</div>
