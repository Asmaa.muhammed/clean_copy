@extends('admin.layout.main', ['pageTitle'=> __('translation::translation.languages')])
@section('subheader') @include('admin.layout.components.breadcrumb',['pageTitle'=> __('translation::translation.languages'),'moduleName'=>'languages','pageName'=> __('translation::translation.show_all') ]) @endsection

@section('content')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    {{ __('translation::translation.languages') }}

                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <div class="dropdown dropdown-inline">
                            <button type="button" class="btn btn-default btn-icon-sm dropdown-toggle"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="la la-download"></i> Export
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__section kt-nav__section--first">
                                        <span class="kt-nav__section-text">Choose an option</span>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-print"></i>
                                            <span class="kt-nav__link-text">Print</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-copy"></i>
                                            <span class="kt-nav__link-text">Copy</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-excel-o"></i>
                                            <span class="kt-nav__link-text">Excel</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-text-o"></i>
                                            <span class="kt-nav__link-text">CSV</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-pdf-o"></i>
                                            <span class="kt-nav__link-text">PDF</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        &nbsp;
                        <a href="{{ route('languages.create') }}" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            {{ __('translation::translation.add') }}
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <table class=" table table-striped- table-bordered table-hover table-checkable dataTable no-footer languages "
                   id="kt_table_1">
                <thead>
                <tr>
                    <th>{{ __('translation::translation.language_name') }}</th>
                    <th>{{ __('translation::translation.locale') }}</th>
                    <th>{{ __('translation::translation.status') }}</th>
                    <th>{{ __('translation::translation.actions') }}</th>

                </tr>
                </thead>

                <tbody>
                @foreach($languages as $language)
                    <tr>
                        <td>{{$language->name}}</td>
                        <td>{{$language->language}}</td>
                        <td>@if($language->status == 1)
                                <span class="kt-badge  kt-badge--primary kt-badge--inline kt-badge--pill">{{ __('translation::translation.status_enable')   }}</span>
                            @else
                                <span
                                    class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill">{{ __('translation::translation.status_disable') }}</span>

                            @endif

                        </td>
                        <td>
                            <div class="dropdown dropdown-inline">
                                <button type="button"
                                        class="btn btn-hover-warning btn-elevate-hover btn-icon btn-sm btn-icon-md btn-circle"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="flaticon-more-1"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" x-placement="top-end"
                                     style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-149px, -189px, 0px);">
                                    <a class="dropdown-item"
                                       href="{{ route("languages.edit", $language) }}"><i
                                            class="la la-pencil-square-o"></i> {{ __('translation::translation.edit')}}
                                    </a>

                                        <a class="dropdown-item"
                                           href="{{ route('languages.translations.index', $language->language) }}"><i
                                                class="la la-edit"></i> {{ __('translation::translation.translations')}}
                                        </a>
                                    <div class="dropdown-divider"></div>

                                    <button type="button" class="delete_btn dropdown-item btn-clean" id="delete-{{$language->id}}">
                                        <i class="la la-trash-o"></i> {{ __('translation::translation.delete')}}
                                    </button>


                                </div>
                            </div>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>


    </div>

@endsection
@section('additionalScripts')
    <script src="{{asset('/public/admin')}}/assets/js/modules.js" type="text/javascript"></script>
@endsection
