@extends('admin.layout.main', ['pageTitle'=> __('translation::translation.add_language')])
@section('subheader') @include('admin.layout.components.breadcrumb',['pageTitle'=>'Languages','moduleName'=>'languages','pageName'=> __('translation::translation.add_language') ]) @endsection

@section('content')

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile"
                 id="kt_page_portlet">


                {!! Form::open(['route'=>'languages.store', 'method'=>'post','class'=>'kt-form kt-form--labe-right','id'=>'kt_form']) !!}
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">  {{ __('translation::translation.add_language') }}</h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <a href="#" class="btn btn-clean kt-margin-r-10">
                            <i class="la la-arrow-left"></i>
                            <span class="kt-hidden-mobile">   {{ __('translation::translation.back') }}</span>
                        </a>
                        <div class="btn-group">
                            <button type="submit" class="btn btn-brand">
                                <i class="la la-check"></i>
                                <span class="kt-hidden-mobile">   {{ __('translation::translation.save') }}</span>
                            </button>


                        </div>
                    </div>
                </div>

                <div class="kt-portlet__body">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="form-group row">
                                @include('translation::forms.text', ['field' => 'name', 'label' => __('translation::translation.language_name'), ])
                                @include('translation::forms.text', ['field' => 'locale', 'label' => __('translation::translation.locale'), ])
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <div class="form-group">

                                        {!! Form::label( __('translation::translation.sorting'),  __('translation::translation.sorting')) !!}
                                        {!! Form::text('sorting',  null , ['class'=>'form-control']) !!}
                                    </div>

                                </div>
                                <div class="col-lg-6">
                                    {!! Form::label( __('translation::translation.direction'),  __('translation::translation.direction')) !!}
                                    {!! Form::select('direction', $directions,null, ['class'=>'form-control',  'placeholder'=> __('translation::translation.direction_placeholder')]) !!}

                                </div>
                            </div>

                        </div>
                        <div class="col-lg-1"></div>
                        <div class="col-lg-3">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <label class=" form-label">{{__('translation::translation.status')}}</label>
                                    <div class="col-3">
                                        <span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--info">
											<label>
                                                {!! Form::checkbox('status',1,true) !!}
                                                <span></span>
                                            </label>

										</span>

                                    </div>

                                </div>
                            </div>
                        </div>


                    </div>
                </div>


                {!! Form::close() !!}
            </div>

        </div>
    </div>

@endsection
