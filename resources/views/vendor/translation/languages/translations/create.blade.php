@extends('admin.layout.main', ['pageTitle'=>__('translation::translation.add_translation')])
@section('subheader') @include('admin.layout.components.breadcrumb',['pageTitle'=>'Languages','moduleName'=>'languages','pageName'=>__('translation::translation.add_translation') ]) @endsection
@section('additionalStyles')
    <link rel="stylesheet" href="{{asset("public/")}}/vendor/translation/css/main.css">
@endsection

@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile"
                 id="kt_page_portlet">
                {!! Form::open(['route'=>['languages.translations.store',$language], 'method'=>'post','class'=>'kt-form kt-form--labe-right','id'=>'kt_form']) !!}
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">  {{ __('translation::translation.add_translation') }}</h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <a href="#" class="btn btn-clean kt-margin-r-10">
                            <i class="la la-arrow-left"></i>
                            <span class="kt-hidden-mobile">   {{ __('translation::translation.back') }}</span>
                        </a>
                        <div class="btn-group">
                            <button type="submit" class="btn btn-brand">
                                <i class="la la-check"></i>
                                <span class="kt-hidden-mobile">   {{ __('translation::translation.save') }}</span>
                            </button>


                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group row">

                                @include('translation::forms.text', ['field' => 'group', 'label' => __('translation::translation.group_label'), 'placeholder' => __('translation::translation.group_placeholder')])

                                @include('translation::forms.text', ['field' => 'key', 'label' => __('translation::translation.key_label'), 'placeholder' => __('translation::translation.key_placeholder')])
                            </div>
                            <div class="form-group row">
                                @include('translation::forms.text', ['field' => 'value', 'label' => __('translation::translation.value_label'), 'placeholder' => __('translation::translation.value_placeholder')])
                                @include('translation::forms.text', ['field' => 'namespace', 'label' => __('translation::translation.namespace_label'), 'placeholder' => __('translation::translation.namespace_placeholder')])

                            </div>


                        </div>


                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>



@endsection
@section('additionalScripts')
    <script src="{{asset("public/")}}/vendor/translation/js/app.js"></script>
@endsection
