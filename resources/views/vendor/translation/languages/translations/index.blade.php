@extends('admin.layout.main', ['pageTitle'=>   __('translation::translation.translations') ])
@section('subheader') @include('admin.layout.components.breadcrumb',['pageTitle'=>__('translation::translation.languages'),'moduleName'=>'languages','pageName'=>__('translation::translation.translations') ]) @endsection
@section('additionalStyles')
    <link rel="stylesheet" href="{{asset("public/")}}/vendor/translation/css/main.css">
@endsection

@section('content')

    <div id="app">
        <form action="{{ route('languages.translations.index', ['language' => $language]) }}" method="get">

            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            {{ __('translation::translation.translations') }}

                        </h3>
                    </div>

                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">

                            @include('translation::forms.search', ['name' => 'filter', 'value' => Request::get('filter')])

                            @include('translation::forms.select', ['name' => 'language', 'items' => $languages, 'submit' => true, 'selected' => $language])

                            <div class="sm:hidden lg:flex items-center">

                                @include('translation::forms.select', ['name' => 'group', 'items' => $groups, 'submit' => true, 'selected' => Request::get('group'), 'optional' => true])

                                <a href="{{ route('languages.translations.create', $language) }}" class="btn btn-brand btn-elevate btn-icon-sm">
                                    <i class="la la-plus"></i>   {{ __('translation::translation.add') }}
                                </a>

                            </div>

                        </div>
                    </div>

                </div>

                <div class=" kt-portlet__body panel-body">

                    @if(count($translations))

                        <table class=" table table-striped- table-bordered table-hover table-checkable dataTable no-footer "
                               id="kt_table_1">

                            <thead>
                            <tr>
                                <th class="w-1/5 uppercase font-thin">{{ __('translation::translation.group_single') }}</th>
                                <th class="w-1/5 uppercase font-thin">{{ __('translation::translation.key') }}</th>
                                <th class="uppercase font-thin">{{ config('app.locale') }}</th>
                                <th class="uppercase font-thin">{{ $language }}</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($translations as $type => $items)

                                @foreach($items as $group => $translations)

                                    @foreach($translations as $key => $value)

                                        @if(!is_array($value[config('app.locale')]))
                                            <tr>
                                                <td>{{ $group }}</td>
                                                <td>{{ $key }}</td>
                                                <td>{{ $value[config('app.locale')] }}</td>
                                                <td>
                                                    <translation-input
                                                        initial-translation="{{ $value[$language] }}"
                                                        language="{{ $language }}"
                                                        group="{{ $group }}"
                                                        translation-key="{{ $key }}"
                                                        route="{{ route('languages.translations.update', $language) }}">
                                                    </translation-input>
                                                </td>
                                            </tr>
                                        @endif

                                    @endforeach

                                @endforeach

                            @endforeach
                            </tbody>

                        </table>

                    @endif

                </div>

            </div>

        </form>
    </div>

@endsection
@section('additionalScripts')
    <script src="{{asset("public/")}}/vendor/translation/js/app.js"></script>
@endsection
