<!DOCTYPE html>
<html lang="en">

<!-- begin::Head -->
<head>
    <base href="../../../">
    <meta charset="utf-8"/>
    <title>Dashboard Login</title>
    <meta name="description" content="Login page example">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--begin::Fonts -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

    <!--end::Fonts -->

    <!--begin::Page Custom Styles(used by this page) -->
    <link href="{{asset("public/admin/assets/css/pages/login/login-4.css")}}" rel="stylesheet" type="text/css"/>

    <!--end::Page Custom Styles -->

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{{asset("public/admin/assets/plugins/global/plugins.bundle.css")}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset("public/admin/assets/css/style.bundle.css")}}" rel="stylesheet" type="text/css"/>

    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <link href="{{asset("public/admin/assets/css/skins/header/base/light.css")}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset("public/admin/assets/css/skins/header/menu/light.css")}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset("public/admin/assets/css/skins/brand/dark.css")}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset("public/admin/assets/css/skins/aside/dark.css")}}" rel="stylesheet" type="text/css"/>

    <!--end::Layout Skins -->
    <link rel="shortcut icon" href="{{asset('/public/admin/assets/media/logos/diva-ico.png')}}"/>
    <style>
        /* ---- reset ---- */
        body {
            margin: 0;
        }

        canvas {
            vertical-align: bottom;
            position: absolute;
        }

        /* ---- particles.js container ---- */
        #particles {
            position: absolute;
            width: 100%;
            height: 100%;
            background-color: #1a1029;
            background-repeat: no-repeat;
            background-size: cover;
            background-position: 50% 50%;
            z-index: 0;
        }

        .kt-root-edit {
            z-index: 1000;
        }

        .kt-login-form-edit {
            background-color: #1a1029;
            padding: 10px;
            box-shadow: 0 0 5px 5px #1a10294D;
        }
    </style>
</head>

<!-- end::Head -->

<!-- begin::Body -->
<body
    class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

<!-- begin:: Page -->
<div class="kt-grid kt-grid--ver kt-grid--root kt-root-edit">
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v4 kt-login--signin" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
            <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                <div class="kt-login__container">

                    <div class="kt-login__signin kt-login-form-edit">
                        ;
                        <div class="kt-login__logo">
                            <a href="#">
                                <img src="{{asset('/public/admin/assets/media/logos/diva-large.png')}}">
                            </a>
                        </div>
                        {!! Form::open(['method'=>'post', 'route'=>'login', 'class'=>'kt-form']) !!}
                        @if(session('status'))
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <div class="alert-text">{{session('status')}}</div>
                                <div class="alert-close">
                                    <i class="flaticon2-cross kt-icon-sm" data-dismiss="alert"></i>
                                </div>
                            </div>
                        @endif
                        @error('email')
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <div class="alert-text">{{$errors->first('email')}}</div>
                                <div class="alert-close">
                                    <i class="flaticon2-cross kt-icon-sm" data-dismiss="alert"></i>
                                </div>
                            </div>
                        @enderror
                        @include('admin.layout.components.flash_messages')
                        <div class="input-group">
                            {!! Form::email('email', null, ['class'=>'form-control', 'placeholder'=>__('E-Mail Address')]) !!}
                        </div>
                        <div class="input-group">
                            {!! Form::password('password', ['class'=>'form-control', 'placeholder'=>__('Password')]) !!}
                        </div>
                        <div class="row kt-login__extra">
                            <div class="col">
                                <label class="kt-checkbox">
                                    {!! Form::checkbox('remember') !!}
                                    {{ __('Remember Me') }}
                                    <span></span>
                                </label>
                            </div>
                            <div class="col kt-align-right">
                                <a href="#" id="kt_login_forgot" class="kt-login__link">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            </div>
                        </div>
                        <div class="kt-login__actions">
                            <button id="kt_login_signin_submit" type="submit"
                                    class="btn btn-outline-success">{{ __('Login') }}
                            </button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="kt-login__forgot kt-login-form-edit">
                        <div class="kt-login__head">
                            <h3 class="kt-login__title">Forgotten Password ?</h3>
                            <div class="kt-login__desc">Enter your email to reset your password:</div>
                        </div>
                        {!! Form::open(['method'=>'post', 'route' => 'password.email', 'class'=>'kt-form']) !!}
                            <div class="input-group">
                                {!! Form::email('email', null, [
                                                    'class'=>'form-control',
                                                    'id'=>'kt_email',
                                                    'placeholder'=>__('E-Mail Address'),
                                                    'autocomplete'=>'off'
                                                   ])
                                 !!}
                            </div>
                            <div class="kt-login__actions">
                                <button id="kt_login_forgot_submit"
                                        class="btn btn-outline-success">Request
                                </button>&nbsp;&nbsp;
                                <button id="kt_login_forgot_cancel"
                                        class="btn btn-outline-secondary">Cancel
                                </button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div id="particles"></div>

<!-- end:: Page -->

<!-- begin::Global Config(global config for global JS sciprts) -->
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": [
                    "#c5cbe3",
                    "#a1a8c3",
                    "#3d4465",
                    "#3e4466"
                ],
                "shape": [
                    "#f0f3ff",
                    "#d9dffa",
                    "#afb4d4",
                    "#646c9a"
                ]
            }
        }
    };
</script>

<!-- end::Global Config -->

<!--begin::Global Theme Bundle(used by all pages) -->
<script src="{{asset("public/admin/assets/plugins/global/plugins.bundle.js")}}" type="text/javascript"></script>
<script src="{{asset("public/admin/assets/js/scripts.bundle.js")}}" type="text/javascript"></script>

<!--end::Global Theme Bundle -->

<!--begin::Page Scripts(used by this page) -->
<script src="{{asset("public/admin/assets/js/pages/custom/login/login-general.js")}}" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
<script>
    particlesJS("particles", {
        "particles": {
            "number": {"value": 80, "density": {"enable": true, "value_area": 800}},
            "color": {"value": "#ffffff"},
            "shape": {
                "type": "circle",
                "stroke": {"width": 0, "color": "#000000"},
                "polygon": {"nb_sides": 5},
                "image": {"src": "img/github.svg", "width": 100, "height": 100}
            },
            "opacity": {
                "value": 0.5,
                "random": false,
                "anim": {"enable": false, "speed": 1, "opacity_min": 0.1, "sync": false}
            },
            "size": {
                "value": 3,
                "random": true,
                "anim": {"enable": false, "speed": 40, "size_min": 0.1, "sync": false}
            },
            "line_linked": {"enable": true, "distance": 150, "color": "#ffffff", "opacity": 0.4, "width": 1},
            "move": {
                "enable": true,
                "speed": 2,
                "direction": "none",
                "random": false,
                "straight": false,
                "out_mode": "out",
                "bounce": false,
                "attract": {"enable": false, "rotateX": 600, "rotateY": 1200}
            }
        },
        "interactivity": {
            "detect_on": "canvas",
            "events": {
                "onhover": {"enable": false, "mode": "repulse"},
                "onclick": {"enable": true, "mode": "push"},
                "resize": true
            },
            "modes": {
                "grab": {"distance": 400, "line_linked": {"opacity": 1}},
                "bubble": {"distance": 400, "size": 40, "duration": 2, "opacity": 8, "speed": 3},
                "repulse": {"distance": 200, "duration": 0.4},
                "push": {"particles_nb": 4},
                "remove": {"particles_nb": 2}
            }
        },
        "retina_detect": true
    });
</script>

<!--end::Page Scripts -->
</body>

<!-- end::Body -->
</html>
